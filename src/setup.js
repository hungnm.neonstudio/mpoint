import { Provider } from 'react-redux';
import configStore from './configStore';
import AppContainer from './appContainer';
import {
    AppRegistry, BackAndroid,
    ToastAndroid,
    Platform, Linking, AsyncStorage
} from 'react-native';
import React, { Component } from 'react';
import api from './api'
import dataSrv from './api/dataService'
import SplashScreen from 'react-native-splash-screen'
import FCM from 'react-native-fcm'
const store = configStore();
import i18 from './components/i18';
import { getLanguages } from 'react-native-i18n';
api.setStore(store)
let isBack = true
export default class Main extends Component {
    componentWillMount() {
        dataSrv.getListCategory().then(rs => {
            if (rs.err == 0) {
                api.setCategory(rs.data)
            }
        })
        try {
            AsyncStorage
                .getItem('first')
                .then((p) => {
                    if (p != 'first') {
                        api.showConfirm(i18.t('contentLang'), i18.t('titleLang'), i18.t('en'), i18.t('vi'),
                            function en() {
                                api.setLangEn()
                                api.hideConfirm()
                            },
                            function vi() {
                                api.setLangVi()
                                api.hideConfirm()
                            }
                        )

                        api.resetRoute({ key: 'welcome' })
                    }
                });
        } catch (error) {
            console.log('Error when getting data', error);
        }
        try {
            AsyncStorage
                .getItem('lang')
                .then((p) => {
                    if (p == 'vi') {
                        api.setLangVi()
                    }
                    else if (p == 'en') {
                        api.setLangEn()
                    } else {
                        getLanguages().then(rs => {
                            if (rs && rs[0] && rs[0].indexOf('vi') != -1) {
                                api.setLangVi();
                            } else {
                                api.setLangEn();
                            }
                        })
                    }
                });
        } catch (error) {
            console.log('Error when getting data', error);
        }
    }
    componentDidMount() {
        // dataSrv.getListShopGPS()
        SplashScreen.hide();
        if (Platform.OS === "android")
            BackAndroid.addEventListener('hardwareBackPress', this._goBack.bind(this));
    }
    componentWillUnmount() {

        //console.log('unmount')
        if (Platform.OS === 'android')
            BackAndroid.removeEventListener('hardwareBackPress', this._goBack.bind(this));
    }
    _goBack = () => {
        // alert(store.getState().navState.index)
        if (store.getState().navState.index === 0) {
            if (isBack) {
                isBack = false
                ToastAndroid.show(i18.t('toastBack'), ToastAndroid.SHORT);
                // BackAndroid.addEventListener('hardwareBackPress', BackAndroid.exitApp);
            }
            else {
                // BackAndroid.removeEventListener('hardwareBackPress', BackAndroid.exitApp);
                return false
            }
            setTimeout(() => {
                isBack = !isBack
            }, 2000)
        }
        api.pop()
        return true
    }
    render() {
        return (
            <Provider store={store}>
                <AppContainer />
            </Provider>
        )
    }
}
// if(!url.contains("http")){
//     mEventDispatcher.dispatchEvent(new NavigationStateChangeEvent(getId(), SystemClock.nanoTime(), view.getTitle(), false, url, view.canGoBack(), view.canGoForward()));
//     return true;
// }