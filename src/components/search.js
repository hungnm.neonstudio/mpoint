import {
    Container,
    Header,
    Title,
    Content,
    Icon,
    Button
} from 'native-base';
import React, { Component } from 'react';
import { Text, ListView, ActivityIndicator, Image, View, Platform } from 'react-native';
import { connect } from 'react-redux';
import { pop } from '../actions/navActions';
import Item from '../components/tab/item';
import dataService from '../api/dataService';
import api from '../api';
var item_num_load = 5;
import i18 from './i18'
class Search extends Component {
    constructor(props) {
        super(props);
        this.state = {
            item: [],
            start: 0,
            isLoading: true,
            canLoadMore: true
        }
    }
    componentWillMount() {
        this._loadMoreData()
    }
    _loadMoreData() {
        if (this.state.canLoadMore) {
            //console.log('Load more');
            dataService
                .search(this.props.userState.token, this.props.navState.routes[this.props.navState.index].keyWord, this.state.item.length, item_num_load)
                .then((data) => {
                    console.log('data search', data)
                    this.setState({ isLoading: false })
                    if (data.err == 0) {
                        console.log('====================================');
                        console.log(data);
                        console.log('====================================');
                        if (data.promotions.length < item_num_load) {
                            this.setState({ canLoadMore: false })
                        }
                        this.setState({
                            item: this
                                .state
                                .item
                                .concat(data.promotions)
                        })
                    }
                    // console.log("New data: ", this.state.item);
                })
        }
    }

    render() {
        const ds = new ListView.DataSource({
            rowHasChanged: (r1, r2) => r1 !== r2
        });
        var page = null;
        if (this.state.isLoading) {
            page = (<ActivityIndicator size='large' />);
        } else {
            if (this.state.item.length === 0) {
                page = (
                    <View
                        style={{
                            alignSelf: 'center',
                            marginTop: 10
                        }}>
                        <Text style={{ color: '#fff' }}>{i18.t('searchFailed')}</Text>
                    </View>
                );
            } else {
                page = (
                    <ListView
                        dataSource={ds.cloneWithRows(this.state.item)}
                        onEndReached
                        ={() => this._loadMoreData()}
                        renderRow={(item, i) => {
                            let tag = ''
                            switch (item.promotionTypeId) {
                                case 1: tag = item.percent ? item.percent + '%' : ''; break;
                                case 2: tag = item.exchangePoint ? item.exchangePoint + '\n' + i18.t('point') : ''; break;;
                                case 3: tag = item.giftPoint ? item.giftPoint + '\n' + i18.t('point') : ''; break;
                                case 4: tag = i18.t('stamps'); break;
                                case 5: tag = i18.t('gifts'); break;
                                case 6: tag = i18.t('gifts'); break;
                                case 7: tag = i18.t('points'); break;
                                default: break;
                            }
                            return (<Item
                                logo={{
                                    uri: item.partner.logo
                                }}
                                slogan={item.partner.slogan != null && item.partner.slogan != undefined ? item.partner.slogan : ''}
                                id={item.id}
                                tag={tag}
                                name={item.partner.name}
                                title={item.shortDescription}
                                img={item.images ? item.images : []
                                }
                                time={item.endDate}
                                area={item.area} />);
                        }}
                        renderFooter={() => {
                            if (this.state.canLoadMore) {
                                return (<ActivityIndicator size='large' />);
                            }
                        }} />
                );
            }
        }
        return (
            <Container>
                <Header
                    style={{
                        backgroundColor: '#009688'
                    }}>
                    <Button transparent onPress={this.props.pop}>
                        <Icon name='ios-arrow-back' style={{ color: Platform.OS == 'ios' ? '#000' : '#fff' }} />
                    </Button>
                    <Title style={{ alignSelf:'center' } }>{i18.t('search')}</Title>
                </Header>
                <Image
                    source={require('../img/bg_login.jpg')}
                    style={{ width: api.getRealDeviceWidth(), height: api.getRealDeviceHeight() - 56 }}
                >
                    {page}
                </Image>
            </Container>
        );
    }
}

const mapStateToProps = (state) => {
    return { navState: state.navState, userState: state.userState }
}
const mapDispatchToProp = (dispatch) => {
    return {
        pop: () => dispatch(pop())
    }
}
export default connect(mapStateToProps, mapDispatchToProp)(Search)