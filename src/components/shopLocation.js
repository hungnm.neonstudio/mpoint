import React, { Component } from 'react';
import { StyleSheet } from 'react-native';
import { Text, View, ActivityIndicator, Platform, Image } from 'react-native';
import api from '../api';
import dataService from '../api/dataService';
import {
    Container,
    Content,
    Header,
    Title,
    Icon,
    Button
} from 'native-base';
import { connect } from 'react-redux';
import { pop } from '../actions/navActions';
import MapView from 'react-native-maps';
import i18 from './i18'
const styles = StyleSheet.create({
    map: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0
    }
});
var coords;
class ShopLocation extends Component {
    constructor(props) {
        super(props);
        this.state = {
            coords: []
        }
        coords = this
            .props
            .navState
            .routes[this.props.navState.index]
            .gps
            .split(',');
    }
    componentWillMount() {
        navigator
            .geolocation
            .getCurrentPosition((position) => {
                //console.log(position);
                this.setState({
                    userLocation: {
                        latitude: position.coords.latitude,
                        longitude: position.coords.longitude
                    },
                    isLoading: false
                })
                //console.log('userLocation', this.state.userLocation)
            }, (error) => console.log('Vui lòng kiểm tra GPS'), {
                enableHighAccuracy: true,
                timeout: 20000,
                maximumAge: 1000
            });
    }
    _selectLocation() {
        var mode = 'driving'; // 'walking';
        var origin = this.state.userLocation.latitude + ',' + this.state.userLocation.longitude;
        var destination = this.props.navState.routes[this.props.navState.index].gps;
        dataService
            .getRoutesDirection(origin, destination, mode)
            .then((responseJson) => {
                if (responseJson.routes.length) {
                    this.setState({
                        coords: api.decode(responseJson.routes[0].overview_polyline.points)
                    });
                }
            })
    }
    render() {
        let logo = this.props.navState.routes[this.props.navState.index].logo
        var poli = null;
        poli = (<MapView.Polyline
            coordinates={[...this.state.coords]}
            strokeWidth={5}
            strokeColor='#00b3fd' />);
        if (this.state.isLoading) {
            return (<ActivityIndicator size='large' />);
        } else {
            return (
                <Container>
                    <Header
                        style={{
                            backgroundColor: '#009688'
                        }}>
                        <Button transparent onPress={this.props.pop}>
                            <Icon name='ios-arrow-back' style={{ color: Platform.OS == 'ios' ? '#000' : '#fff' }} />
                        </Button>
                        <Title style={{ alignSelf:'center' } }>{i18.t('location')}</Title>
                    </Header>
                    <View style={{
                        flex: 1,
                        backgroundColor: '#f0f0f0'
                    }}>
                        <MapView
                            provider={MapView.PROVIDER_GOOGLE}
                            showsScale={true}
                            style={styles.map}
                            zoomEnabled={true}
                            showsUserLocation={true}
                            initialRegion={{
                                latitude: Number(coords[0]),
                                longitude: Number(coords[1]),
                                latitudeDelta: 0.0922,
                                longitudeDelta: 0.0421
                            }}>
                            <MapView.Marker
                                onPress={() => {
                                    this._selectLocation()
                                }}
                                coordinate={{
                                    latitude: Number(coords[0]),
                                    longitude: Number(coords[1])
                                }}>
                                <View>
                                    <Image
                                        style={{ height:47,width:30,paddingTop:3,alignItems:'center'}}
                                        source={require('../img/marker.png')}
                                    >
                                        <Image
                                            style={{ height:24,width:24,zIndex:1,borderRadius:12 }}
                                            source={{ uri: logo ? logo.replace('Uploads', 'Thumbnails') : 'i' }} />
                                    </Image>
                                </View>
                                <MapView.Callout
                                    style={{
                                        width: api.getRealDeviceWidth() / 1.5
                                    }}>
                                    <View>
                                        <Text
                                            style={{
                                                fontSize: 15,
                                                fontWeight: 'bold',
                                                color: '#387ef5',
                                                padding: 3
                                            }}>{this.props.navState.routes[this.props.navState.index].name}</Text>
                                        <Text
                                            style={{
                                                fontSize: 15,
                                                padding: 3
                                            }}>{this.props.navState.routes[this.props.navState.index].des}</Text>
                                        <Text
                                            style={{
                                                padding: 3

                                            }}>{i18.t('here')} {this.props.navState.routes[this.props.navState.index].address}</Text>
                                        <Text
                                            style={{
                                                fontSize: 15,
                                                padding: 3
                                            }}>{this.props.navState.routes[this.props.navState.index].dueDate}</Text>

                                    </View>
                                </MapView.Callout>
                            </MapView.Marker>
                            {poli}
                        </MapView>
                    </View>
                </Container>
            );
        }

    }
}

const mapState = (state) => {
    return { navState: state.navState }
}
const mapDispatch = (dispatch) => {
    return {
        pop: () => dispatch(pop())
    }
}

export default connect(mapState, mapDispatch)(ShopLocation);