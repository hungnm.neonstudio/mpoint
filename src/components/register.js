/*import {
    Container,
    Header,
    Content,
    Button,
    Icon,
    Title,
    Card,
    Text,
    Input,
    InputGroup,
    Col
} from 'native-base'
import React, {Component} from 'react'
import {Image, Dimensions, View,Alert} from 'react-native'
import {connect} from 'react-redux'
import {push, pop} from '../actions/navActions'
import api from '../api'
import dataService from '../api/dataService'
import {
  Analytics,Hits
} from 'react-native-google-analytics';
let phone =''
let gotoRegCard = true
let otpID = ''
class Reg extends Component {

    // componentDidMount(){
    //     console.log('mount')
    //    BackAndroid.addEventListener('hardwareBackPress',this._goBack.bind(this))
    // }

    // componentWillUnmount(){
    //     console.log('unmount')
    //     BackAndroid.removeEventListener('hardwareBackPress',this._goBack.bind(this))
    // }
    constructor(props) {
        super(props)
        // console.log(props)
    }

    // _goBack = () => {
    //     if(this.props.navState.index ===0 ){ToastAndroid.show('Nhấn lần nữa để thoát',ToastAndroid.SHORT);BackAndroid.addEventListener('hardwareBackPress',()=>BackAndroid.exitApp())}
    //     this
    //         .props
    //         .pop()
    //         return true
    // }

    _gotoRegVT = () => {
        api.push({key: 'regVT'})
        // let isloadding = true
        // api.showLoading()
        // dataService.getUserCode()
        // .then((data)=>{
        //     if(data.error === 0 ){
        //         this.props.push({key: 'regVT',userCode:data.user_code})
        //         api.hideLoading()
        //     }
        //     else{
        //         api.showMessage(data.msg,'Thông báo')
        //         api.hideLoading()
        //     }
        //     isloadding = false
        // }     
        // )
        // setTimeout(()=>{
        //     if(isloadding) api.showMessage('Server không phản hồi. Vui lòng kiểm tra kết nối mạng','Thông báo')
        // },5000)
    }
    _onConfirmOTP(otp){
        
        // alert(otp+phone)
        if(otp == undefined || otp == '') {
            api.showMessage('Vui lòng điền vào mã OTP')
            return
        }
        
        api.hideInputBox()
        setTimeout(()=>api.showLoading(),1)
        dataService.postVerifyOtpRegister(phone,otp,otpID)
        .then((rs)=>{
                gotoRegCard = false
                if(rs.err == 0)
                {
                    // api.hideInputBox()
                    
                    api.setUserToken(rs.token)
                    try {
                                     AsyncStorage.setItem('@RFTK:key', rs.refreshToken);
                                       // alert(dt.refreshToken)
                                         } catch (error) {
                                      console.log('Error when saving data', error);
                                      }
                    api.push({key:'regCard',phone:phone})
                }else if(rs.err == 2){
                    // api.showMessage(rs.msg,'Thông báo')
                    api.showInputBox(undefined,undefined,rs.msg,this._onConfirmOTP)
                    api.hideLoading()
                }   
        })
    }
    _goToRegCard(uuid){
        
        if(!api.validatePhone(phone)) {
            api.showMessage('Hãy nhập đúng số điện thoại','Thông báo')
            return false
        }
            api.showLoading()
            dataService.postReg(phone)
            .then(rs=>{
                api.hideLoading()
                if(rs.err ==0 ){
                    otpID = rs.otpId
                    api.showInputBox('Nhập OTP','Nhập OTP','',this._onConfirmOTP)
                }
                else{
                    api.showMessage(rs.msg,'Thông báo')
                }
            })


        //     api.setUserToken('3eec5de1-ccd9-459b-9918-a7ceb3071f70')
        //    api.showInputBox('Nhập OTP','Nhập OTP','',this._onConfirmOTP)
            // api.showMessage('Số điện thoại đã đăng ký. Vui lòng đăng nhập với số điện thoại này.','Thông báo')
            // api.pop()
            // dataService.getUserToken(phone,uuid)
            // .then(data=>{
            //     if(data.error === 0){
            //         api.push({key:'regCard',phone:phone})
            //     }
            //     else api.showMessage(data.msg,'Thông báo')
            //     gotoRegCard = false
            // })

        // setTimeout(()=>{
        //     if(gotoRegCard)
        //     api.showMessage('Server không phản hồi. Vui lòng kiểm tra kết nối mạng',"Thông báo")
        // },5000)
    }
    render() {
        if(this.props.userState.uuid!== undefined){
            ga = new Analytics('UA-90479808-1',this.props.userState.uuid, 1, 'userAgent');
            var screenView = new Hits.ScreenView('Mpoint','Register')
            ga.send(screenView);
        }
        return (
            <Container>
                <Header
                    style={{
                    backgroundColor: '#009688'
                }}>
                    <Button transparent onPress={() => this.props.pop()}>
                        <Icon name='ios-arrow-back' style={{ color:'#fff' }}/>
                    </Button>
                    <Title>Đăng ký hội viên</Title>
                </Header>
                <Content>
                    <Image
                        style={{
                        height: api.getRealDeviceHeight()-62,
                        width: api.getRealDeviceWidth(),
                        paddingTop: 10
                    }}
                        //source={require('../img/login-bg.png')}>

                        <Image
                            style={{
                            height: 70,
                            width: 250,
                            alignSelf: 'center'
                        }}
                            source={require('../img/logo.png')}/>
                        <Text
                            style={{
                            color: 'gray',
                            marginTop: 5,
                            alignSelf: 'center'
                        }}>VÍ THẺ ƯU ĐÃI VÀ TÍCH ĐIỂM TRÊN MOBILE</Text>

                        <View
                            style={{
                            padding: 10,
                            borderTopColor: '#dadada',
                            borderTopWidth: 1,
                            flexDirection: 'column'
                        }}>
                            <Text
                                style={{
                                textAlign: 'center',
                                color:'gray',
                                lineHeight:25
                            }}>Hàng ngàn ưu đãi đang đợi bạn {'\n'}
                                Nhập số điện thoại để nhận thẻ</Text>

                            <InputGroup
                                style={{
                                marginTop:20,
                                backgroundColor: '#fff',
                                borderRadius: 2,
                                marginBottom: 10,
                                width:api.getRealDeviceWidth()/2,
                                 minWidth:300,
                                alignSelf:'center',
                                
                               
                            }}>
                                <Input
                                placeholderTextColor='gray'
                                    placeholder='Số điện thoại'
                                    onChangeText={(text) => phone = text}
                                    keyboardType='phone-pad'
                                    style={{  width:api.getRealDeviceWidth() }}/>
                            </InputGroup>

                            <Button
                                small
                                style={{
                                padding: 0,
                                marginTop: 10,
                                alignSelf: 'center',
                                paddingLeft: 30,
                                paddingRight: 30
                            }}
                            onPress={()=>this._goToRegCard(this.props.userState.uuid)}
                            >
                                TIẾP TỤC
                            </Button>

                            <Button
                                small
                                style={{
                                padding: 0,
                                marginTop: 25,
                                backgroundColor: '#fa6428',
                                alignSelf: 'center'
                            }}
                                onPress={() => this._gotoRegVT()}>
                                KHÁCH HÀNG VIETTEL NHẬN THẺ TẠI ĐÂY
                            </Button>
                        </View>
                    </Image>
                </Content>
            </Container>
        )
    }
}

mapStateToProps = (state) => ({userState: state.userState,
        navState: state.navState,})
mapDispatchToProps = (dispath) => ({
    push: (route) => dispath(push(route)),
    pop: () => dispath(pop())

})
export default connect(mapStateToProps, mapDispatchToProps)(Reg)*/