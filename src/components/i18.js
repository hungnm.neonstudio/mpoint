import I18n,{getLanguages} from 'react-native-i18n';
import en from './lang/en';
import vi from './lang/vi';
I18n.fallbacks = true;
// I18n.locale = 'en'
I18n.translations = {
  en,
  vi
};
export default i18 = I18n