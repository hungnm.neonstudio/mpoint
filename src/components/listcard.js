import {
    Container,
    Header,
    Content,
    Button,
    Icon,
    Title,
    Card,
    Text,
    Grid,
    Input,
    InputGroup,
    Col,
    Row,
    CardItem,
    CheckBox
} from 'native-base'
import React, { Component } from 'react'
import DeviceInfo from 'react-native-device-info'
import {
    Image,
    View,
    Dimensions,
    Alert,
    BackAndroid,
    ToastAndroid,
    Platform,
    AsyncStorage,
    ListView,
    ActivityIndicator,
    TouchableOpacity, ScrollView, Keyboard
} from 'react-native'
import MyCardItem from './tab/cardItem'
import { connect } from 'react-redux'
import { push, pop } from '../actions/navActions'
// import {setShopLocation} from '../actions/locationAction';
import {
    showMessage,
    hideMessageBox,
    showInputBox,
    hideInputBox,
    showLoading,
    hideLoading
} from '../actions/uiActions'
import { setUserInfo, setUserToken, setUuid } from '../actions/userActions'
import request from '../api/request'
import api from '../api'
import dataService from '../api/dataService';
import { IndicatorViewPager, PagerDotIndicator } from 'rn-viewpager';
import i18 from './i18'
// import SplashScreen from 'react-native-splash-screen'
import { Analytics, Hits } from 'react-native-google-analytics';
//let markers = []
let gotoUpdate = () => { }
let gotoPromotion = () => { }
export class Login extends Component {
    constructor(props) {
        super(props)
        this.state = {
            card: '',
            memberReged: [],
            members: [],
            memberClose: [],
            memberGroup: [],
            isLoading: true
        }
    }

    componentWillMount() {
        gotoUpdate = this.props.navState.routes[this.props.navState.index].gotoUpdate
        gotoPromotion = this.props.navState.routes[this.props.navState.index].gotoPromotion
        dataService.postListMember(2, 2, this.props.userState.token)
            .then(rs => {
                this.setState({ members: rs.members, isLoading: false })
            })
        dataService.postListMember(1, 2, this.props.userState.token)
            .then(rs => {
                this.setState({ memberClose: rs.members, isLoading: false })
            })
        dataService.postListMember(0, 3, this.props.userState.token)
            .then(rs => {
                this.setState({ memberGroup: rs.members, isLoading: false })
            })
    }
    _renderDotIndicator() {
        return <PagerDotIndicator pageCount={3} />;
    }
    _onMemberClick(member) {
        this
            .props
            .push({ key: 'addCard' })
    }
    ds = new ListView.DataSource({
        rowHasChanged: (r1, r2) => r1 !== r2
    });
    _onInsert() {
        Keyboard.dismiss()
        if (this.state.card == '') {
            api.showMessage(i18.t('blankCard'), i18.t('titleMsgLogin'))
            return
        }
        token = this.props.userState.token
        api.showLoading()
        let isLoading = true
        dataService.postAddCard(this.state.card, token)
            .then((rs) => {
                api.hideLoading()
                if (rs.err == 0) {
                    gotoPromotion()
                    //api.resetRoute({key:'listcard',gotoUpdate:gotoUpdate})
                    dataService.getUserInfo(token)
                        .then(rs => {
                            if (rs.err == 0) {
                                api.setUserInfo(rs)
                            }
                        })
                    dataService.getListMember(token, 1, 0)
                        .then((kq) => {
                            if (kq.err == 0)
                                api.setListMember(kq.members)
                        })
                    api.showConfirm(rs.msg.replace('cập nhật lại thông tin của bạn', 'xem thông tin của thẻ ưu đãi'), i18.t('titleMsgLogin'), (i18.t('update')), i18.t('openCard'),
                        function capnhat() {

                            gotoUpdate()
                            api.resetRoute({ key: 'home' })
                            //api.push({key:'infoCard',item:rs.data})
                            api.hideConfirm()
                        },
                        function mothe() {
                            gotoUpdate()
                            gotoPromotion()
                            api.resetRoute({ key: 'home', type: true })
                            api.hideConfirm()

                        })
                    this.componentWillMount()
                }
                else
                    api.showMessage(rs.msg, i18.t('titleMsgLogin'))
                isLoading = false
            })
        setTimeout(() => {
            if (isLoading) {
                api.hideLoading()
                api.showMessage(i18.t('timeoutTextMsgContentLogin'), i18.t('titleMsgLogin'))
            }
        }, 15000)
    }
    _selectCard(id, item) {
        token = this.props.userState.token
        api.showLoading()
        let isLoading = true
        dataService.postRegisterFreeCard(id, token)
            .then(rs => {
                api.hideLoading()
                if (rs.err == 0) {
                    dataService.getUserInfo(token)
                        .then(rs => {
                            if (rs.err == 0) {
                                api.setUserInfo(rs)
                                gotoPromotion()
                            }
                        })
                    dataService.getListMember(token, 1, 0)
                        .then((kq) => {
                            if (kq.err == 0)
                                api.setListMember(kq.members)
                        })
                    //api.resetRoute({key:'listcard',gotoUpdate:gotoUpdate})
                    api.showConfirm(rs.msg.replace('cập nhật lại thông tin của bạn', 'xem thông tin của thẻ ưu đãi'), i18.t('titleMsgLogin'), (i18.t('update')).toUpperCase(), i18.t('openCard'),
                        function onok() {
                            gotoUpdate()
                            api.resetRoute({ key: 'home' })
                            //api.push({key:'infoCard',item:item})
                            setTimeout(() => {
                                api.hideConfirm()
                            }, 200)
                        },
                        function oncancel() {
                            gotoUpdate()
                            gotoPromotion()
                            api.resetRoute({ key: 'home', type: true })
                            setTimeout(() => {
                                api.hideConfirm()
                            }, 200)
                        })
                    this.componentWillMount()
                }
                else
                    api.showMessage(rs.msg, i18.t('titleMsgLogin'))
                isLoading = false
            })
        setTimeout(() => {
            if (isLoading) {
                api.hideLoading()
                api.showMessage(i18.t('timeoutTextMsgContentLogin'), i18.t('titleMsgLogin'))
            }
        }, 15000)
    }
    _renderOpen(arr, type) {

        if (type == 2) {
            return (
                <ScrollView
                    style={{ paddingBottom: 10 }}
                >
                    <ListView
                        enableEmptySections={true}
                        dataSource={this.ds.cloneWithRows(arr)}
                        renderRow={(item, sectionID, id) => {
                            return (
                                <TouchableOpacity
                                    key={id}
                                    style={{
                                        alignSelf: 'center',
                                        borderRadius: 5
                                    }}
                                    onPress={() => {
                                        api.push({ key: 'bna', item: item, partners: item.partners })
                                    }}><Image
                                        source={{
                                            uri: item.banner
                                        }}
                                        style={{
                                            width: api.getRealDeviceWidth() - 10,
                                            height: (api.getRealDeviceWidth() - 10) / 5.7,
                                            resizeMode: 'stretch',
                                            marginVertical: 5,
                                            borderRadius: 5
                                        }}
                                    /></TouchableOpacity>
                            )
                        }}
                        renderFooter={
                            () => {
                                return this.state.isLoading ? <ActivityIndicator size='large' style={{ alignSelf: 'center', flex: 1 }} /> : null
                            }
                        }
                    />
                </ScrollView>
            );
        }
        return (
            <ScrollView
                style={{ paddingBottom: 10 }}
            >
                <ListView
                    enableEmptySections={true}
                    dataSource={this.ds.cloneWithRows(arr)}
                    contentContainerStyle={{
                        flexDirection: 'row',
                        flexWrap: 'wrap',
                        alignItems: 'stretch'
                    }}
                    renderRow={(item, sectionID, id) => (
                        <TouchableOpacity
                            key={id}
                            style={{
                                alignSelf: 'center',
                                borderRadius: 10
                            }}
                            onPress={() => {
                                {/*if(type)
                                {
                                    if(item.id == 123)api.push({key: 'viettel', item:item})
                                    if(item.id == 354)api.push({key: 'mpoint', item:item})
                                }
                                else
                            api.push({key: 'addCardFromLogin', item:item})
                            console.log(item)*/}
                                switch (type) {
                                    case 0:
                                        this._selectCard(item.id, item)
                                        {/*if(item.id == 123)api.push({key: 'viettel', item:item})
                                    if(item.id == 354)api.push({key: 'mpoint', item:item})*/}
                                        break;
                                    case 1:
                                        api.push({ key: 'regCard', item: item })
                                        break;

                                }
                            }}><Image
                                source={{
                                    uri: item.card
                                }}
                                style={{
                                    width: (api.getRealDeviceWidth() - 25) / 2,
                                    height: (api.getRealDeviceWidth() - 25) / 3,
                                    resizeMode: 'stretch',
                                    margin: 5,
                                    borderRadius: 10
                                }}
                            /></TouchableOpacity>
                    )}
                    renderFooter={
                        () => {
                            return this.state.isLoading ? <ActivityIndicator size='large' style={{ alignSelf: 'center', flex: 1 }} /> : null
                        }
                    }
                />
            </ScrollView>
        )
    }
    _renderClose() {
        return (
            <ScrollView
                style={{ paddingBottom: 10 }}
            >
                <ListView
                    dataSource={this.ds.cloneWithRows(this.state.members.memberClose)}
                    contentContainerStyle={{
                        flexDirection: 'row',
                        flexWrap: 'wrap',
                        alignItems: 'stretch'
                    }}
                    renderRow={(item, sectionID, id) => (
                        <TouchableOpacity
                            key={id}
                            style={{
                                alignSelf: 'center'
                            }}
                            onPress={() => {
                                if (item.id == 123) {
                                    api
                                        .push({ key: 'viettel', id: item.id, card: item.card, detail: item.detail, name: item.name })
                                } else
                                    if (item.id == 354) {
                                        api
                                            .push({ key: 'mpoint', id: item.id, card: item.card, detail: item.detail, name: item.name })
                                    } else
                                        api.push({ key: 'addCardFromLogin', item: item })
                            }}><Image
                                source={{
                                    uri: item.card
                                }}
                                style={{
                                    width: (api.getRealDeviceWidth() - 25) / 2,
                                    height: (api.getRealDeviceWidth() - 25) / 3,
                                    resizeMode: 'stretch',
                                    margin: 5
                                }} /></TouchableOpacity>
                    )} />
            </ScrollView>
        )
    }
    render() {

        ga = new Analytics('UA-90479808-1', DeviceInfo.getUniqueID(), 1, DeviceInfo.getUserAgent());
        var screenView = new Hits.ScreenView('Mpoint', 'Login')
        ga.send(screenView);
        let minus = Platform.OS == 'ios' ? 0 : 4
        let fontSize = api.getRealDeviceWidth() > 320 ? 13 : 12;
        let marginTop = api.getRealDeviceWidth() < 320 ? -8 - minus : 0 - minus;
        let imgHeight = api.getRealDeviceWidth() > 320 ? 25 : 23;
        return (
            <Container>
                <Header
                    style={{
                        backgroundColor: '#009688'
                    }}>
                    <Button
                        transparent
                        onPress={() => {
                            if (this.props.navState.index == 0)
                                api.resetRoute({ key: 'home' })
                            else
                                api.pop()
                        }}>
                        <Icon name='ios-arrow-back' style={{ color: Platform.OS == 'ios' ? '#000' : '#fff' }} />
                    </Button>
                    <Title style={{ alignSelf: 'center' }}>{i18.t('titleAddCardPage')}</Title>
                </Header>

                <Image
                    source={require('../img/bg_login.jpg')}
                    style={{ width: api.getRealDeviceWidth(), height: api.getRealDeviceHeight() - 56 }}
                >
                    <View>
                        <Image
                            style={{
                                height: api.getRealDeviceWidth() / 5,
                                width: api.getRealDeviceWidth() / 5 * 2.13,
                                alignSelf: 'center',
                                marginTop: 20
                            }}
                            source={require('../img/logo_deperecate.png')} />
                        <Text
                            style={{
                                backgroundColor: 'transparent',
                                color: '#fff',
                                marginTop: 5,
                                alignSelf: 'center',
                                fontWeight: '500',
                                marginBottom: 10
                            }}>{i18.t('headLogin')}</Text>
                    </View>

                    <ScrollView
                        keyboardShouldPersistTaps={'handled'}
                    >
                        <View
                            style={{
                                padding: 10,
                                backgroundColor: '#fff',
                                //borderTopWidth: 1,
                                flexDirection: 'column',
                                //width:api.getRealDeviceWidth()*0.8,
                                margin: 10,
                                borderRadius: 10,
                                //alignSelf:'center' 
                            }}>
                            <Text
                                style={{
                                    textAlign: 'center',
                                    color: 'gray',
                                    lineHeight: 25
                                }}>{i18.t('addCardTitle')}</Text>

                            <View
                                style={{
                                    marginTop: 10,
                                    borderRadius: 2,
                                    marginBottom: 10,
                                    width: api.getRealDeviceWidth() * 0.8,
                                    //minWidth: 300,
                                    alignSelf: 'center',
                                    minHeight: 90
                                }}>
                                <Input
                                    returnKeyLabel={i18.t('addcard')}
                                    onSubmitEditing={() => { this._onInsert() }}
                                    placeholderTextColor='gray'
                                    placeholder={i18.t('codecard')}
                                    onChangeText={(text) => { this.setState({ card: text }) }}
                                    //defaultValue={phone}
                                    style={{
                                        width: api.getRealDeviceWidth() * 0.8,
                                        backgroundColor: '#fff',
                                        borderColor: '#cdcdcd',
                                        borderWidth: 1,
                                        alignSelf: 'center'
                                    }} />
                                <View
                                    style={{
                                        flexDirection: 'row', marginTop: 10
                                    }}>
                                    <Button
                                        style={{
                                            backgroundColor: '#fa6428',
                                            width: api.getRealDeviceWidth() * 0.8,
                                        }}
                                        onPress={this._onInsert.bind(this)}>
                                        <Text
                                            style={{
                                                color: 'white'
                                            }}>{(i18.t('continue')).toUpperCase()}</Text>
                                    </Button>
                                </View>
                            </View>
                            {/* <Text style={{
                                textAlign: 'center',
                                color: 'blue',
                                lineHeight: 25
                            }}>
                                {i18.t('footerBody')}
                            </Text> */}
                        </View>

                        {/*{this.state.members.length>0?<Text style={{ color:'gray',textAlign:'center',paddingVertical:10,shadowColor:'gray',fontWeight:'300' }}>THẺ ƯU ĐÃI ĐĂNG KÝ BẰNG SỐ ĐIỆN THOẠI</Text>:null}*/}

                        {/* !this.state.isLoading && this.state.members.length > 0 ? <Image
                                source={require('../img/bg.png')}
                                style={{
                                    width: imgHeight * 13.95, alignSelf: 'center',
                                    resizeMode: 'stretch',
                                    height: imgHeight,
                                    marginVertical: 5
                                }}
                            >
                                <Text style={{ marginTop: marginTop, color: '#fff', alignSelf: 'center', fontWeight: '400', fontSize: fontSize }}>{i18.t('regByphone')}</Text>
                            </Image> : null
                         */}
                        {/* this._renderOpen(this.state.members, 0) */}
                        {/*<Text style={{ textAlign:'center',color:'black',paddingTop:10,borderTopColor:'#cdcdcd',borderTopWidth:1,paddingVertical:10,fontWeight:'300' }}>
                        CỘNG ĐỒNG DOANH NGHIỆP
                    </Text>*/}
                        {/* <Image
                            source={require('../img/bg.png')}
                            style={{
                                width: imgHeight * 13.95, alignSelf: 'center',
                                resizeMode: 'stretch',
                                marginBottom: 5,
                                height: imgHeight
                            }}
                        >
                            <Text style={{ marginTop: marginTop, color: '#fff', alignSelf: 'center', fontWeight: '400', fontSize: fontSize }}>{i18.t('cardOfBusiness')}</Text>
                        </Image> */}
                        {/* this._renderOpen(this.state.memberGroup, 2) */}
                        {/*{this.state.memberClose.length>0?<Text style={{ textAlign:'center',color:'gray',paddingTop:10,paddingVertical:10,fontWeight:'300' }}>THẺ ƯU ĐÃI ĐĂNG KÝ BẰNG MÃ THẺ</Text>:null}*/}
                        {/* <Image
                            source={require('../img/bg.png')}
                            style={{
                                width: imgHeight * 13.95, alignSelf: 'center',
                                resizeMode: 'stretch',
                                marginBottom: 5,
                                height: imgHeight
                            }}
                        >
                            <Text style={{ marginTop: marginTop, color: '#fff', alignSelf: 'center', fontWeight: '400', fontSize: fontSize }}>{i18.t('regByCard')}</Text>
                        </Image> */}
                        {/* this._renderOpen(this.state.memberClose, 1) */}
                    </ScrollView>
                </Image>
            </Container>
        )
    }
}

mapStateToProps = (state) => {
    return { userState: state.userState, navState: state.navState, lang: state.langState.lang }
}
mapDispatchToProps = (dispatch) => ({
    push: (route) => dispatch(push(route)),
    showmess: () => dispatch(showMessage('1', '2')),
    pop: () => dispatch(pop()),
    showloading: () => dispatch(showLoading()),
    hideloading: () => dispatch(hideLoading()),
    setUserInfo: (data) => dispatch(setUserInfo(data)),
    setUserToken: (token) => dispatch(setUserToken(token)),
    setUuid: (uuid) => dispatch(setUuid(uuid)),
    setShopLocation: (markers) => dispatch(setShopLocation(markers))
})
export default connect(mapStateToProps, mapDispatchToProps)(Login)