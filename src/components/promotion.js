import React, { PureComponent } from 'react';
import { View, ScrollView, Image, StyleSheet, Platform, Text } from 'react-native';
import MainTab from './tab/mainTab';
import Food from './tab/food';
import Shopping from './tab/shopping';
import Other from './tab/other';
import api from '../api';
import i18 from './i18';
import { connect } from 'react-redux';
import { TabViewAnimated, TabBar, SceneMap } from 'react-native-tab-view';
class Promotion extends PureComponent {
    state = {
        index: 0,
        routes: [
            { key: '0', title: this._subString(this.props.me) },
            // { key: '2', title: i18.t('food') },
            // { key: '3', title: i18.t('shopping') },
            // { key: '4', title: i18.t('other') },
        ],
        renderScene: {
            '0': () => { return <MainTab category={0} type={this.props.type} gotoinfo={this.props.gotoinfo} gotoGift={this.props.gotoGift} /> },
        }
    };
    constructor(props) {
        super(props);
        let { routes, renderScene } = this.state
        let { Category } = this.props
        Category.map(el => {
            routes.push({
                key: '' + el.id,
                title: el.name
            })
            renderScene[el.id + ''] = () => { return <Food category={el.id} type={this.props.type} /> }
        })
        // alert(JSON.stringify(Category))
        // this.setState({
        //     routes: routes,
        //     renderScene: renderScene
        // })
    }
    _handleChangeTab = index => this.setState({ index });

    _renderHeader = props => <TabBar scrollEnabled={this.props.Category ? this.props.Category.length > 3 ? true : false : false} {...props}
        style={{ backgroundColor: '#fdfdfd' }}
        indicatorStyle={{ borderBottomColor: '#fa6428', borderBottomWidth: 4 }}
        renderLabel={(payload) => { return <Text style={{ color: payload.focused ? '#fa6428' : '#000', fontWeight: '700', fontSize: Platform.OS == 'ios' ? 14 : 15 }}>{payload.route.title}</Text> }}

    />;
    componentDidMount() {
        setTimeout(function () {
            api.hideLoading();
        }, 500);
    }
    _renderScene = SceneMap(this.state.renderScene);
    _subString(str) {
        if (str.length > 8) {
            return str.substring(0, 5) + '...'
        } else return str
    }
    render() {
        return <Image
            source={require('../img/bg_login.jpg')}
            style={{ width: api.getRealDeviceWidth(), height: api.getRealDeviceHeight() - 115 }}
        >
            <TabViewAnimated
                style={styles.container}
                navigationState={this.state}
                renderScene={this._renderScene}
                renderHeader={this._renderHeader}
                onRequestChangeTab={this._handleChangeTab}
            />
        </Image>
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
});
mapStateToProps = (state) => ({ lang: state.langState.lang, Category: state.giftCountState.Category })
mapDispatchToProps = () => ({})
export default connect(mapStateToProps, mapDispatchToProps)(Promotion)