import {
    Container,
    Header,
    Content,
    Button,
    Icon, 
    Title,
    Card,
    Text,
    Grid,
    Input, 
    InputGroup,
    Row,
    CardItem,
    Radio
} from 'native-base';
var moment = require('moment');
import i18 from './i18'
import React, {Component} from 'react'
import {Image,View,Platform} from 'react-native'
import {connect} from 'react-redux'
import {push, pop} from '../actions/navActions'
// import Datepicker from './datepicker'
import DatePicker from 'react-native-datepicker'
import api from '../api'
// import Drop from './dropdown'
// import ModalDropdown  from 'react-native-modal-dropdown'

let now = new Date()
// let name =''
// let dob =''
// let sex =''
// let email =''
class RegHoivien extends Component {
    constructor(props) {
        super(props)
        data = this.props.data.endUser || { }
        enduser_birthday = data.birthday
        this.state={info:{
            name:data.name,
            dob:enduser_birthday!=null && enduser_birthday != undefined?(new Date(enduser_birthday).getDate() +'-'+(new Date(enduser_birthday).getMonth()+1)+'-'+new Date(enduser_birthday).getFullYear()):( moment().subtract(20, 'years')).format('DD-MM-YYYY'),
            gender:false,
            mail:data.email
        }}
        // console.log(props)
    }
    _lessThanTen(t){
        return t<10 ?'0'+t:t
    }
    _goBack = () => {
        this
            .props
            .pop()
    }
    _gotoAddress = () => {
        name = this._rePlaceDoubleSpace(this.state.info.name)
        if(name=='') {api.showMessage(i18.t('blankName'),i18.t('titleMsgLogin'));return  }
        if(name.length>99) {api.showMessage(i18.t('longName'),i18.t('titleMsgLogin'));return  }
        if(api.validateSpecialCharacter(name)){
        api.showMessage(i18.t('invalidSpecialCharacter'),i18.t('titleMsgLogin'))
        return
        }
        if(this.state.info.dob == null){
            api.showMessage(i18.t('blankDOB'),i18.t('titleMsgLogin'))
            return
        }
        if(this.state.info.mail=='') {api.showMessage(i18.t('blankEmail'),i18.t('titleMsgLogin'));return  }
        if(!api.validateEmail(this.state.info.mail)){api.showMessage(i18.t('invalidEmail'),i18.t('titleMsgLogin'));return  }

        api.showLoading()
        api.setRegInfo('',name,'',this.state.info.dob,this.state.info.mail,this.state.info.gender?1:0,'','','')
        this.props .push({key: 'regAddress'})
        api.hideLoading()
    }
    _rePlaceDoubleSpace(str){
    if(str == null || str == undefined )return ''
     let s = str
    while(s.search('  ')!=-1){
       s =  s.replace('  ',' ')
    }
    return s.trim();
 }
    render() {
        return (
            <Container>
                <Header
                    style={{
                    backgroundColor: '#009688'
                }}>
                    <Button transparent onPress={() => this._goBack()}>
                        <Icon name='ios-arrow-back' style={{ color:Platform.OS == 'ios'?'#000':'#fff' }}/>
                    </Button>
                    <Title style={{ alignSelf:'center' } }>{i18.t('reguserTitle')}</Title>
                </Header>
                
                <Content
                    style={{
                    paddingLeft: 10,
                    paddingRight: 10,
                    height:api.getRealDeviceHeight(),
                }}>
                    <InputGroup style={{ paddingTop:5,paddingBottom:5,paddingLeft:18 }}>
                        <Icon
                            name='ios-person-outline'
                            style={{
                            flex: 0
                        }}/>
                        <Input
                        autoFocus
                         autoCapitalize='characters'
                            style={{
                            flex: 10
                        }} onChangeText={(text)=>this.state.info.name=text} defaultValue = {this.state.info.name} placeholder={i18.t('placeHolderInputName')}/>
                    </InputGroup>
                    <CardItem  style={{ paddingTop:5,paddingBottom:5 }}>
                        <Icon name='ios-calendar-outline' style={{ marginTop:5 }}/>
                    
                    <DatePicker
                            style={{width: 200,borderWidth:0}}
                            date={this.state.info.dob}
                            mode="date"
                            androidMode='spinner'
                            placeholder={i18.t('placeholderChooseDOB')}
                            format="DD-MM-YYYY"
                            minDate="01-01-1900"
                            maxDate={now}
                            confirmBtnText={i18.t('confirm')}
                            cancelBtnText={i18.t('close')}
                            showIcon = {false}
                            customStyles={{
                            dateIcon: {
                                position: 'absolute',
                                left: 0,
                                top: 4,
                                marginLeft: 0
                            },
                            dateInput: {
                                marginLeft: 0,
                                borderWidth:0,
                                alignSelf:'auto',alignItems:'flex-start'
                            },
                            placeholderText:{
                                color:'#555'
                            }
                            // ... You can check the source to find the other keys.
                            }}
                            onDateChange={(date) => {this.setState({info:Object.assign({},this.state.info,{dob:date})})}}
                            // onDateChange={(date) => {n=date}}
                        />
                    </CardItem>

                        <Row  style={{ paddingTop:10,paddingBottom:10,borderBottomWidth:0.5,borderBottomColor:'#cdcdcd',paddingLeft:18 }}>
                            <Icon name='ios-people-outline'/>
                            <Radio style={{marginLeft:10}} onPress={() => {this.setState({info:Object.assign({},this.state.info,{gender:false})})}} selected={!this.state.info.gender}/><Text style={{ marginLeft:5 }}>{i18.t('men')}</Text>
                            <Radio style={{marginLeft:10}} onPress={() => {this.setState({info:Object.assign({},this.state.info,{gender:true})})}} selected={this.state.info.gender}/><Text  style={{ marginLeft:5 }}>{i18.t('women')}</Text>
                        </Row>
                         
                    <InputGroup  style={{ paddingTop:5,paddingBottom:5 ,paddingLeft:18}}>
                        <Icon
                            name='ios-mail-outline'
                            style={{
                            flex: 0
                        }}/>
                        <Input
                            style={{
                            flex: 10
                        }} onChangeText={(text)=>{this.state.info.mail=text}} defaultValue={this.state.info.mail} keyboardType='email-address' placeholder='Email'/>
                    </InputGroup>
                    <Grid
                        style={{
                        marginTop: 20,
                        marginBottom: 50,paddingLeft:18
                    }}>
                        <Row>
                            <Button
                                style={{
                                flex: 1,
                                backgroundColor: 'gray',
                            }}
                                onPress={() => this._goBack()}>{i18.t('confirmCancelText_gotoReg')}</Button>
                            <Button
                                style={{
                                flex: 1,
                                marginLeft: 15,
                                backgroundColor: '#fa6428'
                            }} onPress={this._gotoAddress}>{i18.t('buttonTextLogin')}</Button>
                        </Row>
                    </Grid>
                    
                </Content>
                <View>
                    <Button
                    onPress={()=>{ 
                        api.resetRoute({key:'home'})
                     }}
                     style={{ alignSelf:'center',paddingHorizontal:api.getRealDeviceWidth()/3,borderRadius:5,
                     marginBottom:10,
                     backgroundColor:'#009688'
                      }}
                >
                    <Text style={{ alignSelf:'center',color:'#fff'}}
                    >{i18.t('later')}</Text>
                </Button>
                </View>
            </Container>
        )
    }
}

mapStateToProps = (state) => (state.userState)
mapDispatchToProps = (dispath) => ({
    push: (route) => dispath(push(route)),
    pop: () => dispath(pop())

})
export default connect(mapStateToProps, mapDispatchToProps)(RegHoivien)