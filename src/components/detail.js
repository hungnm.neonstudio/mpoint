import React, { Component } from 'react'
import {
    Container,
    Button,
    Header,
    Footer,
    Title,
    Icon,
    Content,
    Card,
    Grid,
    Col,
    Row,
    List,
    ListItem,
    Input,
    InputGroup,
    CardItem
} from 'native-base'
// import ReadMore from '@exponent/react-native-read-more-text'
import { connect } from 'react-redux'
import {
    Image,
    Text,
    ActivityIndicator,
    Linking,
    Alert,
    ScrollView,
    View, Platform, PermissionsAndroid, WebView, ListView, Keyboard, TextInput
} from 'react-native'
import i18 from './i18'
import { ShareDialog } from 'react-native-fbsdk'
import { push, pop } from '../actions/navActions'
import dataService from '../api/dataService'
import api from '../api'
import { showInputBox, showQR, showRate } from '../actions/uiActions'
import { SHOW_QR_CONFIRM } from '../actions/actionTypes'
import { Analytics, Hits } from 'react-native-google-analytics';
import { IndicatorViewPager, PagerTitleIndicator, PagerDotIndicator } from 'rn-viewpager'
import Item from '../components/tab/detail/item';
import Ad from './adDetail';
import Reply from './comment/replies';
import Octicons from 'react-native-vector-icons/Octicons';
import QRCode from 'react-native-qrcode';
import Barcode from 'react-native-barcode-builder';
import KeyboardSpacer from 'react-native-keyboard-spacer';
let bottomY = 0
let isGettingPromotionCode = false
let isLoading = true
let userInfo = {
    user_name: '',
    avatar: ''
}
let setGiftCount = (token) => {
    dataService.getGiftCount(token)
        .then((rs) => {
            api.setGitfCount(rs.err == 0 ? rs.amount : '0')
        })
}

let gotoGift = () => { }
let functionRefresh = () => { }
let refreshStamp = () => { }
let promotionTypeId = ''
class Detail extends Component {
    token = ''
    promotionId = ''
    constructor(props) {
        super(props)
        //alert(this.props.navState.routes[this.props.navState.index].arrCode.length)
        this.state = {
            promotionCode: [],
            onChildScroll: false,
            page: 1,
            data: {},
            styleTab1: { backgroundColor: '#d9d9d9', color: '#fa9428' },
            styleTab2: { color: 'gray' },
            haveStamp: [],
            remainStamp: [],
            partner: {
                logo: ''
            }
        }
        // get ItemInfo

        // get userInfo
        let user = this.props.userState
        console.log('user', user);
        this.userInfo = {
            user_name: user.data.endUser.name,
            avatar: user.data.endUser.avatar
        }
        console.log('userInfo', this.userInfo);
    }

    componentWillMount() {
        // alert(this.props.navState.routes[this.props.navState.index].tag)
        setTimeout(() => {
            dataService
                .getItemInfo(this.props.userState.token, this.props.navState.routes[this.props.navState.index].id)
                .then((data) => {
                    if (data.promotion.limitTypeId == 0 && !this.state.code && data.promotion.promotionTypeId != 2) this._getPromotionCode(data.promotion.limitTypeId);
                    if (data.err == 0) promotionTypeId = data.promotion.promotionTypeId
                    have = data.promotion.stamps.length > 0 ? data.promotion.stamps[0].collected : 0
                    remain = data.promotion.stamp - have > 0 ? data.promotion.stamp - have : 0
                    api.setPer(data.promotion.percent ? data.promotion.percent : '')
                    dataService
                        .getPartnerInfo(this.props.userState.token, data.promotion.partnerId)
                        .then((dt) => {
                            this.setState({
                                data: dt.data, haveStamp: new Array(have).fill(1),
                                remainStamp: new Array(remain).fill(1)
                            });
                            isLoading = false
                        })
                    console.log(' ItemInfo data', data);
                    if (data.promotion.likes.length > 0)
                        this.setState(Object.assign({}, data.promotion, {
                            iconLike: 'ios-heart',
                            currentComment: '',
                            isSendding: false
                            // promotionCode: this.props.navState.routes[this.props.navState.index].code !== undefined?[this.props.navState.routes[this.props.navState.index].code]:[]
                        }))
                    else
                        this.setState(Object.assign({}, data.promotion, {
                            iconLike: 'ios-heart-outline',
                            currentComment: '',
                            isSendding: false,
                            // promotionCode: this.props.navState.routes[this.props.navState.index].code !== undefined?[this.props.navState.routes[this.props.navState.index].code]:[]
                        }))
                    // isLoading = false
                })
        }, 400)
        // get 3 Comment first

        dataService
            .getComment(this.props.userState.token, this.props.navState.routes[this.props.navState.index].id, 0, 3)
            .then((data) => {
                this.setState(Object.assign({}, this.state, { listcomment: data.comments }))
            })
        setTimeout(() => {
            if (isLoading && this.props.navState.routes[this.props.navState.index].key == 'detail') {
                api.showMessage(i18.t('timeoutTextMsgContentLogin'), i18.t('titleMsgLogin'))
                api.pop()
            }
        }, 30000)
    }
    _share() {
        if (this.state.partner.website == undefined || this.state.partner.website == null)
        { api.showMessage(i18.t('errShare'), i18.t('titleMsgLogin')); return }
        const shareLinkContent = {
            contentType: 'link',
            contentUrl: this.state.partner.website,
            contentDescription: i18.t('shareTitle'),
        };
        ShareDialog.canShow(shareLinkContent).then(
            function (canShow) {
                if (canShow) {
                    return ShareDialog.show(shareLinkContent);
                }
            }
        ).then(
            result => {
                //if (result.postId != undefined) {
                //api.showMessage('Chia sẻ thành công!',i18.t('titleMsgLogin'))
                if (result.isCancelled) return
                loading = true
                api.showLoading()
                dataService.shareFacebook(token, promotionId)
                    .then((rs) => {
                        loading = false
                        console.log('shareFB', rs)
                        if (rs.err == 0) {
                            api.showMessage(rs.msg, i18.t('titleMsgLogin'))
                        }
                        api.hideLoading()
                    })
                setTimeout(() => {
                    if (loading) {
                        loading = false
                        api.showMessage(i18.t('timeoutTextMsgContentLogin'), i18.t('titleMsgLogin'))
                        api.hideLoading()
                    }
                }, 30000)
                //}
            },
            err => {
                console.log('====================================')
                console.log('day la loi',err)
                console.log('====================================')
                api.showMessage(i18.t('unknowErr'), i18.t('titleMsgLogin'))
            }
            );
    }
    _onLikeButtonClick() {
        if (this.state.iconLike == 'ios-heart')
            this.setState({
                iconLike: 'ios-heart-outline',
                likeCount: this.state.likeCount - 1
            })
        else
            this.setState({
                iconLike: 'ios-heart',
                likeCount: this.state.likeCount + 1
            })
        dataService
            .getLike(this.props.userState.token, this.props.navState.routes[this.props.navState.index].id)
            .then((data) => {
                if (data.err !== 0) {
                    api.showMessage(i18.t('errLike'), i18.t('titleMsgLogin'))
                } else {
                }
            })
    }
    _renderStamp() {
        if (this.state.promotionTypeId == 4) {
            if (this.state.haveStamp.length + this.state.remainStamp.length < 2) return;
            return (
                <View style={{ backgroundColor: '#fff', flex: 1, borderRadius: 3, marginTop: 5 }}>
                    <View style={{ flexDirection: 'row', alignSelf: 'center', padding: 5 }}>
                        {this.state.haveStamp.map((item, id) => (
                            <Image key={id} source={require('../img/icon/bought.png')}
                                style={{ height: api.getRealDeviceWidth() / 10, width: api.getRealDeviceWidth() / 10, marginLeft: 5 }}
                            />
                        ))}

                        {this.state.remainStamp.map((item, id) => (
                            <Image key={id} source={require('../img/icon/remaining.png')}
                                style={{ height: api.getRealDeviceWidth() / 10, width: api.getRealDeviceWidth() / 10, marginLeft: 5 }} />
                        ))}
                        {/*<Image source={ require('../../img/icon/oneplus.png') }
                        style={{ height:api.getRealDeviceWidth()/10,width:api.getRealDeviceWidth()/10,marginLeft:5 }}/>*/}
                    </View>
                </View>
            )
        }
    }
    componentDidMount() {
        token = this.props.userState.token
        promotionId = this.props.navState.routes[this.props.navState.index].id
        gotoGift = this.props.navState.routes[this.props.navState.index].gotoGift
        // alert(this.props.navState.routes[this.props.navState.index].promotionTypeId)
        func = this.props.navState.routes[this.props.navState.index].functionRefresh
        functionRefresh = (code) => {
            this.componentWillMount()
            this._setDoiSoat(code)
            if (func != undefined) func()
        }
        if (this.props.navState.routes[this.props.navState.index].code !== undefined)
            this._getPromotionCode()
        // this.setState({
        //                 promotionCode: [
        //                    this.props.navState.routes[this.props.navState.index].code
        //                 ]
        //             })
    }

    _getPromotionCode(type) {
        if (this.state.msg) return api.showMessage(this.state.msg);
        if (!isGettingPromotionCode) {
            if ((type != 0 && this.state.limitTypeId != 0) || this.state.promotionTypeId == 2) api.showLoading()
            isGettingPromotionCode = true
            dataService
                .getQRcode(this.props.userState.token, this.props.navState.routes[this.props.navState.index].id)
                .then((data) => {
                    this.setState({
                        msg: data.msg
                    })
                    if (data.err == 0) {
                        this.setState({ remainCode: this.state.remainCode != '' && this.state.remainCode != undefined ? this.state.remainCode - 1 : '' })
                        if (this.props.navState.routes[this.props.navState.index].promotionTypeId == 2 && data.err == 0) {
                            dataService
                                .getUserInfo(this.props.userState.token)
                                .then((datas) => {
                                    if (datas.err === 0) {
                                        api.setUserInfo(datas)
                                    }
                                })
                            //&& data.msg != undefined){
                            setGiftCount(this.props.userState.token)
                            //         api.showConfirm(data.msg!=undefined?data.msg:'Quý khách có muốn xác thực',i18.t('titleMsgLogin'),'Xác thực','Đóng',
                            //             function ok(){
                            // api.showQR("YÊU CẦU NHÂN VIÊN CỬA HÀNG xác thực MÃ ƯU ĐÃI", data.code, false,promotionId,()=>functionRefresh(data.code),promotionTypeId)
                            //                 {/*if(gotoGift!=undefined){
                            //                     api.showDoisoat(promotionId,'YÊU CẦU NHÂN VIÊN CỬA HÀNG Xác thực MÃ ƯU ĐÃI',data.code,false,functionRefresh)
                            //                     // api.pop()
                            //                     // gotoGift()
                            //                 }*/}
                            //                 api.hideConfirm()
                            //             },
                            //             function cancel(){
                            //                 api.hideConfirm()
                            //             }
                            //         )
                            api.showStorelet(data.point + '\n' + i18.t('point'), data.msg, i18.t('vertify'),
                                function ok() {
                                    api.showQR(i18.t('titleQRvetify'), data.code, false, promotionId, () => functionRefresh(data.code), promotionTypeId)
                                    api.hideStorelet()
                                }
                            )
                            this._setDoiSoat(data.code)
                            isGettingPromotionCode = false
                            api.hideLoading()
                            return
                        }
                        this._setDoiSoat(data.code, true)
                        // api.showQR(i18.t('titleQRvetify'), data.code, false, this.state.id, () => functionRefresh(data.code), promotionTypeId)
                        api.hideLoading()
                    } else {
                        if (this.props.navState.routes[this.props.navState.index].QRpage === true)
                            api.pop()
                        if (this.state.limitTypeId != 0 || this.state.promotionTypeId == 2) api.showMessage(data.msg, i18.t('titleMsgLogin'))
                        // Alert.alert(i18.t('titleMsgLogin'),'Không lấy được mã ưu đãi \n' + data.msg )
                        api.hideLoading()
                    }
                    isGettingPromotionCode = false
                })
        }
        setTimeout(() => {
            if (isGettingPromotionCode) {
                api.hideLoading()
                api.showMessage(i18.t('timeoutTextMsgContentLogin'), i18.t('titleMsgLogin'))
                isGettingPromotionCode = false
            }
        }, 60000)
    }
    _setDoiSoat(code, init) {
        this.setState({
            code: code,
            isVerify: init ? false : true
        })
        return;
        let arr = []
        this.state.promotionCode.map((item) => {
            if (item.code != code) {
                arr.push(item)
            }
        })
        arr.push({
            code: code,
            isDoiSoat: init != undefined ? false : true
        })

        this.setState({
            promotionCode: arr
        })
    }
    renderCode = () => {
        if (!this.state.code) return <View style={{  borderRadius: 0,marginVertical:1 }}>
            {this._renderFooter()}
        </View>
        let code = [];
        code = this.state.code.split('');
        return <View style={{ marginVertical:1, backgroundColor: 'white', padding: 5,paddingTop:10, borderRadius: 0 }}>
            <View style={{ flexDirection: 'row',paddingLeft:5,alignItems:'center'}}>
            <QRCode size={70}
                value={this.state.code}
                bgColor='#000'
                fgColor='#fff' />
            <View style={{ marginLeft: 5, flex: 1,alignItems:'center' }}>
            <Text style={{ color: '#000', fontSize: 15, fontWeight: '400' }}>{i18.t('voucherCode')}</Text>
            <View style={{ flexDirection:'row',alignSelf:'center' }}>
                {
                    code.map((el,index)=>{
                        return <Text key={index} style={{ color: '#000',paddingHorizontal:2, fontSize: 15, fontWeight: '500' }}>{el}</Text>
                    })
                }
            </View>
                {
                    this.state.isVerify ? <Text>{i18.t('verified')}</Text>:null
                }
                <Text style={{ fontSize:13}}>{i18.t('applyTo')} {api.getTime(new Date(), 'DD/MM/YYYY')}</Text>
            </View>
            </View>
            {
                this.state.isVerify ? null
                    :
                    <View style={{ marginTop:5,flexDirection:'row',alignItems:'center' }}>
                        <Text style={{ flex:1,fontSize:13,textAlign:'center',paddingLeft:5,paddingRight:5 }}>
                            {i18.t('giveEmployee')}
                        </Text>
                            {/* Cung cấp mã ưu đãi cho nhân viên cửa hàng để sử dụng</Text> */}
                        <Button
                            onPress={() => { api.showQR(i18.t('titleQRvetify'), this.state.code, false, this.state.id, () => functionRefresh(this.state.code), promotionTypeId) }}
                            style={{marginRight:3,marginBottom:3,alignSelf:'flex-end', backgroundColor: '#ff3300', paddingHorizontal: 10, height: 25 }}
                        >
                            {i18.t('vertify')}
                        </Button>
                    </View>
            }
        </View>

    }
    _renderListPromotionCode(arr) {
        if (arr !== undefined)
            if (arr.length !== 0)
                return (
                    <View
                        style={{
                            borderRadius: 3,
                            width: api.getRealDeviceWidth() * 2 / 3,
                            alignSelf: 'center',
                            marginTop: 10,
                            backgroundColor: '#fff',
                            shadowRadius: 3,
                            borderBottomColor: '#cdcdcd',
                            borderBottomWidth: 1
                        }}>
                        <View>
                            <Text
                                style={{
                                    fontWeight: 'bold',
                                    borderBottomWidth: 0.5,
                                    borderBottomColor: '#dadada',
                                    textAlign: 'center',
                                    flex: 1,
                                    paddingVertical: 3
                                }}>{i18.t('promotionList')}</Text>
                        </View>
                        <List
                            style={{
                                padding: 0
                            }}
                            dataArray={arr}
                            renderRow={(item, i) => <View
                                key={i}
                                style={{
                                    paddingHorizontal: 2,
                                }}>
                                <Text
                                    style={{
                                        textAlign: 'center',
                                        flex: 1,
                                        marginTop: 1,
                                        paddingVertical: 5,
                                        backgroundColor: item.isDoiSoat ? '#cdcdcd' : '#fff',
                                        color: item.isDoiSoat ? '#fff' : '#000',
                                    }}
                                    onPress={() => { if (item.isDoiSoat) { api.showMessage(i18.t('errVertified'), i18.t('titleMsgLogin')); return } else { api.showQR(i18.t('titleQRvetify'), item.code, false, this.state.id, () => { functionRefresh(item.code) }, promotionTypeId); }; }}>{item.code}</Text>
                            </View>}></List>
                    </View>
                )
    }
    _lessThanTen(t) {
        return t < 10
            ? '0' + t
            : t
    }
    _DateTime(t) {
        let d = new Date(t)
        return (
            <Text style={{
                fontSize: 12
            }}>{api.getTime(t, 'HH:mm DD/MM/YYYY')}</Text>
        )
    }
    _comment = () => {
        if (this.state.currentComment == '' || this.state.currentComment == undefined) {
            api.showMessage(i18.t('blankComment'), i18.t('titleMsgLogin'))
            return
        }
        let com = {
            // user_name: this.userInfo.user_name, content: this.state.currentComment,
            // avatar: this.userInfo.avatar, create_time: new Date(),
            content: this.state.currentComment,
            createdAt: new Date(),
            endUser: {
                avatar: this.userInfo.avatar,
                name: this.userInfo.user_name
            }
        }

        if (com.content !== '' && com.content !== undefined && !this.state.isSendding) {
            this.setState({ isSendding: true })
            return (dataService.sendComment(this.props.userState.token, this.props.navState.routes[this.props.navState.index].id, this.state.currentComment).then((data) => {
                if (data !== undefined)
                    //api.showMessage(data.msg)
                    this.setState({
                        listcomment: [
                            com, ...this.state.listcomment
                        ],
                        currentComment: '',
                        isSendding: false,
                        commentCount: this.state.commentCount + 1
                    })
            }))
        }
    }
    _renderListComment(arr) {
        const ds = new ListView.DataSource({
            rowHasChanged: (r1, r2) => r1 !== r2
        });
        console.log('arr', arr);
        // alert(arr.length)
        if (arr !== undefined) {
            if (arr.length == 0)
                return <Text
                    style={{
                        marginTop: 10,
                        textAlign: 'center',
                        flex: 1,
                        marginBottom: 10, color: '#fff', backgroundColor: 'transparent'
                    }}>{i18.t('noComment')}</Text>
            else
                return (
                    <View style={{ marginTop: 0, borderRadius: 5 }}>
                        <Card>
                            <ListView
                                style={{ borderRadius: 5 }}
                                contentContainerStyle={{ backgroundColor: '#fff', }}
                                dataSource={ds.cloneWithRows(arr)}
                                renderRow={(item, i) => {
                                    return (
                                        <View style={{ padding: 5, paddingBottom: 0 }}>
                                            <View>
                                                <View style={{ flexDirection: 'row' }}>
                                                    <Image
                                                        source={{
                                                            uri: item.endUser.avatar
                                                        }}
                                                        style={{
                                                            width: api.getRealDeviceWidth() / 12,
                                                            height: api.getRealDeviceWidth() / 12,
                                                            borderRadius: api.getRealDeviceWidth() / 24
                                                        }} />
                                                    <View style={{ marginLeft: 10, flex: 1 }}>
                                                        <View style={{ flexDirection: 'row' }}>
                                                            <Text
                                                                style={{
                                                                    fontWeight: 'bold',
                                                                    fontSize: 13,
                                                                    flex: 1
                                                                }}>{item.endUser.name}
                                                            </Text>
                                                            {this._DateTime(item.createdAt)}
                                                        </View>
                                                        <Text
                                                            style={{
                                                                fontSize: 13
                                                            }}>{item.content}
                                                        </Text>
                                                    </View>
                                                </View>
                                                <View style={{ marginTop: 5, flex: 1, height: 1, backgroundColor: '#cdcdcd' }} />
                                            </View>
                                            <Reply data={item.replies} />
                                        </View>
                                    );
                                }}
                            />
                        </Card>
                        {
                            arr.length > 2 ? <Button
                                style={{

                                }}
                                transparent
                                block
                                onPress={() => this.props.push({ key: 'comment', id: this.state.id })}>
                                <Text
                                    style={{
                                        color: '#fff'
                                    }}>
                                    {i18.t('showMoreComment')}</Text>
                            </Button> : null
                        }
                    </View>
                )
        } else
            return <ActivityIndicator size='large' />
    }

    _checkPer(key, gps, name, address, des, dueDate) {
        if (Platform.OS === 'android' && Platform.Version >= 23)
            PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION).then((result) => {
                if (result) {
                    console.log("Permission is OK");
                    api.push({
                        key: 'shopLocation',
                        gps: gps,
                        name: name,
                        address: address,
                        des: des,
                        dueDate: dueDate,
                        logo: this.state.partner.logo
                    })
                } else {
                    PermissionsAndroid.requestPermission(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION).then((result) => {
                        if (result) {
                            console.log("User accept");
                            api.push({
                                key: 'shopLocation',
                                gps: gps,
                                name: name,
                                address: address,
                                des: des,
                                dueDate: dueDate,
                                logo: this.state.partner.logo
                            })
                        } else {
                            console.log("User refuse");
                            api.showMessage(i18.t('errCamPer'), i18.t('titleMsgLogin'))
                        }
                    });
                }
            });
        else
            api.push({
                key: 'shopLocation',
                gps: gps,
                name: name,
                address: address,
                des: des,
                dueDate: dueDate,
                logo: this.state.partner.logo
            })
    }
    _renderImage(arr) {
        let tag = this.props.navState.routes[this.props.navState.index].tag ? this.props.navState.routes[this.props.navState.index].tag + '' : ''
        let length = 0;
        let fontSize = 15
        let temp = tag.replace(i18.t('point'), '')
        if (tag) length = temp.length; else length = 0
        if (length > 9) {
            fontSize = 10
        } else if (length > 6) {
            fontSize = 12
        }
        console.log('arr image', arr);
        if (arr !== undefined)
            if (arr.length > 0)
                return (
                    <View>
                        <Image source={require('../img/tag.png')} style={{ paddingBottom: 17, justifyContent: 'center', zIndex: 1, position: 'absolute', height: 50 * 1.581699346, top: 6.5, right: 0, width: 50, resizeMode: 'stretch' }}>
                            {tag ?
                                <Text style={{ backgroundColor: 'transparent', fontFamily: Platform.OS == 'ios' ? 'Arial' : 'San Francisco', zIndex: 1000000000, textAlign: 'center', fontSize: fontSize, color: '#fff' }}>{tag}</Text>
                                :
                                <Octicons name='gift' style={{ paddingLeft: Platform.OS == 'ios' ? 5 : 2, alignSelf: 'center', fontSize: 26, color: '#fff', backgroundColor: 'transparent' }} />
                            }
                        </Image>
                        <IndicatorViewPager
                            indicator={arr.length > 1 ? (<PagerDotIndicator pageCount={arr.length} />) : null}
                            style={{ height: (api.getRealDeviceWidth() - 10) / 1.9, borderRadius: 2, }}
                        >
                            {arr.map((item, i) => (
                                <View key={i} style={{ borderRadius: 2 }}><Image source={{ uri: item.url }} style={{
                                    flex: 1,
                                    resizeMode: 'stretch',
                                    height: (api.getRealDeviceWidth() - 10) / 1.9,
                                    width: api.getRealDeviceWidth() - 10,
                                    borderRadius: 2,
                                    marginTop: 7
                                }} /></View>
                            ))}
                        </IndicatorViewPager>
                    </View>
                )

    }
    _renderShop(arr) {
        console.log('arr shop', arr);
        if (arr != undefined)
            if (arr.length > 0)
                return (
                    <View
                        style={{ backgroundColor: '#fff', borderRadius: 2, marginTop: 5 }}
                    >
                        <Text
                            style={{
                                fontWeight: 'bold',
                                padding: 10,
                                borderBottomWidth: 1,
                                borderBottomColor: '#dadada'
                            }}>
                            <Icon name="ios-pin" style={{ color: '#808080' }} />
                            {' '}{i18.t('listShop')}</Text>
                        <List
                            dataArray={arr}
                            renderRow={(item, i) => <ListItem
                                key={i}
                                onPress={() => {
                                    this._checkPer('shopLocation', item.latitude + ',' + item.longitude, (item.name).toUpperCase(), item.address, this.state.shortDescription, this.state.endDate)
                                }}>
                                <Col>
                                    <Text
                                        style={{
                                            fontWeight: 'bold',
                                            marginTop: -2
                                        }}><Icon
                                            name='ios-pin-outline'
                                            style={{
                                                fontSize: 14
                                            }} /> {item.name}</Text>
                                    <Text>{item.address}</Text>
                                </Col>
                            </ListItem>}></List>
                    </View>
                )
    }


    _renderPage() {
        switch (this.state.page) {
            case 1:
                return (
                    <View
                        style={{
                            padding: 10,
                            //borderRadius: 4,
                            marginTop: 0, backgroundColor: '#fff'
                        }}>
                        {/*<Text>
                                {this.state.name}
                            </Text>*/}
                        <Text>
                            <Icon
                                name='ios-calendar-outline'
                                style={{
                                    fontSize: 14,
                                    color: 'black'
                                }} /> {'  ' + this.state.endDate}
                        </Text>

                        <View style={{ backgroundColor: '#fff', flex: 1 }}>
                            <Text>
                                {this.state.description}
                            </Text>
                        </View>

                    </View>
                );
            case 2:
                return (
                    <View style={{
                        padding: 10,
                        //borderRadius: 4,
                        marginTop: 0, backgroundColor: '#fff'
                    }}>
                        {this.state.data.detail != undefined && this.state.data.detail != '' && this.state.data.detail != null ? (<View
                        >
                            <Text>
                                {this.state.data.detail}
                            </Text>
                        </View>
                        ) : null}
                        <View
                            style={{
                                borderRadius: 3,
                                backgroundColor: '#fff'
                            }}>
                            <Text
                                style={{
                                    color: 'orange',
                                    fontWeight: 'bold',
                                    marginBottom: 5
                                }}>{i18.t('promtionGift')}</Text>

                            <List
                                dataArray={this.state.data.Promotions}
                                renderRow={(item, i) => <Text
                                    key={i}
                                    style={{
                                    }}>
                                    {item.name}
                                </Text>}></List>

                            <Text>
                                {this.state.data.name}
                            </Text>
                        </View>
                    </View>
                );
        }
    }
    _renderFooter() {
        // return null;
        return (
            <Footer
                style={{
                    backgroundColor: 'white',
                    flexDirection: 'row',
                    borderRadius: 0
                }}>
                <View
                    style={{
                        flex: 1,
                        flexDirection: 'row'
                    }}>
                    <View
                        style={{
                            flex: 2,
                            flexDirection: 'column',
                            padding: 5
                        }}>
                        <Text style={{ marginTop: 5, flex: 1, textAlign: 'center', color: '#000', fontSize: 9, textAlignVertical: 'center' }}>
                            {this.state.limitTypeId == 0 || this.state.remainCode < 0 ? '' : i18.t('remainTotalCode')}
                        </Text>
                        <Text style={{ flex: 1, textAlign: 'center', color: '#000', fontSize: 9, textAlignVertical: 'center' }}>
                            {this.state.limitTypeId == 0 || this.state.remainCode < 0 ? '' : this.state.remainCode}
                        </Text>
                    </View>
                    <View
                        style={{
                            flex: 3,
                            flexDirection: 'column'
                        }}>
                        <Button
                            onPress={() =>{if(!api.requestLogin()){ return; } this._getPromotionCode()}}
                            style={{ flex: 1, borderRadius: 20, backgroundColor: '#ff3f14', marginVertical: 5 }} block>
                            <Text style={{ fontSize: 12, color: '#fff' }}>
                                {this.state.button != undefined ? this.state.button.toUpperCase() : i18.t('recievePromotionCode')}
                            </Text>
                        </Button>
                    </View>
                    <View
                        style={{
                            flex: 2,
                            flexDirection: 'column',
                            padding: 5
                        }}>
                        <Text style={{ marginTop: 5, flex: 1, textAlign: 'center', color: '#000', fontSize: 9, textAlignVertical: 'center' }}>
                            {this.state.limitTypeId == 0 || this.state.remainDay < 0 ? '' : i18.t('remainCode')}
                        </Text>
                        <Text style={{ flex: 1, textAlign: 'center', color: '#000', fontSize: 9, textAlignVertical: 'center' }}>
                            {this.state.limitTypeId == 0 || this.state.remainDay < 0 ? '' : this.state.remainDay}
                        </Text>
                    </View>
                </View>
            </Footer>
        );
    }
    componentDidUpdate() {
        if (this.props.navState.routes[this.props.navState.index].rateLevel != undefined && this.state != null && this.props.navState.routes[this.props.navState.index].rateLevel != this.state.rate)
            this.setState({ rate: this.props.navState.routes[this.props.navState.index].rateLevel })
    }
    renderHtml() {
        if (this.state.html != undefined && this.state.html != '') return (
            <WebView
                source={{ header: '<head><meta charset="utf-8"/><style>*{font-size: 13px ;color:gray;margin-right: 2px}img{width:100% !important;height: 100% !important;}</style></head>', body: this.state.html }}
                style={{ paddingRight: 100, width: api.getRealDeviceWidth(), height: api.getRealDeviceHeight() * 0.5 }}
            />
        )
    }
    render() {
        if (this.props.userState.uuid !== undefined) {
            ga = new Analytics('UA-90479808-1', this.props.userState.uuid, 1, 'userAgent');
            var screenView = new Hits.ScreenView('Mpoint', 'Detail')
            ga.send(screenView);
        }
        if (this.state !== null && this.state.images !== undefined) {
            return (
                <Container>
                    <Header
                        style={{
                            backgroundColor: '#009688'
                        }}>
                        <Button transparent onPress={this.props.pop}>
                            <Icon name='ios-arrow-back' style={{ color: Platform.OS == 'ios' ? '#000' : '#fff' }} />
                        </Button>
                        <Title style={{ alignSelf:'center' } }>{i18.t('detail')}</Title>
                    </Header>
                    <Image
                        source={require('../img/bg_login.jpg')}
                        style={{
                            width: api.getRealDeviceWidth(),
                            height: this.props.navState.routes[this.props.navState.index].isGift ? api.getRealDeviceHeight() - 56 : api.getRealDeviceHeight() - 56
                        }}
                    >
                        <ScrollView
                            keyboardShouldPersistTaps={'handled'}
                            style={{
                                paddingLeft: 5,
                                paddingRight: 5,
                                paddingBottom: 10,
                                zIndex: 99,
                                //marginBottom: 20
                            }}
                            onContentSizeChange={(contentWidth, contentHeight) => bottomY = contentHeight}
                            ref={(component) => this._scrollView = component}
                        >

                            {this._renderImage(this.state.images)}
                            {this._renderStamp()}
                            <Item logo={{ uri: this.state.partner.logo }} name={this.state.partner.name} title={this.state.name} />
                            {this.renderCode()}
                            <View style={{
                                flexDirection: 'row',
                                height: api.getRealDeviceHeight() * 0.05, backgroundColor: '#fff',
                            }}>
                                <Text style={[{
                                    flex: 1, textAlign: 'center', borderRightWidth: 0.5,
                                    borderRightColor: 'gray', textAlignVertical: 'center', paddingTop: Platform.OS == 'ios' ? 5 : 0
                                }, this.state.styleTab1]}
                                    onPress={() => {
                                        this.setState({ page: 1, styleTab1: { backgroundColor: '#d9d9d9', color: '#fa9428' }, styleTab2: { color: 'gray' } })
                                    }}>
                                    {i18.t('promotionInfo')}
                                </Text>
                                <Text
                                    onPress={() => { this.setState({ page: 2, styleTab1: { color: 'gray' }, styleTab2: { backgroundColor: '#d9d9d9', color: '#fa9428' } }) }}
                                    style={[{ flex: 1, textAlign: 'center', textAlignVertical: 'center', paddingTop: Platform.OS == 'ios' ? 5 : 0 }, this.state.styleTab2]}>
                                    {i18.t('partnerInfomation')}
                                </Text>
                            </View>
                            
                            {this._renderPage()}
                            <View style={{ flexDirection: 'row', backgroundColor: '#fff', marginTop: api.getRealDeviceWidth() > 320 ? 0.5 : 1, paddingVertical: 5, borderBottomLeftRadius: 2, borderBottomRightRadius: 2 }}>
                                <Col
                                    style={{
                                        flexDirection: 'column',
                                        alignItems: 'center'
                                    }}
                                    onTouchEndCapture={() => this._onLikeButtonClick()}>
                                    <Row>
                                        <Icon
                                            name={this.state.iconLike}
                                            style={{
                                                color: 'red'
                                            }} />
                                        <Text
                                            style={{
                                                fontSize: 10,
                                                marginTop: 10
                                            }}>{this.state.likeCount}
                                        </Text>
                                    </Row>
                                    <Text>{i18.t('btLikeText')}</Text>
                                </Col>
                                <Col
                                    style={{
                                        flexDirection: 'column',
                                        alignItems: 'center'
                                    }}
                                    onTouchEndCapture={() => {
                                        {/* this.refs['input'].focus(); */}
                                        
                                         this._scrollView.scrollTo({y: bottomY-(Platform.OS == 'ios' ? 450:0), x: 0, }); 
                                        setTimeout(()=> {
                                            this.refs.input._textInput.focus();
                                        }, 50);
                                    }}
                                >
                                    <Row>
                                        <Icon
                                            name='ios-chatboxes-outline'
                                            style={{
                                                color: 'red'
                                            }} />
                                        <Text
                                            style={{
                                                fontSize: 10,
                                                marginTop: 10
                                            }}>{' ' + this.state.commentCount}
                                        </Text>
                                    </Row>
                                    <Text>{i18.t('btCmtText')}</Text>
                                </Col>
                                <Col
                                    style={{
                                        flexDirection: 'column',
                                        alignItems: 'center'
                                    }}
                                    onTouchEndCapture={() => this.props.showRate()}>
                                    <Row>
                                        <Icon
                                            name='ios-star-outline'
                                            style={{
                                                color: 'red'
                                            }} />
                                        <Text
                                            style={{
                                                fontSize: 10,
                                                marginTop: 10
                                            }}>{this.state.rate != undefined && this.state.rate != null ? (this.state.rate + '').substring(0, 3) : ''}
                                        </Text>
                                    </Row>
                                    <Text>{i18.t('btRateText')}</Text>
                                </Col>
                                <Col
                                    style={{
                                        flexDirection: 'column',
                                        alignItems: 'center'
                                    }}
                                    onTouchEndCapture={this._share.bind(this)}>
                                    <Row>
                                        <Icon
                                            name='logo-facebook'
                                            style={{
                                                color: '#3b5998'
                                            }} />
                                    </Row>
                                    <Text>{i18.t('btShareText')}</Text>
                                </Col>
                            </View>
                            {/* {this.renderCode()} */}
                            {/* {this._renderListPromotionCode(this.state.promotionCode)} */}
                            {this.renderHtml()}
                            <View
                                style={{
                                    padding: 10,
                                    marginTop: 5,
                                    backgroundColor: '#fff',
                                    borderRadius: 2
                                }}>
                                {
                                    this.state.partner.phone != undefined && this.state.partner.phone != null ?
                                        <View style={{ flexDirection: 'row' }}>
                                            <Icon name='ios-call-outline' style={{ textAlignVertical: 'center' }} />
                                            <Text
                                                style={{
                                                    color: '#6495ED',
                                                    paddingLeft: 5,
                                                    paddingVertical: 5,
                                                    textAlignVertical: 'center'
                                                }}
                                                onPress={() => Linking.openURL('tel:' + this.state.partner.phone)}>{' ' + this.state.partner.phone}</Text>
                                        </View> : null
                                }
                                {
                                    this.state.partner.website == null || this.state.partner.website == undefined ? null :
                                        <View style={{ flexDirection: 'row' }}>
                                            <Icon name='ios-globe-outline' style={{ textAlignVertical: 'center' }} />
                                            <Text
                                                style={{
                                                    color: '#6495ED',
                                                    paddingLeft: 5,
                                                    paddingVertical: 5,
                                                    textAlignVertical: 'center'
                                                }}
                                                onPress={() => Linking.openURL(this.state.partner.website)}>{i18.t('webLinkText')}</Text>
                                        </View>
                                }
                            </View>
                            {this._renderShop(this.state.partner.shops)}
                            <View
                                style={{
                                    flexDirection: 'row',
                                    padding: 10,
                                    backgroundColor: '#fff',
                                    marginTop: 5,
                                    borderRadius: 3
                                }}>
                                <Image
                                    source={{
                                        uri: this.userInfo.avatar !== undefined
                                            ? this.userInfo.avatar
                                            : 'https://d30y9cdsu7xlg0.cloudfront.net/png/17241-200.png'
                                    }}
                                    style={{
                                        height: api.getRealDeviceWidth() / 8,
                                        width: api.getRealDeviceWidth() / 8,
                                        borderRadius: api.getRealDeviceWidth() / 16,
                                        maxHeight: 70,
                                        maxWidth: 70
                                    }} />
                                <Input
                                    //onFocus={() => { this._scrollView.scrollTo({ y: bottomY - 420, x: 0, }) }}
                                    ref='input'
                                    placeholder={i18.t('addComment')}
                                    placeholderTextColor='gray'
                                    value={this.state.currentComment}
                                    onChangeText={(t) => this.setState({ currentComment: t })}
                                    style={{
                                        borderWidth: 1,
                                        borderColor: '#cdcdcd',
                                        marginLeft: 10,
                                        borderRadius: 2
                                    }} />
                                <Button transparent onPress={() => this._comment()}>
                                    <Icon name="ios-paper-plane-outline" />
                                </Button>
                            </View>
                            <KeyboardSpacer topSpacing={10}/>
                            {this._renderListComment(this.state.listcomment)}
                            <Ad token={this.props.userState.token} />
                        </ScrollView>
                    </Image>
                    {/* {this.props.navState.routes[this.props.navState.index].isGift ? null : this._renderFooter()} */}

                </Container>
            )
        }
        return (
            <Container>
                <Header
                    style={{
                        backgroundColor: '#009688'
                    }}>
                    <Button transparent onPress={this.props.pop}>
                        <Icon name='ios-arrow-back' style={{ color: Platform.OS == 'ios' ? '#000' : '#fff' }} />
                    </Button>
                    <Title style={{ alignSelf:'center' } }>{i18.t('detail')}</Title>
                </Header>

                <Content>
                    <ActivityIndicator size='large' />
                </Content>
            </Container>
        )

    }

}
mapStateToProps = (state) => ({ navState: state.navState, userState: state.userState })

mapDispatchToProps = (dispatch) => ({
    pop: () => dispatch(pop()),
    push: (route) => dispatch(push(route)),
    showRate: () => dispatch(showRate())
})
export default connect(mapStateToProps, mapDispatchToProps)(Detail)