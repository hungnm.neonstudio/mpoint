import React, {Component} from 'react';
import {View, Image, Text, ScrollView,
    Platform,AsyncStorage,PermissionsAndroid, ListView,
TouchableOpacity} from 'react-native';
import {
    Header,
    Title,
    Container,
    Content,
    Button,
    Icon,
    Input,
    InputGroup
} from 'native-base';
import api from '../api';
import dataService from '../api/dataService';
import DeviceInfo  from 'react-native-device-info'
import {connect} from 'react-redux';
import PopupDialog,{ SlideAnimation,DialogTitle } from 'react-native-popup-dialog';
import {setUserInfo, setUserToken, setUuid} from '../actions/userActions'
import i18 from './i18'
let phone = ''
let otpID = ''
let OTP  = ''
let cardID = ''

class BNA extends Component {
    constructor(){
        super();
        // console.log('data in contructor', this.props.navState.routes[this.props.navState.index].item);
        this.state={
            showinput:true,
            phone:phone!=''?phone:'',
            isloading:false,
            card:'',
            title:'',
            data: [],
            partners: [],
            read:false
        }
       
    }
     ds = new ListView.DataSource({
        rowHasChanged: (r1, r2) => r1 !== r2
    });
    componentWillMount(){
        phone = this.props.navState.routes[this.props.navState.index].phone
        if(this.props.navState.routes[this.props.navState.index].item !== undefined) {
            this.setState({data: this.props.navState.routes[this.props.navState.index].item});
        }
    }
    componentDidMount(){
        if(this.props.navState.routes[this.props.navState.index].item!=undefined) {
            this.setState({data:this.props.navState.routes[this.props.navState.index].item})
        }
        console.log('data',this.props.navState.routes[this.props.navState.index].item)
        if(this.props.navState.routes[this.props.navState.index].partners !=undefined) {
            this.setState({partners:this.props.navState.routes[this.props.navState.index].partners})
        }
    }
    componentDidUpdate(){
        if(this.props.navState.routes[this.props.navState.index].code!=undefined && this.state.card!=this.props.navState.routes[this.props.navState.index].code)
        {this.setState({card:this.props.navState.routes[this.props.navState.index].code})
        cardID = this.props.navState.routes[this.props.navState.index].code
        }
    }
    _renderListPartners() {
        console.log('list partners', this.state.partners);
        return(
            <ScrollView
                style={{ paddingBottom:0 }}
                >
            <ListView
                enableEmptySections={true}
                dataSource={this.ds.cloneWithRows(this.state.partners)}
                contentContainerStyle={{
                flexDirection: 'row',
                flexWrap: 'wrap',
                alignItems: 'stretch'
            }}
                renderRow={(item, sectionID, id) => {
                    if(!item.isGroup) {
                    return(
                                <TouchableOpacity
                                key={id}
                                style={{
                                    alignSelf: 'center'
                                    }}
                                    onPress={() => {
                                        if(this.props.userState.listMember.length!=0){
                                            api.push({key: 'regCard', item:item})
                                        }
                                        else
                                         api.push({key: 'addCardFromLogin', item:item,phone:phone})
                                }}><Image
                                    source={{
                                    uri: item.card
                                }}
                                    style={{
                                    width: (api.getRealDeviceWidth() - 25) / 2,
                                    height: (api.getRealDeviceWidth() - 25) / 3,
                                    resizeMode: 'stretch',
                                    margin: 5,
                                    borderRadius:10
                                }}
                            /></TouchableOpacity>
                            )
                        } else {
                            return null
                        }
                    }}
                />
            </ScrollView>
        );
    }

     _renderIntroduce(arr){
        return arr.length ==1 && arr[0]==''? <Text style={{ color:'#fff' }}>
                 <Text style={{ color:'#2FCC77' }}>•</Text> {i18.t('updating')}.
            </Text>: arr.map((item,id)=>(
            item==''?null:<Text key={id} style={{ color:'#fff' }}>
                 <Text style={{ color:'#2FCC77' }}>•</Text> {item.replace('-','')}.
            </Text>
        ))
    }
    _renderContact(arr) {
         return (
             <View
                style={{ padding:10,backgroundColor:'rgba(20,20,20,.5)',borderBottomRightRadius:5,borderTopRightRadius:5,borderBottomLeftRadius:5 }}
             >
                 {
                     arr.length == 1 && arr[0]==''? <Text style={{ color:'#fff' }}>
                 <Text style={{ color:'#2FCC77' }}>•</Text> {i18.t('updating')}.
            </Text>:arr.map((item,id)=>(
        item==''?null:<Text key={id} style={{ color:'#fff' }}>
             {item.replace('-','')}
        </Text>))
                 }
             </View>
         )
    }
    render() {
        return (
            <Container>
                <Header
                    style={{
                    backgroundColor: '#009688'
                }}>
                    <Title style={{ alignSelf:'center' } }>
                        {i18.t('titleNewRegCard')}
                    </Title>
                    <Button transparent onPress={() => api.pop()}>
                        <Icon name='ios-arrow-back' style={{ color:Platform.OS =='ios'?'#000':'#fff' }}/>
                    </Button>
                </Header>
                 <Image
                        style={{ height:api.getRealDeviceHeight()-56,width:api.getRealDeviceWidth() }}
                        source={require('../img/bg_login.jpg')}
                    >
                <ScrollView>
                    
                   <Image
                        source={{
                        uri: this.state.data.banner
                    }}
                        style={{
                        width: api.getRealDeviceWidth() * 0.95,
                        height: api.getRealDeviceWidth() * 0.95 / 5.7,
                        resizeMode: 'stretch',
                        margin: 5,
                        borderRadius:5,
                        alignSelf: 'center'}}/>
                    
                    {/*<ScrollView
                        style={{
                        height: 1,
                        backgroundColor: 'gray',
                        marginRight: 10,
                        marginLeft: 10,minHeight:api.getRealDeviceHeight()
                    }}></ScrollView>*/}
                    <View style={{
                        flex: 1,
                        minHeight:api.getRealDeviceHeight()/2,
                        paddingBottom:70
                    }}>
                       <View style={{padding: 10}}>
                            {/*<Text style={{ fontSize:20,fontWeight:'400',color:'#2FCC77',textAlign:'center',marginBottom:10 }}>
                            THẺ ƯU ĐÃI VÀ TÍCH ĐIỂM {this.state.data.name}
                        </Text>*/}
                        {/*<Text style={{ fontSize:18,color:'#2FCC77',fontWeight:'300' }}>
                            Giới thiệu
                        </Text>*/}
                        <View style={{ flexDirection:'row',marginTop:10 }}>
                                <Image source={ require('../img/icon/lienhe.png') } style={{ width:20*4.47,height:20,resizeMode:'stretch' }}
                                >
                                <Text style={{ color:'#fff',fontSize:13,marginLeft:5,textAlignVertical:'center',marginTop:2,backgroundColor:'transparent' }}>{i18.t('intro')}</Text>
                                </Image>
                        </View>
                        <View
                style={{ padding:10,backgroundColor:'rgba(20,20,20,.5)',borderBottomRightRadius:5,borderTopRightRadius:5,borderBottomLeftRadius:5 }}
            >
                        <Text style={{ color:'#fff' }}>{this.state.data.introduce!=''?this.state.data.introduce:'- '+i18.t('updating')}</Text>
                        </View>
                        {/*{this._renderIntroduce(this.state.data.introduce.split('\n'))}*/}
                        <View style={{ flexDirection:'row',marginTop:10 }}>
                                <Image source={ require('../img/icon/lienhe.png') } style={{ width:20*4.47,height:20,resizeMode:'stretch' }}
                                >
                                <Text style={{ color:'#fff',fontSize:13,marginLeft:5,textAlignVertical:'center',marginTop:2,backgroundColor:'transparent'  }}>{i18.t('contact')}</Text>
                                </Image>
                                <Text style={{ textDecorationLine:'underline',fontSize:15,color:'#fff',backgroundColor:'transparent' }}
                                    onPress={()=>{
                                        this.setState({read:!this.state.read})
                                    }}
                                >{this.state.read?i18.t('showLess'):i18.t('showMore')}</Text>
                        </View>
                        {this.state.read?this._renderContact(this.state.data.contact.split('\n')):null}
                       </View>
                       <Image source={ require('../img/icon/nen.png') } style={{ width:20*11.7,height:20,resizeMode:'stretch',marginLeft:10,marginBottom:10 }}
                                >
                        <Text style={{ marginTop:Platform.OS == 'ios'?3:0 ,marginLeft:5,color:'#fff', fontSize: 13,backgroundColor:'transparent'}}>
                            {i18.t('listCardBNA')}
                        </Text>
                        </Image>
                        {this._renderListPartners()}
                    </View>
                </ScrollView>
                </Image>
            </Container>
        );
    }
}
const mapState = (state) => {
    return {navState: state.navState,userState: state.userState}
}
const mapDispatch = (dispatch) => {
    return {
        setUserInfo: (data) => dispatch(setUserInfo(data)),
    }
}
export default connect(mapState, mapDispatch)(BNA);