import React, {Component} from 'react';
import {View, Image, Text, Keyboard,ScrollView,Platform,AsyncStorage,PermissionsAndroid} from 'react-native';
import {
    Header,
    Title,
    Container,
    Content,
    Button,
    Icon,
    Input,
    InputGroup
} from 'native-base';
import api from '../api';
import dataService from '../api/dataService';
import DeviceInfo  from 'react-native-device-info'
import {connect} from 'react-redux';
import PopupDialog,{ SlideAnimation,DialogTitle } from 'react-native-popup-dialog';
import {setUserInfo, setUserToken, setUuid} from '../actions/userActions'
import IconQR from 'react-native-vector-icons/FontAwesome';
import i18 from './i18'
let phone = ''
let otpID = ''
let OTP  = ''
let cardID = ''
let check = ''
let _showConfirm=()=>{
    api.hideLoading()
    api.showConfirm(i18.t('contentConfirmBNA'),i18.t('titleConfirmBNA'),i18.t('confirmOKLogin'),i18.t('confirmCancelTextLogin'),
        function onok(){
            api.push({key:'RegUser'})
            api.hideConfirm()
        },
        function oncancel(){
            api.showLoading()
            // api.resetRoute({key:'home'})
            api.gotoHome();
            api.hideConfirm();
        }
    )
}
let setGiftCount = (token)=>{
    dataService.getGiftCount(token)
    .then((rs)=>{
            api.setGitfCount(rs.err==0?rs.amount:'0')
    })
}
class AddCardFromLogin extends Component {
    constructor(){
        super()
        this.state={
            showinput:true,
            phone:phone!=''?phone:'',
            isloading:false,
            card:'',
            title:'',
            data:{
                introduce:'',
                howuse:'',
                object:''
            }
        }
    }
    componentWillUnmount(){
        phone = ''
        otpID = ''
        OTP  = ''
        cardID = ''
        check = ''
    }
    componentWillMount(){
        //phone = this.props.navState.routes[this.props.navState.index].phone
        if(this.props.navState.routes[this.props.navState.index].phone!=undefined){
            phone = this.props.navState.routes[this.props.navState.index].phone;
            this.setState({phone:this.props.navState.routes[this.props.navState.index].phone})
        }
    }
    componentDidMount(){
        if(this.props.navState.routes[this.props.navState.index].item!=undefined)
        this.setState({data:this.props.navState.routes[this.props.navState.index].item})
        console.log(this.state.data)
    }
    componentDidUpdate(){
        if(this.props.navState.routes[this.props.navState.index].code!=undefined && check!=this.props.navState.routes[this.props.navState.index].code && cardID !=this.props.navState.routes[this.props.navState.index].code)
        {this.setState({card:this.props.navState.routes[this.props.navState.index].code})
        cardID = this.props.navState.routes[this.props.navState.index].code
        check =  this.props.navState.routes[this.props.navState.index].code
    }
    }

    _regCard = () =>{
        api.showLoading();
        dataService.postAddCard(cardID).then(rs=>{
            api.hideLoading();
            if(rs.err == 0){
               _showConfirm();
            }else{
                api.showMessage(rs.msg);
            }
            
        });
    }
    _onpress(){
        Keyboard.dismiss();
        if(cardID == '')
        {
            api.showMessage(i18.t('blankCard'),i18.t('titleMsgLogin'))
            return
        }
        if(api.getToken()) return this._regCard();
        this.setState({isloading:true})
        api.showLoading()
        dataService.getUserToken(cardID, DeviceInfo.getUniqueID())
        .then(rs=>{
            this.setState({isloading:false})
            if(rs.err == 3){
                this.setState({title:rs.msg})
                this.popupDialog.openDialog();
            }
            else {
                api.showMessage(i18.t('invalidCard'),i18.t('titleMsgLogin'))
            }
            api.hideLoading()
        })
        setTimeout(()=>{
            if(this.state.isloading)
           { api.hideLoading()
            api.showMessage(i18.t('timeoutTextMsgContentLogin'),i18.t('titleMsgLogin'))}
        },10000)
        // this.popupDialog.openDialog();
    }
    _onInputPhoneOK = () =>{
        this.setState({phone:phone})
        this.popupDialog.closeDialog();
        this.DialogOTP.openDialog();
    }
    _onconfirmOTP(){
        if(OTP != '')
        {
        api.showLoading()
        dataService.postVerifyOtpLogin(otpID,OTP, DeviceInfo.getUniqueID())
        .then(rs=>{
                if (rs.err == 0) {
                    dataService.getListShopGPS(rs.token)
                                                .then(gps=>{
                                                    api.setLocationData(gps.shops)
                                                })
                    api.setUserToken(rs.token)
                    setGiftCount(rs.token)
                    dataService
                        .postAddDevice( DeviceInfo.getUniqueID(),  DeviceInfo.getUniqueID(), rs.token)
                        .then(rs => {})
                                            dataService.postAddCard(cardID,rs.token)
                        .then(data=>{
                            if(data.err == 0 ){
                                try {
                            AsyncStorage.setItem('@RFTK:key', rs.refreshToken);
                            // alert(dt.refreshToken)
                        } catch (error) {
                            console.log('Error when saving data', error);
                        }
                                dataService
                            .getUserInfo(rs.token)
                            .then((datas) => {
                                console.log('datas', datas);
                                if (datas.err == 0) {

                                    this
                                        .props
                                        .setUserInfo(datas)
                                    api.resetRoute({key: 'home'})
                                }
                                // api.hideLoading()
                            })
                        }
                        else{
                            api.hideLoading()
                        }
                        // api.showMessage(data.msg,i18.t('titleMsgLogin'))
                        })
                    //setTimeout(this._gotoPromotion(), 1000);

                } 
               else{
                  api.hideLoading()
                  api.showMessage(rs.msg,i18.t('titleMsgLogin')) 
               }
        })
    }
        else{
            api.showMessage(i18.t('errOtpMsgLogin'),i18.t('titleMsgLogin'))
        }
    }
_checkCamPer(){
                if (Platform.OS === 'android' && Platform.Version >= 23)
        PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.CAMERA).then((result) => {
                if (result) {
                  console.log("Permission is OK");
                  camPer = true
                  api.push({key:'QRscancode'})
                } else {
                  PermissionsAndroid.requestPermission(PermissionsAndroid.PERMISSIONS.CAMERA).then((result) => {
                    if (result) {
                      console.log("User accept");
                       api.push({key:'QRscancode'})
                    } else {
                      console.log("User refuse");
                      api.showMessage(i18.t('errCamPer'),i18.t('titleMsgLogin'))
                    }
                  });
                }
          });
          else  api.push({key:'QRscancode'})
        }
_onconfirmRegOTP(){
        if(OTP != '')
        {
        api.showLoading()
        dataService.postVerifyOtpRegister(phone,OTP,otpID)
        .then((rs)=>{
            // api.hideInputBox()
                if(rs.err == 0)
                {
                    // api.hideInputBox()
                    api.setUserToken(rs.token)
                    dataService.getListShopGPS(rs.token)
                                                .then(gps=>{
                                                    api.setLocationData(gps.shops)
                                                })
                    setGiftCount(rs.token)
                    // dataService.changeCard(rs.token,526)
                    // .then(data=>{
                        
                    // })
                    try {
                                     AsyncStorage.setItem('@RFTK:key', rs.refreshToken);
                                       // alert(dt.refreshToken)
                                         } catch (error) {
                                      console.log('Error when saving data', error);
                                      }
                    // api.setUserToken('125064b0-12bc-4a38-a03f-717156e4b6e4')
                    
                    // dataService.postRegisterFreeCard(123,rs.token)
                    // // dataService.postRegisterFreeCard(350,'44046d79-a01c-46aa-95e9-4632cd65ac74')
                    // .then(kq=>{
                        
                        // if(kq.err == 0){
                            // alert('123')
                            // api.resetRoute({key:'login'})
                            // api.push({key:'home'})

                             dataService.postAddCard(cardID,rs.token)
        .then(rsx=>{
            api.hideLoading()
            // api.showMessage(rsx.msg,i18.t('titleMsgLogin'))
             if(rs.err == 0){
                 dataService
                            .getUserInfo(rs.token)
                            .then((datas) => {
                                console.log('datas', datas);
                                if (datas.err == 0) {
                                    dataService.postAddDevice( DeviceInfo.getUniqueID(), DeviceInfo.getUniqueID())
                                    .then(rs1=>{

                                    })
                                api.setUserInfo(datas)
                                _showConfirm()
                                }
                            })
                
            }
        })
                }
                else {
                    api.showMessage(rs.msg,i18.t('titleMsgLogin'))
                    api.hideLoading()
                }   
                api.hideLoading()
        })
    }
        else{
            api.showMessage(i18.t('errOtpMsgLogin'),i18.t('titleMsgLogin'))
        }
    }
    _reg(){
        // alert(phone)
         api.showLoading()
            dataService.postReg(phone)
            .then(rs=>{
                
                if(rs.err ==0 ){
                    otpID = rs.otpId
                    //alert(rs.otpId)
                    // api.showInputBox('Nhập OTP','Nhập OTP','',this._onConfirmOTP.bind(this))
                    // api.setRegInfo('781160c2-f543-4ec8-8f66-b425a38af40c')
                    this.popupDialog.closeDialog();
                    this.regDialogOTP.openDialog();
                    }
                    else{
                        api.showMessage(rs.msg,i18.t('titleMsgLogin'))
                    }
                    setTimeout(()=>{
                        api.hideLoading()
                    },1)
                })
    }
      _checkLogin = async() => {
        if(phone != '' && api.validatePhone(phone))
        {
            this.setState({phone:phone})
            api.showLoading()
        dataService.getUserToken(phone, DeviceInfo.getUniqueID())
        .then(rs=>{
              if (rs.err == 0) {
                        setGiftCount(rs.token)
                        api.setUserToken(rs.token)
                        dataService.getListShopGPS(rs.token)
                                                .then(gps=>{
                                                    api.setLocationData(gps.shops)
                                                })
                        dataService.postAddCard(cardID,rs.token)
                        .then(data=>{
                            if(data.err == 0 ){
                                try {
                            AsyncStorage.setItem('@RFTK:key', rs.refreshToken);
                            // alert(dt.refreshToken)
                        } catch (error) {
                            console.log('Error when saving data', error);
                        }
                                dataService
                            .getUserInfo(rs.token)
                            .then((datas) => {
                                console.log('datas', datas);
                                if (datas.err == 0) {

                                    this
                                        .props
                                        .setUserInfo(datas)
                                    api.resetRoute({key: 'home'})
                                }
                                // api.hideLoading()
                            })
                        }
                        else{
                            api.hideLoading()
                            api.showMessage(data.msg,i18.t('titleMsgLogin'))
                        }
                        
                        })
                        
                        //setTimeout(this._gotoPromotion(), 1000);

                    }
            else if (rs.err == 4) {
                        otpID = rs.otpId
                        api.hideLoading()
                        // alert(otpID)
                        this._onInputPhoneOK()
                    } 
            else if(rs.err == 2){
               this._reg()
            }
            else{
                api.hideLoading()
                api.showMessage(rs.msg,i18.t('titleMsgLogin'))
            }
        }
        
        )}
        else{
            api.showMessage(i18.t('invalidPhone'),i18.t('titleMsgLogin'))
        }
    }
    _renderIntroduce(arr){
        return arr.length ==1 && arr[0]==''? <Text>
                 <Text style={{ color:'#2FCC77' }}>•</Text> {i18.t('updating')}.
            </Text>: arr.map((item,id)=>(
            item==''?null:<Text key={id}>
                 <Text key={id} style={{ color:'#2FCC77' }}>•</Text> {item.replace('-','')}.
            </Text>
        ))
    }
   _renderHowUse(arr){
        return (
            <View
                style={{ padding:10,backgroundColor:'rgba(20,20,20,.5)',borderBottomRightRadius:5,borderTopRightRadius:5,borderBottomLeftRadius:5 }}
            >
                {arr.length == 1 && arr[0]==''? <Text style={{ color:'#fff' }}>
                 <Text style={{ color:'#fff' }}>•</Text> {i18.t('updating')}.
            </Text>:arr.map((item,id)=>(
        item==''?null:<Text key={id} style={{ color:'#fff' }}>
             <Text style={{ color:'#fff' }}>»</Text> {item.replace('-','')}
        </Text>))}
            </View>
        )
    }
    _renderObject(arr){
        return (
            <View
                style={{ padding:10,backgroundColor:'rgba(20,20,20,.5)',borderBottomRightRadius:5,borderTopRightRadius:5,borderBottomLeftRadius:5 }}
            >
                {
                    arr.length == 1  && arr[0]==''? <Text style={{ color:'#fff' }}>
                 <Text style={{ color:'#fff' }}>•</Text> {i18.t('updating')}.
            </Text>:arr.map((item,id)=>(
            item==''?null:<Text key={id} style={{ color:'#fff' }}>
             <Text  style={{ color:'#fff' }}>»</Text> {item.replace('-','')}
        </Text>
        ))
                }
            </View>
        )
    }
    render() {
        return (
            <Container>
                <Header
                    style={{
                    backgroundColor: '#009688',
                    zIndex:1000
                }}>
                    <Title style={{ alignSelf:'center' } }>
                        {i18.t('titleAddCardPage')}
                    </Title>
                    <Button transparent onPress={() => api.pop()}>
                        <Icon name='ios-arrow-back' style={{ color:Platform.OS == 'ios'?'#000':'#fff' }}/>
                    </Button>
                </Header>
                    <Image
                        style={{ height:api.getRealDeviceHeight(),width:api.getRealDeviceWidth(),marginTop:-56 }}
                        source={require('../img/bg_login.jpg')}
                    >
                    <ScrollView
                        keyboardShouldPersistTaps={'handled'}
                    >
                        <View
                        style={{
                            marginTop:66,
                            backgroundColor: 'white',
                            borderRadius: 10,
                            width:api.getRealDeviceWidth()-20,
                            height:(api.getRealDeviceWidth-20)/1.57,
                            //resizeMode: 'stretch',
                            margin: 10
                    }}>
                    <View style={{
                            marginTop: 10,
                            borderColor:'#cdcdcd',
                            borderBottomWidth:1,
                        }}>
                        <View style={{ flexDirection:'row-reverse',paddingHorizontal: 10, }}>
                            
                            <Image style={{ width:api.getRealDeviceWidth()/4,height:api.getRealDeviceWidth()/(4*1.57),marginLeft:10,marginBottom:10,right:0,borderRadius:2 }} source ={{ uri:this.state.data.card }} />
                            <Text style={{textAlign: 'center', fontSize: Platform.OS == 'ios'?11:api.getRealDeviceWidth()>320?13:12,fontWeight:'400',textAlignVertical:'center',color:'#000',marginTop:Platform.OS == 'ios'?10:0}}>{i18.t('leftHeadAddCardPage')} {'\n'}{i18.t('rightHeadAddCardPage')}</Text>
                        </View>
                    </View>
                    <View
                            style={{ margin:5,backgroundColor:'rgba(255, 255, 255, 1)',
                            width:api.getRealDeviceWidth()-40,
                            alignSelf:'center',
                            borderRadius:5,
                            paddingVertical:5
                             }}
                        >
                            <View style={{ flexDirection:'row' }}> 
                                <View style={{ flex:7 }}>
                                   
                            <Input 
                            returnKeyLabel={i18.t('continue')}
                            onSubmitEditing={()=>{ this._onpress() }}
                            placeholder={i18.t('placeHolderInputCard')} onChangeText={(t)=>{cardID = t}}  defaultValue={this.state.card}
                             style={{
                                backgroundColor: '#fff',
                                borderColor:'#cdcdcd',
                                borderWidth:1,
                            }}
                            />
                        <Button block onPress={this._onpress.bind(this)}
                            style={{ backgroundColor:'#fa6428',marginTop:5 }}
                        >
                                {(i18.t('continue')).toUpperCase()}
                            </Button>
                                </View> 
                                <View style={{ flex:3,marginTop:15}} onTouchEndCapture={()=>{
                                    this._checkCamPer()
                                    }}>
                                   <View style={{ alignSelf:'center',borderColor:'#000',borderWidth:1,borderRadius:21,paddingHorizontal:Platform.OS == 'ios'?8 :6,paddingVertical:Platform.OS =='ios'?5 :1 }}>
                                        <IconQR name="qrcode" size={Platform.OS =='ios'?30: 40} color="#000"/>
                                   </View>
                                    <Text  style={{ color:'#000',textAlign:'center' }}>{i18.t('scantitle')}</Text>
                                </View>
                            </View>
                        </View>
                    </View>
                    
                    {/*<ScrollView
                        style={{
                        height: 1,
                        backgroundColor: 'gray',
                        marginRight: 10,
                        marginLeft: 10,minHeight:api.getRealDeviceHeight()
                    }}></ScrollView>*/}
                    <View style={{
                        flex: 1,
                        minHeight:api.getRealDeviceHeight()/2,
                        padding:10,
                        paddingBottom:70
                    }}>
                    <View
                style={{ padding:5,backgroundColor:'rgba(20,20,20,.5)',borderBottomRightRadius:5,borderTopRightRadius:5,borderBottomLeftRadius:5 }}
            >
                        <Text style={{ fontSize:Platform.OS=='ios'?18:20,fontWeight:'400',color:'#fff',textAlign:'center'}}>
                            {i18.t('bodyAddcardPage')} {(this.state.data.name!=undefined?this.state.data.name:'').toUpperCase()}
                        </Text>
                        </View>
                        {/*<Text style={{ fontSize:18,color:'#2FCC77',fontWeight:'300' }}>
                            Giới thiệu
                        </Text>*/}
                        <View style={{ flexDirection:'row',marginTop:10 }}>
                                <Image source={ require('../img/icon/lienhe.png') } style={{ width:20*4.47,height:20,resizeMode:'stretch' }}
                                >
                                <Text style={{ color:'#fff',fontSize:13,marginLeft:5,textAlignVertical:'center',backgroundColor:'transparent' }}>{i18.t('intro')}</Text>
                                </Image>
                        </View>
                        <View
                style={{ padding:10,backgroundColor:'rgba(20,20,20,.5)',borderBottomRightRadius:5,borderTopRightRadius:5,borderBottomLeftRadius:5 }}
            >
                        <Text style={{ color:'#fff' }}>{this.state.data.introduce!=''?this.state.data.introduce:'- '+i18.t('updating')}</Text>
                        </View>
                        {/*{this._renderIntroduce(this.state.data.introduce.split('\n'))}*/}
                        {/*<Text style={{ fontSize:18,color:'#2FCC77',fontWeight:'300' }}>
                            Hướng dẫn sử dụng
                        </Text>*/}
                        <View style={{ flexDirection:'row',marginTop:10 }}>
                                <Image source={ require('../img/icon/nen.png') } style={{ width:20*7,height:20,resizeMode:'stretch' }} >
                                    <Text style={{ color:'#fff',fontSize:13,marginLeft:5,textAlignVertical:'center',backgroundColor:'transparent'  }}>{i18.t('instruction')}</Text>
                                </Image></View>
                        <View
                style={{ padding:10,backgroundColor:'rgba(20,20,20,.5)',borderBottomRightRadius:5,borderTopRightRadius:5,borderBottomLeftRadius:5 }}
            >
                        <Text style={{ color:'#fff' }}>{this.state.data.howuse!=''?this.state.data.howuse:'- '+i18.t('updating')}</Text>
                        </View>
                        <Image source={ require('../img/icon/lienhe.png') } style={{  width:20*4.47,height:20,resizeMode:'stretch',marginTop:10 }}
                                >
                        <Text style={{ marginTop:Platform.OS == 'ios'?3:0 ,marginLeft:5,color:'#fff', fontSize: 13,backgroundColor:'transparent'}}>
                            {i18.t('object')}
                        </Text>
                        </Image>
                        <View
                style={{ padding:10,backgroundColor:'rgba(20,20,20,.5)',borderBottomRightRadius:5,borderTopRightRadius:5,borderBottomLeftRadius:5 }}
            >
                        <Text style={{ color:'#fff' }}>{this.state.data.object!=''?this.state.data.object:'- '+i18.t('updating')}</Text>
                    </View>
                    </View>
                    
                </ScrollView>
                     <PopupDialog
                     width={api.getRealDeviceWidth()*0.9}
                     height={250}
                     closeOnHardwareBackPress={false}
        dialogTitle={<DialogTitle title={i18.t('titleMsgLogin')} />}
    ref={(popupDialog) => { this.popupDialog = popupDialog; }}
    dialogAnimation = { new SlideAnimation({ slideFrom: 'bottom' }) }
    style={{ backgroundColor:'red' }}
  >
    <Text style={{ color:'#aaa',margin:10,textAlign:'center' }}>{this.state.title}</Text>
      <InputGroup><Input placeholder={i18.t('inputPlaceholderSDT')}   keyboardType='phone-pad' 
      onChangeText={t=>phone = t}
      defaultValue={phone}
      placeholderTextColor='#cdcdcd'
      /></InputGroup>
    <View style={{ flexDirection:'row',marginTop:10,padding:10 }}>
        <Button block style={{ flex:1,backgroundColor:'#cdcdcd' }}  onPress={()=>this.popupDialog.closeDialog()}> {i18.t('Cancel')}</Button>
        <Button block style={{ flex:1,marginLeft:10,backgroundColor:'#fa6428' }} onPress={()=>{
            this.popupDialog.closeDialog()
            this._checkLogin()}}>{i18.t('continue')}</Button>
    </View>
  </PopupDialog>
                    {/*// otp POP up */}
                                         <PopupDialog
                                         width={api.getRealDeviceWidth()*0.9}
                                         height={230}
                                         closeOnHardwareBackPress={false}
        dialogTitle={<DialogTitle title={i18.t('otpTitle')} />}
    ref={(DialogOTP) => { this.DialogOTP = DialogOTP; }}
    dialogAnimation = { new SlideAnimation({ slideFrom: 'bottom' }) }
  >
        <View style={{ paddingBottom:20 }}>
            <Text style={{ textAlign:'center' }}>{i18.t('contentLeftInputLogin')}     {this.state.phone}, {i18.t('contentRightInputLogin')} </Text>
      <InputGroup><Input placeholder={i18.t('placeholderInputLogin')}  keyboardType='phone-pad'
      placeholderTextColor='#cdcdcd'
       onChangeText={t=>OTP = t}/></InputGroup>
    <View style={{ flexDirection:'row',marginTop:10,padding:10 }}>
        <Button block style={{ flex:1,backgroundColor:'#cdcdcd' }} onPress={()=>this.DialogOTP.closeDialog()}> {i18.t('Cancel')}</Button>
        <Button block style={{ flex:1,marginLeft:10,backgroundColor:'#fa6428' }} onPress={this._onconfirmOTP.bind(this)}>{i18.t('continue')}</Button>
    </View>
        </View>
  </PopupDialog>

                      {/*// otp reg POP up */}
                                         <PopupDialog
                                         width={api.getRealDeviceWidth()*0.9}
                                         height={230}
                                         closeOnHardwareBackPress={false}
        dialogTitle={<DialogTitle title={i18.t('otpTitle')} />}
    ref={(regDialogOTP) => { this.regDialogOTP = regDialogOTP; }}
    dialogAnimation = { new SlideAnimation({ slideFrom: 'bottom' }) }
  >
        <View>
            <Text style={{ textAlign:'center' }}>{i18.t('contentLeftInputLogin')}    {this.state.phone}, {i18.t('contentRightInputLogin')} </Text>
      <InputGroup><Input placeholder={i18.t('placeholderInputLogin')}  keyboardType='phone-pad'
      
       onChangeText={t=>OTP = t}/></InputGroup>
    <View style={{ flexDirection:'row',marginTop:10,padding:10 }}>
        <Button block style={{ flex:1,backgroundColor:'#cdcdcd' }} onPress={()=>this.regDialogOTP.closeDialog()}> {i18.t('Cancel')}</Button>
        <Button block style={{ flex:1,marginLeft:10,backgroundColor:'#fa6428' }} onPress={this._onconfirmRegOTP.bind(this)}>{i18.t('continue')}</Button>
    </View>
        </View>
  </PopupDialog>
                {/*<View style={{ bottom:10,position:'absolute',width:api.getRealDeviceWidth() }}>
                    <Button
                        style={{ alignSelf:'center',borderRadius:20,paddingHorizontal:20,paddingVertical:5,backgroundColor:'#fa6428' }}
                        onPress={this._checkCamPer}
                    >
                        <Text style={{ textAlignVertical:'center',textAlign:'center',fontSize:18,color:'#fff',fontWeight:'300' }}>Quét mã thẻ</Text>
                    </Button>
                </View>*/}
                    </Image>
            </Container>
        );
    }
}
const mapState = (state) => {
    return {navState: state.navState,userState: state.userState}
}
const mapDispatch = (dispatch) => {
    return {
        setUserInfo: (data) => dispatch(setUserInfo(data)),
    }
}
export default connect(mapState, mapDispatch)(AddCardFromLogin);