import MapView, { PROVIDER_GOOGLE } from 'react-native-maps';
import {
    StyleSheet,
    View,
    ActivityIndicator,
    Text,
    ScrollView, Image, Platform
} from 'react-native';
import React, { Component } from 'react';
import dataService from '../../api/dataService';
import api from '../../api';
import { connect } from 'react-redux';
import cache from '../../api/cache'
import i18 from "../i18"
import supercluster from 'supercluster'
import Marker from './marker'
import ClusterMarker from './ClusterMaker';
import { getRegion, getCluster } from './MapUtils';
const styles = StyleSheet.create({
    map: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 56,
        height: Platform.OS == 'ios' ? api.getRealDeviceHeight() - 118 : null,
        width: api.getRealDeviceWidth()
    }
});

let location = api.getLocationData() ? api.getLocationData() : [] ;
class Location extends Component {
    itv = null
    constructor(props) {
        super(props);
        this.state = {
            currentLocation: {
                latitude: 21.023999904,
                longitude: 105.851496594,
                latitudeDelta: 0.0022,
                longitudeDelta: 0.0021,
            },
            isLoading: true,
            isLoadingShopLocation: true,
            userLocation: {
                latitude: 21.023999904,
                longitude: 105.851496594,
                latitudeDelta: 0.0022,
                longitudeDelta: 0.0021,
            },
            show: true,
            coords: [],
            location: [],
            false: false
        }
    }
    componentWillUnmount() {
        clearInterval(itv)
        location = []
    }
    componentWillMount() {
        itv = setInterval(() => {
            if (location != undefined && location.length > 0) {
                // this.setState({isLoading:false,location:location})
                clearInterval(itv)
                this.setState({ isLoadingShopLocation: false })
                // alert(this.state.isLoading)
            }
            else {
                location = api.getLocationData() ? api.getLocationData() : [] ;
                // this.setState({isLoadingShopLocation:true})
            }
        }, 350)
        setTimeout(() => {
            if (this.state.isLoadingShopLocation) this.setState({ false: true })
        }, 50000)
        let position = cache.get('userLocation');
        if (position) {
            this.setState({
                userLocation: {
                    ...this.state.userLocation,
                    latitude: position.latitude,
                    longitude: position.longitude
                },
                currentLocation: {
                    ...this.state.userLocation,
                    latitude: position.latitude,
                    longitude: position.longitude
                },
                false: false, isLoading: false
            })
        } else
            navigator.geolocation.getCurrentPosition((position) => {
                cache.set('userLocation', position.coords);
                this.setState({
                    userLocation: {
                        ...this.state.userLocation,
                        latitude: position.coords.latitude,
                        longitude: position.coords.longitude
                    },
                    currentLocation: {
                        ...this.state.userLocation,
                        latitude: position.coords.latitude,
                        longitude: position.coords.longitude
                    },
                    false: false, isLoading: false
                })
                //console.log('userLocation', this.state.userLocation)
            }, (error) => { api.showMessage(i18.t('errGPS'), i18.t('titleMsgLogin')); this.setState({ false: true }) }, {
                    enableHighAccuracy: false,
                    timeout: 15000,
                    maximumAge: 1000
                });
        setTimeout(() => {
            if (location == undefined) {
                this.setState({ isLoading: false, location: [], false: true })
                clearInterval(itv)
            }
        }, 2000)
    }

    renderMarker(marker, _index) {
        const key = _index + marker.geometry.coordinates[0];
        if (marker.properties) {
            return (
                <MapView.Marker
                    key={`${key}${marker.properties.cluster_id}`}
                    coordinate={{ latitude: marker.geometry.coordinates[1], longitude: marker.geometry.coordinates[0] }}
                    onPress={() => { this.handleClusterMarkerPress(marker, marker.properties.point_count); }}
                >
                    <ClusterMarker count={marker.properties.point_count} />
                </MapView.Marker>
            )
        }
        else {
            return (
                <Marker
                    key={_index}
                    marker={marker.geometry.marker}
                    selectLocation={this._selectLocation.bind(this)}
                />
            )
        }
    }
    // _renderMarker(arr) {
    //     if (Array.isArray(arr)) return arr.map((marker, i) => {
    //         if (marker.latitude != null && marker.longitude != null && marker.latitude > this.state.currentLocation.latitude - this.state.currentLocation.latitudeDelta / 2 && marker.latitude < this.state.currentLocation.latitude + this.state.currentLocation.latitudeDelta / 2
    //             && marker.longitude > this.state.currentLocation.longitude - this.state.currentLocation.longitudeDelta / 2
    //             && marker.longitude < this.state.currentLocation.longitude + this.state.currentLocation.longitudeDelta / 2
    //         ) return (
    //             <Marker
    //                 key={i}
    //                 marker={marker}
    //                 selectLocation={this._selectLocation.bind(this)}
    //             />
    //         )
    //     })
    // }
    // calculate point cluster

    handleClusterMarkerPress(marker, count) {
        // alert(count)
        const region = getRegion(
            marker.geometry.coordinates[1],
            marker.geometry.coordinates[0],
            parseInt(count) < 5 ? 0.009 : this.state.currentLocation.latitudeDelta * 0.5,
            parseInt(count) < 5 ? 0.0085 : this.state.currentLocation.longitudeDelta * 0.5
        );
        this.refs.map.animateToRegion(region, 200)
        // this.setState({ currentLocation: region });
    }
    _selectLocation(destination) {
        var mode = 'driving'; // 'walking';
        var origin = this.state.userLocation.latitude + ',' + this.state.userLocation.longitude;
        // var destination = this.props.locationState.markers[i].latitude + ',' +
        //     this.props.locationState.markers[i].longitude;
        //   var destination = location[i].latitude + ',' +
        //     location[i].longitude;
        dataService
            .getRoutesDirection(origin, destination, mode)
            .then((responseJson) => {
                if (responseJson.routes.length) {
                    this.setState({
                        coords: api.decode(responseJson.routes[0].overview_polyline.points)
                    });
                }
            })
    }
    onRegionChange(region) {
        this.setState({ currentLocation: region })
    }

    render() {
        //alert(location.length)
        //console.log('Marker', this.props.locationState.markers);
        var poli = null;
        poli = (<MapView.Polyline
            coordinates={[...this.state.coords]}
            strokeWidth={5}
            strokeColor='#00b3fd' />);
        // if (this.state.false) {
        //     return (
        //         <Image
        //             source={require('../../img/bg_login.jpg')}
        //             style={{ width: api.getRealDeviceWidth(), height: api.getRealDeviceHeight() - 60 }}
        //         >
        //             <Text style={{ width: api.getRealDeviceWidth(), color: '#fff', textAlign: 'center', marginTop: 10, backgroundColor: 'transparent' }}>{i18.t('NoneGPS')}</Text>
        //         </Image>);

        // }
        // else
            // if (this.state.isLoading || this.state.isLoadingShopLocation) {
            //     return (
            //         <Image
            //             source={require('../../img/bg_login.jpg')}
            //             style={{ width: api.getRealDeviceWidth(), height: api.getRealDeviceHeight() - 60 }}
            //         >
            //             <ActivityIndicator size='large' />
            //         </Image>
            //     );
            // } 
            // else
            {
                const places = location.map(place => {
                    return {
                        geometry: {
                            coordinates: [
                                place.longitude,
                                place.latitude,
                            ],
                            marker: place
                        }
                    }
                });
                const cluster = getCluster(places, this.state.currentLocation);
                let unit = parseInt(location.length / 4)
                return (
                    <Image
                        source={require('../../img/bg_login.jpg')}
                        style={{ width: api.getRealDeviceWidth(), height: api.getRealDeviceHeight() - 60 }}
                    >
                        <MapView
                            //followsUserLocation={true}
                            provider={PROVIDER_GOOGLE}
                            //cacheEnabled={true}
                            onRegionChangeComplete={this.onRegionChange.bind(this)}
                            ref='map'
                            showsScale={true}
                            style={styles.map}
                            zoomEnabled={true}
                            showsUserLocation={true}
                            initialRegion={this.state.currentLocation}>
                            {
                                cluster.markers.map(this.renderMarker.bind(this))
                            }
                            <MapView.Marker.Animated
                                coordinate={new MapView.AnimatedRegion({
                                    latitude: this.state.userLocation.latitude,
                                    longitude: this.state.userLocation.longitude
                                })}
                                //image={imgBlue}
                                pinColor={'blue'}
                            />
                            {/*{
                                    this._renderMarker(location.slice(0, unit))
                                }
                                {
                                    this._renderMarker(location.slice(unit + 1, unit * 2))
                                }
                                {
                                    this._renderMarker(location.slice(unit * 2 + 1, unit * 3))
                                }
                                {
                                    this._renderMarker(location.slice(unit * 3 + 1, location.length - 1))
                                }*/}
                            {poli}
                        </MapView>
                        {this.state.isLoading || this.state.isLoadingShopLocation ? <Text style={{ position: 'absolute', zIndex: 1, bottom: 60, right: 10 }}>{i18.t('loading')}</Text>
                            : null}
                    </Image>
                );
            }

    }
}

const mapStateToProps = (state) => {
    return { locationState: state.locationState, userState: state.userState }
}
const mapDispatchToProps = (dispatch) => { return {} }
export default connect(mapStateToProps, mapDispatchToProps)(Location);