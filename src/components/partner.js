import React, {Component, StyleSheet} from 'react'
import {
    Container,
    List,
    ListItem,
    Header,
    Button,
    Icon,
    Title,
    Content,
    Card,
    Col,
    Row
} from 'native-base'
import i18 from './i18'
import {connect} from 'react-redux'
import {Image, Text, ActivityIndicator,View,Platform,PermissionsAndroid,ScrollView} from 'react-native'
import {push, pop} from '../actions/navActions'
import {showInputBox} from '../actions/uiActions'
import dataService from '../api/dataService'
import api from '../api'
import {Analytics, Hits} from 'react-native-google-analytics';
class Partner extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: []
        }
        setTimeout(()=>{
            dataService
            .getPartnerInfo(this.props.userState.token, this.props.navState.routes[this.props.navState.index].partnerId)
            .then((data) => {
                this.setState({data: data.data})
            })
        },400)
    }
    _checkPer(key,gps,name,address){
        if (Platform.OS === 'android' && Platform.Version >= 23)
        PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION).then((result) => {
                if (result) {
                  console.log("Permission is OK");
                  api.push({key: 'shopLocation',
                                    gps:gps,
                                    name: name,
                                    address: address})
                } else {
                  PermissionsAndroid.requestPermission(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION).then((result) => {
                    if (result) {
                      console.log("User accept");
                      api.push({key: 'shopLocation',
                                    gps:gps,
                                    name: name,
                                    address: address})
                    } else {
                      console.log("User refuse");
                      api.showMessage(i18.t('errLocationPer'),i18.t('titleMsgLogin'))
                    }
                  });
                }
          });
          else 
          api.push({key: 'shopLocation',
                                    gps:gps,
                                    name: name,
                                    address: address})
}
    _renderShop(arr) {
        //console.log('arr', arr);
        if (arr !== undefined) 
            if (arr.length > 0) 
                return (
                    <View style={{ backgroundColor:'#fff',borderRadius:2,marginTop:10 }}>
                        <Text
                            style={{
                            fontWeight: 'bold',
                            padding: 10,
                            borderBottomWidth: 1,
                            borderColor: '#cacaca'
                        }}>
                            <Icon name="ios-pin" style={{ color:'#808080' }}/>
                            {' '}{i18.t('listShop')}</Text>
                        <List
                            dataArray={arr}
                            renderRow={(item, i) => <ListItem
                            key={i}
                            onPress={() => {
                           this._checkPer('shopLocation',item.latitude + ',' + item.longitude,item.name,item.address)
                        }}>
                            <Col>
                                <Text
                                    style={{
                                    fontWeight: 'bold',
                                    marginTop: -2
                                }}><Icon
                                    name='ios-pin-outline'
                                    style={{
                            fontSize: 14
                        }}/> {item.name}</Text>
                                <Text>{item.address}</Text>
                            </Col>
                        </ListItem>}></List>
                    </View>
                )
    }
    render() {
        if (this.props.userState.uuid !== undefined) {
            ga = new Analytics('UA-90479808-1', this.props.userState.uuid, 1, 'userAgent');
            var screenView = new Hits.ScreenView('Mpoint', 'Partner')
            ga.send(screenView);
        }
        if (this.state.data.length <1)
        return (
            <Container>
                <Header
                    style={{
                    backgroundColor: '#009688'
                }}>
                    <Button transparent onPress={this.props.pop}>
                        <Icon name='ios-arrow-back' style={{ color:Platform.OS == 'ios'?'#000':'#fff' }}/>
                    </Button>
                    <Title  style={{ alignSelf:'center' } }>{i18.t('')}</Title>
                </Header>

                <Content>
                    <ActivityIndicator size='large'/>
                </Content>
            </Container>
        )
        else
            return (
                <Container>
                    <Header
                        style={{
                        backgroundColor: '#009688'
                    }}>
                        <Button transparent onPress={this.props.pop}>
                            <Icon name='ios-arrow-back'  style={{ color:Platform.OS == 'ios'?'#000':'#fff' }}/>
                        </Button>
                        <Title style={{ alignSelf:'center' } }>{i18.t('partnerInfomation')}</Title>
                    </Header>

                    <Image 
                        source={ require('../img/bg_login.jpg') }
                        style={{
                        padding: 5,
                        width:api.getRealDeviceWidth(),
                        height:api.getRealDeviceHeight()-60
                    }}>
                        <Content>
                            <View
                            style={{
                            flexDirection: 'row',
                            padding: 5,
                            borderRadius: 3,
                            backgroundColor:'#fff'
                        }}>
                            <Image
                                source={{
                                uri: this.state.data.logo
                            }}
                                style={{
                                width: api.getRealDeviceWidth() / 5,
                                height: api.getRealDeviceWidth() / 5
                            }}/>
                            <Col
                                style={{
                                flexDirection: 'column',
                                padding: 5
                            }}>
                                <Text
                                    style={{
                                    color: 'orange',
                                    fontWeight: 'bold',
                                    fontSize: 15
                                }}>{this.state.data.name}</Text>
                                <Text
                                    style={{
                                    fontSize: 13
                                }}>{this.state.data.slogan}</Text>
                            </Col>
                        </View>
                        {this.state.data.detail != undefined && this.state.data.detail !='' && this.state.data.detail != null? (<Card
                            style={{
                            padding: 5,
                            borderRadius: 3
                        }}>
                           <Text
                                style={{
                                fontSize: 13
                            }}>
                                {this.state.data.detail}
                            </Text>
                        </Card>
                        ):null}
                        <View
                            style={{
                            padding: 5,
                            borderRadius: 3,
                            backgroundColor:'#fff'
                        }}>
                            <Text
                                style={{
                                color: 'orange',
                                fontWeight: 'bold',
                                fontSize: 14,
                                marginBottom: 5
                            }}>{i18.t('promtionGift')}</Text>

                            <List
                                dataArray={this.state.data.Promotions}
                                renderRow={(item, i) => <Text
                                key={i}
                                style={{
                                fontSize: 13
                            }}>
                                {item.name}
                            </Text>}></List>

                            <Text
                                style={{
                                fontSize: 13
                            }}>
                                {this.state.data.name}
                            </Text>
                        </View>

                        {this._renderShop(this.state.data.shops)}
                        </Content>
                    </Image>
                </Container>
            )
    }
}

mapStateToProps = (state) => ({navState: state.navState, userState: state.userState})

mapDispatchToProps = (dispatch) => ({
    pop: () => dispatch(pop()),
    showInputBox: () => dispatch(showInputBox('1232', 123123, () => {}, () => {})),
    push: (route) => dispatch(push(route))
    // push: (route) => dispatch(push(route))
})
export default connect(mapStateToProps, mapDispatchToProps)(Partner)