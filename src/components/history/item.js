import React, {Component} from 'react';
import {View,ScrollView,Image,Platform,Text,Picker} from 'react-native';
import {IndicatorViewPager, PagerTitleIndicator} from 'rn-viewpager';
import {Container,Header,Icon,Button} from 'native-base'
import api from '../../api';
import i18 from '../../components/i18'

export default class History extends Component {
    constructor(props){
        super(props);
        this.state={
            selected:'1'
        };
    }

    // function to change selected header
    _selectChange(str){
        this.setState({
            selected:str
        });
    }
    
    // function to render Body
    _renderBody(){
        
    }
    renderTopRight(){
        if(this.props.point!=undefined){
            return  <Text style={{ color:'#000',fontWeight:'bold',paddingTop:5 }}>
                        {this.props.point} {i18.t('point')}
                    </Text>
        }
        if(this.props.type!='used'){
            return  <Button block style={{ backgroundColor:'#f16219',marginRight:5,marginTop:3 }}
                        onPress={()=>{
                            api.showQR(i18.t('titleQRvetify'),this.props.code,this.props.gift,this.props.promotionId,this.props.functionRefresh,this.props.promotionTypeId);
                        }}
                    >
                     <Text style={{ color:'#fff' }}>{i18.t('vertify')}</Text>
                    </Button>
        }
        return <Text style={{ fontWeight:'bold',color:'#000',paddingBottom:5,paddingTop:5 }}>
                       {i18.t('vertifier')}: {this.props.manager}
                    </Text>
    }
     _lessThanTen(t) {
        return t < 10? '0' + t: t
    }
    processhour(number){
        t = number.toString();
        return t.substr(t.length -1,2);
    }
    _DateTime(t) {
        return api.getTime(t,'HH:mm DD/MM/YYYY');
        // let d = new Date(t)
        // return (
        //    + ' ' + (this.processhour(d.getHours())) + ':' + (this._lessThanTen(d.getMinutes())) +' ' + (this._lessThanTen(d.getDate()))+'-'+ (this._lessThanTen(d.getMonth() + 1)) + '-'+  d.getFullYear()
        // )
    }

    render() {
        return (
            <View style={{backgroundColor:this.props.id %2 != 0?'#f7f7f7':'#fff' }}>
                <View style={{  width:api.getRealDeviceWidth(),flexDirection:'row' }}>
                    <View style={{ width:30 }}>
                    <Text style={{ textAlign:'center',color:'#000',marginVertical:5 }}>{this.props.id}</Text>
                    </View>
                    <View style={{ flex:5 }}>
                    <Text style={{ color:'#000',marginVertical:5,paddingRight:10,fontSize:14 }} >
                       {this.props.shop? <Text style={{ fontWeight:'bold',color:'#000' }}>
                            {this.props.shop+': '}{'\n'}
                        </Text>:null}
                        {this.props.des}
                    </Text>
                    </View>
                <View style={{ flex:3 }}>
                    {this.renderTopRight()}
                </View>
                </View>
                <View  style={{  width:api.getRealDeviceWidth(),flexDirection:'row' }}>
                    <View style={{ width:30 }}>
                    </View>
                    <View style={{ flex:5 }}>
                    <Text style={{ fontWeight:'bold',color:'#000',paddingBottom:5 }}>
                        {i18.t('code')} {this.props.type=='used'?i18.t('vertifySlow'):i18.t('promotionSlow')}: {(this.props.code)}
                    </Text>
                    <Image 
                        style={{ height:30,width:30,resizeMode:'stretch' }}
                        source={{ uri:this.props.img  }}/>
                </View>
                <View style={{ flex:3 }}>
                    <Text style={{ fontSize:13,color:'#000' }}>
                        {i18.t('time')+'\n'}
                        {this._DateTime(this.props.day)}
                    </Text>
                </View>
                </View>
            </View>
        )
    }
}
