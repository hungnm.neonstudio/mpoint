import React, {Component} from 'react';
import {View,ScrollView,Image,Platform,Text,Picker,ListView,ActivityIndicator} from 'react-native';
import {IndicatorViewPager, PagerTitleIndicator} from 'rn-viewpager';
import {Container,Header,Icon,Button} from 'native-base'
import api from '../../api';
import dataService from '../../api/dataService';
import { connect } from 'react-redux';
import Item from './item'
import i18 from '../../components/i18'

class History extends Component {
    ds = new ListView.DataSource({
            rowHasChanged: (r1, r2) => r1 !== r2
        });
    numload = 10
    constructor(props) {
        super(props);
        this.state={
            itemReceived:[],
            canLoadMoreRecieve:true,
            loadingRecieve:true,
        }
       
    }
    componentDidMount(){
        this.loadMoreGift();
    }
    // function to load more history received 
    loadMoreGift(){
       if(this.state.canLoadMoreRecieve) dataService.getExchangePointHistory(this.props.token,this.state.itemReceived.length,this.numload)
        .then(rs=>{
            if(rs.err == 0) this.setState({
                itemReceived:this.state.itemReceived.concat (rs.pointhistories),
                canLoadMoreRecieve:rs.pointhistories.length<this.numload?false:true,
                loadingRecieve:false
            });
        })
    }
    render() {
        var id = 1
         return(
             <Image
                  source={ require('../../img/bg_login.jpg') }
                  style= {{ width:api.getRealDeviceWidth(),height:api.getRealDeviceHeight()-56 }}
                  >
             <ListView
                enableEmptySections={true}
                onEndReached={()=>{ if(this.state.itemReceived.length>0) this.loadMoreGift(); }}
                renderHeader={()=>{
                    if(this.state.itemReceived.length==0 && !this.state.loadingRecieve)
                        return <View style={{ width:api.getRealDeviceWidth(),backgroundColor:'transparent' }}><Text style={{ textAlign:'center',color:'#fff',backgroundColor:'transparent' }}>{i18.t('noneData')}</Text></View>
                }}
                dataSource = {this.ds.cloneWithRows(this.state.itemReceived)}
                renderRow={(item)=>(
                    <Item
                        id = {id++}
                        point={item.point}
                        shop={item.Add?item.Add.name:''}
                        des = {item.description}
                        code={item.code}
                        type='recieve'
                        day={item.createdAt}
                        promotionId={item.id}
                        img={item.Add?item.Add.logo:null}
                    />
                )}
                renderFooter={()=>{
                    if(this.state.loadingRecieve) return <ActivityIndicator size='large'/>
                }}
            />
            </Image>
         )
    }
}
function mapStateToProp(state) {
    return { token: state.userState.token ,navState:state.navState,giftCount:state.giftCountState.GiftCount};
}
function mapDispatchToProp(dispatch) {
    return{};
}
export default connect(mapStateToProp, mapDispatchToProp)(History);