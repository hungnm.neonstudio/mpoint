import React, {Component} from 'react';
import {View,ScrollView,Image,Platform,Text,Picker as PickerAndroid} from 'react-native';
import {IndicatorViewPager, PagerTitleIndicator} from 'rn-viewpager';
import {Container,Header,Icon,Button,Picker as PickerIOS} from 'native-base'
import api from '../../api';
import i18 from '../i18'
import Gift from './gift';
import Point from './point';
import Promotion from './promotion';
let Picker = null
export default class History extends Component {
    constructor(props){
        super(props);
        this.state={
            selected:i18.t('promotionHis')
        };
    }
    componentWillMount(){
        Picker = Platform.OS != 'ios' ?PickerAndroid:PickerIOS;
    }
    // function to change selected header
    _selectChange(str){
        this.setState({
            selected:str
        });
    }
    
    // function to render Body
    _renderBody(){
        if(this.state.selected ==i18.t('promotionHis') ){
            return <Promotion/>
        }else if(this.state.selected ==i18.t('exchangeHis') ){
            return <Point/>
        }else if(this.state.selected ==i18.t('giftHis') ){
            return <Gift/>
        }
    }
    render() {
        return (
            <Container>
                    <View style={{ flexDirection:'row',padding:0,backgroundColor:'#009688',paddingTop:Platform.OS=='ios'?20:2 }}>
                        <Button transparent onPress = {()=>{ api.resetRoute({key:'home'}) }}>
                            <Icon name='ios-arrow-back'  style={{ color:Platform.OS == 'ios'?'#000':'#fff',marginTop:5,padding:10 }}/>
                        </Button>
                        <Text style={{ position:'absolute',marginTop:Platform.OS=='ios'?30:15,left:this.state.selected.length*10+24,zIndex:1000,fontSize:18,color:Platform.OS== 'ios'?'#000':'#fff'}}><Text style={{ fontSize:14,marginTop:2 }}>▼</Text></Text>
                        <Picker
                            mode="dropdown"
                            style={{ backgroundColor:'#009688',color:'#fff',flex:8 }}
                            selectedValue={this.state.selected}
                            onValueChange={this._selectChange.bind(this)}>
                        <Picker.Item label = {i18.t('promotionHis')}   value={i18.t('promotionHis')}/>
                        <Picker.Item label = {i18.t('exchangeHis')} value={i18.t('exchangeHis')} />
                        <Picker.Item label = {i18.t('giftHis')} value={i18.t('giftHis')}/>
                        </Picker>
                        <Button transparent onPress = {()=>{  }}>
                            <Icon name='ios-search'  style={{ color:Platform.OS == 'ios'?'#000':'#fff',marginTop:5,padding:10 }}/>
                        </Button>
                </View>
                <View>
                    {this._renderBody()}
                </View>
            </Container>
        )
    }
}
