import {
    Container,
    Icon,
    Content,
    Button,
    Header,
    Title
} from 'native-base';
import React, {Component} from 'react';
import {
    ScrollView,
    View,
    Image,
    Text,
    ListView,
    ActivityIndicator,
    TouchableOpacity,Platform
} from 'react-native';
import i18 from './i18'
import {pop, push} from '../actions/navActions';
import {connect} from 'react-redux';
import dataService from '../api/dataService';
import api from '../api';
var item_num_load = 25;
class ListPartners extends Component {
    constructor(props) {
        super(props);
        this.state = {
            items: [],
            start: 0,
            isLoading: true,
            canLoadMore: true
        }
    }
    componentWillMount() {
        dataService
            .getListPartners(this.props.userState.token, this.state.start, item_num_load)
            .then((data) => {
                //console.log('data', data);
                if (data.partners.length < item_num_load) {
                    this.setState({canLoadMore: false})
                }
                this.setState({
                    items: data.partners,
                    isLoading: false,
                    start: this.state.start + item_num_load
                })
            })
    }
    _loadMoreData() {
        if (this.state.canLoadMore) {
            //console.log('Load more');
            this.setState({
                start: this.state.start + item_num_load
            });
            dataService
                .getListPartners(this.props.userState.token, this.state.start, item_num_load)
                .then((data) => {
                    //console.log('data', data);
                    if (data.partners.length < item_num_load) {
                        this.setState({canLoadMore: false})
                    }
                    this.setState({
                        items: this
                            .state
                            .items
                            .concat(data.partners)
                    })
                    console.log("New data: ", this.state.items);
                })
        }
    }
    render() {
        const ds = new ListView.DataSource({
            rowHasChanged: (r1, r2) => r1 !== r2
        });
        if (this.state.isLoading) {
            return (
                <Container>
                    <Header
                        style={{
                        backgroundColor: '#009688'
                    }}>
                        <Title style={{ alignSelf:'center' } }>{i18.t('partner')}</Title>
                        <Button transparent onPress={() => this.props.pop()}>
                            <Icon name='ios-arrow-back'  style={{ color:Platform.OS == 'ios'?'#000':'#fff' }}/>
                        </Button>
                    </Header>
                    <View>
                        <ActivityIndicator size='large'/>
                    </View>
                </Container>
            );
        } else {
            return (
                <Container>
                    <Header
                        style={{
                        backgroundColor: '#009688'
                    }}>
                        <Title style={{ alignSelf:'center' } }>{i18.t('partner')}</Title>
                        <Button transparent onPress={() => this.props.pop()}>
                            <Icon name='ios-arrow-back'  style={{ color:Platform.OS == 'ios'?'#000':'#fff' }}/>
                        </Button>
                    </Header>
                    <Image
                        source={ require('../img/bg_login.jpg') }
                        style= {{ width:api.getRealDeviceWidth(),height:api.getRealDeviceHeight()-60 }}
                    >
                        <ListView
                            dataSource={ds.cloneWithRows(this.state.items)}
                            contentContainerStyle={{
                            flexDirection: 'row',
                            flexWrap: 'wrap',
                            alignItems: 'stretch'
                        }}
                            renderRow={(item, i) => {
                            return (<PartnerItem
                                uri={item.logo}
                                name={item.name}
                                onClick={() => this.props.push({key: 'partner', partnerId: item.id})}/>);
                        }}
                            renderFooter={() => {
                            if (this.state.canLoadMore) {
                                return (<ActivityIndicator size='large'/>);
                            }
                        }}
                        onEndReached={()=>{this._loadMoreData()}}/>
                    </Image>
                </Container>
            );
        }
    }
}

class PartnerItem extends Component {
    render() {
        var name = this.props.name;
        if (name.length > 12) {
            name = name.substring(0,api.getRealDeviceWidth()>320?12:10) + '...';
        }
        return (

            <TouchableOpacity
                style={{
                alignSelf: 'center'
            }}
                onPress={this.props.onClick}>
                <Image
                    source={{
                    uri: this.props.uri
                }}
                    style={{
                    width: (api.getRealDeviceWidth() - 30) / 3,
                    height: (api.getRealDeviceWidth()-30) / 3,
                    resizeMode: 'stretch',
                    marginRight: 5,
                    marginTop: 5,
                    marginLeft: 5
                }}/>
                <Text
                    style={{
                    marginRight: 5,
                    marginBottom: 5,
                    marginLeft: 5,
                    backgroundColor: '#253238',
                    textAlign: 'center',
                    padding: 5,
                    width: (api.getRealDeviceWidth() - 30) / 3,
                    color: '#fff',
                    fontSize: 12
                }}>{name}</Text>
            </TouchableOpacity>
        );
    }
}
const mapState = (state) => {
    return {userState: state.userState}
}
const mapDispatch = (dispatch) => {
    return {
        pop: () => dispatch(pop()),
        push: (route) => dispatch(push(route))
    }
}
export default connect(mapState, mapDispatch)(ListPartners)