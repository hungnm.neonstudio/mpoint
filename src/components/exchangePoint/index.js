import React from 'react';
import { View,Text,Image,Platform,TextInput } from 'react-native';
import { Container,Header,Content,Button,Icon,Title } from 'native-base';
import Ionicons from 'react-native-vector-icons/Ionicons';
import api from '../../api';
import { connect } from 'react-redux';
import SelectCard from './selectCard';
import dataService from '../../api/dataService';
import i18 from '../i18';
class exchangePoint extends React.PureComponent {
    state = {
        availablePointSource:'',
        availablePointDes:'',
        source : {},
        des:{},
        valueSource:0,
        valueDes:0,
        rate:1,
        limit:[]
    }
    
    _changeSource=(val)=>{
        if(isNaN(val))return;
        if(val > parseInt(this.state.availablePointSource)) return api.showMessage(i18.t('notEnoughPoint'));
        this.setState({
            valueSource:val,
            valueDes:val*this.state.rate
        });
    }
    _changeDes=(val)=>{
        if(isNaN(val))return;
        if(val/this.state.rate > parseInt(this.state.availablePointSource)) return api.showMessage(i18.t('notEnoughPoint'));
        
        this.setState({
            valueSource:val/this.state.rate,
            valueDes:val
        })
    }
    _change=()=>{
        this.setState({
            source : this.state.des,
            des:this.state.source,
            valueSource:this.state.valueDes,
            valueDes:this.state.valueSource,
            rate:1/this.state.rate
        })
    }
    
    exchangePoint = ()=>{
        let { valueDes,valueSource,source,des } = this.state;
        if( !(valueDes && valueSource) ){
            return api.showMessage(i18.t('invalidPoint'));
        }
        if( (valueSource - parseInt(valueSource ) > 0) ){
            return api.showMessage(i18.t('sourcePointMustBeNumber'));
        }
        if( (valueDes - parseInt(valueDes ) > 0) ){
            return api.showMessage(i18.t('desPointMustBeNumber'));
        }
        api.showLoading();
        dataService.exchangePoint(source.id,des.id,valueSource)
        .then(rs=>{
            api.hideLoading();
            if(rs.err == 0){
                api.resetRoute({ key:'home' });
                dataService.getUserInfo()
                .then(kq=>{
                    api.setUserInfo(kq);
                })
            }
            api.showMessage(rs.msg)
        })
    }
    render(){
        let isEnough = this.state.des.shortName && this.state.source.shortName;
        let s = 1;
        let d = this.state.rate;
        while (d - parseInt(d) > 0) {
            s *= 10;
            d = s*this.state.rate;
        };
        let availablePointSource = this.state.availablePointSource ?  `Số điểm trong thẻ :\n${this.state.availablePointSource} ${i18.t('point')}` : '';
        let availablePointDes = this.state.availablePointDes ? `Số điểm trong thẻ :\n${this.state.availablePointDes} ${i18.t('point')}` : '';

        return(
            <Container>
                <Header
                    style={{
                        backgroundColor: '#009688'
                    }}>
                    <Button
                        transparent
                        onPress={() => {
                            api.pop()
                        }}>
                        <Icon name='ios-arrow-back' style={{ color: Platform.OS == 'ios' ? '#000' : '#fff' }} />
                    </Button>
                    <Title style={{ alignSelf:'center' } }>{(i18.t('exchangePoint')||'').toUpperCase()}</Title>
                </Header>

                <Image
                    source={require('../../img/bg_login.jpg')}
                    style={{ width: api.getRealDeviceWidth(), height: api.getRealDeviceHeight() - 60 }}
                ><Content
                    keyboardShouldPersistTaps={'handled'}   
                    style={{
                        width: api.getRealDeviceWidth()
                    }}>
                    <Text style={{ color:'#fff',alignSelf:'center',marginTop:10,fontSize:20,fontWeight:'400' }}>{i18.t('exchangePointWithCard')}</Text>
                    
                    <View style={{ flexDirection:'row',marginTop:10 }}>
                        <View style={{ flex:1,alignItems:'center' }}>
                            <SelectCard source payload = {this.state.source} changeCard = {(source)=>{ 
                                let nguon = source.exchangeRates[0];
                                let des = this.state.des.exchangeRates ? this.state.des.exchangeRates[0] : 1;
                                let rate = (nguon.point / nguon.money) / (des.point / des.money);
                                dataService.getPoint(source.id)
                                .then(rs=>{
                                    if(rs.err == 0){
                                        let limit = [];
                                        rs.info.map(el=>{
                                            if(el.id != this.state.source.id){
                                                limit.push(el.destinationId)
                                            }
                                        });
                                        console.log('====================================');
                                        console.log({ availablePointSource:rs.point+'',limit,des:{} });
                                        console.log('====================================');
                                        this.setState({ availablePointSource:rs.point+'',limit,des:{},valueDes:0,availablePointDes:0 });
                                    };
                                })
                                this.setState({
                                    source:source,
                                    rate:rate,
                                    valueSource:0,
                                    valueDes:0
                                })
                             }} />
                            <Text style={{ color:'#fff',fontSize:18,fontWeight:'300' }}>{this.state.source.shortName}</Text>
                            <Text style={{ color:'#fff',textAlign:'center' }}>{availablePointSource}</Text>
                        </View>
                        <Ionicons name='ios-arrow-round-forward-outline' onPress={this._change} style={{ color:'#fff',fontSize:50,fontWeight:'900',textAlignVertical:'center' }}/>
                        <View style={{ flex:1,alignItems:'center' }}>
                            <SelectCard des limit={this.state.limit} id = {this.state.source.id} payload = {this.state.des} changeCard = {(des)=>{ 
                                let source = this.state.source.exchangeRates ? this.state.source.exchangeRates[0] : 1;
                                let dich = des.exchangeRates[0];
                                let rate = (source.point / source.money) / (dich.point / dich.money);
                                dataService.getPoint(des.id)
                                .then(rs=>{
                                    if(rs.err == 0){
                                        this.setState({ availablePointDes:rs.point+'' });
                                    }
                                })
                                this.setState({
                                    des:des,
                                    rate:rate,
                                    valueSource:0,
                                    valueDes:0
                                })    
                             }} />
                            <Text style={{ color:'#fff',fontSize:18,fontWeight:'300' }}>{this.state.des.shortName}</Text>
                            <Text style={{ color:'#fff',textAlign:'center' }}>{availablePointDes}</Text>
                        </View>
                    </View>
                    {
                        isEnough ?  <Text style={{ marginTop:10,backgroundColor:'transparent',color:'#fff',alignSelf:'center',fontWeight:'400',fontSize:17 }}>Tỷ lệ đổi điểm</Text> : null

                    }
                    { isEnough ? <Text style={{ alignSelf:'center',color:'#fff' }}>{`${s} ${this.state.source.shortName} point = ${d} ${this.state.des.shortName}`}</Text> : null }
                    {/* <Text style={{ color:'#cdcdcd',backgroundColor:'transparent',marginLeft:10 }}>Hệ số {this.state.rate*100} %</Text> */}
                    {
                        isEnough ? <View style={{ flexDirection:'row',alignSelf:'center',marginTop:20 }}>
                             <View>
                                <TextInput defaultValue=''
                                style={{ borderRadius:2,textAlign:'center',color:"#000",backgroundColor:'#fff',padding:0,margin:0,width:api.getRealDeviceWidth()/2 - 20 }}
                                underlineColorAndroid='transparent'
                                onChangeText={this._changeSource}
                                keyboardType='phone-pad'   
                                value={this.state.valueSource+''}                         
                                />
                                <Text style={{ color:'#fff',backgroundColor:'transparent',alignSelf:'center' }}>{this.state.source.shortName}</Text>
                             </View>
                             <View style={{ marginLeft:35 }}>
                                <TextInput defaultValue=''
                                style={{borderRadius:2,textAlign:'center',color:"#000",backgroundColor:'#fff',padding:0,margin:0,width:api.getRealDeviceWidth()/2 - 20 }}
                                underlineColorAndroid='transparent'
                                keyboardType='phone-pad'
                                onChangeText={this._changeDes}
                                value={this.state.valueDes+''}
                                />
                                <Text style={{ color:'#fff',backgroundColor:'transparent',alignSelf:'center' }}>{this.state.des.shortName}</Text>
                             </View>
                    </View>
                    :null
                    }
                    
                    {
                        isEnough ? <Button 
                    style={{ marginTop:20,backgroundColor:'#fa6428',alignSelf:'center',marginBottom:20,paddingHorizontal:30 }}
                    onPress={this.exchangePoint}
                    >{(i18.t('exchangePoint')||'').toUpperCase()}</Button> 
                    :null
                    }
                </Content>
                </Image>
            </Container>
        )
    }
}
mapStateToProps=(state)=>{
    return {
        listMember:state.userState.listMember
    }
}
 export default connect (mapStateToProps,undefined) (exchangePoint);