import React from 'react';
import { View,Modal,TouchableOpacity,Image,Text ,ListView} from 'react-native';
import { connect } from 'react-redux';
import api from '../../api';
import i18 from '../i18';

class SelectCard extends React.Component {
    state = {
        show:false,
        active : {},
        card:this.props.card,
        limit :this.props.limit||[]
    }

    componentWillReceiveProps(nextProps) {
        if(nextProps.payload.id == this.state.active.id) return;
        console.log('====================================');
        console.log('nextProps',nextProps);
        console.log('====================================');
        this.setState({
            active:nextProps.payload,
            limit:nextProps.limit||[]
        });
    }
    renderListCard=(changCard)=>{
        let width = api.getRealDeviceWidth() * .6;  
        const ds = new ListView.DataSource({
            rowHasChanged: (r1, r2) => r1 !== r2
        });
        return ( 
            <ListView
            contentContainerStyle={{ paddingBottom:10 }}
            dataSource={ds.cloneWithRows(this.state.card)}
            renderRow={(item, sectionID, rowID) => {
                let value = <TouchableOpacity
                onPress={()=>{
                    this.props.changeCard(item)
                    this.setState({
                        active:item,
                        show:false
                    })
                }}
            >
                <Image
                    source={{ uri:item.card }}
                    style={{ width:width,height:width/1.5,resizeMode:'stretch',alignSelf:'center',marginTop:10 }}
                />
            </TouchableOpacity>
                if(this.state.limit.length){
                    let check  = false;
                    this.state.limit.forEach(el=>{
                        if(parseInt(el) == parseInt(item.id)){
                            return check = true;
                        }
                    });
                    if(check) return value;
                } else{
                    if(item.id == this.props.id || !(item.exchangeRates.length)  ) return null;
                    return value;
                } 
                return null;
               }
            }/>
        )
    }
    renderModal=()=>{
        return <Modal
                    visible={this.state.show}
                    transparent={true}
                    onRequestClose={()=>{}}
                >
                <View style={{ flex:1,backgroundColor:'rgba(0,0,0,0.52)' }}>
                    <View style={{ marginHorizontal:10,backgroundColor:'#fff',marginTop:20,borderRadius:3 }}>
                        <Text style={{ textAlign:'center',fontSize:18,fontWeight:'300',paddingVertical:10 }}>Chọn thẻ</Text>
                        <Text style={{ height:1,backgroundColor:'#cdcdcd' }}/>
                        <View style={{ maxHeight:api.getRealDeviceHeight()/2 }}>
                            {this.renderListCard()}
                        </View>
                        <TouchableOpacity
                            onPress={()=>{ this.setState({ show:false }) }}
                        style={{ backgroundColor:'#fa6428',paddingVertical:10,borderRadius:3,borderTopLeftRadius:0,borderTopRightRadius:0 }}>
                            <Text style={{ alignSelf:'center',color:'#fff' }}>
                                {i18.t('close')}
                            </Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </Modal>
    }
    render(){
        let width = (api.getRealDeviceWidth()/2)-30
        return (
            <TouchableOpacity
                activeOpacity={0.8}
                onPress={()=>{ this.setState({
                    show:true
                }) }}
                
            >
            {this.renderModal()}
                {
                    this.state.active.id ? <Image
                    source={{ uri:this.state.active.card }}
                    style={{ width:width,height:width/1.5,resizeMode:'stretch' }}
                /> : <View style={{ width:width,height:width/1.5,borderWidth:1,borderColor:'#cdcdcd',borderRadius:5,justifyContent:'center' }}>
                    <Text style={{ backgroundColor:'transparent',alignSelf:'center',color:'#fff' }}>{this.props.source ? i18.t('selectCardGive') : i18.t('selectCardRecieve')}</Text>
                </View>
                }
            </TouchableOpacity>
        )
    }
}
mapStateToProps= (state)=>(
    {
        card:state.userState.listMember
    }
)
export default connect(mapStateToProps)(SelectCard)