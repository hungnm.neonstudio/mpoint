import {
    Container,
    Header,
    Content,
    Button,
    Icon,
    Title,
    Card,
    Text,
    Grid,
    Input,
    InputGroup,
    Col,
    Row,
    CardItem,
    CheckBox
} from 'native-base'
import React, { Component } from 'react'
import {
    Image,
    View,
    Dimensions,
    Alert,
    BackAndroid,
    ToastAndroid,
    Platform,
    AsyncStorage,
    ListView
} from 'react-native'
import MyCardItem from './tab/cardItem'
import { connect } from 'react-redux'
import { push, pop } from '../actions/navActions'
// import {setShopLocation} from '../actions/locationAction';
import {
    showMessage,
    hideMessageBox,
    showInputBox,
    hideInputBox,
    showLoading,
    hideLoading
} from '../actions/uiActions'
import { setUserInfo, setUserToken, setUuid } from '../actions/userActions'
import request from '../api/request'
import api from '../api'
import dataService from '../api/dataService';
import { IndicatorViewPager, PagerDotIndicator } from 'rn-viewpager';
// import SplashScreen from 'react-native-splash-screen'
import { Analytics, Hits } from 'react-native-google-analytics';
//let markers = []
let otpID = '1'
let isLogin = true
export class Login extends Component {
    constructor(props) {
        super(props)
        this.state = {
            the: '',
            markers: [],
            members: []
        }
    }
    listMembers = []
    generateUUID() {
        var d = new Date().getTime();
        var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, (c) => {
            var r = (d + Math.random() * 16) % 16 | 0;
            d = Math.floor(d / 16);
            return (c == 'x'
                ? r
                : (r & 0x3 | 0x8)).toString(16);
        });
        //console.log('uuid', uuid);
        return uuid;
    }
    componentWillMount() {
        dataService.listAvaiableMembers('0').then(result => {
            console.log(result)
            this.listMembers = result.members
            this.setState({
                members: result.members
            })
        })
        try {
            AsyncStorage
                .getItem('@RFTK:key')
                .then((rftk) => {
                    if (rftk != null && rftk !== '' && rftk != undefined) {
                        // alert(rftk)
                        api.showLoading()
                        let loading = true
                        dataService.postRefreshToken(rftk)
                            .then(rs => {
                                api.setUserToken(rs.token)
                                // alert(rs.token)
                                dataService
                                    .getUserInfo(rs.token)
                                    .then((datas) => {
                                        console.log('datas', datas);
                                        if (datas.err === 0) {
                                            loading = false
                                            this
                                                .props
                                                .setUserInfo(datas)
                                            this._gotoPromotion()
                                        }
                                    })
                            })
                        setTimeout(() => {
                            if (loading) api.showMessage('Server không trả lời vui lòng kiểm tra mạng internet', 'Thông báo')
                            api.hideLoading()
                        }, 15020)
                        // alert(rftk)   
                    }
                });
        } catch (error) {
            console.log('Error when getting data', error);
        }
        try {
            AsyncStorage
                .getItem('@UUIDStore:key')
                .then((uuid) => {
                    if (uuid !== null) {
                        console.log(uuid);
                        this
                            .props
                            .setUuid(uuid);
                    } else {
                        try {
                            let newuuid = this.generateUUID()
                            AsyncStorage.setItem('@UUIDStore:key', newuuid);
                            this
                                .props
                                .setUuid(newuuid);
                        } catch (error) {
                            console.log('Error when saving data', error);
                        }
                    }
                });
        } catch (error) {
            console.log('Error when getting data', error);
        }
    }

    _gotoReg = () => {
        this
            .props
            .push({ key: 'reg' })
    }
    _gotoPromotion = () => {

        api.resetRoute({ key: 'home' })
        // if(!isLogin) api.hideLoading()
    }
    _onConfirmOTP(otp) {
        if (otp == undefined || otp == '') {
            api.showMessage('Vui lòng điền vào mã OTP')
            return
        }
        api.hideInputBox()
        setTimeout(() => api.showLoading(), 1)
        dataService.postVerifyOtpLogin(otpID, otp, this.props.userState.uuid)
            .then(rs => {
                // api.hideLoading()
                if (rs.err == 0) {
                    api.setUserToken(rs.token)
                    dataService.postAddDevice(this.props.userState.uuid, this.props.userState.uuid, this.props.userState.token)
                        .then(rs => {

                        })
                    dataService
                        .getUserInfo(rs.token)
                        .then((datas) => {
                            console.log('datas', datas);
                            if (datas.err === 0) {
                                try {
                                    AsyncStorage.setItem('@RFTK:key', rs.refreshToken);
                                    // alert(dt.refreshToken)
                                } catch (error) {
                                    console.log('Error when saving data', error);
                                }

                                this
                                    .props
                                    .setUserInfo(datas)
                                this._gotoPromotion()
                            }
                        })
                    //setTimeout(this._gotoPromotion(), 1000);

                } else {
                    api.showInputBox(undefined, undefined, rs.msg, this._onConfirmOTP.bind(this))
                    api.hideLoading()
                }
            })
    }
    _checkLogin = async () => {
        if (this.state.the != '' && this.state.the != undefined) {
            api.showLoading()
            dataService
                .getUserToken(this.state.the, this.props.userState.uuid)
                .then((dt) => {
                    isLogin = false
                    console.log("Data", dt);

                    if (dt.err == 0) {

                        try {
                            AsyncStorage.setItem('@RFTK:key', dt.refreshToken);
                            // alert(dt.refreshToken)
                        } catch (error) {
                            console.log('Error when saving data', error);
                        }


                        this
                            .props
                            .setUserToken(dt.token);
                        dataService
                            .getUserInfo(dt.token)
                            .then((datas) => {
                                console.log('datas', datas);
                                if (datas.err === 0) {

                                    this
                                        .props
                                        .setUserInfo(datas)
                                    this._gotoPromotion()
                                }
                                // api.hideLoading()
                            })
                        //setTimeout(this._gotoPromotion(), 1000);


                    }
                    else if (dt.err == 4) {
                        otpID = dt.otpId
                        api.hideLoading()
                        // alert(otpID)
                        api.showInputBox('Mã OTP', 'Xác thực OTP', '', this._onConfirmOTP.bind(this))
                    }
                    else {
                        api.showMessage(dt.msg, 'Thông báo')
                        api.hideLoading()
                    }

                })
        } else {
            api.showMessage('Hãy điền vào số điện thoại hoặc thẻ ưu đãi', 'Thông báo')
            isLogin = false
        }

        setTimeout(() => {
            if (isLogin) {
                api.hideLoading()
                api.showMessage('Vui lòng kiểm tra mạng internet', 'Thông báo')
            }
        }, 1500)
    }

    _renderDotIndicator() {
        return <PagerDotIndicator pageCount={3} />;
    }
    ds = new ListView.DataSource({
        rowHasChanged: (r1, r2) => r1 !== r2
    });
    render() {
        if (this.props.userState.uuid !== undefined) {
            ga = new Analytics('UA-90479808-1', this.props.userState.uuid, 1, 'userAgent');
            var screenView = new Hits.ScreenView('Mpoint', 'Login')
            ga.send(screenView);
        }
        return (
            <Container>
                <View>
                    <Image
                        style={{
                            height: 70,
                            width: 250,
                            alignSelf: 'center'
                        }}
                        source={require('../img/logo.png')} />
                    <Text
                        style={{
                            color: 'gray',
                            marginTop: 5,
                            alignSelf: 'center'
                        }}>VÍ THẺ ƯU ĐÃI VÀ TÍCH ĐIỂM TRÊN MOBILE</Text>
                </View>
                <Content>
                    <View
                        style={{
                            padding: 10,
                            borderTopColor: '#dadada',
                            borderTopWidth: 1,
                            flexDirection: 'column'
                        }}>
                        <Text
                            style={{
                                textAlign: 'center',
                                color: 'gray',
                                lineHeight: 25
                            }}>Nhập số điện thoại hoặc mã thẻ để sử dụng</Text>

                        <InputGroup
                            style={{
                                marginTop: 20,
                                backgroundColor: '#fff',
                                borderRadius: 2,
                                marginBottom: 10,
                                width: api.getRealDeviceWidth() / 2,
                                minWidth: 300,
                                alignSelf: 'center',


                            }}>
                            <Input
                                placeholderTextColor='gray'
                                placeholder='Số điện thoại'
                                onChangeText={(text) => this.setState({ the: text })}
                                style={{ width: api.getRealDeviceWidth() }} />
                        </InputGroup>
                        <View style={{ flexDirection: 'row' }}>
                            <Button
                                style={{
                                    backgroundColor: '#fa6428',
                                    flex: 1,
                                    width: null
                                }}
                                full
                                small
                                onPress={this._checkLogin}>
                                <Text style={{ color: 'white' }}>ĐĂNG NHẬP</Text>
                            </Button>
                        </View>
                        <Text
                            style={{
                                textAlign: 'center',
                                color: 'gray',
                                lineHeight: 25
                            }}>Hoặc chọn thẻ để đưa vào ví</Text>
                        <ListView
                            enableEmptySections={true}
                            dataSource={this.ds.cloneWithRows(this.state.members)}
                            contentContainerStyle={{
                                flexDirection: 'row',
                                flexWrap: 'wrap',
                                alignItems: 'stretch'
                            }}
                            renderRow={(item, sectionID, rowID) => {
                                console.log(sectionID, rowID)
                                return (<Image
                                    source={{
                                        uri: item.card
                                    }}
                                    style={{
                                        width: (api.getRealDeviceWidth() - 40) / 2,
                                        height: api.getRealDeviceWidth() / 3,
                                        resizeMode: 'stretch',
                                        margin: 5,
                                    }}
                                    onCLick={() => {
                                        alert('select ' + item.name)
                                    }} />);
                            }} />
                    </View>
                </Content>
            </Container>
        )
    }
}

mapStateToProps = (state) => {
    return { userState: state.userState, navState: state.navState }
}
mapDispatchToProps = (dispatch) => ({
    push: (route) => dispatch(push(route)),
    showmess: () => dispatch(showMessage('1', '2')),
    pop: () => dispatch(pop()),
    showloading: () => dispatch(showLoading()),
    hideloading: () => dispatch(hideLoading()),
    setUserInfo: (data) => dispatch(setUserInfo(data)),
    setUserToken: (token) => dispatch(setUserToken(token)),
    setUuid: (uuid) => dispatch(setUuid(uuid)),
    setShopLocation: (markers) => dispatch(setShopLocation(markers))
})
export default connect(mapStateToProps, mapDispatchToProps)(Login)