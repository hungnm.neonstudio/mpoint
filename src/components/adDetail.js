import React, {Component} from 'react';
import {View,ScrollView,Image,Linking,TouchableWithoutFeedback} from 'react-native';
import api from '../api'
import dataService from '../api/dataService'
import {IndicatorViewPager,PagerTitleIndicator,PagerDotIndicator} from 'rn-viewpager'
import Swiper from 'react-native-swiper'
export default class Promotion extends Component {

    constructor(props) {

        super(props);
        this.state={
            page:0,
            ad:[]
        }
        dataService.getAd(this.props.token,2)
        .then(rs=>{
            if(rs.err==0){
                this.setState({
                ad:rs.data
            })
            }
        })
    }
_renderAd(){
    if(this.state.ad.length>0)
        return (
        <Swiper
                    height={parseInt((api.getRealDeviceWidth()-10)/1.9)}
                    width={parseInt((api.getRealDeviceWidth-10))}
                    autoplay
                    autoplayTimeout={10}
                    dot={<View style={{backgroundColor: 'rgba(255,255,255,.8)', width: 5, height: 5, borderRadius: 4, marginLeft: 3, marginRight: 3, marginTop: 3, marginBottom: 3}} />}
                    activeDot={<View style={{backgroundColor: '#fff', width: 8, height: 8, borderRadius: 4, marginLeft: 3, marginRight: 3, marginTop: 3, marginBottom: 3}} />}
                     paginationStyle={{
                    bottom:5, alignSelf:'center'
                         }}
                    buttonWrapperStyle={{backgroundColor: 'transparent', flexDirection: 'row', position: 'absolute', top: 0, left: 0, flex: 1, paddingHorizontal: 10, paddingVertical: 10, paddingRight:20,justifyContent: 'space-between', alignItems: 'center',}}
                >
        {
            this.state.ad.map((item,id)=>(
                <TouchableWithoutFeedback
                 key={id} 
                    onPress={()=>{
                        if(item.bannerTypeId==1){
                            api.pop()
                            setTimeout(()=>{
                                api.push({key:'detail',id:item.link})
                            },700)
                        }
                        if(item.bannerTypeId==2&&item.link.indexOf('http') != -1){
                            Linking.openURL(item.link)
                        }
                    }}
                >
                    <View style={{flex:1}}
                    >
                        <Image
                            style={{ flex:1,width:api.getRealDeviceWidth()-10,resizeMode:'stretch'}}
                            source={{ uri:item.banner }}
                        />
                    </View>
                </TouchableWithoutFeedback>
                ) 
            )
        }
        </Swiper>
        )
    }
    _subString(str){
        if(str.length > 8){
            return str.substring(0,5)+'...'
        }else return str
    }
    _renderTitleIndicator() {
        return <PagerTitleIndicator titles={[this._subString(this.props.me),'Ẩm thực', 'Mua sắm', 'Khác']}/>;
    }
    render() {
        return (
            <View>
                {this._renderAd()}
            </View>
        );
    }
}
