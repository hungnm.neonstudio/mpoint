import {
    Container,
    Header,
    Icon,
    Button,
    Footer,
    Title,
    List,
    ListItem,
    Content,
    Input,
    InputGroup,
    Badge,
    Fab
} from 'native-base';
import { connect } from 'react-redux';
import Promotion from './promotion';
import Gift from './gift';
import MyGift from './myGift';
import Card from './card';
import Location from './location'
import React, { Component } from 'react';
import IconQR from 'react-native-vector-icons/FontAwesome';
import IconOcticons from 'react-native-vector-icons/Octicons';
import IconEntypo from 'react-native-vector-icons/Entypo';
import Drawer from 'react-native-drawer'
import {
    View,
    Text,
    TouchableOpacity,
    StyleSheet,
    Image,
    Modal,
    PermissionsAndroid,
    ScrollView, AsyncStorage, Platform,
    DrawerLayoutAndroid, ActivityIndicator, DeviceEventEmitter, Keyboard, Linking, BackAndroid
} from 'react-native';
import { PagerTabIndicator, IndicatorViewPager, PagerTitleIndicator, PagerDotIndicator } from 'rn-viewpager';
import api from '../api';
import dataService from '../api/dataService'
// import ActionButton from 'react-native-action-button';
import { setListMember } from '../actions/userActions';
import DeviceInfo from 'react-native-device-info'
import FCM, { FCMEvent } from 'react-native-fcm'
import i18 from "./i18"
let route = ''
var images = {
    bg_drawer: require('../img/bg_login.jpg'),
    logo: require('../img/logo.png')
}
let searchText = ''
let refreshPromotion = () => { };
let gotoUpdateUserInfo = () => { };
class Home extends Component {
    componentDidMount() {
        refreshPromotion = () => {
            this.onCardPress()
            setTimeout(() => {
                this.onPromotionPress()
            }, 500)
        }
        gotoUpdateUserInfo = () => this.onCardPress()
        //api.setGotoInfoCard(1)
        this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this.keyboardDidShow.bind(this))
        this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this.keyboardDidHide.bind(this))
        // setTimeout(()=>{
        // },1000)
    }
    setLangtoStorage(lang) {
        try {
            AsyncStorage.setItem('lang', lang);
            // alert(dt.refreshToken)
        } catch (error) {
            console.log('Error when saving data', error);
        }
    }
    _changLang() {
        this.drawerRef.close()
        api.showConfirm(i18.t(this.props.lang == 'en' ? 'contentSwitchVi' : 'contentSwitchEn'), i18.t('title'),
            i18.t('CloseApp'), i18.t('Maylater'),
            function OK() {
                api.hideConfirm()
                // BackAndroid.exitApp()
                api.resetRoute({ key: 'login' })
            },
            function Cancel() {
                api.hideConfirm()
            }
        )
        if (this.props.lang == 'en') {
            api.setLangVi()
            //this.setLangtoStorage('vi')
        } else {
            api.setLangEn()
            //this.setLangtoStorage('en')
        }
    }


    componentWillMount() {
        dataService.insertGPS();
        FCM.requestPermissions(); // for iOS
        FCM.getFCMToken().then(token => {
            console.log('fcm toekn', token)
            dataService.regNotification(Platform.OS, DeviceInfo.getUniqueID(), token, this.props.userState.token, this.props.userState.data.endUser.phone)
                .then((rs) => {

                })
        });
        FCM.on(FCMEvent.Notification, (notif) => {
            console.log('notification', notif)
            let noti = Platform.OS == 'android' ? notif.fcm : notif.notification ? notif.notification : notif.aps.alert
            if (!noti.body || !noti.title) return
            if (notif.type == 0)
                api.showConfirm(noti.body, noti.title, i18.t('viewPromotion'), i18.t('close'),
                    function OK() {
                        if (route == 'detail') api.pop()
                        setTimeout(() => {
                            api.push({ key: 'detail', id: notif.link });
                        }, 500)
                        api.hideConfirm()
                    },
                    function Cancel() {
                        api.hideConfirm()
                    })
            else if (notif.type == 1) {
                api.showConfirm(noti.body, noti.title, i18.t('viewPage'), i18.t('close'),
                    function OK() {
                        Linking.openURL(notif.link)
                        api.hideConfirm()
                    },
                    function Cancel() {
                        api.hideConfirm()
                    })
            } else if (notif.type == 2) {
                api.showMessage(noti.body, noti.title)
            }
        })
        // let  route = this.props.navState.routes[this.props.navState.index].key
        // FCM.requestPermissions(); // for iOS
        // FCM.getFCMToken().then(token => {
        //     console.log('fcm toekn',token)
        //     dataService.regNotification(Platform.OS,DeviceInfo.getUniqueID(),token,this.props.userState.token,this.props.userState.data.endUser.phone)
        //     .then((rs)=>{

        //     })
        // });
        // FCM.on(FCMEvent.Notification,(notif) => {
        //     console.log('notification',notif)
        //     let noti = notif.fcm
        //     if(!noti.body || !noti.title ) return
        //     if(notif.type ==0)
        //         api.showConfirm(noti.body,noti.title,i18.t('viewPromotion'),i18.t('close'),
        //         function OK(){
        //             if(route == 'detail') api.pop()
        //             setTimeout(()=>{
        //                 api.push({key:'detail',id:notif.link});
        //             },300)
        //             api.hideConfirm()
        //         },
        //         function Cancel(){
        //             api.hideConfirm()
        //         })
        //     else if(notif.type ==1){
        //         api.showConfirm(noti.body,noti.title,i18.t('viewPage'),i18.t('close'),
        //         function OK(){
        //             Linking.openURL(notif.link)
        //             api.hideConfirm()
        //         },
        //         function Cancel(){
        //             api.hideConfirm()
        //         })
        //     }
        // })
        // alert(this.props.userState.data.endUser.phone)
        try {
            AsyncStorage
                .getItem('@phone:key')
                .then((phone) => {
                    if (phone !== null || phone != this.props.userState.data.endUser.phone)
                        try {
                            AsyncStorage.setItem('@phone:key', this.props.userState.data.endUser.phone);
                        } catch (error) {
                            console.log('Error when saving data', error);
                        }
                });
        } catch (error) {
            console.log('Error when getting data', error);
        }
        //             api.hideLoading()

        // dataService
        //     .getListMember(this.props.userState.token, 1, 0)
        //     .then((data) => {
        //         this.props.setListMember(data.members);
        //         setTimeout(() => {
        //             api.hideLoading()
        //         }, 0)
        //     })
    }
    drawerRef = null
    _openDrawer = () => {
        if (this.drawerRef != null) {
            this.drawerRef.open()
        }
    }

    constructor(props) {
        super(props);
        this.state = {
            showfFooter: true,
            search: [],
            showQRbutton: true,
            showMenu: false,
            showMenuPrev: true,
            page: 0,
            pageGift: 0,
            title: i18.t('titleMainHome'),
            displayIntroduce: false,
            showSearchBar: false,
            displaySearchButton: true,
            propmotionStyle: {
                backgroundColor: '#fa6428',
                flex: 1
            },
            giftStyle: {
                backgroundColor: '#009688',
                flex: 1
            },
            locationStyle: {
                backgroundColor: '#009688',
                flex: 1
            },
            myGiftStyle: {
                backgroundColor: '#009688',
                flex: 1
            },
            cardStyle: {
                backgroundColor: '#009688',
                flex: 1
            },
            type: 1,
            searchText: ''
        }
        this.onPromotionPress = this
            .onPromotionPress
            .bind(this);
        this.displayIntroduce = this.displayIntroduce.bind(this);
    }
    logOut = () => {
        dataService.logOut().then(rs => {

        });
        try {
            AsyncStorage.setItem('@RFTK:key', '');
            // alert(dt.refreshToken)
        } catch (error) {
            ('Error when saving data', error);
        }
        try {
            AsyncStorage.setItem('@phone:key', '');
            // alert(dt.refreshToken)
        } catch (error) {
            ('Error when saving data', error);
        }
        api.resetRoute({ key: 'login' })
        api.setGitfCount(0);
    }
    onLogout = () => {
        api.showConfirm(i18.t('confirmExit'), i18.t('logout'), i18.t('yes'), i18.t('no'), () => {
            this.logOut();
            api.hideConfirm();
        }, () => {
            api.hideConfirm()
        }
        )
    }
    _refreshPromotion() {
        this.setState({
            showSearchBar: true
        })
        setTimeout(() => {
            this.setState({
                showSearchBar: false
            })
        }, 2000)
    }
    gotoUpdateUserInfo = () => {
        this.drawerRef.close();
        this.setState({ showMenu: false })
        api.push({ key: 'RegUser' })
    }
    displayIntroduce() {
        this.drawerRef.close();
        this.setState({ displayIntroduce: true, showMenu: false, showMenuPrev: true, showQRbutton: true });
    }
    showPartners = () => {
        this.drawerRef.close();
        this.setState({ showMenu: false, showMenuPrev: true, showQRbutton: true });
        api.push({ key: 'partners' });
    }
    goToShopLocation = () => {
        this.drawerRef.close();
        this.setState({ showMenu: false, showMenuPrev: true, showQRbutton: true });
        this.onLocationPress();
    }
    onPromotionPress() {
        this.setState({
            showQRbutton: true,
            page: 0,
            title: i18.t('titleMainHome'),
            type: 1,
            propmotionStyle: {
                backgroundColor: '#fa6428',
                flex: 1
            },
            giftStyle: {
                backgroundColor: '#009688',
                flex: 1
            },
            locationStyle: {
                backgroundColor: '#009688',
                flex: 1
            },
            myGiftStyle: {
                backgroundColor: '#009688',
                flex: 1
            },
            cardStyle: {
                backgroundColor: '#009688',
                flex: 1
            },
            showSearchBar: false,
            displaySearchButton: true
        })
    }
    onGiftPress() {
        this.setState({
            showQRbutton: true,
            page: 1,
            title: i18.t('titleGiftHome'),
            type: 2,
            propmotionStyle: {
                backgroundColor: '#009688',
                flex: 1
            },
            giftStyle: {
                backgroundColor: '#fa6428',
                flex: 1
            },
            locationStyle: {
                backgroundColor: '#009688',
                flex: 1
            },
            myGiftStyle: {
                backgroundColor: '#009688',
                flex: 1
            },
            cardStyle: {
                backgroundColor: '#009688',
                flex: 1
            },
            showSearchBar: false,
            displaySearchButton: true
        })
    }
    _checkCamPer() {
        if (Platform.OS === 'android' && Platform.Version >= 23)
            PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.CAMERA).then((result) => {
                if (result) {
                    console.log("Permission is OK");
                    camPer = true
                    api.push({ key: 'QRscan', refreshPromotion: refreshPromotion, gotoUpdateUserInfo: gotoUpdateUserInfo })
                } else {
                    PermissionsAndroid.requestPermission(PermissionsAndroid.PERMISSIONS.CAMERA).then((result) => {
                        if (result) {
                            console.log("User accept");
                            api.push({ key: 'QRscan', refreshPromotion: refreshPromotion, gotoUpdateUserInfo: gotoUpdateUserInfo })
                        } else {
                            console.log("User refuse");
                            api.showMessage(i18.t('errCamPer'), i18.t('titleMsgLogin'))
                        }
                    });
                }
            });
        else api.push({ key: 'QRscan', refreshPromotion: refreshPromotion, gotoUpdateUserInfo: gotoUpdateUserInfo })
    }
    _checkPer() {

        if (Platform.OS === 'android' && Platform.Version >= 23)
            PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION).then((result) => {
                if (result) {
                    console.log("Permission is OK");
                    this.goToShopLocation()
                } else {
                    PermissionsAndroid.requestPermission(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION).then((result) => {
                        if (result) {
                            console.log("User accept");
                            this.goToShopLocation()
                        } else {
                            console.log("User refuse");
                            api.showMessage(i18.t('errLocationPer'), i18.t('titleMsgLogin'))
                        }
                    });
                }
            });
        else
            this.goToShopLocation()
        //         async function requestCameraPermission() {
        //         try {
        //     const granted = await PermissionsAndroid.requestPermission(
        //       PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
        //       {
        //         'title': 'Thông báo',
        //         'message': 'Hãy cho ứng dụng quyền để lấy vị trí hiện tại'
        //       }
        //             )
        //               if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        //              console.log("You can use the camera")
        //              rs = true
        //             } else {
        //              console.log("Camera permission denied")
        //              rs = false
        //          }
        //          } catch (err) {
        //          console.warn(err)
        //         }
        //         rs = false
        // }
        // return rs
    }
    onLocationPress() {
        this.setState({
            showQRbutton: false,
            page: 2,
            title: i18.t('location'),
            propmotionStyle: {
                backgroundColor: '#009688',
                flex: 1
            },
            giftStyle: {
                backgroundColor: '#009688',
                flex: 1
            },
            locationStyle: {
                backgroundColor: '#fa6428',
                flex: 1
            },
            myGiftStyle: {
                backgroundColor: '#009688',
                flex: 1
            },
            cardStyle: {
                backgroundColor: '#009688',
                flex: 1
            },
            showSearchBar: false,
            displaySearchButton: false
        })
    }
    onMyGiftPress(page) {
        this.setState({
            showQRbutton: false,
            page: 3,
            pageGift: page == undefined ? 0 : 1,
            title: i18.t('giftbox'),
            propmotionStyle: {
                backgroundColor: '#009688',
                flex: 1
            },
            giftStyle: {
                backgroundColor: '#009688',
                flex: 1
            },
            locationStyle: {
                backgroundColor: '#009688',
                flex: 1
            },
            myGiftStyle: {
                backgroundColor: '#fa6428',
                flex: 1
            },
            cardStyle: {
                backgroundColor: '#009688',
                flex: 1
            },
            showSearchBar: false,
            displaySearchButton: false
        })
    }
    onCardPress(page) {
        //this.drawerRef.close();
        this.setState({
            showQRbutton: false,
            page: 4,
            pageCard: page != undefined ? 0 : 1,
            title: i18.t('promotioncard'),
            propmotionStyle: {
                backgroundColor: '#009688',
                flex: 1
            },
            giftStyle: {
                backgroundColor: '#009688',
                flex: 1
            },
            locationStyle: {
                backgroundColor: '#009688',
                flex: 1
            },
            myGiftStyle: {
                backgroundColor: '#009688',
                flex: 1
            },
            cardStyle: {
                backgroundColor: '#fa6428',
                flex: 1
            },
            showSearchBar: false,
            displaySearchButton: false
        })
    }
    _search() {
        this.setState({ searchText: searchText, search: [], isSearching: true })
        // // this.setState({search:[searchText,...this.state.search]})
        if (searchText != '')
            dataService.suggestText(this.props.userState.token, searchText)
                .then(rs => {
                    this.setState({ search: rs.partners, isSearching: false })
                    console.log(this.state)
                })
        else {
            this.setState({ isSearching: false })
        }
    }
    _renderSuggest(arr) {
        /*if(searchText==''){
            this.setState({search:[]})
            return true
        }
        // return arr.map((item,i)=>(<Text>{item}</Text>))
        let id = 1
        return arr.map((item,i)=>
            <Text key={i} onPress={()=>{api.push({key: 'search', keyWord: item.name, type: this.state.type});
                this.setState({search:[],showSearchBar:false})
                }} style={{ borderBottomWidth:1,borderBottomColor:'#cacaca',marginLeft:30,flex:1,paddingTop:5,paddingBottom:5 }}>{item.name}</Text>
        )*/
    }
    _gotoGift() {
        this.onMyGiftPress('gotoGift')
    }
    _renderPage() {
        switch (this.state.page) {
            case 0:
                return <Promotion type={this.state.type} me={this.props.userState.data.active != undefined & this.props.userState.data.active.partner != undefined ? this.props.userState.data.active.partner.shortName : 'NamePartner'} gotoinfo={this.onCardPress.bind(this)} gotoGift={this._gotoGift.bind(this)} />
            case 1:
                return <Gift type={this.state.type} gotoinfo={this.onCardPress.bind(this)} me={this.props.userState.data.active != undefined & this.props.userState.data.active.partner != undefined ? this.props.userState.data.active.partner.shortName : 'NamePartner'} />
            case 2:
                return <Location />
            case 3:
                return <MyGift page={this.state.pageGift} gotoPromotion={this.onCardPress.bind(this)} gotoGift={this._gotoGift.bind(this)} />
            case 4:
                return <Card gotoPromotion={this.onPromotionPress} page={this.state.pageCard} gotoUpdate={() => this.onCardPress()} />
        }
    }
    keyboardDidShow(e) {
        let newSize = api.getRealDeviceHeight() - e.endCoordinates.height - 165
        this.setState({
            showfFooter: false
        })
    }

    keyboardDidHide(e) {
        this.setState({
            showfFooter: true
        })
    }
    componentWillUnmount() {
        this.keyboardDidShowListener.remove()
        this.keyboardDidHideListener.remove()
    }
    renderHeader() {
        if (this.state.showSearchBar) {
            return (
                <Header
                    searchBar
                    style={{
                        backgroundColor: '#009688'
                    }}>
                    <View style={{ flexDirection: 'row' }}>
                        <Icon
                            style={{
                                backgroundColor: '#009688',
                                color: 'white',
                                textAlignVertical: 'center'
                            }}
                            name="md-close"
                            onPress={() => {
                                this.setState({ showSearchBar: false, searchText: '', search: [] })
                            }} />
                        {/*<Input
                            style={{
                            backgroundColor: '#009688',
                            color: 'white'
                        }}
                            placeholder="Search"
                            placeholderTextColor='#cdcdcd'
                            onChangeText={(text) => {searchText=text,this._search(text)}}
                            //autoFocus={true}
                            />*/}
                        <Icon
                            style={{
                                backgroundColor: '#009688',
                                color: 'white',
                                textAlignVertical: 'center'
                            }}
                            name='ios-search'
                            onPress={() => {
                                api.push({ key: 'search', keyWord: searchText, type: this.state.type })
                                this.setState({ showSearchBar: false, searchText: '', search: [] })
                            }} />
                    </View>
                </Header>
            )
        } else {
            return (
                <Header
                    style={{
                        backgroundColor: '#009688'
                    }}>
                    <Button transparent onPress={() => { this._openDrawer() }}>
                        <Icon name='ios-menu'
                            style={{ color: Platform.OS == 'ios' ? '#000' : '#fff' }}
                        />
                    </Button>
                    <Title style={{ alignSelf: 'center' }}>{this.state.title}</Title>
                    {this.state.displaySearchButton ? <Button
                        transparent
                        onPress={() => {
                            this.setState({ showSearchBar: true })
                        }}>
                        <Icon name='ios-search'
                            style={{ color: Platform.OS == 'ios' ? '#000' : '#fff' }}
                        />
                    </Button> : null}
                </Header>
            );
        }
    }
    footer() {
        if (this.state.showfFooter) return (<Footer
            style={{
                backgroundColor: '#009688',
                flexDirection: 'row'
            }}>
            <View
                style={{
                    flex: 1,
                    flexDirection: 'row'
                }}>
                <View
                    style={{
                        flex: 1,
                        flexDirection: 'column'
                    }}>
                    <MyButton
                        name='ios-pricetags-outline'
                        text={i18.t('promotion')}
                        style={this.state.propmotionStyle}
                        onPress={() => this.onPromotionPress()} />
                </View>
                <View
                    style={{
                        flex: 1
                    }}>
                    <MyButton
                        name='ios-ribbon-outline'
                        text={i18.t('gift')}
                        style={this.state.giftStyle}
                        onPress={() => this.onGiftPress()} />
                </View>
                <View
                    style={{
                        flex: 1
                    }}>
                    <MyButton
                        name='ios-pin-outline'
                        text={i18.t('nearby')}
                        style={this.state.locationStyle}
                        onPress={() => this._checkPer()} />
                </View>
                <View
                    style={{
                        flex: 1
                    }}>
                    <MyButton
                        name='ios-archive-outline'
                        text={i18.t('yourgift')}
                        my_gift={true}
                        //gift_count={this.props.giftCount}
                        style={this.state.myGiftStyle}
                        onPress={() => { if (!api.requestLogin()) { return; } this.onMyGiftPress() }} />
                    {this.props.giftCount != '' && this.props.giftCount != undefined && this.props.giftCount > 0 ? <View style={{ top: 2, left: (api.getRealDeviceWidth() / 5) / 2 + 10, position: 'absolute', zIndex: 10, borderColor: '#fff', borderWidth: api.getRealDeviceWidth() > 320 ? 0.5 : 1, borderRadius: 10, paddingHorizontal: 5, backgroundColor: 'red' }} >
                        <Text style={{ fontSize: 12, textAlignVertical: 'center', color: '#fff' }}>
                            {this.props.giftCount}
                        </Text>
                    </View> : <View></View>}
                </View>
                <View
                    style={{
                        flex: 1
                    }}>
                    <MyButton
                        name='ios-card-outline'
                        text={i18.t('promotioncard')}
                        style={this.state.cardStyle}
                        onPress={() => this.onCardPress(0)} />
                </View>
            </View>
        </Footer>)
        else {
            return null
        }
    }
    render() {
        navigationView = (<Container>
            <Header
                style={{
                    backgroundColor: '#009688',
                    zIndex: 1
                }}>
                <Title style={{ alignSelf: 'center' }}>MENU</Title>
            </Header>
            <Image source={images.bg_drawer}
                style={{ height: api.getRealDeviceHeight() - 60, width: api.getRealDeviceWidth() * .8 }}
            >
                <ScrollView>
                    <ListItem style={styles.menu_item}>
                        <TouchableOpacity style={{ flexDirection: 'row' }} onPress={() => { this.onPromotionPress(); this.drawerRef.close(); }}>
                            <Image style={{ backgroundColor: 'transparent', height: 20, width: 20, resizeMode: 'stretch', marginTop: 15, marginLeft: 5 }} source={require('../img/icon/home.png')} />
                            <Text style={styles.menu_item_text}>{i18.t('home')}</Text>
                        </TouchableOpacity>
                    </ListItem>
                    <ListItem style={styles.menu_item}>
                        <TouchableOpacity style={{ flexDirection: 'row' }} onPress={() => {
                            if (!api.requestLogin()) { return; }
                            this.drawerRef.close();
                            this.onCardPress()
                        }}>
                            <Image style={{ height: 20, width: 20, resizeMode: 'stretch', marginTop: 15, marginLeft: 5 }} source={require('../img/icon/partner.png')} />
                            <Text style={styles.menu_item_text}>{i18.t('updateInfo')}</Text>
                        </TouchableOpacity>
                    </ListItem>
                    <ListItem style={styles.menu_item}>
                        <TouchableOpacity style={{ flexDirection: 'row' }} onPress={() => { if (!api.requestLogin()) { return; } api.resetRoute({ key: 'his' }); this.drawerRef.close(); }}>
                            <IconQR name='history' style={{ fontSize: 20, color: '#fff', marginTop: 15, marginLeft: 5, backgroundColor: 'transparent' }} />
                            <Text style={styles.menu_item_text}> {i18.t('history')}</Text>
                        </TouchableOpacity>
                    </ListItem>
                    <ListItem style={styles.menu_item}>
                        <TouchableOpacity style={{ flexDirection: 'row' }} onPress={() => this.showPartners()}>
                            <IconQR name='random' style={{ fontSize: 20, color: '#fff', marginTop: 15, marginLeft: 5, backgroundColor: 'transparent' }} />
                            <Text style={styles.menu_item_text}>{i18.t('partner')}</Text>
                        </TouchableOpacity>
                    </ListItem>
                    <ListItem style={styles.menu_item}>
                        <TouchableOpacity style={{ flexDirection: 'row' }} onPress={() => this._checkPer()}>
                            <Image style={{ height: 20, width: 20, resizeMode: 'stretch', marginTop: 15, marginLeft: 5 }} source={require('../img/icon/location.png')} />
                            <Text style={styles.menu_item_text}>{i18.t('mapPromotion')}</Text>
                        </TouchableOpacity>
                    </ListItem>
                    <ListItem style={styles.menu_item}>
                        <TouchableOpacity style={{ flexDirection: 'row' }} onPress={() => { api.push({ key: "intro" }); this.drawerRef.close(); }}>
                            <IconEntypo name='info' style={{ fontSize: 20, color: '#fff', marginTop: 15, marginLeft: 5, backgroundColor: 'transparent' }} />
                            <Text style={styles.menu_item_text}>{i18.t('intro')}</Text>
                        </TouchableOpacity>
                    </ListItem>
                    <ListItem style={styles.menu_item}>
                        <TouchableOpacity style={{ flexDirection: 'row' }} onPress={() => { if (!api.requestLogin()) { return; } api.push({ key: 'listRestaurant' }); this.drawerRef.close() }}>
                            <Icon name='ios-restaurant' style={{ fontSize: 25, color: '#fff', marginTop: 15, marginLeft: 5, backgroundColor: 'transparent' }} />
                            <Text style={styles.menu_item_text}>  {i18.t('titleRestaurant')}</Text>
                        </TouchableOpacity>
                    </ListItem>
                    {
                        this.props.userState.listMember.length < 2 ? null : <ListItem style={styles.menu_item}>
                            <TouchableOpacity style={{ flexDirection: 'row' }} onPress={() => { if (!api.requestLogin()) { return; } api.push({ key: 'exchangePoint' }); this.drawerRef.close() }}>
                                <Icon name='md-sync' style={{ fontSize: 25, color: '#fff', marginTop: 15, marginLeft: 5, backgroundColor: 'transparent' }} />
                                <Text style={styles.menu_item_text}>  {i18.t('exchangePoint')}</Text>
                            </TouchableOpacity>
                        </ListItem>
                    }
                    <ListItem style={styles.menu_item}>
                        <TouchableOpacity style={{ flexDirection: 'row' }} onPress={() => { if (!api.requestLogin()) { return; } api.push({ key: 'addPcode' }); this.drawerRef.close() }}>
                            <IconOcticons name='pencil' style={{ fontSize: 20, color: '#fff', marginTop: 15, marginLeft: 5, backgroundColor: 'transparent' }} />
                            <Text style={styles.menu_item_text}>{i18.t('typePcode')}</Text>
                        </TouchableOpacity>
                    </ListItem><ListItem style={styles.menu_item}>
                        <TouchableOpacity style={{ flexDirection: 'row' }} onPress={this._changLang.bind(this)}>
                            <IconQR name='exchange' style={{ fontSize: 20, color: '#fff', marginTop: 15, marginLeft: 5 }} />
                            <Text style={styles.menu_item_text}>{i18.t(this.props.lang == 'vi' ? 'switchLangEn' : 'switchVi')}</Text>
                        </TouchableOpacity>
                    </ListItem>
                    <ListItem style={styles.menu_item}>
                        <TouchableOpacity style={{ flexDirection: 'row' }} onPress={() => { if (!api.requestLogin()) { return; } this.drawerRef.close(); this.onLogout() }}>
                            <IconOcticons name='sign-out' style={{ fontSize: 20, color: '#fff', marginTop: 15, marginLeft: 5, backgroundColor: 'transparent' }} />
                            <Text style={styles.menu_item_text}>{i18.t('exit')}</Text>
                        </TouchableOpacity>
                    </ListItem>
                </ScrollView>
            </Image>
        </Container>)
        route = this.props.navState.routes[this.props.navState.index].key

        /*const menu = <DrawerMenu
            displayIntroduce={this.displayIntroduce.bind(this)}
            onPartnersPress={this.showPartners.bind(this)}
            goToShopLocation={this.goToShopLocation.bind(this)}
            gotoUpdateUserInfo={this.gotoUpdateUserInfo.bind(this)}
            onLogout={this.onLogout}
        />*/
        // var page = this._renderPage();
        //var header = this.renderHeader();

        if (this.state.showSearchBar)
            return (
                <View style={{}}>

                    <InputGroup style={{ paddingTop: Platform.OS == 'ios' ? 10 : 0, backgroundColor: '#009688', minHeight: 55, flexDirection: 'row' }}>
                        <Icon
                            style={{
                                color: Platform.OS == 'ios' ? '#000' : 'white',
                                textAlignVertical: 'center',
                                textAlign: 'center',
                                paddingLeft: 5,
                                paddingRight: 5
                            }}
                            name="md-close"
                            onPress={() => {
                                this.setState({ showSearchBar: false, searchText: '', search: [], isSearching: false })
                            }} />
                        <Input
                            autoFocus={true}
                            returnKeyType='search'
                            onSubmitEditing={() => {
                                if (this.state.searchText != '') {
                                    api.push({ key: 'search', keyWord: searchText, type: this.state.type })
                                    setTimeout(() => {
                                        this.setState({ showSearchBar: false, searchText: '', search: [] })
                                    }, 1500)
                                } else {
                                    api.showMessage(i18.t('blakSearchInput'), i18.t('titleMsgLogin'))
                                }
                            }}
                            onChangeText={t => {
                                searchText = t; this._search()
                            }} placeholder={i18.t('placeholderSearch_home')} style={{ flex: 10, color: Platform.OS == 'ios' ? '#000' : 'white', borderBottomColor: '#cdcdcd' }} placeholderTextColor={Platform.OS == 'ios' ? '#000' : '#fff'} />
                        <Icon
                            style={{
                                color: Platform.OS == 'ios' ? '#000' : 'white',
                                textAlignVertical: 'center',
                                textAlign: 'center',
                                paddingLeft: 5,
                                paddingRight: 5
                            }}
                            name='ios-search'
                            onPress={() => {
                                if (this.state.searchText != '') {
                                    api.push({ key: 'search', keyWord: searchText, type: this.state.type })
                                    setTimeout(() => {
                                        this.setState({ showSearchBar: false, searchText: '', search: [] })
                                    }, 1500)
                                } else {
                                    api.showMessage(i18.t('blakSearchInput'), i18.t('titleMsgLogin'))
                                }
                            }} />
                    </InputGroup>
                    <Image
                        source={require('../img/bg_login.jpg')}
                        style={{ width: api.getRealDeviceWidth(), height: api.getRealDeviceHeight() - 56 }}
                    >
                        {this.state.isSearching ? <ActivityIndicator /> :
                            this.state.search.length < 1 && this.state.searchText != '' ?
                                <Text style={{ marginTop: 20, flex: 1, textAlign: 'center', minHeight: 30, color: '#fff' }}>{i18.t('noneRs')}</Text> : null}
                        {this.state.search.length > 0 ?
                            this.state.search.map((item, i) => {
                                return (
                                    <Text key={i}
                                        onPress={() => {
                                            api.push({ key: 'search', keyWord: item.name, type: this.state.type })
                                            {/*this.setState({ showSearchBar: false, searchText: '', search: [] })*/ }
                                        }}
                                        style={{ minHeight: 30, textAlignVertical: 'center', borderBottomColor: '#cacaca', color: '#9a9a9a', borderBottomWidth: 0.5, backgroundColor: '#fff', paddingLeft: 30 }}
                                    >{item.name}</Text>
                                )
                            }) : null
                        }
                    </Image>
                </View>
            )
        else
            return (
                <Drawer
                    ref={(ref) => { this.drawerRef = ref }}
                    //drawerWidth={api.getRealDeviceWidth()*0.8}
                    //drawerPosition={DrawerLayoutAndroid.positions.Left}
                    //renderNavigationView={() => { return navigationView }}
                    type="overlay"
                    content={navigationView}
                    tapToClose={true}
                    openDrawerOffset={0.2}
                >
                    <Container>
                        {this.renderHeader}
                        <View style={{
                            flex: 1,
                            backgroundColor: '#fff'
                        }}>
                            {this.renderHeader()}
                            {this._renderPage()}
                            {
                                this.state.showQRbutton ? (
                                    <Fab
                                        active={false}
                                        direction="up"
                                        style={{ backgroundColor: '#5067FF' }}
                                        position="bottomRight"
                                        containerStyle={{ bottom: 15, right: 15 }}
                                        onPress={this._checkCamPer}
                                    >
                                        <Text>
                                            <IconQR name="qrcode" size={Platform.OS == 'ios' ? 30 : 40} color="#fff" />
                                        </Text>

                                    </Fab>
                                ) : null
                            }
                        </View>
                        {this.footer()}
                    </Container>
                </Drawer>
            )
    }
}
class MyButton extends Component {
    render() {
        return (
            <TouchableOpacity style={this.props.style} onPress={this.props.onPress}>
                <Icon
                    name={this.props.name}
                    style={{
                        alignSelf: 'center',
                        color: '#fff',
                        paddingTop: 5
                    }} />
                <Text
                    style={{
                        color: '#fff',
                        fontSize: 12,
                        textAlign: 'center'
                    }}>{this.props.text}</Text>
            </TouchableOpacity>
        );
    }
}
const styles = StyleSheet.create({
    menu_container: {
        margin: 0,
        padding: 0,
        paddingTop: 30,
        height: api.getRealDeviceHeight() - 56
    },
    menu_item: {
        margin: 0,
        marginLeft: 0,
        padding: 0,
        borderBottomColor: '#eee',
        borderBottomWidth: 0.5
    },
    menu_item_text: {
        color: 'white',
        padding: 15,
        paddingLeft: 5,
        backgroundColor: 'transparent',
        marginTop: 1
    },
    backgroundImage: {
        flex: 1,
        resizeMode: 'stretch',
        height: api.getRealDeviceHeight()
    }
})
function mapStateToProp(state) {
    //console.log("State", state);
    return { userState: state.userState, navState: state.navState, giftCount: state.giftCountState.GiftCount, lang: state.langState.lang }
}
function mapDispatchToProp(dispatch) {
    return {
        setListMember: (listMember) => {
            dispatch(setListMember(listMember))
        }
    }
}
export default connect(mapStateToProp, mapDispatchToProp)(Home);