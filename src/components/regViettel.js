import {
    Container,
    Header,
    Content,
    Text,
    Input,
    Card,
    Button,
    Icon,
    Title,
    Grid,
    Row
} from 'native-base'

import {setUserInfo} from '../actions/userActions'  
import {Image} from 'react-native'
import React, {Component} from 'react'
import api from '../api'
import dataService from '../api/dataService'
 import {connect} from 'react-redux'
import {push,pop} from '../actions/navActions'
import {showMessage,hideMessageBox,showInputBox,hideInputBox} from '../actions/uiActions'
let phoneNumber =''
let otp =''
let otpID =''
class RegViettel extends Component {
    constructor(props) {
        super(props)
        // console.log(props)
        
    }

    _goBack = ()=>{
        api.pop()
    }
    _checkPhoneViettel(){
        let startNumber = phoneNumber.substring(0,4)
        // alert(startNumber)
        if( startNumber == '0163' || startNumber == '0164' || startNumber == '0165' || startNumber == '0166' || startNumber == '0167' ||startNumber == '0168' ||startNumber == '0169') return true
        startNumber = phoneNumber.substring(0,3)
        //  alert(startNumber)
         if( startNumber == '097' || startNumber == '096' || startNumber == '098' || startNumber == '095') return true
         return false
    }

        _onConfirmOTP(otp){
        
        // alert(otp+phone)
        if(otp == undefined || otp == '') {
            api.showMessage('Vui lòng điền vào mã OTP')
            return
        }
        
        api.hideInputBox()
        setTimeout(()=>api.showLoading(),1)
        dataService.postVerifyOtpRegister(phoneNumber,otp,otpID)
        .then((rs)=>{
            api.hideInputBox()
                if(rs.err == 0)
                {
                    // api.hideInputBox()
                    api.setUserToken(rs.token)
                    dataService.changeCard(rs.token,526)
                    .then(data=>{
                        
                    })
                    try {
                                     AsyncStorage.setItem('@RFTK:key', rs.refreshToken);
                                       // alert(dt.refreshToken)
                                         } catch (error) {
                                      console.log('Error when saving data', error);
                                      }
                    // api.setUserToken('125064b0-12bc-4a38-a03f-717156e4b6e4')
                    
                    dataService.postRegisterFreeCard(123,rs.token)
                    // dataService.postRegisterFreeCard(350,'44046d79-a01c-46aa-95e9-4632cd65ac74')
                    .then(kq=>{
                        
                        if(kq.err == 0){
                            // alert('123')
                            // api.resetRoute({key:'login'})
                            // api.push({key:'home'})
                            api.showConfirm('Bạn có muốn cập nhật thông tin','Xác nhận','Có','Không',
                            ()=>{
                                api.hideConfirm()
                                api.push({key:'RegUser'})
                            }
                            ,()=>{
                                api.hideConfirm()
                                dataService
                            .getUserInfo(this.props.userState.token)
                            .then((datas) => {
                                console.log('datas', datas);
                                if (datas.err === 0) {
                                    dataService.postAddDevice(this.props.userState.uuid,this.props.userState.uuid,this.props.userState.token)
                                    .then(rs=>{
                                        
                                    })
                                    this
                                        .props
                                        .setUserInfo(datas)
                                        api.resetRoute({key:'login'})
                                        api.push({key:'home'})
                                }
                                 })
                            })
                        }
                        api.showMessage(kq.msg,'Thông báo')
                        api.hideLoading()
                    })
                }else if(rs.err == 2){
                    // api.showMessage(rs.msg,'Thông báo')
                    api.showInputBox(undefined,undefined,rs.msg,this._onConfirmOTP)
                    api.hideLoading()
                }   
        })
    }

    _gotoInfoPage(){
        if(this.props.navState.routes[this.props.navState.index].member_id == undefined)
        if(!api.validatePhone(phoneNumber) || !this._checkPhoneViettel()) {
            api.showMessage('Hãy nhập đúng số điện thoại của Viettel','Thông báo')
            return
        }
        
        if(phoneNumber!==''){
            api.showLoading()
            dataService.postReg(phoneNumber)
            .then(rs=>{
                
                if(rs.err ==0 ){
                    otpID = rs.otpId
                    //alert(rs.otpId)
                    api.showInputBox('Nhập OTP','Nhập OTP','',this._onConfirmOTP.bind(this))
                    // api.setRegInfo('781160c2-f543-4ec8-8f66-b425a38af40c')
                        // api.setUserToken(rs.token)
                    }
                    else{
                        api.showMessage(rs.msg,'Thông báo')
                    }
                    setTimeout(()=>{
                        api.hideLoading()
                    },1)
                })




            // dataService.regGetOTP(phoneNumber,this.props.navState.routes[this.props.navState.index].userCode,this.props.navState.routes[this.props.navState.index].member_id == undefined?undefined:this.props.navState.routes[this.props.navState.index].member_id)
            // .then((data)=>{
            //     if(data.error === 0){ 
            //         api.hideLoading()
            //         api.showInputBox('','Vui lòng điền vào thông tin','Điền vào mã số được gửi tới số điện thoại của quý khách',
            //         (text)=>{
            //             if(text !== ''){
                            
            //                 api.hideInputBox()
            //                 setTimeout(()=>{
            //                     api.showLoading()
            //                 },10 )
            //                 dataService.verifyCode(phoneNumber,text,this.props.userState.uuid)
            //                 .then((data)=>{
            //                     if(data.error === 0){
            //                         // alert(data.token)
            //                         // api.showLoading()
            //                     api.setRegInfo(data.token,'','','','','','','','')
            //                     api.push({key:'RegUser'})
            //                     api.hideInputBox()
            //                     api.hideLoading()
            //                 }
            //                 else{
            //                     // api.showMessage(data.msg,'Thông báo')
            //                     api.showInputBox('',this.props.uiState.inputTitle,'Sai mã OTP',this.props.uiState.inputSubmit)
            //                     api.hideLoading()
                                
            //             }
            //                 })
            //                          // api.showLoading()
            //             }else {
            //                         api.showMessage('Vui lòng điền vào mã','Thông báo')
            //                     }}                        
            //                   )
            //     }else{
            //         api.showMessage(data.msg,'Thông báo')
            //         api.hideLoading()
            //     }
            // }
            // )
            
            }
            else{
              api.showMessage('Vui lòng nhập vào số điện thoại','Thông báo');
              api.hideLoading()
              }               
    }
    render() {
        return (
            <Container>
                <Header
                    style={{
                    backgroundColor: '#009688'
                }}>
                    <Button transparent onPress={()=>this._goBack()}>
                        <Icon name='ios-arrow-back'/>
                    </Button>
                    <Title style={{ alignSelf:'center' } }>Đăng ký hội viên</Title>
                </Header>

                <Content style={{
                    padding: 10
                }}>
                    <Text
                        style={{
                        alignSelf: 'center'
                    }}>Nhập số điện thoại</Text>
                    <Card>
                        <Input placeholder='Số điện thoại' onChangeText={(t)=>phoneNumber=t} keyboardType='phone-pad'/>
                    </Card>
                    <Grid
                        style={{
                        alignSelf: 'center',
                        flex: 1,
                        paddingLeft: 70,
                        paddingRight: 70
                    }}>
                        <Image
                            source={{
                            uri: this.props.navState.routes[this.props.navState.index].image != undefined?this.props.navState.routes[this.props.navState.index].image:'http://mpoint.vn/Uploads/571ae8ca79490viettel.png'
                        }}
                            style={{
                               
                            width: api.getRealDeviceWidth()*(this.props.navState.routes[this.props.navState.index].image == undefined?0.6:0.3),
                            height: api.getRealDeviceWidth()*0.3,
                            alignSelf:'center',
                            resizeMode: 'stretch'
                        }}/>
                    </Grid>
                    <Text
                        style={{
                        alignSelf: 'center',
                        color: '#fa6428'
                    }}>Đăng ký hội viên {this.props.navState.routes[this.props.navState.index].member_name == undefined?'Viettel':this.props.navState.routes[this.props.navState.index].member_name}</Text>
                    <Grid
                        style={{
                        marginTop: 20,
                        marginBottom: 50
                    }}>
                        <Row>
                            <Button
                                style={{
                                flex: 1,
                                backgroundColor: 'gray'
                            }} onPress={()=>this._goBack()}>Quay lại</Button>
                            <Button
                                style={{
                                flex: 1,
                                marginLeft: 15,
                                backgroundColor: '#fa6428'
                            }} onPress={ this._gotoInfoPage.bind(this)}
                               >Tiếp tục</Button>
                        </Row>
                    </Grid>
                </Content>
            </Container>
        )
    }
}
mapStateToProps = (state) => (state)
mapDispatchToProps = (dispatch) => ({
    push: (route)=>dispath(push(route)),
    pop:()=>dispath(pop()),
    setUserInfo: (data) => dispatch(setUserInfo(data)),
})
export default connect(mapStateToProps, mapDispatchToProps)(RegViettel)