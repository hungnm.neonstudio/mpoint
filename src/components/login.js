import {
    Container,
    Header,
    Content,
    Button,
    Icon,
    Title,
    Card,
    Text,
    Grid,
    Input,
    InputGroup,
    Col,
    Row,
    CardItem,
    CheckBox
} from 'native-base'
import React, { Component } from 'react'
import DeviceInfo from 'react-native-device-info'
import {
    Image,
    View,
    Dimensions,
    Alert,
    BackAndroid,
    ToastAndroid,
    Platform,
    AsyncStorage,
    ListView,
    ActivityIndicator,
    TouchableOpacity, ScrollView, Keyboard
} from 'react-native'
import MyCardItem from './tab/cardItem'
import { connect } from 'react-redux'
import { push, pop } from '../actions/navActions'
// import {setShopLocation} from '../actions/locationAction';
import {
    showMessage,
    hideMessageBox,
    showInputBox,
    hideInputBox,
    showLoading,
    hideLoading
} from '../actions/uiActions'
import { setUserInfo, setUserToken, setUuid } from '../actions/userActions'
import request from '../api/request'
import api from '../api';
import dataService from '../api/dataService';
import { IndicatorViewPager, PagerDotIndicator } from 'rn-viewpager';
import i18 from './i18'
// import SplashScreen from 'react-native-splash-screen'
import { stamp } from './nativeModule';
// import RNAccountKit from 'react-native-facebook-account-kit';
//let markers = []
let uuid = DeviceInfo.getUniqueID()
let phone = ''
let otpID = ''
let isLogin = true
let cardId = ''
let setGiftCount = (token) => {
    dataService.getGiftCount(token)
        .then((rs) => {
            api.setGitfCount(rs.err == 0 ? rs.amount : '0')
        })
}
let _showConfirm = () => {
    api.hideLoading()
    api.showConfirm(i18.t('confirmTextLogin'), i18.t('confirmTitleLogin'), i18.t('confirmOKLogin'), i18.t('confirmCancelTextLogin'),
        function onok() {
            api.push({ key: 'RegUser' })
            api.hideConfirm()
        },
        function oncancel() {
            api.showLoading()
            api.resetRoute({ key: 'home' })
            api.hideConfirm()
        }
    )
}
export class Login extends Component {
    constructor(props) {
        super(props)
        this.state = {
            the: '',
            markers: [],
            members: [],
            memberClose: [],
            memberGroup: [],
            isLoading: true
        }
    }

    componentWillMount() {
        let { dontShowConfirm } = this.props.navState.routes[this.props.navState.index]
        setTimeout(() => {
            this.setState({})
        }, 100);
        // RNAccountKit.configure({
        //     titleType: 'login',
        //     defaultCountry: 'VN',
        // })
        api.sendAnalytic('login');
        try {
            AsyncStorage
                .getItem('@phone:key')
                .then((p) => {
                    if (p !== null && p != '') {
                        phone = p
                        this.setState({ the: phone })
                        this._checkLogin()
                    } else {
                        if (!dontShowConfirm) try {
                            AsyncStorage
                                .getItem('first')
                                .then((p) => {
                                    if (p == 'first') {
                                        api.showConfirm(i18.t('helloLogin'), i18.t('titleMsgLogin'),
                                            i18.t('loginButton'), i18.t('viewPromotionBefore'),
                                            () => {
                                                api.hideConfirm();
                                            },
                                            () => {
                                                api.resetRoute({
                                                    key: 'home'
                                                });
                                                api.hideConfirm();
                                            })
                                    }
                                });
                        } catch (error) {
                            console.log('Error when getting data', error);
                        }


                    }
                });
        } catch (error) {
            console.log('Error when getting data', error);
        }
        this.props.setUuid(DeviceInfo.getUniqueID());

    }

    _gotoHome = () => {

    }
    _onRegConfirmOTP = (otp) => {

        // alert(otp+phone)
        if (otp == undefined || otp == '') {
            api.showMessage(i18.t('errOtpMsgLogin'), i18.t('titleMsgLogin'))
            return
        }
        api.hideInputBox()
        setTimeout(() => api.showLoading(), 1)
        dataService.postVerifyOtpRegister(phone, otp, otpID)
            .then((rs) => {
                gotoRegCard = false
                if (rs.err == 0) {
                    try {
                        AsyncStorage.setItem('@RFTK:key', rs.refreshToken);
                        // alert(dt.refreshToken)
                    } catch (error) {
                        console.log('Error when saving data', error);
                    }
                    // api.hideInputBox()
                    dataService.getListShopGPS(rs.token)
                        .then(gps => {
                            api.setLocationData(gps.shops)
                        })
                    api.setUserToken(rs.token);
                    dataService.addFreeCard(rs.token);
                    try {
                        AsyncStorage.setItem('@RFTK:key', rs.refreshToken);
                        // alert(dt.refreshToken)
                    } catch (error) {
                        console.log('Error when saving data', error);
                    }
                    dataService.getListMember(undefined, 1, 0).then(kq => {
                        if (kq.err == 0) {
                            api.setListMember(kq.members);
                        }
                    })
                    if (cardId != '' && cardId) {
                        dataService.postAddCard(cardId, rs.token)
                            .then(dt => {
                                if (dt.err == 0) {
                                    api.showMessage(i18.t('registerDone'), null, i18.t('continue').toUpperCase(),
                                        () => api.gotoHome()
                                    )

                                }
                            })
                    }
                    else {
                        api.showConfirm(i18.t(api.isViettel(phone) ? 'registerViettel' : 'registerMpoint'), i18.t('titleMsgLogin'), i18.t(api.isViettel(phone) ? 'buttonVietttel' : 'buttonmPoint'), i18.t('ortherCard'),
                            () => {
                                api.showConfirm(i18.t('contentConfirmBNA'), i18.t('titleMsgLogin'), i18.t('confirmOKLogin'), i18.t('confirmCancelTextLogin'),
                                    function onok() {
                                        api.push({ key: 'RegUser' });
                                        api.hideConfirm()
                                    },
                                    function oncancel() {
                                        api.gotoHome();
                                        dataService.changeCard(null, api.isViettel(phone) ? 123 : 354).then(rs => { })
                                        api.hideConfirm();
                                    }
                                )
                            },
                            () => {
                                api.push({
                                    key: 'addCardRegister',
                                    phone: phone
                                })
                                api.hideConfirm();
                            }
                        )
                    }
                    phone = '';
                    cardId = '';
                } else if (rs.err == 2) {
                    // api.showMessage(rs.msg,'Thông báo')
                    api.showInputBox(undefined, undefined, rs.msg, this._onConfirmOTP)
                    api.hideLoading()
                }
            })
    }

    _reg = () => {
        api.showLoading();
        dataService.postReg(phone)
            .then(rs => {
                api.hideLoading();
                if (rs.err == 0) {
                    otpID = rs.otpId
                    api.showInputBox(i18.t('placeholderInputLogin'), i18.t('titleInputLogin'), i18.t('contentLeftInputLogin') + phone + ' , ' + i18.t('contentRightInputLogin'), this._onRegConfirmOTP)
                }
                else {
                    api.showMessage(rs.msg)
                }
            })
    }
    _gotoReg = () => {
        // api.showConfirm(i18.t('confirmContentLogin_gotoReg'), i18.t('titleMsgLogin'), i18.t('buttonTextLogin'), i18.t('confirmCancelText_gotoReg'),
        //     function gotoreg() {
        // api.push({ key: 'newRegCard', phone: phone })
        dataService.getUserInfo(undefined).then(rs => {
            api.setUserInfo(rs);
        });
        // dataService.getListMember(undefined,1,0).then(rs=>{
        //     if(rs.err == 0) {
        //         api.setListMember(rs.members);
        //     }
        // })
        api.push({ key: 'selectCard', phone: phone })

        //         api.hideConfirm()
        //     },
        //     function cancel() {
        //         api.hideConfirm()
        //     }
        // )
    }

    _gotoPromotion = () => {

        api.resetRoute({ key: 'home' })
        // if(!isLogin) api.hideLoading()
    }
    _onConfirmOTP(otp) {
        if (otp == undefined || otp == '') {
            api.showMessage(i18.t('contentMsgLogin_onConfirmOTP'))
            return
        }
        api.hideInputBox()
        setTimeout(() => api.showLoading(), 1)
        dataService
            .postVerifyOtpLogin(otpID, otp, DeviceInfo.getUniqueID())
            .then(rs => {
                // api.hideLoading()
                if (rs.err == 0) {
                    try {
                        AsyncStorage.setItem('@RFTK:key', rs.refreshToken);
                        // alert(dt.refreshToken)
                    } catch (error) {
                        console.log('Error when saving data', error);
                    }
                    dataService.getListShopGPS(rs.token)
                        .then(gps => {
                            api.setLocationData(gps.shops)
                        })
                    api.setUserToken(rs.token)
                    if (cardId) {
                        dataService.postAddCard(cardId, rs.token)
                            .then(dt => {
                                api.showMessage(`Chúc mừng bạn đã thêm thẻ ${dt.data.shortName} thành công`);
                                api.gotoHome()

                            });

                    } else {
                        api.gotoHome();
                    }
                    phone = ''; cardId = '';

                } else if (rs.err == 2) {
                    api.hideLoading();
                    api.showInputBox(undefined, undefined, rs.msg, this._onConfirmOTP.bind(this))
                } else {
                    // if(rs.err == 3){
                    api.hideLoading()
                    api.showMessage(rs.msg);
                    // api.showInputBox(undefined, undefined, rs.msg, this._onConfirmOTP.bind(this))
                }
            })
    }
    to = null
    _checkLogin = async () => {
        Keyboard.dismiss()
        clearTimeout(this.to)
        //Account kit
        // RNAccountKit.loginWithPhone(phone)
        // .then((token) => {
        //   if (!token) {
        //     console.log('Login cancelled')
        //   } else {
        //     console.log('Token Account Kit',token)
        //   }
        // })
        if (phone != '' && phone != undefined && phone.search(' ') == -1) {
            api.showLoading()
            dataService.getUserToken(phone, DeviceInfo.getUniqueID())
                .then((dt) => {
                    isLogin = false
                    console.log("Data", dt);
                    dataService.getListShopGPS(dt.token)
                        .then(gps => {
                            api.setLocationData(gps.shops)
                        })
                    if (dt.err == 0) {
                        try {
                            AsyncStorage.setItem('@RFTK:key', dt.refreshToken);
                            // alert(dt.refreshToken)
                        } catch (error) {
                            console.log('Error when saving data', error);
                        }
                        dataService.getListMember(dt.token, 1, 0).then(kq => {
                            if (kq.err == 0) {
                                api.setListMember(kq.members);
                            }
                        })
                        api.setUserToken(dt.token);
                        dataService
                            .getUserInfo(dt.token)
                            .then((datas) => {
                                if (datas.err === 0) {
                                    setGiftCount(dt.token);
                                    api.setUserInfo(datas);
                                    api.resetRoute({ key: 'home' });
                                    phone = '';
                                }
                                // api.hideLoading()
                            })
                        //setTimeout(this._gotoPromotion(), 1000);

                    } else if (dt.err == 4) {
                        api.hideLoading();
                        if (!api.isViettel(phone)) {
                            let p = '16';
                            if (api.validatePhone(phone)) p = phone.substr(1, phone.length);
                            dataService.getAccountKitToken((payload) => {
                                console.log('====================================')
                                console.log(payload)
                                console.log('====================================')
                            }, p);
                        }
                        else {
                        otpID = dt.otpId
                        // alert(otpID)
                        api.showInputBox(i18.t('inputPlaceholderLogin_checkLogin'), i18.t('titleInputLogin'), i18.t('contentLeftInputLogin') + phone + ' , ' + i18.t('contentRightInputLogin'), this._onConfirmOTP.bind(this))
                        }
                    } else
                        if (dt.err == 2) {
                            if (api.validatePhone(phone)) {
                                this._reg();
                                // this._gotoReg()
                            }
                            else {
                                api.showMessage(dt.msg, i18.t('titleMsgLogin'))
                                api.hideLoading()
                            }
                        } else if (dt.err == 3) {
                            if (!api.isViettel(phone)) {
                                let p = '16';
                                if (api.validatePhone(phone)) p = phone.substr(1, phone.length);
                                dataService.getAccountKitToken((payload) => {
                                    console.log('====================================')
                                    console.log(payload)
                                    console.log('====================================')
                                }, p);
                            } else {
                            cardId = phone
                            api.hideLoading()
                            this._input(dt.msg)
                            }
                        } else {
                            api.hideLoading();
                            api.showMessage(dt.msg);
                        }

                })
        } else {
            api.showMessage(i18.t('msgContentLoginErr__checkLogin'), i18.t('titleMsgLogin'))
            isLogin = false
        }

        this.to = setTimeout(() => {
            if (isLogin) {
                api.hideLoading()
                api.showMessage(i18.t('timeoutTextMsgContentLogin'), i18.t('titleMsgLogin'))
            }
            isLogin = true
        }, 15000)
    }
    _input(msg) {
        api.showInputBox(i18.t('inputPlaceholderSDT'), i18.t('titleMsgLogin'), msg,
            (sdt) => {
                phone = sdt
                if (api.validatePhone(phone))
                    dataService.getUserToken(phone, DeviceInfo.getUniqueID())
                        .then(rs => {
                            if (rs.err == 0) {
                                try {
                                    AsyncStorage.setItem('@RFTK:key', rs.refreshToken);
                                    // alert(dt.refreshToken)
                                } catch (error) {
                                    console.log('Error when saving data', error);
                                }

                                api.showLoading()
                                if (cardId) {
                                    dataService.postAddCard(cardId, rs.token)
                                        .then(kq => {
                                            if (kq.err == 0) {
                                                api.showMessage(`Chúc mừng bạn đã thêm thẻ ${kq.data.shortName} thành công`);
                                                api.gotoHome();
                                            }
                                            else {
                                                api.showMessage(kq.msg, i18.t('titleMsgLogin'))
                                            }
                                        })
                                } else {
                                    api.gotoHome();
                                }
                                phone = ''; cardId = '';
                            } else if (rs.err == 2) {
                                this._reg()
                            }
                            else if (rs.err == 4) {
                                otpID = rs.otpId
                                // this._onConfirmOTP();
                                api.showInputBox(i18.t('inputPlaceholderLogin_checkLogin'), i18.t('titleInputLogin'), i18.t('contentLeftInputLogin') + phone + ' , ' + i18.t('contentRightInputLogin'), this._onConfirmOTP.bind(this))

                            }
                        })
                else {
                    api.showMessage(i18.t('msgInputPhoneErr_input'), i18.t('titleMsgLogin'))
                    this._input(msg)
                }
            }
        )
    }

    ds = new ListView.DataSource({
        rowHasChanged: (r1, r2) => r1 !== r2
    });

    render() {
        return (
            <View style={{ height: api.getRealDeviceHeight() }}>
                <Image
                    source={require('../img/bg_lg.jpg')}
                    style={{ width: api.getRealDeviceWidth(), resizeMode: 'stretch', height: api.getRealDeviceHeight() }}
                >
                    <View>
                        <Image
                            style={{
                                height: api.getRealDeviceWidth() / 5,
                                width: api.getRealDeviceWidth() / 5 * 2.13,
                                alignSelf: 'center',
                                marginTop: 20
                            }}
                            source={require('../img/logo_deperecate.png')} />
                        <Text
                            style={{
                                color: '#fff',
                                marginTop: 5,
                                alignSelf: 'center',
                                fontWeight: '500',
                                marginBottom: 10
                            }}>{i18.t('headLogin')}</Text>
                    </View>
                    <ScrollView
                        keyboardShouldPersistTaps={'handled'}
                        style={{ minHeight: api.getRealDeviceHeight() * 1.2 }}
                    >
                        <View
                            style={{
                                padding: 10,
                                backgroundColor: '#fff',
                                //borderTopWidth: 1,
                                margin: 10,
                                borderRadius: 10,
                                flexDirection: 'column'
                            }}>
                            <Text
                                style={{
                                    textAlign: 'center',
                                    color: 'gray',
                                    lineHeight: 25,
                                    fontSize: Platform.OS == 'ios' ? 13 : 15
                                }}>{i18.t('bodyLogin')}</Text>

                            <View
                                style={{
                                    marginTop: 20,
                                    borderRadius: 2,
                                    marginBottom: 10,
                                    width: api.getRealDeviceWidth() * 0.8,
                                    alignSelf: 'center',
                                    minHeight: 90
                                }}>
                                <Input
                                    returnKeyLabel={i18.t('buttonTextLogin')}
                                    onSubmitEditing={() => { this._checkLogin() }}
                                    placeholderTextColor='gray'
                                    placeholder={i18.t('placeholderLogin')}
                                    onChangeText={(text) => { phone = text; this.setState({ the: text }) }}
                                    defaultValue={phone}
                                    style={{
                                        width: api.getRealDeviceWidth() * 0.8,
                                        backgroundColor: '#fff',
                                        borderColor: '#cdcdcd',
                                        borderWidth: 1,
                                        alignSelf: 'center',
                                        fontSize: Platform.OS == 'ios' ? 13 : 15
                                    }} />
                                <Button
                                    style={{
                                        backgroundColor: '#fa6428',
                                        //flex: 1,
                                        //width: null
                                        width: api.getRealDeviceWidth() * 0.8,
                                        alignSelf: 'center'
                                    }}
                                    onPress={this._checkLogin}>
                                    <Text
                                        style={{
                                            color: 'white',
                                            fontSize: Platform.OS == 'ios' ? 13 : 15
                                        }}>{(i18.t('buttonTextLogin')).toUpperCase()}</Text>
                                </Button>
                            </View>

                        </View>
                    </ScrollView>
                </Image>
                <View
                    style={{ bottom: 20, position: 'absolute', width: api.getRealDeviceWidth() }}
                >
                    <Text onPress={() => { api.push({ key: 'newRegCard' }) }} style={{ textDecorationLine: 'underline', color: '#fff', color: '#fff', textAlign: 'center', alignSelf: 'center', fontSize: Platform.OS == 'ios' ? 13 : 15 }}>

                        {i18.t('listMemberCard')}

                        {/* <Text
                        {i18.t('footerText')}
                            style={{ textDecorationLine: 'underline', color: '#fff' }}
                            onPress={() => { api.push({ key: 'newRegCard' }) }}
                        >{i18.t('linkText')}
                        </Text>  */}
                    </Text>
                </View>
            </View>
        )
    }
}

mapStateToProps = (state) => {
    return { userState: state.userState, navState: state.navState, lang: state.langState.lang }
}
mapDispatchToProps = (dispatch) => ({
    push: (route) => dispatch(push(route)),
    showmess: () => dispatch(showMessage('1', '2')),
    pop: () => dispatch(pop()),
    showloading: () => dispatch(showLoading()),
    hideloading: () => dispatch(hideLoading()),
    setUserInfo: (data) => dispatch(setUserInfo(data)),
    setUserToken: (token) => dispatch(setUserToken(token)),
    setUuid: (uuid) => dispatch(setUuid(uuid)),
    setShopLocation: (markers) => dispatch(setShopLocation(markers))
})
export default connect(mapStateToProps, mapDispatchToProps)(Login)