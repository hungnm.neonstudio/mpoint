import React,{Component} from 'react'
import {View,Text,Platform} from 'react-native'
import ModalDropdown from 'react-native-modal-dropdown';
import api from '../api'
import {Button,Input,InputGroup,Container,Header,Icon,Title,Content} from 'native-base'
import dataService from '../api/dataService'
import {connect} from 'react-redux'
import DeviceInfo  from 'react-native-device-info'
import i18 from './i18'
import {setUserInfo, setUserToken} from '../actions/userActions'
let Province =''
let District =''
let Ward =''
let Address =''
class RegAddress extends Component{
    constructor(props){
        super(props)
        this.state={
            listprovince:dataService.getListProvinces(),
            listdistrict:dataService.getListDistricts(this.props.userState.data.endUser.provinceId),
            listward:dataService.getListWards(this.props.userState.data.endUser.districtId),
            province:'',
            district:'',
            ward:'',
        }
        Address = this.props.userState.data.endUser!= undefined?this.props.userState.data.endUser.address:''
        Province = this.props.userState.data.endUser!= undefined?this.props.userState.data.endUser.provinceId:''
        District = this.props.userState.data.endUser!= undefined?this.props.userState.data.endUser.districtId:''
        Ward = this.props.userState.data.endUser!= undefined?this.props.userState.data.endUser.wardId:''
        
        // dataService.getListProvinces()
        // .then((data)=>{

        //     this.setState({
        //         listprovince:data,
        //         listdistrict:[{id:'',name:'Chưa có dữ liệu'}],
        //         listward:[{id:'',name:'Chưa có dữ liệu'}]
        //     })
        // })
    }
    // Province
    _getProvinceList() {
    return this.refs['PROVINCELIST'];
  }
  componentWillUnMount(){
      Province = ''
      District = ''
      Ward = ''
  }
componentWillMount(){
   setTimeout(()=>{
        this.state.listprovince.map(item=>{
        if(parseInt(item.id) == Province){
                    this.setState({province:item.name})
        }
    })
        this.state.listdistrict.map(item=>{
        if(parseInt(item.id) == District){
                    this.setState({district:item.name})
        }
    })
     this.state.listward.map(item=>{
        if(parseInt(item.id) == Ward){
                    this.setState({ward:item.name})
        }
    })
   },1000)
}
  _selectProvice(i) {
      Province=this.state.listprovince[i].id
      this.setState({listdistrict:dataService.getListDistricts(Province),province:this.state.listprovince[i].name})
        setTimeout(()=>{
        this._selectDistrict(0)
    },500)
    // dataService.getListDistricts(this.state.listprovince[i].id)
    // .then((data)=>{
        
    //     if(data!==undefined && data!==null )
    //     this.setState({
    //         listdistrict:data
    //     })
    // })
  }

  // district
   _selectDistrict(i) {
    District=this.state.listdistrict[i].id
  this.setState({
      district:this.state.listdistrict[i].name,
      districtId:this.state.listdistrict[i].id,
      listward:dataService.getListWards(District),
  })
  setTimeout(()=>{
        this._selectWard(0)
    },500)
//   District=this.state.listdistrict[i].id
    // dataService.getListWards(this.state.listdistrict[i].id)
    // .then((data)=>{
    //     if(data!==undefined && data!==null )
    //     this.setState({
    //         listward:data
    //     })
    // })
  }
  // Ward
   _selectWard(i) {
    Ward=this.state.listward[i].id
    this.setState({
        wardId:this.state.listward[i].id,
        ward:this.state.listward[i].name
    })
  }
  _onCancel(msg){
      dataService
                            .getUserInfo(this.props.userState.token)
                            .then((datas) => {
                                console.log('datas', datas);
                                if (datas.err === 0) {
                                    dataService.postAddDevice(DeviceInfo.getUniqueID(),DeviceInfo.getUniqueID(),this.props.userState.token)
                                    .then(rs=>{
                                        
                                    })
                                    this
                                        .props
                                        .setUserInfo(datas)
                                        api.resetRoute({key:'home'})
                                        //api.push({key:'home'})
                                        if(msg!= undefined){
                                            api.showMessage(msg,i18.t('titleMsgLogin'))
                                        }
                                }
                                 })
  }
 _rePlaceDoubleSpace(str){
     if(str == null) return ''
     let s = str
    while(s.search('  ')!=-1){
       s =  s.replace('  ',' ')
    }
    return s.trim();
 }
 _onPress(){
    //  console.log(this.props.token)
    //  alert(this.props.regState.name)
        Address  = this._rePlaceDoubleSpace(Address)
     if(Address=='') {api.showMessage(i18.t('blankADD'),i18.t('titleMsgLogin'));return  }
     if(Province==''){api.showMessage(i18.t('blankProvince'),i18.t('titleMsgLogin'));return  }
     if(District==''){api.showMessage(i18.t('blankDistrict'),i18.t('titleMsgLogin'));return  }
     if(Ward==''){api.showMessage(i18.t('blankWard'),i18.t('titleMsgLogin'));return  }
    //  api.setRegInfo('','',Address,'','','',Province,District,Ward)
    api.showLoading()

    // dataService.postUpdateUserInfo(this.props.regState.name,Address,this.props.regState.sex,this.props.regState.dob,this.props.regState.email,District,this.props.userState.token)
    dataService.postUpdateUserInfo(this.props.regState.name,Address,this.props.regState.sex,this.props.regState.dob,this.props.regState.email,District,Province,Ward,'none',this.props.userState.token)
    .then(rs=>{
        if(rs.err == 0){
            this._onCancel(rs.msg)
        }
        else{
            api.hideLoading()
            api.showMessage(rs.msg)
        }
        
        // api.showMessage(rs.msg,'Thông báo')
    })

    // dataService.updateUserInfo(this.props.token,this.props.name,Address,this.props.dob,this.props.email,this.props.sex,Province,District,Ward)
    // .then((data)=>{
    //     if(data.error ===0 ){
    //         // api.hideLoading()
    //         this.props.setUserToken(this.props.token);
    //         dataService.getUserInfo(this.props.token)
    //         .then((datas) => {
    //         if (datas.error === 0) {
    //            this.props.setUserInfo(datas.activeMember, datas.activeCode, datas.user_code, datas.data, datas.giftCount, datas.mLoyalty, datas.listGroups)
    //             api.push({key: 'home'})
    //             api.hideLoading()
    //             }})
    //     }else
    //     {   
    //         api.showMessage(data.msg+this.props.token,'Thông báo')
    //         api.hideLoading()
    //     }
    // })
 }
 _renderList=(arr)=>{
     if(arr !== undefined && arr.length >0){
         return arr.map((item)=>(
             item.name
         ))
     }
 }
    render() {
        
        if(this.state !== null)
    return (<Container>
                <Header
                    style={{
                    backgroundColor: '#009688'
                }}>
                    <Button
                        transparent
                        onPress={() => {
                        api.pop()
                    }}>
                        <Icon name='ios-arrow-back' style={{ color:Platform.OS == 'ios'?'#000':'#fff' }}/>
                    </Button>
                    <Title style={{ alignSelf:'center' } }>{i18.t('inputADD')}</Title>
                </Header>
                <Content
                    style={{
                    paddingLeft: 10,
                    paddingRight: 10,
                    flex: 1,
                    height:api.getRealDeviceHeight()
                }}>
      <View
        style={{ height:api.getRealDeviceHeight()-70 }}
      >

            <View style= {{ borderColor:'gray',borderWidth:1 ,marginLeft:10,marginRight:10,borderRadius:3,marginTop:20}}>
            <View style={{ flexDirection:'row',borderBottomColor:'gray',borderBottomWidth:1 }}>
                <Icon name='ios-home-outline' style={{ marginLeft:10,marginTop:5 }}/>
                <Input onChangeText={(text)=>Address=text} style={{ color:'gray',paddingLeft:10,paddingRight:10 }}
                    placeholder={i18.t('addr')}
                    placeholderTextColor='gray'
                    defaultValue = {Address}
                />
            </View>
            <View style={{ flexDirection:'row' }}>
            <ModalDropdown 
            dropdownTextStyle={{ fontSize:13 }}
            options={this._renderList(this.state.listprovince)}
            defaultValue={(this.state.province!=''?this.state.province:i18.t('chooseProvince'))+' ▼'}
            onSelect={(id)=>this._selectProvice(id)}
            textStyle={{ fontSize:15 }}
            style={{ padding:10,borderLeftWidth:0.5,borderLeftColor:'gray' }}
            dropdownStyle={{ width:api.getRealDeviceWidth()*0.7,height:api.getRealDeviceHeight()*0.7 }}
          />
            </View><View  style={{ flexDirection:'row',borderColor:'gray',borderBottomWidth:0.5,borderTopWidth:0.5 }}>
          <ModalDropdown 
          dropdownTextStyle={{ fontSize:13 }}
            options={this._renderList(this.state.listdistrict)}
            defaultValue={(this.state.district!=''?this.state.district:i18.t('chooseDistrict'))+' ▼'} //'Chọn quận huyện ▼'            onSelect={(id)=>this._selectDistrict(id)}
            textStyle={{ fontSize:15, }}
             onSelect={(id)=>this._selectDistrict(id)}
            style={{ padding:10,borderLeftWidth:0.5,borderLeftColor:'gray' }}
            dropdownStyle={{ width:api.getRealDeviceWidth()*0.7,height:api.getRealDeviceHeight()*0.6 }}
          /></View><View  style={{ flexDirection:'row' }}>
          <ModalDropdown 
          dropdownTextStyle={{ fontSize:13 }}
            options={this._renderList(this.state.listward)}
             onSelect={(id)=>this._selectWard(id)}
            defaultValue={(this.state.ward!=''?this.state.ward:i18.t('chooseWard'))+' ▼'}            onSelect={(id)=>this._selectWard(id)}
            textStyle={{ fontSize:15 }}
            style={{ padding:10,borderLeftWidth:0.5,borderLeftColor:'gray' }}
            dropdownStyle={{ width:api.getRealDeviceWidth()*0.7,height:api.getRealDeviceHeight()*0.5 }}
          /></View>
            </View>
          <View style={{ flexDirection:'row',flex:1 ,paddingLeft:20,paddingRight:20,marginTop:20}}>
            <Button  style={{ flex:1,backgroundColor:'gray'}} onPress={()=>{ api.pop() }}>{i18.t('confirmCancelText_gotoReg')}</Button>
            <Button  style={{ flex:1,marginLeft:20,backgroundColor:'#fa6428'}} onPress={this._onPress.bind(this)}>{i18.t('buttonTextLogin')}</Button>
            </View>
            <View
        style={{ bottom:0 }}
      >
          <Button
          style={{ paddingHorizontal:api.getRealDeviceWidth()/3,alignSelf:'center',backgroundColor:'#009688' }}
          onPress={()=>{ api.resetRoute({key:'home'}) } }
      >
          <Text
        style={{ color:'#fff',alignSelf:'center'}}
      >
         {i18.t('confirmCancelTextLogin')}
        </Text>
      </Button>
      </View>
      </View>
      
      </Content>
            </Container>
    ); else return false
    }
}
mapStateToProps=(state)=>(
    state
)

mapDispatchToProps=(dispatch)=>{
    return{
        setUserToken: (token) => dispatch(setUserToken(token)),
        setUserInfo: (activeMember, activeCode, user_code, data, giftCount, mLoyalty, listGroups) => dispatch(setUserInfo(activeMember, activeCode, user_code, data, giftCount, mLoyalty, listGroups)),
        // setShopLocation: (markers) => dispatch(setShopLocation(markers))
    }
}
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(RegAddress)

