import { NativeModules } from 'react-native';
import i18 from './i18';
let scheme = 'ckit'

export const stamp = {
    getEchosserviceUrl: (regionCode, merchantCode, licenseId, authKey, userCode, done, err) => {
        let info = {
            regionCode: regionCode,
            merchantCode: merchantCode,
            licenseId: licenseId,
            authKey: authKey
        }
        NativeModules.stamp.setBackgroundOpacity('0.2');
        stamp.setDescription(i18.t('descriptionStamp'));        
        NativeModules.stamp.setLoadingYn('Y');
        return NativeModules.stamp.getEchosserviceUrl(regionCode, scheme, merchantCode, userCode, licenseId, authKey, done, err);
    },
    setBackgroundColor: (color) => {
        return NativeModules.stamp.setBackgroundColor(color);
    },
    setBackgroundOpacity:(opacity)=>{
        return NativeModules.stamp.setBackgroundOpacity(opacity);
    },
    setDescription: (des) => {
        return NativeModules.stamp.setDescription(des);
    },
    setLoadingYn: (bool) => {
        return NativeModules.stamp.setLoadingYn(bool);
    },
    showToast: (msg) => {
        return NativeModules.stamp.showMessage(msg);
    }
}