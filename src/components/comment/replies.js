import React, { Component } from 'react'
import {
    Header,
    Container,
    Content,
    Button,
    Title,
    Icon,
    List,
    Card,
    Col,
    Row,
    Input,
    InputGroup
} from 'native-base'
import i18 from '../i18'

import dataService from '../../api/dataService'
import { View, Text, ActivityIndicator, Image, ListView, Platform } from 'react-native'
import { connect } from 'react-redux'
import api from '../../api'
let num_load = 10
let userInfo = {}
class Comment extends Component {
    constructor(props) {
        super(props)
        this.state = ({
            item:[
                {
                    content:"cảm ơn bạn nhiều",
                    createdAt:"2017-06-10T05:11:23.000Z",
                    id:172,
                    name:'Hung'
                },
                {
                    content:"cảm ơn bạn nhiều",
                    createdAt:"2017-06-10T05:11:23.000Z",
                    id:172,
                    name:'Hung'
                },
                {
                    content:"cảm ơn bạn nhiều",
                    createdAt:"2017-06-10T05:11:23.000Z",
                    id:172,
                    name:'Hung'
                },
                {
                    content:"cảm ơn bạn nhiều",
                    createdAt:"2017-06-10T05:11:23.000Z",
                    id:172,
                    name:'Hung'
                }
            ]
        })
    }
    componentDidMount() {
       
    }
    render() {
    if(!this.props.data) return null
    const ds = new ListView.DataSource({
        rowHasChanged: (r1, r2) => r1 !== r2
    });
    if(this.props.data.length > 0)  return <ListView
                style={{ paddingVertical: 5,paddingLeft:15 }}
                enableEmptySections={true}
                dataSource={ds.cloneWithRows(this.props.data)}
                renderRow={(item, sectionID, rowID) => {
                    let {avatar,name} = item.endUser
                    return (
                        <View style={{ paddingVertical:5,flex:1,borderBottomColor:'#cdcdcd',borderBottomWidth:1 }}>
                            <View style={{ flexDirection:'row' }}>
                                <Image
                                    source={{ uri:avatar }}
                                    style={{
                                        width: api.getRealDeviceWidth() / 12,
                                        height: api.getRealDeviceWidth() / 12,
                                        borderRadius: api.getRealDeviceWidth() / 24,
                                        marginRight:10
                                    }} 
                                />
                                <View style={{ flex:1 }}>
                                    <View style={{ flexDirection:'row' }}>
                                        <Text style={{ flex:1,color:'#4b71de' }}>{name?name:i18.t('nullShopKeeper')}</Text>
                                        <Text style={{ fontSize:12 }}>{api.getTime(item.createdAt,'HH:mm DD/MM/YYYY')}</Text>
                                    </View>
                                    <Text>{item.content}</Text>
                                </View>
                            </View>
                            
                        </View>
                    );
                }} />
    else return null
    }
}

mapStateToProps = (state) => ({ navState: state.navState, userState: state.userState })
mapDispatchToProps = (dispatch) => ({})

export default connect(mapStateToProps, mapDispatchToProps)(Comment)