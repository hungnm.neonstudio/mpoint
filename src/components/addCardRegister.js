import {
    Container,
    Header,
    Content,
    Button,
    Icon,
    Title,
    Card,
    Text,
    Grid,
    Input,
    InputGroup,
    Col,
    Row,
    CardItem,
    CheckBox
} from 'native-base'
import React, { Component } from 'react'
import DeviceInfo from 'react-native-device-info'
import {
    Image,
    View,
    Dimensions,
    Alert,
    BackAndroid,
    ToastAndroid,
    Platform,
    AsyncStorage,
    ListView,
    ActivityIndicator,
    TouchableOpacity, ScrollView, Keyboard
} from 'react-native'
import MyCardItem from './tab/cardItem'
import { connect } from 'react-redux'
import { push, pop } from '../actions/navActions'
// import {setShopLocation} from '../actions/locationAction';
import {
    showMessage,
    hideMessageBox,
    showInputBox,
    hideInputBox,
    showLoading,
    hideLoading
} from '../actions/uiActions'
import { setUserInfo, setUserToken, setUuid } from '../actions/userActions'
import request from '../api/request'
import api from '../api'
import dataService from '../api/dataService';
import { IndicatorViewPager, PagerDotIndicator } from 'rn-viewpager';
import i18 from './i18'
// import SplashScreen from 'react-native-splash-screen'
import { Analytics, Hits } from 'react-native-google-analytics';
//let markers = []
let gotoUpdate = () => { }
let gotoPromotion = () => { }
export class Login extends Component {
    constructor(props) {
        super(props)
        this.state = {
            card: '',
            memberReged: [],
            members: [],
            memberClose: [],
            memberGroup: [],
            isLoading: true
        }
    }

    componentWillMount() {
        api.hideLoading();
    }
    _onInsert = () => {
        if (!this.state.card) return api.showMessage(i18.t('blankCard'));
        api.showLoading();
        // let { phone } = this.props.navState.routes[this.props.navState.index];
        dataService.postAddCard(this.state.card)
            .then(rs => {
                api.hideLoading();
                if (rs.err == 0) {
                    dataService.getUserInfo()
                        .then(o => {
                            if (o.err == 0) {
                                api.setUserInfo(o);
                                api.showConfirm(rs.msg, i18.t('updateUserInfo'), i18.t('update'), i18.t('openCard'),
                                    function OK() {
                                        api.push({ key: 'RegUser' });
                                        api.hideConfirm()
                                    },
                                    function Cancel() {
                                        setTimeout(() => {
                                            api.showLoading();
                                        }, 50);
                                        dataService.getGiftCount()
                                            .then((rs) => {
                                                api.setGitfCount(rs.err == 0 ? rs.amount : '0')
                                            });
                                        dataService.getListShopGPS()
                                            .then(gps => {
                                                api.setLocationData(gps.shops)
                                            });
                                        dataService.getUserInfo().then(rs => {
                                            if (rs.err == 0) {
                                                api.setUserInfo(rs);
                                                api.push({ key: 'home' });
                                            } else {
                                                api.showMessage(rs.msg);
                                            }
                                        })
                                        api.hideConfirm();

                                    }
                                )
                            }
                        })
                    dataService.getListMember(undefined, 1, 0).then(kq => {
                        if (kq.err == 0) {
                            api.setListMember(kq.members);

                        }
                    })
                } else {
                    api.showMessage(rs.msg);
                }
            })
    }
    render() {
        let minus = Platform.OS == 'ios' ? 0 : 4
        let fontSize = api.getRealDeviceWidth() > 320 ? 13 : 12;
        let marginTop = api.getRealDeviceWidth() < 320 ? -8 - minus : 0 - minus;
        let imgHeight = api.getRealDeviceWidth() > 320 ? 25 : 23;
        let { phone } = this.props.navState.routes[this.props.navState.index];
        return (
            <Container>
                <Header
                    style={{
                        backgroundColor: '#009688'
                    }}>
                    <Button
                        transparent
                        onPress={() => {
                            api.pop()
                        }}>
                        <Icon name='ios-arrow-back' style={{ color: Platform.OS == 'ios' ? '#000' : '#fff' }} />
                    </Button>
                    <Title style={{ alignSelf: 'center' }}>{i18.t('titleAddCardPage')}</Title>
                </Header>

                <Image
                    source={require('../img/bg_login.jpg')}
                    style={{ width: api.getRealDeviceWidth(), height: api.getRealDeviceHeight() - 56 }}
                >
                    <View>
                        <Image
                            style={{
                                height: api.getRealDeviceWidth() / 5,
                                width: api.getRealDeviceWidth() / 5 * 2.13,
                                alignSelf: 'center',
                                marginTop: 20
                            }}
                            source={require('../img/logo_deperecate.png')} />
                        <Text
                            style={{
                                color: '#fff',
                                marginTop: 5,
                                alignSelf: 'center',
                                fontWeight: '500',
                                marginBottom: 10
                            }}>{i18.t('headLogin')}</Text>
                    </View>

                    <ScrollView
                        keyboardShouldPersistTaps={'handled'}
                    >
                        <View
                            style={{
                                padding: 10,
                                backgroundColor: '#fff',
                                //borderTopWidth: 1,
                                flexDirection: 'column',
                                //width:api.getRealDeviceWidth()*0.8,
                                margin: 10,
                                borderRadius: 10,
                                //alignSelf:'center' 
                            }}>
                            <Text
                                style={{
                                    textAlign: 'center',
                                    color: 'gray',
                                    lineHeight: 25
                                }}>{i18.t('registerCard')}</Text>

                            <View
                                style={{
                                    marginTop: 10,
                                    borderRadius: 2,
                                    marginBottom: 10,
                                    width: api.getRealDeviceWidth() * 0.8,
                                    //minWidth: 300,
                                    alignSelf: 'center',
                                    minHeight: 90
                                }}>
                                <Input
                                    returnKeyLabel={i18.t('addcard')}
                                    onSubmitEditing={() => { this._onInsert() }}
                                    placeholderTextColor='gray'
                                    placeholder={i18.t('placeholderIputCard').toUpperCase()}
                                    onChangeText={(text) => { this.setState({ card: text }) }}
                                    //defaultValue={phone}
                                    style={{
                                        width: api.getRealDeviceWidth() * 0.8,
                                        backgroundColor: '#fff',
                                        borderColor: '#cdcdcd',
                                        borderWidth: 1,
                                        alignSelf: 'center',
                                        textAlign: 'center'
                                    }} />
                                <View
                                    style={{
                                        flexDirection: 'row', marginTop: 10
                                    }}>
                                    <Button
                                        style={{
                                            backgroundColor: '#fa6428',
                                            width: api.getRealDeviceWidth() * 0.8,
                                        }}
                                        onPress={this._onInsert.bind(this)}

                                    >
                                        <Text
                                            style={{
                                                color: 'white'
                                            }}>{(i18.t('continue')).toUpperCase()}</Text>
                                    </Button>
                                </View>
                            </View>
                        </View>


                    </ScrollView>
                    <Button
                        transparent
                        style={{
                            alignSelf: 'center',
                            margin: 2
                        }}
                        onPress={() => {
                            api.push({ key: 'selectCard', phone: phone })
                        }}

                    >
                        <Text
                            style={{
                                color: 'white',
                            }}>{i18.t('footerAddCard')}
                            <Text style={{
                                color: 'white',
                                textDecorationLine: 'underline', fontStyle: 'italic'
                            }}>
                                {i18.t('tapHere')}
                            </Text>
                        </Text>
                    </Button>
                </Image>

            </Container>
        )
    }
}

mapStateToProps = (state) => {
    return { userState: state.userState, navState: state.navState, lang: state.langState.lang }
}
mapDispatchToProps = (dispatch) => ({
    push: (route) => dispatch(push(route)),
    showmess: () => dispatch(showMessage('1', '2')),
    pop: () => dispatch(pop()),
    showloading: () => dispatch(showLoading()),
    hideloading: () => dispatch(hideLoading()),
    setUserInfo: (data) => dispatch(setUserInfo(data)),
    setUserToken: (token) => dispatch(setUserToken(token)),
    setUuid: (uuid) => dispatch(setUuid(uuid)),
    setShopLocation: (markers) => dispatch(setShopLocation(markers))
})
export default connect(mapStateToProps, mapDispatchToProps)(Login)