import React, {Component} from 'react';
import {
    Image,
    View,
    Text,
    TouchableOpacity,
    ListView,
    Modal,
    ScrollView,
    ActivityIndicator,Platform
} from 'react-native';
import {ListItem, Content, Container, Button} from 'native-base';
import {connect} from 'react-redux';
import dataService from '../../api/dataService';
import api from '../../api';
import {setUserInfo, setListMember} from '../../actions/userActions';
import {push} from '../../actions/navActions';
import MyCardItem from './cardItem';
import i18 from '../i18'
var images = {
    newCard: require('../../img/new-card.png'),
    listCard: require('../../img/list-card.png')
}
let gotoPromotion
class AnotherCard extends Component {
    constructor(props) {
        super(props);
        gotoPromotion = this.props.gotoPromotion
        this.state = {
            displayListCard: false,
            isLoading: false
        }
    }
    
    _selectCard(item) {
        if(!api.requestLogin()){ return; }
        this.setState({isLoading:true})
        token = this.props.userState.token
        api.showLoading()
        dataService
            .changeCard(this.props.userState.token, item.memberCards[0].id)
            .then((data) => {
                 dataService.getGiftCount(token)
    .then((rs)=>{
            api.setGitfCount(rs.err==0?rs.amount:'0')
    })
            dataService.getListShopGPS(token,1)
                                                .then(gps=>{
                                                    api.setLocationData(gps.shops)
                                                })
        this.setState({isLoading:false})
                dataService
                    .getUserInfo(this.props.userState.token)
                    .then((datas) => {
                        if (datas.err === 0) {
                            this
                                .props
                                .setUserInfo(datas)
                                api.hideLoading()
                                // api.showMessage(data.msg,'Thông báo')
                        }
                        
        // dataService
        //     .getListMember(this.props.userState.token, 1, 0)
        //     .then((dt) => {
        //         api.setListMember(dt.members);
        //         setTimeout(() => {
        //             api.hideLoading()
        //         }, 0)
        //     })
                        console.log("New user info", this.props.userState)
                        //alert(this.props.userState.user_code)
                        // this
                        //     .props
                        //     .gotoPromotion();
                        this.props.gotoPromotion()
                    })
            });
           
        setTimeout(()=>{
        if(this.state.isLoading){
            api.hideLoading()
            api.showMessage(i18.t('timeoutTextMsgContentLogin'),i18.t('titleMsgLogin'))
        }
    },15000)
}
// componentWillMount(){
//     gotoPromotion()
//     api.resetRoute({key:'home'})
// }
// componentDidUpdate(){
//     if(this.props.navState.routes[this.props.navState.index].key == 'home')
//     dataService
//             .getListMember(this.props.userState.token, 1, 0)
//             .then((dt) => {
//                 api.setListMember(dt.members);
//                 setTimeout(() => {
//                     api.hideLoading()
//                 }, 0)
//             })
// }
    _toggleCard(item) {
        // alert(item.memberCards[0].id)
        this.setState({displayListCard:false,isLoading:true})
        api.showLoading()
        dataService.toggleCard(this.props.userState.token, item.memberCards[0].id)
        .then((data) => {
        this.setState({isLoading:false})
          dataService
            .getListMember(this.props.userState.token, 1, 0)
            .then((data) => {
                console.log('data after toggle', data);
                this.props.setListMember(data.members);
                this.setState({displayListCard:true})
                api.hideLoading()
            })
        })
    setTimeout(()=>{
        if(this.state.isLoading){
            api.hideLoading()
            api.showMessage(i18.t('timeoutTextMsgContentLogin'),i18.t('titleMsgLogin'))
        }
    },15000)
    }
    render() {
        //console.log('listMember', this.props.userState.listMember);
        const ds = new ListView.DataSource({
            rowHasChanged: (r1, r2) => r1 !== r2
        });
        //console.log("AnotherCard", this.props.userState.data);
        if (this.state.isLoading) {
            return (
                    <ActivityIndicator size='large'/>
            );
        } else {
            console.log('listMember', this.props.userState.listMember);
            return (
                <View style={{ paddingBottom:5 }}>
                    <Modal
                        transparent={true}
                        visible={this.state.displayListCard}
                        animationType='fade'
                        onRequestClose={() => {}}>
                        <Container>
                            <Content
                                style={{
                                flex: 1,
                                backgroundColor: 'rgba(0,0,1, 0.5);'
                            }}>
                                <View
                                    style={{
                                    backgroundColor: 'white',
                                    marginTop: 60,
                                    marginLeft: 30,
                                    marginRight: 30,
                                    height: api.getRealDeviceHeight()*0.7,
                                    borderTopLeftRadius:5,
                                    borderTopRightRadius:5
                                }}>
                                    <Text
                                        style={{
                                        paddingVertical:10,
                                        fontSize: 15,
                                        color: '#000',
                                        textAlign: 'center'
                                    }}>{i18.t('headPopupChooseOtherCard')}</Text>
                                    <View style={{ height:1,width:api.getRealDeviceWidth()-60,backgroundColor:'#cdcdcd' }}/>
                                    <ListView
                                        dataSource={ds.cloneWithRows(this.props.userState.listMember)}
                                        renderRow={(item, sectionID, rowID) => {
                                            return (<MyCardItem
                                                opacity={item.memberCards[0].status == true?1:0.2}
                                                img={{
                                                uri: item.card
                                            }}
                                                style={{
                                                alignSelf: 'center'
                                            }}
                                                onCLick={() => {
                                                this._toggleCard(item)
                                            }}/>);
                                    }}/></View>
                                <Button
                                    style={{
                                    marginLeft: 30,
                                    marginRight: 30,
                                    backgroundColor: '#cacaca',
                                    borderRadius:0,
                                    borderBottomRightRadius:5,
                                    borderBottomLeftRadius:5
                                }}
                                    block
                                    onPress={() => {
                                    this.setState({displayListCard: false})
                                }}>
                                    <Text
                                        style={{
                                        color: '#444444'
                                    }}>{i18.t('close')}</Text>
                                </Button>
                            </Content>
                        </Container>
                    </Modal>
                    <View
                        style={{
                        flexDirection: 'row'
                    }}>
                        <MyCardItem
                            style={{
                            alignItems: 'center',
                            flex: 1
                        }}
                            img={images['newCard']}
                            text={i18.t('addCard')}
                            onCLick={() => {if(!api.requestLogin()){ return; }this.props.push({key: 'listcard',gotoUpdate:this.props.gotoUpdate,gotoPromotion:gotoPromotion})}}/>
                        <MyCardItem
                            img={images['listCard']}
                            text={i18.t('listcard')}
                            onCLick={() => {
                            this.setState({displayListCard: true})
                        }}
                            style={{
                            alignItems: 'center',
                            flex: 1
                        }}/>
                    </View>
                    <ListView
                        style={{ height:api.getRealDeviceHeight()*.55 }}
                        enableEmptySections={true}
                        dataSource={ds.cloneWithRows(this.props.userState.listMember)}
                        contentContainerStyle={{
                        flexDirection: 'row',
                        flexWrap: 'wrap',
                        alignItems: 'stretch'
                    }}
                        renderRow={(item, sectionID, rowID) => {
                        //console.log('item.memberCards[0].status', item.memberCards[0].status);
                        if (item.memberCards[0].status === true) {
                            return (<MyCardItem
                                img={{
                                uri: item.card
                            }}
                                style={{
                                alignSelf: 'center'
                            }}
                                onCLick={() => {
                                this._selectCard(item)
                            }}/>);
                        } else {
                            return null
                        }
                    }}/>
                </View>
            );
        }
    }
}
const mapState = (state) => {
    return {userState: state.userState,navState:state.navState}
}
const mapDispatch = (dispatch) => {
    return {
        push: (route) => dispatch(push(route)),
        setUserInfo: (data) => dispatch(setUserInfo(data)),
        setListMember: (listMember) => dispatch(setListMember(listMember))
    }
}
export default connect(mapState, mapDispatch)(AnotherCard)