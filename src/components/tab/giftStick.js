import { List, ListItem } from 'native-base';
import React, { Component } from 'react';
import { ActivityIndicator, View, Text, ListView, ScrollView } from 'react-native';
import { connect } from 'react-redux';
import dataService from '../../api/dataService';
import Item from './item';
import i18 from '../i18'

import { Analytics, Hits } from 'react-native-google-analytics';
var item_num_load = 5;
class GiftStick extends Component {
    constructor(props) {
        super(props);
        this.state = {
            item: [],
            isLoading: true,
            canLoadMore: true
        }

    }
    componentWillMount() {
        this._loadMoreData()
    }
    _loadMoreData() {
        let arr = this.state.item
        dataService
            .getGiftsStick(this.props.userState.token, 1, this.state.item.length, item_num_load)
            .then((data) => {
                if (data.err == 0)
                    data.promotions.map((item, id) => {
                        if (item.sentCodes.length > 1) {
                            item.sentCodes.map((sentCodes) => {
                                newItem = { ...item, sentCodes: [{ code: sentCodes.code }] }
                                arr.push(newItem)
                            })

                        }
                        else {
                            arr.push(item)
                        }
                    })
                this.setState({
                    item: arr,
                    isLoading: false,
                    canLoadMore: data.promotions.length < item_num_load ? false : true
                })
            })

    }
    _refresh() {
        this.setState({
            item: [], isLoading: true,
            canLoadMore: true
        })
        this._loadMoreData()
    }
    render() {
        const ds = new ListView.DataSource({
            rowHasChanged: (r1, r2) => r1 !== r2
        });
        if (this.props.userState.uuid !== undefined) {
            ga = new Analytics('UA-90479808-1', this.props.userState.uuid, 1, 'userAgent');
            var screenView = new Hits.ScreenView('Mpoint', 'GiftStick')
            ga.send(screenView);
        }
        if (this.state.isLoading) {
            return (<ActivityIndicator size='large' />);
        } else {
            if (this.state.item.length === 0) {
                return (
                    <View
                        style={{
                            alignSelf: 'center'
                        }}>
                        <Text style={{ color: '#fff', backgroundColor: 'transparent' }}>{i18.t('noneData')}</Text>
                    </View>
                );
            } else {
                return (
                    <ListView
                        dataSource={ds.cloneWithRows(this.state.item)}
                        onEndReached
                        ={() => this._loadMoreData()}
                        renderRow={(item, i) => {
                            return (<Item
                                logo={{
                                    uri: item.partner.logo
                                }}
                                likeCount={item.likeCount}
                                gift={true}
                                refresh={this._refresh.bind(this)}
                                promotionId={item.id}
                                id={item.id}
                                name={item.partner.name}
                                title={item.shortDescription}
                                slogan={item.partner.slogan}
                                img={item.images ? item.images : []
                                }
                                code={item.sentCodes != undefined && item.sentCodes[0].code != undefined ? item.sentCodes[0].code : ''}
                                time={item.endDate}
                                area={item.area} />);
                        }}
                        renderFooter={() => {
                            if (this.state.canLoadMore || this.state.isLoading) {
                                return (<ActivityIndicator size='large' />);
                            }
                        }} />
                );
            }
        }
    }
}
const mapState = (state) => {
    return { userState: state.userState }
}
const mapDispatch = (dispatch) => {
    return {}
}
export default connect(mapState, mapDispatch)(GiftStick);