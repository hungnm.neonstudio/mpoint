import { Card, CardItem, Thumbnail, Icon, Button } from 'native-base';
import React, { Component, PropTypes } from 'react';
import { View, Image, Text, TouchableWithoutFeedback, Platform } from 'react-native';
import { connect } from 'react-redux'
import { push } from '../../actions/navActions'
import api from '../../api'
import i18 from '../i18'
import Swiper from 'react-native-swiper';
import Octicons from 'react-native-vector-icons/Octicons'
class Item extends Component {
    constructor(props) {
        super(props)
        this.state = {
            haveStamp: [],
            remainStamp: [],
            width: api.getRealDeviceWidth() - 10,
            height: (api.getRealDeviceWidth() - 10) / 1.911262799
        }
    }
    componentWillMount() {
        Image.getSize(this.props.img ? this.props.img[0] ? this.props.img[0].url : 'i' : 'i', (width, height) => {
            if (height && width) this.setState({
                height: this.state.width * height / width,
            })
        });
        this.setState({
            haveStamp: new Array(parseInt(this.props.stamp) > 0 && this.props.stamp != undefined ?
                this.props.stamp
                : 0).fill(1),
            remainStamp: new Array(parseInt(this.props.remainStamp) < 0 || this.props.remainStamp == undefined ? 0 : parseInt(this.props.remainStamp)).fill(1)
        })
    }
    _receiveGift() {
        api.setPer('')
        api.showQR(i18.t('titleQRvetify'), this.props.code, true, this.props.promotionId, this.props.refresh, true)
        // api.showDoisoat(this.props.promotionId,'Thông báo',this.props.code,true,this.props.refresh)
    }
    _renderCode(code) {
        if (code == undefined || code == '') return
        return (
            <View
                style={{ paddingTop: 5, marginTop: 5, borderTopColor: '#cdcdcd', borderTopWidth: api.getRealDeviceHeight() > 720 ? 0.5 : 1, flex: 1 }}
            >
                <View style={{ alignSelf: 'center', flexDirection: 'row' }}>
                    <Text
                        style={{ alignSelf: 'center', fontSize: 16 }}
                    >
                        {i18.t('giftcode')}: {code}
                    </Text>
                    <Button
                        style={{ marginLeft: 20, paddingHorizontal: 20, alignSelf: 'center', borderRadius: 10, backgroundColor: '#fa9428' }}
                        onPress={this._receiveGift.bind(this)}
                    >
                        <Text style={{ color: '#fff' }}>{i18.t('recieveGift')}</Text>
                    </Button>
                </View>
            </View>
        )
    }
    _renderStamp() {
        if (this.props.promotionTypeId == 4) {
            if (this.state.haveStamp.length + this.state.remainStamp.length < 2) return;
            return (
                <View style={{ flexDirection: 'row', alignSelf: 'center', padding: 5, paddingBottom: 0 }}>
                    {this.state.haveStamp.map((item, id) => (
                        <Image key={id} source={require('../../img/icon/bought.png')}
                            style={{ height: api.getRealDeviceWidth() / 10, width: api.getRealDeviceWidth() / 10, marginLeft: 5 }}
                        />
                    ))}

                    {this.state.remainStamp.map((item, id) => (
                        <Image key={id} source={require('../../img/icon/remaining.png')}
                            style={{ height: api.getRealDeviceWidth() / 10, width: api.getRealDeviceWidth() / 10, marginLeft: 5 }} />
                    ))}
                    {/*<Image source={ require('../../img/icon/oneplus.png') }
                        style={{ height:api.getRealDeviceWidth()/10,width:api.getRealDeviceWidth()/10,marginLeft:5 }}/>*/}
                </View>
            )
        }
    }
    _renderImage() {
        let tag = this.props.tag ? this.props.tag + '' : ''
        let length = 0;
        let fontSize = 15
        let temp = tag.replace(i18.t('point'), '')
        if (tag) length = temp.length; else length = 0
        if (length > 9) {
            fontSize = 10
        } else if (length > 6) {
            fontSize = 12
        }
        return <Image
            source={{ uri: this.props.img ? this.props.img[0] ? this.props.img[0].url : 'i' : 'i' }}
            style={{
                width: this.state.width,
                resizeMode: 'stretch',
                height: this.state.height,
            }}
        >
            <Image source={require('../../img/tag.png')} style={{ paddingBottom: 17, justifyContent: 'center', zIndex: 100000, position: 'absolute', height: 50 * 1.581699346, right: 0, width: 50, resizeMode: 'stretch' }}>
                {tag ?
                    <Text style={{ backgroundColor: 'transparent', fontFamily: Platform.OS == 'ios' ? 'Arial' : 'San Francisco', zIndex: 1000000000, textAlign: 'center', fontSize: fontSize, color: '#fff' }}>{tag}</Text>
                    :
                    <Octicons name='gift' style={{ paddingLeft: Platform.OS == 'ios' ? 5 : 2, alignSelf: 'center', fontSize: 26, color: '#fff', backgroundColor: 'transparent' }} />
                }
            </Image>
        </Image>

    }
    render() {
        return (
            <TouchableWithoutFeedback
                onPress={() => this.props.push({ key: 'detail', id: this.props.id, functionRefresh: this.props.refresh, isGift: this.props.gift ? true : false, arrCode: this.props.arrCode, promotionTypeId: this.props.promotionTypeId, gotoGift: this.props.gotoGift, tag: this.props.tag })}
            >
                <View
                    style={{
                        margin: 5,
                        borderRadius: 5,
                        //borderBottomWidth: 0,
                        backgroundColor: '#fff',
                        paddingTop: 10,
                        paddingBottom: 10,
                        width: api.getRealDeviceWidth() - 10
                    }}
                >

                    <View
                        style={{
                            flexDirection: 'row',
                            marginTop: -5
                            //padding: 5
                        }}>

                        <Thumbnail square source={this.props.logo} size={50} style={{ marginRight: 5, marginLeft: 5, resizeMode: 'stretch' }} />
                        <View
                            style={{
                                flexDirection: 'column',
                                flex: 1
                            }}>
                            <Text style={{ fontSize: 15, fontWeight: 'bold', flexWrap: 'wrap' }}>{this.props.name}</Text>
                            <Text>{this.props.slogan != undefined && this.props.slogan != 'null' ? this.props.slogan : ''}</Text>
                        </View>
                    </View>
                    <View
                        style={{ paddingTop: 5, paddingBottom: 5 }}
                    >
                        {this._renderImage()}

                        <View
                            style={{ backgroundColor: '#fff' }}
                        >
                            {this._renderStamp()}
                            <View
                                style={{ flexDirection: 'row', marginHorizontal: 10, paddingVertical: 5 }}
                            >
                                <View style={{ flexDirection: 'row', }}>
                                    <Icon name='ios-heart' style={{ textAlignVertical: 'center', color: '#e32348', marginTop: Platform.OS == 'ios' && (this.props.title).length < 50 ? 0 : 10 }} />
                                    <View>
                                        <View style={{ flex: 20 }}></View>
                                        <View style={{ flex: 1, borderRadius: 5, backgroundColor: '#e32348', marginLeft: -7, height: 10, marginTop: 7 }}>
                                            <Text style={{ backgroundColor: 'transparent', color: '#fff', borderColor: '#fff', borderRadius: 5, borderWidth: 0.5, height: 10, paddingHorizontal: 5, textAlignVertical: 'center', fontSize: 8, textAlign: 'center' }}>{this.props.likeCount != undefined ? this.props.likeCount : '0'}</Text>
                                        </View>
                                        <View style={{ flex: 20 }}></View>
                                    </View>
                                </View>
                                <View>
                                    <View style={{ flex: 20 }}></View>
                                    <Text style={{ marginLeft: 5, flex: 1, maxWidth: api.getRealDeviceWidth() * 0.83 }}>{this.props.title != undefined ? this.props.title : ''}</Text>
                                    <View style={{ flex: 20 }}></View>
                                </View>
                            </View>
                        </View>
                        {/*{
                            this.props.percent?<View style={{ flexDirection:'row' }}>
                                <Text style={{ margin:10,borderRadius:10,color:'#fa6428',borderColor:'#fa6428',borderWidth:1,paddingHorizontal:10,paddingVertical:1 }}>{this.props.percent}</Text>
                                <View />
                            </View>:null
                        }*/}
                    </View>
                    {/* <View> */}
                        <View style={{ height:1,backgroundColor:'#cdcdcd' }}/>
                        <View
                            style={{
                                flexDirection: 'row',
                                justifyContent: 'space-around',
                                //borderTopColor: '#cdcdcd',
                                //borderTopWidth: api.getRealDeviceHeight() > 720 ? 0.5 : 1,
                                paddingTop: 5, backgroundColor: '#fff'
                            }}>
                            <Text style={{ backgroundColor: 'transparent' }}><Icon name='ios-calendar-outline' style={{ fontSize: 15 }} /> {this.props.time}</Text>
                            <Text style={{ backgroundColor: 'transparent' }}> <Icon name='ios-pin' style={{ fontSize: 15 }} /> {this.props.area == undefined ? i18.t('national') : this.props.area}</Text>
                        </View>
                    {/* </View> */}
                    {this._renderCode(this.props.code)}
                </View>

            </TouchableWithoutFeedback>
        );
    }
}
Item.propTypes = {
    logo: PropTypes.object.isRequired,
    name: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    // img: PropTypes.object.isRequired,
    time: PropTypes.string.isRequired
    // area: PropTypes.string.isRequired
}
mapStateToProps = (state) => ({
    lang: state.langState.lang
})

mapDispatchToProps = (dispatch) => ({
    push: (route) => dispatch(push(route))
})
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Item)
