import { List, ListItem } from 'native-base';
import React, { Component } from 'react';
import { ActivityIndicator, View, Text, ListView } from 'react-native';
import { connect } from 'react-redux';
import dataService from '../../api/dataService';
import Item from './item';
import i18 from '../i18'

import { Analytics, Hits } from 'react-native-google-analytics';
var item_num_load = 5;
class GiftCardStick extends Component {
    constructor(props) {
        super(props);
        this.state = {
            item: [],
            isLoading: true,
            canLoadMore: true
        }

    }
    _refresh() {
        this.props.gotoPromotion()
        this.props.gotoGift()
        this.setState({
            item: [],
            isLoading: true,
            canLoadMore: true
        })
        this._loadMoreData()
    }
    componentWillMount() {
        this._loadMoreData()
    }
    _loadMoreData() {
        if (this.state.canLoadMore) {
            dataService
                .getGiftsStick(this.props.userState.token, 2, this.state.item.length, item_num_load)
                .then((data) => {
                    this.setState({
                        item: this
                            .state
                            .item
                            .concat(data.promotions),
                        isLoading: false,
                        canLoadMore: data.promotions.length < item_num_load ? false : true
                    })
                })
        }
    }
    render() {
        const ds = new ListView.DataSource({
            rowHasChanged: (r1, r2) => r1 !== r2
        });
        if (this.props.userState.uuid !== undefined) {
            ga = new Analytics('UA-90479808-1', this.props.userState.uuid, 1, 'userAgent');
            var screenView = new Hits.ScreenView('Mpoint', 'GiftCardStick')
            ga.send(screenView);
        }
        if (this.state.isLoading) {
            return (<ActivityIndicator size='large' />);
        } else {
            if (this.state.item.length === 0) {
                return (
                    <View
                        style={{
                            alignSelf: 'center'
                        }}>
                        <Text style={{ color: '#fff', backgroundColor: 'transparent' }}>{i18.t('noneData')}</Text>
                    </View>
                );
            } else {
                return (
                    <ListView
                        dataSource={ds.cloneWithRows(this.state.item)}
                        onEndReached
                        ={() => this._loadMoreData()}
                        renderRow={(item, i) => {
                            return (<Item
                                logo={{
                                    uri: item.partner.logo
                                }}
                                id={item.id}
                                arrCode={item.sentCodes}
                                refresh={this._refresh.bind(this)}
                                promotionTypeId={item.promotionTypeId}
                                slogan={item.partner.slogan}
                                stamp={item.stamps != undefined && item.stamps.length > 0 ? parseInt(item.stamps[0].collected) : 0}
                                remainStamp={item.stamps == undefined || item.stamps == undefined || item.stamp < 1 ? 0 :
                                    // neu mang stamps trong
                                    item.stamps.length < 1 ?
                                        // tra ve so tong so tem - 1
                                        item.stamp :
                                        // neu mang stamps co , kiem tra co >2
                                        parseInt(item.stamp) - parseInt(item.stamps[0].collected)
                                }
                                name={item.partner.name}
                                title={item.shortDescription}
                                img={item.images ? item.images : []
                                }
                                time={item.endDate}
                                area={item.area} />);
                        }}
                        renderFooter={() => {
                            if (this.state.canLoadMore || this.state.isLoading) {
                                return (<ActivityIndicator size='large' />);
                            }
                        }} />
                );
            }
        }

    }
}

const mapState = (state) => {
    return { userState: state.userState }
}
const mapDispatch = (dispatch) => {
    return {}
}
export default connect(mapState, mapDispatch)(GiftCardStick);