import { List, ListItem, Card, CardItem, Icon } from 'native-base';
import React, { Component } from 'react';
import ImagePicker from 'react-native-image-crop-picker';
import config from '../../config';
import PopupDialog, { DialogTitle } from 'react-native-popup-dialog';
import { IndicatorViewPager, PagerTitleIndicator, PagerDotIndicator } from 'rn-viewpager'
import {
    ActivityIndicator,
    View,
    Text,
    Image,
    ListView,
    TouchableOpacity,
    Platform,
    ScrollView, PermissionsAndroid, ImageEditor, Linking, TouchableWithoutFeedback, Modal
} from 'react-native';
import { connect } from 'react-redux';
import dataService from '../../api/dataService';
import Item from './item';
import api from '../../api';
import RNFetchBlob from 'react-native-fetch-blob'
import { setUserInfo } from '../../actions/userActions';
import Swiper from 'react-native-swiper'
import i18 from '../i18'
import Food from './food'
var item_num_load = 5;
var options = {
    title: 'Chọn ảnh',
    cancelButtonTitle: 'Hủy',
    takePhotoButtonTitle: 'Chụp ảnh…',
    // quality: 0.1,
    chooseFromLibraryButtonTitle: 'Chọn từ thư viện…',
    noData: true,
    maxWidth: 100,
    maxHeight: 200,
    storageOptions: {
        skipBackup: true,
        path: 'images',
        waitUntilSaved: true
    }
};
let userInfo
let gotoinfo = ''
class MainTab extends Component {
    constructor(props) {
        super(props);
        userInfo = this.props.userState
        gotoinfo = this.props.gotoinfo
    }
    _refresh() {
        this.setState({
            item: [],
            isLoading: true,
            canLoadMore: true
        })
        this._loadMoreData()
    }
    render() {
        let card = <CardInfo
            token={this.props.userState.token}
            havePromotion={true}
            data={this.props.userState.data}
            mLoyalty={this.props.userState.mLoyalty}
            setUserInfo={this.props.setUserInfo}
            giftCount={this.props.userState.giftCount}
            user_code={this.props.userState.user_code}
            bg={this.props.userState.data.active.partner.background}
        />
        return (
            <Food category={this.props.category} type={this.props.type} card={card} />
        );
    }
    // }

    // }
}

var bg_panel = require('../../img/user-panel.png');
let main = null
class CardInfo extends Component {
    constructor(props) {
        super(props)
        this.state = {
            ad: [],
            page: 0,
            avt: '', height: 50, width: 50,
            showUploadAvt: false
        }

        // setInterval(()=>{
        //     this.setState({page:this.state.ad.length==0?0:this.state.page<this.state.ad.length?this.state.page+1:0})
        // },3000)
    }
    componentWillMount() {
        let logo = this.props.data.active.partner.logo
        Image.getSize(logo, (width, height) => {
            this.setState({
                height: height,
                width: width
            })
        });
        dataService.getAd(this.props.token, 1)
            .then(rs => {
                if (rs.err == 0) {
                    rs.data.unshift({})
                    this.setState({
                        ad: rs.data
                    })
                }
            })
    }
    componentWillUnmount() {
        this.setState({
            ad: []
        })
    }
    _checkStoragePer() {
        PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE).then((result) => {
            if (result) {
                console.log("Permission is OK");
                this.setState({ showUploadAvt: true })
            } else {
                PermissionsAndroid.requestPermission(PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE).then((result) => {
                    if (result) {
                        console.log("User accept"); thisthis.setState({ showUploadAvt: true })
                    } else {
                        console.log("User refuse");
                        api.showMessage(i18.t('errStoragePer'), i18.t('titleMsgLogin'))
                    }
                });
            }
        });
    }
    _checkCamPer() {
        if (Platform.OS === 'android' && Platform.Version >= 23)
            PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.CAMERA).then((result) => {
                if (result) {
                    console.log("Permission is OK");
                    camPer = true
                    this._checkStoragePer()
                } else {
                    PermissionsAndroid.requestPermission(PermissionsAndroid.PERMISSIONS.CAMERA).then((result) => {
                        if (result) {
                            console.log("User accept");
                            this._checkStoragePer()
                        } else {
                            console.log("User refuse");
                            api.showMessage(i18.t('errCamPer'), i18.t('titleMsgLogin'))
                        }
                    });
                }
            });
        else this.setState({ showUploadAvt: true })
    }
    _up(uri) {
        RNFetchBlob.fetch('POST', config.HOST + (api.getHost() ? ':' + api.getHost() : '') + '/api/uploadAvatar', {
            Authorization: 'Bearer ' + this.props.token
        }, [
                {
                    name: 'file',
                    filename: 'a.jpg',
                    type: 'image/jpg',
                    data: RNFetchBlob.wrap(uri)
                }
            ]).then((res) => {
                console.log('avatar res', res)
                res = JSON.parse(res.data)
                this.setState({ avt: 'file:///' + uri })
                // dataService
                //     .getUserInfo(this.props.token)
                //     .then((datas) => {
                //         if (datas.err === 0) {
                //             api.setUserInfo(datas);
                api.showMessage(res.msg, i18.t('titleMsgLogin'))
                api.hideLoading();
                api.setUserInfo(Object.assign({}, userInfo.data,
                    {
                        endUser: {
                            ...userInfo.data.endUser,
                            avatar: 'file:///' + uri
                        }
                    }
                ))

                // }
                // })
            }).catch((err) => {
                alert(err)
                // console.log(err.text());
                api.hideLoading();
                api.showMessage(i18.t('errPicker'), i18.t('titleMsgLogin'));
            })
    }
    _renderAd() {
        if (this.state.ad.length > 0) {
            return (
                <Swiper
                    height={parseInt((api.getRealDeviceWidth() - 10) / 1.911262799)}
                    width={parseInt((api.getRealDeviceWidth - 10))}
                    autoplay
                    autoplayTimeout={10}
                    dot={<View style={{ backgroundColor: 'rgba(255,255,255,.8)', width: 5, height: 5, borderRadius: 4, marginLeft: 3, marginRight: 3, marginTop: 3, marginBottom: 3 }} />}
                    activeDot={<View style={{ backgroundColor: '#fff', width: 8, height: 8, borderRadius: 4, marginLeft: 3, marginRight: 3, marginTop: 3, marginBottom: 3 }} />}
                    paginationStyle={{
                        bottom: 5, alignSelf: 'center'
                    }}
                >
                    {
                        this.state.ad.map((item, id) => {
                            if (id > 0) {
                                return (
                                    <View key={id} style={{ flex: 1 }}
                                    >
                                        <TouchableWithoutFeedback
                                            onPress={() => {
                                                if (item.bannerTypeId == 1) {
                                                    api.push({ key: 'detail', id: item.link })
                                                }
                                                if (item.bannerTypeId == 2 && item.link.indexOf('http') != -1) {
                                                    Linking.openURL(item.link)
                                                }
                                            }}
                                        >
                                            <Image
                                                style={{ resizeMode: 'stretch', flex: 1, width: api.getRealDeviceWidth() - 10 }}
                                                source={{ uri: item.banner }}
                                            />
                                        </TouchableWithoutFeedback>
                                    </View>
                                )
                            }
                            else return main
                        }
                        )
                    }
                </Swiper>
            )
        }
        else {
            return main
        }
    }
    renderDot(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
    }
    pickerImage() {
        let close = () => this.setState({ showUploadAvt: false })
        if (Platform.OS != 'ios') ImagePicker.clean().then(() => {
            console.log('removed all tmp images from tmp directory');
        }).catch(e => {
        });
        Platform.OS == 'android' ? close() : null
        ImagePicker.openPicker({
            width: 100,
            height: 100,
            cropping: true
        }).then(image => {
            Platform.OS == 'ios' ? close() : null
            setTimeout(() => {
                api.showLoading()
                let path = image.path.replace('file:///', '')
                this._up(path)
            }, 500)
        }, (err) => {
            Platform.OS == 'ios' ? close() : null
        });
    }
    openCam() {
        let close = () => this.setState({ showUploadAvt: false })
        if (Platform.OS != 'ios') ImagePicker.clean().then(() => {
            console.log('removed all tmp images from tmp directory');
        }).catch(e => {
        });
        Platform.OS == 'android' ? close() : null
        ImagePicker.openCamera({
            width: 100,
            height: 100,
            compressImageQuality: 0.5,
            cropping: true
        }).then(image => {
            Platform.OS == 'ios' ? close() : null
            setTimeout(() => {
                api.showLoading()
                let path = image.path.replace('file:///', '')
                this._up(path)
            }, 500)
        }, (err) => {
            Platform.OS == 'ios' ? close() : null
        });
    }
    render() {
        var data = this.props.data;
        var havePromotion = this.props.havePromotion
        var pro = null;
        if (!havePromotion) {
            pro = <Text style={{
                textAlign: 'center',
                color: '#fff', marginTop: 20, backgroundColor: 'transparent'
            }}>{i18.t('nonePromotion')}</Text>;
        }

        var mLevel = data.mloyalty.cardGroup != undefined
            ? ' ' + data.mloyalty.cardGroup.name
            : i18.t('noneRank');
        var mPoint = this.renderDot(' ' + (data.active.addPoint - data.active.subPoint));
        var user_name = data.endUser.name != null
            ? data.endUser.name
            : i18.t('customer');
        var member_name = data.active.partner != undefined
            ? data.active.partner.name
            : '...';
        let groupPrefix = data.active.partner.groupPrefix != ''
            ? data.active.partner.groupPrefix + ':'
            : '';
        let pointPrefix = data.active.partner.pointPrefix != ''
            ? data.active.partner.pointPrefix + ':'
            : '';
        var logo = data.active.partner != undefined
            ? data.active.partner.logo
            : '...';
        var avatar = data.endUser.avatar;
        var point = this.renderDot(' ' + (data.mloyalty.addPoint - data.mloyalty.subPoint));
        var group_name = data.active.cardGroup != undefined
            ? ' ' + data.active.cardGroup.name
            : '...';
        let height = 0
        let width = 0

        main = <View key={0}>
            <Image
                source={this.props.bg ? { uri: this.props.bg } : bg_panel}
                style={{
                    width: api.getRealDeviceWidth() - 10,
                    height: (api.getRealDeviceWidth() - 10) / 1.911262799,
                    resizeMode: 'stretch'
                }}>
                <Image
                    source={{
                        uri: logo
                    }}
                    style={{
                        width: 50,
                        height: (50) * (this.state.height / this.state.width),
                        resizeMode: 'stretch',
                        margin: 5,
                        top: 0,
                        left: 0, position: 'absolute'
                    }} />

                <View
                    style={{ top: api.getRealDeviceWidth() / 35 }}
                >
                    <Image
                        source={{
                            uri: this.state.avt == '' ? avatar + '' : this.state.avt
                        }}
                        style={{
                            width: api.getRealDeviceWidth() / 5,
                            height: api.getRealDeviceWidth() / 5,
                            alignSelf: 'center',
                            borderRadius: api.getRealDeviceWidth() / 10,
                            //marginTop: -100
                        }}></Image>
                    <TouchableOpacity
                        style={{ zIndex: 1000, }}
                        onPress={() => {
                            if(!api.requestLogin()){ return; }
                            this._checkCamPer();
                        }}>
                        <View
                            style={{
                                alignSelf: 'center', flexDirection: 'row', backgroundColor: 'rgba(20,20,20,.4)', paddingHorizontal: api.getRealDeviceWidth() > 320 ? 20 : 10, borderRadius: 12,
                            }}>
                            <Icon
                                name='ios-camera-outline'
                                style={{
                                    fontSize: api.getRealDeviceWidth() > 320 ? 26 : 18,
                                    color: '#fff',
                                    zIndex: 1000,
                                    textAlignVertical: 'center'
                                }} />
                            <Text style={{ marginTop: Platform.OS == 'ios' ? 5 : 0, color: '#fff', textAlignVertical: 'center', marginLeft: 10, fontSize: api.getRealDeviceWidth() > 320 ? 12 : 10 }}>{i18.t('update')}</Text>
                        </View>
                    </TouchableOpacity>
                </View>
                <View
                    style={{ bottom: 15, position: 'absolute', width: api.getRealDeviceWidth() - 5 }}
                >
                    <Text
                        style={{
                            alignSelf: 'center',
                            fontSize: 15,
                            fontWeight: 'bold',
                            marginTop: 15,
                            paddingTop: 5,
                            backgroundColor: 'transparent'
                        }}
                        onPress={() => gotoinfo()}
                    >{i18.t('hello')} {user_name.toUpperCase()}</Text>
                    <Text
                        style={{
                            alignSelf: 'center',
                            textAlign: 'center',
                            backgroundColor: 'transparent', marginHorizontal: 7
                        }}>{i18.t('usePromotionOf')} {(member_name).toUpperCase()}</Text>
                </View>
            </Image>
        </View>
        return (
            <View
                style={{
                    margin: 5,
                    borderRadius: 5,
                    width: api.getRealDeviceWidth() - 10
                }}>
                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={this.state.showUploadAvt}
                    onRequestClose={() => { }}>
                    <TouchableOpacity
                        style={{ height: api.getRealDeviceHeight() }}
                        onPress={() => { this.setState({ showUploadAvt: false }) }}
                    >
                        <View style={{ flex: 1 }} />
                        <View style={{ borderColor: '#cdcdcd', borderWidth: 0.5, borderRadius: 5, zIndex: 1000, backgroundColor: '#fff', alignSelf: 'center', width: api.getRealDeviceWidth() * 0.7 }}>
                            <Text style={{ fontSize: 15, fontWeight: '500', alignSelf: 'center', paddingVertical: 5 }}>{i18.t('updateAvt')}</Text>
                            <View style={{ height: 1, backgroundColor: '#cdcdcd' }}></View>
                            <TouchableOpacity onPress={this.openCam.bind(this)} style={{ paddingBottom: 10, paddingLeft: 20, marginTop: 20 }}><Text>{i18.t('takePhoto')}</Text></TouchableOpacity>
                            <TouchableOpacity onPress={this.pickerImage.bind(this)} style={{ paddingBottom: 10, paddingLeft: 20 }}><Text>{i18.t('pickerFromGalery')}</Text></TouchableOpacity>
                            <TouchableOpacity onPress={() => this.setState({ showUploadAvt: false })} style={{ borderBottomRightRadius: 5, borderBottomLeftRadius: 5, paddingVertical: 5, backgroundColor: '#fa6428' }}><Text style={{ color: "#fff", alignSelf: 'center' }}>{i18.t('close')}</Text></TouchableOpacity>
                        </View>
                        <View style={{ flex: 1 }} />
                    </TouchableOpacity>
                </Modal>
                {this._renderAd()}
                <View
                    style={{
                        backgroundColor: '#fff',
                        marginTop: 10,
                        padding: 5,
                        borderRadius: 5,
                        width: api.getRealDeviceWidth() - 10,
                        zIndex: 1000
                    }}>
                    <View style={{ alignSelf: 'center', flexDirection: 'row', }}>
                        <View
                            style={{
                                flexDirection: 'row',
                                alignSelf: 'center',
                                minHeight: 19
                            }}>
                            <View>
                                {pointPrefix != '' ? <Text
                                    style={{

                                    }}>{pointPrefix}</Text> : null}
                                <Text
                                    style={{

                                    }}>mPoint:</Text>
                            </View>
                            <View>
                                {pointPrefix != '' ? <Text
                                    style={{

                                    }}>{mPoint}</Text> : null}
                                <Text
                                    style={{

                                    }}>{point}</Text>
                            </View>
                        </View>
                        <View
                            style={{
                                flexDirection: 'row',
                                alignSelf: 'center',
                                marginLeft: api.getRealDeviceWidth() > 320 ? api.getRealDeviceWidth() / 5 : (point.length >= 7 || mPoint.length >= 7) ? 10 : api.getRealDeviceWidth() / 5
                                , minHeight: 19
                            }}>
                            <View>
                                {groupPrefix != '' ? <Text
                                    style={{

                                    }}>{groupPrefix}</Text> : null}
                                <Text
                                    style={{

                                    }}>{i18.t('rank')}:</Text>
                            </View>
                            <View>
                                {groupPrefix != '' ? <Text
                                    style={{

                                    }}>{group_name}</Text> : null}
                                <Text
                                    style={{

                                    }}>{mLevel}</Text>
                            </View>
                        </View>
                    </View>
                </View>
                {pro}
            </View>
        );
    }
}
const mapState = (state) => {
    return { userState: state.userState }
}
const mapDispatch = (dispatch) => {
    return {
        setUserInfo: (data) => dispatch(setUserInfo(data))
    }
}
export default connect(mapState, mapDispatch)(MainTab);