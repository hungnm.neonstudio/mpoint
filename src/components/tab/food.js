import { List, ListItem } from 'native-base';
import React, { Component } from 'react';
import { ActivityIndicator, View, Text, ListView, Platform } from 'react-native';
import { connect } from 'react-redux';
import dataService from '../../api/dataService';
import Item from './item';
import i18 from '../i18'
import {
    Analytics, Hits
} from 'react-native-google-analytics';
var item_num_load = 5;
class Food extends Component {
    constructor(props) {
        super(props);
        this.state = {
            item: [],
            start: 0,
            isLoading: true,
            canLoadMore: true
        }
        //console.log(this.state)
    }
    componentWillMount() {
        this._loadMoreData()
    }
    _loadMoreData() {
        if (this.state.canLoadMore) {
            //console.log('Load more');

            dataService
                .getListPromotion(this.props.userState.token, this.props.category, this.props.type, this.state.item.length, item_num_load)
                .then((data) => {
                    this.setState({
                        item: this
                            .state
                            .item
                            .concat(data.promotions),
                        isLoading: false,
                        canLoadMore: data.promotions && data.promotions.length < item_num_load ? false : true
                    })
                })
        }
    }
    _refresh() {
        this.setState({
            item: [],
            isLoading: true,
            canLoadMore: true
        })
        this._loadMoreData()
    }
    render() {
        if (this.props.userState.uuid !== undefined) {
            ga = new Analytics('UA-90479808-1', this.props.userState.uuid, 1, 'userAgent');
            var screenView = new Hits.ScreenView('Mpoint', 'PromotionFood')
            ga.send(screenView);
        }
        const ds = new ListView.DataSource({
            rowHasChanged: (r1, r2) => r1 !== r2
        });
        // if (this.state.isLoading) {
        //     return (<ActivityIndicator size='large' />);
        // } else {
        // if (this.state.item.length === 0) {
        //     return (
        //         <View
        //             style={{
        //                 alignSelf: 'center',

        //             }}>
        //             <Text style={{ color: '#fff', backgroundColor: 'transparent' }}>{i18.t('nonePromotion')}</Text>
        //         </View>
        //     );
        //     } else {
        return (
            <ListView
                enableEmptySections={true}
                renderHeader={() => { return this.props.card || null }}
                dataSource={ds.cloneWithRows(this.state.item)}
                onEndReached
                ={() => { if (this.state.item.length > 0) this._loadMoreData() }}
                renderRow={(item, i) => {
                    let tag = ''
                    switch (item.promotionTypeId) {
                        case 1: tag = item.percent ? item.percent + '%' : ''; break;
                        case 2: tag = item.exchangePoint ? item.exchangePoint + '\n' + i18.t('point') : ''; break;;
                        case 3: tag = item.giftPoint ? item.giftPoint + '\n' + i18.t('point') : ''; break;
                        case 4: tag = i18.t('stamps'); break;
                        case 5: tag = i18.t('gifts'); break;
                        case 6: tag = i18.t('gifts'); break;
                        case 7: tag = i18.t('points'); break;
                        default: break;
                    }
                    return (<Item
                        logo={{
                            uri: item.partner.logo
                        }}
                        promotionTypeId={item.promotionTypeId}
                        likeCount={item.likeCount}
                        id={item.id}
                        tag={tag}
                        refresh={this._refresh.bind(this)}
                        name={item.partner.name}
                        title={item.shortDescription}
                        slogan={item.partner.slogan != null && item.partner.slogan != undefined ? item.partner.slogan : ''}
                        stamp={item.stamps != undefined && item.stamps.length > 0 ? parseInt(item.stamps[0].collected) : 0}
                        remainStamp={item.stamps == undefined || item.stamps == undefined || item.stamp < 1 ? 0 :
                            // neu mang stamps trong
                            item.stamps.length < 1 ?
                                // tra ve so tong so tem - 1
                                item.stamp :
                                // neu mang stamps co , kiem tra co >2
                                parseInt(item.stamp) - parseInt(item.stamps[0].collected)
                        }
                        img={item.images ? item.images : []
                        }
                        time={item.endDate}
                        area={item.area} />);
                }}
                renderFooter={() => {
                    if (this.state.canLoadMore) {
                        return (<ActivityIndicator size='large' />);
                    }
                    if (this.state.item.length === 0) {
                        return (
                            <View
                                style={{
                                    alignSelf: 'center',

                                }}>
                                <Text style={{ color: '#fff', backgroundColor: 'transparent' }}>{i18.t('nonePromotion')}</Text>
                            </View>
                        );
                    }
                }
                } />
        );
    }
}

// }
// }

const mapState = (state) => {
    return { userState: state.userState, lang: state.langState.lang }
}
const mapDispatch = (dispatch) => {
    return {}
}
export default connect(mapState, mapDispatch)(Food);