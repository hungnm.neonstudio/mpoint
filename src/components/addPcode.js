import React, { Component } from 'react';
import { View, TextInput, Image, Text, Keyboard, ScrollView, Platform, NativeModules } from 'react-native';
import {
    Header,
    Title,
    Container,
    Content,
    Button,
    Icon,
    Input,
    InputGroup
} from 'native-base';
import api from '../api';
import dataService from '../api/dataService';
import { connect } from 'react-redux';
import i18 from './i18';
import { stamp } from './nativeModule'
// POS_1000-> POS_1011Sent on:11:17 am
// From:nhung phamloai tang diem
// From:nhung phamPOS_1012->POS_1026
// From:nhung phamtang quaFrom:nhung phamPOS_1027->POS_1044
// From:nhung phamtích temFrom:nhung phamPOS_1045 ->...
// From:nhung phamtang theo hóa đơn
class AddCardFromLogin extends Component {
    constructor(props) {
        super(props);
        this.state = { text: '' };
    }
    _onChangeText(text) {
        this.setState({ text: text })
    }
    _onPress() {
        // console.log('====================================');
        // console.log(NativeModules);
        // console.log('====================================');
        // stamp.getEchosserviceUrl(success => {
        //     alert(success)
        // }, (err1, err2) => {
        //     console.log('====================================');
        //     console.log(err1,err2);
        //     console.log('====================================');
        // }
        // )
        // return;
        Keyboard.dismiss()
        if (!this.state.text) return api.showMessage(i18.t('blankCodeAddPcodepage'), i18.t('titleMsgLogin'));
        let isLoading = true;
        let token = this.props.token
        api.showLoading();
        dataService.postScanQR(this.state.text, this.props.token).then(rs => {
            api.hideLoading();
            isLoading = false;
            if (rs.err != 101) return api.showMessage(rs.msg, i18.t('titleMsgLogin'));
            if (rs.pos.err != 0) return api.showMessage(rs.pos.msg, i18.t('titleMsgLogin'));
            switch (rs.pos.type) {
                case 1, 2:
                    let point = rs.pos.point + '\n' + i18.t('point');
                    setTimeout(function () {
                        dataService.getUserInfo(token).then(res => {
                            if (res.err == 0) api.setUserInfo(res)
                        });
                    }, 1000);
                    return api.showStorelet(point, rs.pos.msg);
                case 3:
                    setTimeout(() => {
                        dataService.getGiftCount(token)
                            .then((res) => {
                                api.setGitfCount(res.err == 0 ? res.amount : '0')
                            })
                    }, 1000);
                    return api.showStorelet(undefined, rs.pos.msg);
                case 4:
                    return api.showStorelet(undefined, rs.pos.msg, null, null, true);
                default:
                    break;
            }
        });
        setTimeout(() => {
            if (isLoading) {
                isLoading = false;
                api.showMessage(i18.t('timeoutTextMsgContentLogin'), i18.t('titleMsgLogin'));
            }
        }, 30000);
    }
    render() {
        return (
            <Container>
                <Header
                    style={this.style.header}>
                    <Title  style={{ alignSelf:'center' } }>
                        {i18.t('titleAddPcode')}
                    </Title>
                    <Button transparent onPress={() => api.resetRoute({ key: 'home' })}>
                        <Icon name='ios-arrow-back' style={this.style.iconBack} />
                    </Button>
                </Header>
                <Image
                    source={require('../img/bg_login.jpg')}
                    style={this.style.bgImage}>
                    <View style={{ flex: 3 }} />
                    <View>
                        <Text style={this.style.des}>
                            {i18.t('introAddPpage')}
                        </Text>
                        <InputGroup style={{ borderWidth: 0, height: 30 }}>
                            <Input
                                onSubmitEditing={this._onPress.bind(this)}
                                placeholder={i18.t('placeholderAddPpage')}
                                placeholderTextColor='#444'
                                style={this.style.input}
                                onChangeText={this._onChangeText.bind(this)}
                            />
                        </InputGroup>
                        <Button style={this.style.button}
                            onPress={this._onPress.bind(this)}
                        >
                            <Text style={this.style.btnText}>{(i18.t('buttonText').toUpperCase())}</Text>
                        </Button>
                    </View>

                    <View style={{ flex: 7 }} />
                </Image>
            </Container>
        );
    }

    style = {
        header: {
            backgroundColor: '#009688',
            zIndex: 1000
        },
        bgImage: {
            width: api.getRealDeviceWidth(),
            height: api.getRealDeviceHeight() - 56
        },
        iconBack: {
            color: Platform.OS == 'ios' ? '#000' : '#fff'
        },
        des: {
            backgroundColor: 'transparent',
            color: '#fff', maxWidth: 500,
            textAlign: 'center',
            lineHeight: 25,
            marginBottom: 30,
            width: api.getRealDeviceWidth() * .8,
            alignSelf: 'center'
        },
        input: {
            textAlign: 'center',
            maxWidth: 500, width:
            api.getRealDeviceWidth() * .8,
            alignSelf: 'center',
            marginBottom: 10,
            fontSize: 13,
            borderWidth: 1,
            borderColor: '#cdcdcd',
            height: 30,
            backgroundColor: '#fff',
            paddingBottom: Platform.OS == 'ios' ? 0 : 7,
            color: '#444',
            borderRadius: 5
        },
        button: {
            marginTop: 20,
            maxWidth: 500,
            backgroundColor: '#fa6428',
            height: 30,
            borderRadius: 5,
            marginBottom: 10,
            width: api.getRealDeviceWidth() * .8,
            alignSelf: 'center'
        },
        btnText: {
            borderRadius: 5,
            textAlign: 'center',
            paddingVertical: 5,
            color: '#fff'
        }
    }
}
const mapState = (state) => {
    return { navState: state.navState, token: state.userState.token }
}
const mapDispatch = (dispatch) => {
    return {
        setUserInfo: (data) => dispatch(setUserInfo(data)),
    }
}
export default connect(mapState, mapDispatch)(AddCardFromLogin);