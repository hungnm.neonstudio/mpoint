import React, { Component, PropTypes } from 'react';
import {
    StyleSheet,
    View,
    Text,
    Modal,
    ActivityIndicator, Platform
} from 'react-native';
import { Container, Content, Button, Input, InputGroup, Card } from 'native-base'
import { connect } from 'react-redux'
import { showMessage, hideMessageBox, showInputBox, hideInputBox } from '../../actions/uiActions'
import api from '../../api'
import i18 from "../i18"
// import dataService from '../../api/dataService'
// import sharedStyles from '../styles'
class InputBox extends Component {
    /**
   * Optional Flowtype state and timer types
   */
    text = ''
    static propTypes = {
        show: PropTypes.bool.isRequired,
        title: PropTypes.string.isRequired,
        placeholder: PropTypes.string.isRequired,
        // content:PropTypes.string.isRequired,
        submit: PropTypes.func.isRequired,
        // rou:PropTypes.object.isRequired
    }
    constructor(props) {
        super(props);
        // console.log(props.rou)
        this.state = {
            isLoading: false
        }
    }
    _renderLoading() {
        if (this.state.isLoading)
            return (<ActivityIndicator />)
    }
    render() {
        if (this.props.show) {
            return (
                <Modal
                    transparent={true}
                    visible={true}
                    animationType='fade'
                    onRequestClose={() => { }}
                >
                    <Container
                        style={{
                            flex: 1,
                            backgroundColor: 'rgba(0, 0, 0, 0.52);'
                        }}>
                        <Content
                            keyboardShouldPersistTaps={'handled'}>
                            <View style={styles.backdrop}>
                                <View style={styles.modal}>
                                    <View style={styles.modal_content}>
                                        <Text style={styles.modal_title}>{this.props.title}</Text>
                                        {
                                            Platform.OS == 'ios' ? <View style={{ height: 1, flex: 1, backgroundColor: '#cdcdcd', marginBottom: 10 }}>

                                            </View> : null
                                        }
                                        <Text style={{ paddingLeft: 10, paddingRight: 10 }}>{this.props.content}</Text>

                                        <Input placeholderTextColor='gray' onChangeText={(text) => this.text = text} placeholder={this.props.placeholder} style={{ borderRadius: 3, borderColor: '#cacaca', borderWidth: 1, margin: 10 }} keyboardType='phone-pad'
                                            autoFocus={true}
                                            defaultValue=''
                                            onSubmitEditing={()=>this.props.submit(this.text)}
                                        />
                                        {this._renderLoading()}
                                        <View style={{ flexDirection: 'row', paddingLeft: 10, paddingRight: 10, paddingBottom: 20 }}>
                                            <Button onPress={() => api.hideInputBox()} style={{ flex: 1, height: 30, backgroundColor: 'gray' }}>{i18.t('close')}</Button>
                                            <Button onPress={() => {
                                                api.hideInputBox()
                                                this.props.submit(this.text);
                                                this.text = ''
                                            }} style={{ flex: 1, height: 30, marginLeft: 10, backgroundColor: '#fa6428' }}>{i18.t('confirm')}</Button>

                                        </View>
                                    </View>
                                </View>

                            </View>

                        </Content>
                    </Container>
                </Modal>
            );
        }
        return null

    }
}
const styles = StyleSheet.create({
    button_bottom: {
        borderBottomLeftRadius: 2,
        borderBottomRightRadius: 2
    },
    backdrop: {
        //backgroundColor: 'rgba(0, 0, 0, 0.52);',
        zIndex: 1000,
        flex: 1

    },
    modal: {
        // marginTop: api.getRealDeviceHeight() / 3,
        marginTop: 50,
        maxWidth: 500,
        width: api.getRealDeviceWidth() * 0.8,
        overflow: 'hidden',
        alignSelf: 'center',
        borderRadius:5
    },
    modal_content: {
        backgroundColor: 'white',
        borderRadius: 2,
        flex: 1
    },
    modal_title: {
        borderBottomWidth: 1,
        borderBottomColor: '#ddd',
        padding: Platform.OS == 'ios' ? 2 : 10,
        paddingVertical: 10,
        textAlign: 'center',
        fontWeight: 'bold',
        fontSize: Platform.OS == 'ios' ? 12 : 15
    },
    modal_text_content: {
        padding: 10,
        flex: 1
    },
    button_container: {
        flex: 1,
        height: 50
    }
});
mapStateToProps = (state) => ({ lang: state.langState.lang })
mapDispatchToProps = (dispath) => ({
    hideinput: () => dispath(hideInputBox()),
})
export default connect(mapStateToProps, mapDispatchToProps)(InputBox)
