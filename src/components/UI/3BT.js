import React, {Component, PropTypes} from 'react';
import {
    StyleSheet,
    View,
    Text,
    Modal,ScrollView,TouchableWithoutFeedback,Platform
} from 'react-native';
import {Container, Content, Button,Input,InputGroup} from 'native-base'
import {connect} from 'react-redux'
import {showMessage,hideMessageBox,showInputBox,hideInputBox,hideDoiSoat} from '../../actions/uiActions'
import api from '../../api'
import dataService from '../../api/dataService'
import i18 from '../../components/i18'
// import sharedStyles from '../styles'
let change = true
class Doisoat extends Component {

    constructor(props) {
        super(props);
        this.state={
            msg:'',
            select:1,
            confirm:true,
            contentLeft:'',
            contentCenter:'',
            contentRight:'',
            contentBody:'',
            promotionId:'',
            type:1,
            shopId:'',
            partnerId:'',
            tasktime:'',
            bookedtime:''
        }
}
componentWillMount(){
    this.setState(this.state={
            msg:'',
            select:1,
            confirm:true
        })
}
showContent(x){
    a = x+'h'
    return (a.replace(',','h - '))
}
_select(id,bookedtime){
    this.setState({select:id,confirm:false,msg:'',bookedtime:bookedtime})
}
_onconfirm(){
    //this.props.bt3func()
    this.setState({ confirm:true,msg:false })
    console.log('graan',this.state)
    dataService.mGreenSubmit(
        this.props.token,
        this.state.promotionId,
        this.state.type,
        this.state.shopId,
        this.state.partnerId,
        this.state.select,
        this.state.bookedtime
    ).then((rs)=>{
        if(rs.err == 0){
            api.hideMgreen()
            api.showMessage(rs.msg,i18.t('titleMsgLogin'))
        }
        else{
            this.setState({
                msg:rs.msg
            })
        }
    })
}

_ondoisoat(){
    api.hideMgreen()
    api.push({key:'detail',id:this.state.promotionId})
    if(this.state.confirm){
        // this.props.bt2func()
    }
    else{
        this.setState({
            msg:i18.t('err3BTpage')
        })
    }
}
    
    componentDidUpdate(){
        data = this.props.data
        if(data == undefined || data == null || data.mgreen == undefined || !this.props.show) return
        if(change)
       {
            change = false
            this.setState({
            contentLeft:data.mgreen.time.task1,
            contentCenter:data.mgreen.time.task2,
            contentRight:data.mgreen.time.task3,
            contentBody:data.mgreen.msg,
            promotionId:data.promotion,
            partnerId:data.partnerId,
            shopId:data.shopId,
            type:1,
            select:data.mgreen.bookedtime == 2 || data.mgreen.bookedtime == 3 ?data.mgreen.bookedtime:1,
            bookedtime:data.mgreen.bookedtime == 2?data.mgreen.time.task2:data.mgreen.bookedtime == 3?data.mgreen.time.task3:data.mgreen.time.task1
       })
       }
       setTimeout(()=>{
            change = true
       },30000)
    }
    render() {
        if (this.props.show) {
            // code = this.props.code
            // functionRefresh = this.props.functionRefresh
           // alert(this.props.promotionId)
            return (
                <Modal
                    transparent={true}
                    visible={true}
                    animationType='slide'
                    onRequestClose={() => {}}>
                    <Container
                        style={{
                        flex: 1,
                        backgroundColor: 'rgba(0, 0, 0, 0.52);'
                    }}>
                         <ScrollView
                        keyboardShouldPersistTaps={'handled'}>
                            <View style={styles.backdrop}>
                                <View style={styles.modal}>
                                    <View style={styles.modal_content}>
                                        <Text style={styles.modal_title}>mGreen</Text>
                                        {
                                            Platform.OS == 'ios'?<View style={{ height:1,flex:1,backgroundColor:'#cdcdcd' }}>

                                    </View>:null
                                        }
                                        <View style={{ paddingHorizontal:5 }}>
                                           {
                                                this.state.msg!=''?<Text style={{ paddingVertical:10,textAlign:'center',color:'red' }}>
                                                {this.state.msg}
                                                </Text>:null
                                           }
                                            <Text style={{ paddingVertical:10,textAlign:'center' }}>
                                                {this.state.contentBody}
                                            </Text>
                                        </View>
                                        <View style={{ flexDirection:'row',paddingHorizontal:5,marginTop:10 }}>
                                        <View style={{ flex:1,borderColor:'#cdcdcd',borderWidth:1,backgroundColor:this.state.select==1?'#aaa':'#fff' }} >
                                            <TouchableWithoutFeedback
                                                onPress={()=>this._select(1,this.state.contentLeft)}
                                            >
                                                <View>
                                                    <Text style={{ textAlign:'center',paddingVertical:5 }}>{this.showContent(this.state.contentLeft)}</Text>
                                                </View>
                                            </TouchableWithoutFeedback>
                                        </View>
                                       <View style={{ flex:1,borderColor:'#cdcdcd',borderTopWidth:1,borderBottomWidth :1,backgroundColor:this.state.select==2?'#aaa':'#fff' }}>
                                            <TouchableWithoutFeedback
                                                onPress={()=>this._select(2,this.state.contentCenter)}
                                            >
                                                <View>
                                                    <Text style={{ textAlign:'center',paddingVertical:5 }}>{this.showContent(this.state.contentCenter)}</Text>
                                                </View>
                                            </TouchableWithoutFeedback>
                                        </View>
                                        <View style={{ flex:1,borderColor:'#cdcdcd',borderWidth:1,backgroundColor:this.state.select==3?'#aaa':'#fff' }}>
                                            <TouchableWithoutFeedback
                                                onPress={()=>this._select(3,this.state.contentRight)}
                                            >
                                               <View>
                                                    <Text style={{ textAlign:'center',paddingVertical:5 }}>{this.showContent(this.state.contentRight)}</Text>
                                               </View>
                                            </TouchableWithoutFeedback>
                                        </View>
                                        </View>
                                        <View style={{ flexDirection:'row',padding:10 }}>
                                            <Button onPress={()=>{ change = true;api.hideMgreen();this.componentWillMount() }} style={{ flex:1,backgroundColor:'#cdcdcd' }} ><Text style={styles.buttonText}>{i18.t('close')}</Text></Button>
                                            <Button onPress={this._ondoisoat.bind(this)} style={{ flex:1,marginLeft:5,backgroundColor:'#fa6428' }}><Text style={styles.buttonText}>{i18.t('earnPoint')}</Text></Button>
                                            <Button onPress={this._onconfirm.bind(this)} style={{ flex:1,marginLeft:5,backgroundColor:'#00a537' }}><Text style={styles.buttonText}>{i18.t('confirm')}</Text></Button>
                                        </View>
                                    </View>
                                </View>

                            </View>
                        </ScrollView>
                    </Container>
                </Modal>
            );
        }
        return null

    }
}
const styles = StyleSheet.create({
    button_bottom: {
        borderBottomLeftRadius: 2,
        borderBottomRightRadius: 2
    },
    backdrop: {
        //backgroundColor: 'rgba(0, 0, 0, 0.52);',
        flex: 1
    },
    modal: {
        marginTop: api.getRealDeviceHeight() / 7,
        // marginTop:50,
        maxWidth: 500,
        width:api.getRealDeviceWidth()*0.97,

        borderRadius:5,        overflow: 'hidden',
        alignSelf:'center'
    },
    modal_content: {
        backgroundColor: 'white',
        borderRadius: 2,
        flex: 1
    },
    modal_title: {
        borderBottomWidth: 1,
        borderBottomColor: '#ddd',
        padding: 5,
        paddingBottom: 10,
        textAlign: 'center',
        fontSize:20,
        fontWeight:'bold'
    },
    modal_text_content: {
        padding: 10,
        flex: 1
    },
    button_container: {
        flex: 1,
        height: 50
    },
    buttonText:{
        fontSize:Platform.OS == 'ios'?13:15,
        color:'#fff'
    }
});
mapStateToProps = (state) => ({ token:state.userState.token })
mapDispatchToProps = (dispath) => ({
    hideDoisoat:()=>dispath(hideDoiSoat()),
})
export default connect(mapStateToProps, mapDispatchToProps)(Doisoat)
