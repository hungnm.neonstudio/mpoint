import React, {Component, PropTypes} from 'react';
import { StyleSheet, View, Text} from 'react-native';
import {connect} from 'react-redux'
import {showLoading, hideMessageBox, hideConfirm} from '../../actions/uiActions'
// import api from '../../api' 
import Loading from './loading'
import MessageBox from './messageBox'
import Inputbox from './inputbox'
import ConfirmBox from './confirmBox'
import Slide from './slider'
import ThreeBT from './3BT'
import Doisoat from './doisoat'
import QRconfirm from './qrconfirm'
import Storelet from './storelet'
import Weblogin from './loginWebConfirm'
import Web from './webview'
class UI extends Component {
    constructor(props) {
        super(props);
    }
    _onConfirmOk() {
        this
            .props
            .ui
            .onConfirmOk()
    }
    _onConfirmCancel() {
        this
            .props
            .ui
            .onConfirmCancel()
    }
    render() {
        
        let ui = this.props.ui
        // console.log(ui.route)
        return (
            <View>
                <Loading 
                    show={ui.loading}
                />
                <MessageBox
                    show={ui.showMessageBox}
                    title={ui.messageTitle}
                    content={ui.messageContent}
                    onClose={()=>{ if(ui.funcMsg)ui.funcMsg(); this.props.hideMessageBox(); }}
                    btMsg={ui.btMsg}
                    />
                
                <Inputbox
                    show={ui.inputbox}
                    title={ui.inputTitle}
                    placeholder={ui.inputPlaceholder}
                    submit={ui.inputSubmit}
                    content={ui.inputContent}
                    />
                <ConfirmBox
                    show={ui.showConfirmBox}
                    title={ui.confirmTitle}
                    content={ui.confirmContent}
                    confirmOkText={ui.confirmOkText}
                    confirmCancelText={ui.confirmCancelText}
                    onOk={ui.onConfirmOk}
                    onCancel={ui.onConfirmCancel}
                    centerButtonContent={ui.centerButtonContent}
                    centerButtonFunc={ui.centerButtonFunc}
                     />
                <QRconfirm
                    title={ui.QRtitle}
                    image={ui.QRimage}
                    content={ui.QRcontent}
                    show={ui.QRshow}
                    functionRefresh={ui.DoisoatProcess}
                    promotionId={ui.promotionId}
                    promotionTypeId ={ui.promotionTypeId}
                    receive={ui.ReceiveGift}
                    per={this.props.per}
                />
                <Doisoat
                    promotionTypeId ={ui.promotionTypeId}
                    promotionId={ui.promotionId}
                    show={ui.Doisoatshow}
                    //title={'Xác nhận thông tin'}
                    code={ui.Code}
                    receive={ui.ReceiveGift}
                    onClose={()=>{}}
                    functionRefresh={ui.DoisoatProcess}
                    per={this.props.per}
                />
                <ThreeBT
                    show={ui.mShow}
                    data={ui.mGreenData}
                />
                <Slide
                    show={ui.Rateshow}
                />
                <Storelet/>
                <Weblogin/>
                <Web/>
            </View>
        )
    }
}
const bindState = (state) => {
    return {ui: state.uiState,per:state.giftCountState.Percent}
}
const bindDispatch = (dispatch) => {
    return {
        hideMessageBox: () => {
            dispatch(hideMessageBox())
        },
        confirmOk: () => {
            dipatch(hideConfirm('ok'))
        },
        confirmCancel: () => {
            dipatch(hideConfirm('cancel'))
        }
    }
}

export default connect(bindState, bindDispatch)(UI)