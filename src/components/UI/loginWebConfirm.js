import React, { Component, PropTypes } from 'react';
import { ActivityIndicator, StyleSheet, Image, Text, View, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux'
import api from '../../api'
import dataService from '../../api/dataService'
import i18 from '../i18'
class Loading extends Component {
    constructor(props) {
        super(props);
    }
    _onPress(qr) {
        api.hideLoginWebConfirm();
        let isLoadding = true;
        setTimeout(() => {
            api.showLoading();
        }, 30)

        dataService.postScanQR(qr, this.props.token)
            .then(rs => {
                api.hideLoading();
                isLoadding = false;
                setTimeout(() => {
                    if (rs.err != 102) api.showMessage(rs.msg, i18.t('titleMsgLogin'))
                }, 30)
            })
        setTimeout(() => {
            if (isLoadding) {
                isLoadding = false;
                api.hideLoading();
                setTimeout(() => {
                    api.showMessage(i18.t('timeoutTextMsgContentLogin'), i18.t('titleMsgLogin'))
                }, 50)
            }
        }, 30000)
    }
    render() {
        const { showLoginConfirm, qr } = this.props.uiState
        if (showLoginConfirm) {
            return (
                <Image
                    source={require('../../img/bg_login.jpg')}
                    style={styles.wrap}>
                    <Image source={require('../../img/computer.png')}
                        style={{ width: api.getRealDeviceWidth() / 2, height: api.getRealDeviceWidth() / 2.43902439 }}
                    />
                    <Text style={{ backgroundColor: 'transparent', textAlign: 'center', marginTop: 10, color: '#fff' }}>
                        {i18.t('confirmLoginWeb')}
                    </Text>
                    <TouchableOpacity style={styles.btn}
                        onPress={() => { this._onPress(qr) }}
                    >
                        <Text style={{ color: '#fff' }}>
                            {(i18.t('loginButton')).toUpperCase()}
                        </Text>
                    </TouchableOpacity>
                    <Text style={{ backgroundColor: 'transparent', marginTop: 10, color: '#fff', textDecorationLine: 'underline' }}
                        onPress={api.hideLoginWebConfirm}
                    >{i18.t('close')}</Text>
                </Image>
            );
        }
        return null
    }
}
const styles = StyleSheet.create({
    wrap: {
        height: api.getRealDeviceHeight(),
        justifyContent: 'center',
        width: api.getRealDeviceWidth(),
        paddingVertical: api.getRealDeviceHeight() / 7,
        alignItems: 'center',
    },
    gray: {
        backgroundColor: 'red'
    },
    btn: { marginTop: api.getRealDeviceHeight() * .2, backgroundColor: '#fa6428', borderRadius: 15, paddingHorizontal: 30, paddingVertical: 7 }
});
mapStateToProps = (state) => ({ uiState: state.uiState, token: state.userState.token })
mapDispatchToProps = (dispatch) => ({})
export default connect(mapStateToProps, mapDispatchToProps)(Loading)