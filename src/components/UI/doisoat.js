import React, { Component, PropTypes } from 'react';
import {
    StyleSheet,
    View,
    Text,
    Modal, ScrollView, Platform
} from 'react-native';
import { Container, Content, Button, Input, InputGroup } from 'native-base'
import { connect } from 'react-redux'
import { showMessage, hideMessageBox, showInputBox, hideInputBox, hideDoiSoat } from '../../actions/uiActions'
import api from '../../api'
import i18 from '../../components/i18'
import dataService from '../../api/dataService'
import { } from '../../actions/uiActions'
// import sharedStyles from '../styles'
let un = ''
let pw = ''
let cost = ''
let code = ''
let per = ''
let setGiftCount = (token) => {
    dataService.getGiftCount(token)
        .then((rs) => {
            api.setGitfCount(rs.err == 0 ? rs.amount : '0')
        })
}
functionRefresh = () => {

}
let promotionTypeId = ''
class Doisoat extends Component {
    /**
   * Optional Flowtype state and timer types
   */
    static propTypes = {
        show: PropTypes.bool.isRequired,
        // title:PropTypes.string.isRequired,
        //onDoisoat:PropTypes.func.isRequired
    }
    constructor(props) {
        super(props);
        this.state = {
            msg: '',
            cost: '',
            have: '',
            remain: ''
        }
    }
    // componentDidUpdate(){
    //     if(!this.props.show){
    //         // this.setState({
    //         //     msg:''
    //         // })
    //     }
    // }
    _onDoisoat() {
        cost = cost.replace(/\./g, '')
        receiveGift = this.props.receive
        token = this.props.userState.token;
        if (un == '') {
            // api.showMessage('Hãy nhập vào tên đăng nhập',i18.t('titleMsgLogin'))
            this.setState({
                msg: i18.t('blankUsername')
            })
            return
        }
        if (pw == '') {
            // api.showMessage('Hãy nhập vào mật khẩu',i18.t('titleMsgLogin'))
            this.setState({
                msg: i18.t('blankPassword')
            })
            return
        }
        if (code == undefined || code == '') {
            api.showMessage(i18.t('blankPcode'), i18.t('titleMsgLogin'))
            return
        }
        if (/[^\d]/.test(cost)) {
            // api.showMessage('Hãy nhập vào tổng hóa đơn',i18.t('titleMsgLogin'))
            this.setState({
                msg: i18.t('invalidBill')
            })
            return
        }
        if (promotionTypeId == 7 && (cost == '' || cost < 0)) {
            // api.showMessage('Hãy nhập vào tổng hóa đơn',i18.t('titleMsgLogin'))
            this.setState({
                msg: i18.t('blankBill')
            })
            return
        }
        api.hideDoisoat()
        setTimeout(() => { api.showLoading() }, 1)
        dataService.confirmQR(this.props.uiState.promotionId, un, pw, code, cost, this.props.userState.token)
            .then((data) => {
                console.log('====================================');
                console.log('checkcode data', data);
                console.log('====================================');
                if (data.err != 0 && data.err != 11 && data.err != 4) {
                    api.showDoisoat(this.props.uiState.promotionId, i18.t('titleQRvetify'), code, receiveGift, functionRefresh)
                    this.setState({ msg: data.msg })
                }
                else if (data.err == 0) {
                    api.showStorelet(data.point ? data.point + '\n' + i18.t('point') : per ? per + '%' : '', data.msg)
                }else if(data.err == 11){
                    api.showStorelet(null,data.msg,null,null,true)
                }
                else {
                    api.showMessage(data.msg, i18.t('titleMsgLogin'))

                }
                if (data.err == 0) {
                    setGiftCount(token)
                    dataService.getUserInfo(token)
                        .then((o => {
                            if (o.err == 0)
                                api.setUserInfo(o)
                        }))
                    //if(receiveGift){
                    // }
                }
                api.hideLoading()
                un = ''
                pw = ''
                cost = ''
                this.setState({ cost: '', have: '', remain: '' })
                if (functionRefresh != undefined && (data.err == 0 || data.err == 11 || data.err == 4)) functionRefresh()
            })
    }
    onChangeText(text) {
        cost = text

        c = cost.replace(/\./g, '')
        if (/[^\d]/.test(c)) return this.setState({ msg: i18.t('invalidBill') })
        have = parseInt(c) * per / 100
        remain = Math.round(parseInt(c) - have)
        c = this.renderDot(c)
        have = this.renderDot(have) + ' đ'
        remain = this.renderDot(remain) + ' đ'
        this.setState({ msg: '', cost: c, have: cost != '' ? have : '0 đ', remain: cost != '' ? remain : ' 0 đ' })
    }
    renderDot(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
    }
    _close() {
        this.props.hideDoisoat(); this.setState({ msg: 0, cost: '', remain: '', have: '' })
        un = ''; pw = ''; code = ''; cost = ''
    }
    render() {
        per = this.props.per
        promotionTypeId = this.props.promotionTypeId
        place = i18.t('bill')
        if (this.props.promotionTypeId == 7) {
            place += ' (*)'
        }
        if (this.props.show) {
            code = this.props.code
            functionRefresh = this.props.functionRefresh
            // alert(this.props.promotionId)
            return (
                <Modal
                    transparent={true}
                    visible={true}
                    animationType='slide'
                    onRequestClose={() => { }}>
                    <Container
                        style={{
                            flex: 1,
                            backgroundColor: 'rgba(0, 0, 0, 0.52);'
                        }}>
                        <ScrollView
                            keyboardShouldPersistTaps={'handled'}>
                            <View style={styles.backdrop}>
                                <View style={styles.modal}>
                                    <View style={styles.modal_content}>
                                        <Text style={styles.modal_title}>{i18.t('checkcodeTitle')}</Text>
                                        {
                                            Platform.OS == 'ios' ? <View style={{ height: 1, width: api.getRealDeviceWidth() * 0.8 - 20, backgroundColor: '#cdcdcd' }}>

                                            </View> : null
                                        }
                                        {
                                            this.state.msg != '' ? <Text style={{ textAlign: 'center', padding: 10, color: 'red', borderBottomColor: "#cdcdcd", borderBottomWidth: 1 }}>
                                                {this.state.msg}
                                            </Text> : null
                                        }
                                        <View style={{ padding: 10 }}>
                                            <InputGroup>
                                                <Input placeholderTextColor='gray'
                                                    style={{ color: 'gray' }}
                                                    autoFocus={true}
                                                    onChangeText={(text) => { un = text; this.setState({ msg: '' }) }} placeholder={i18.t('placeholderUsername') + ' (*)'} />
                                            </InputGroup>
                                            <InputGroup>
                                                <Input style={{ color: 'gray' }} placeholderTextColor='gray' onChangeText={(text) => { pw = text; this.setState({ msg: '' }) }} placeholder={i18.t('passPlaceholder') + ' (*)'} secureTextEntry={true} />
                                            </InputGroup>
                                            {
                                                this.props.receive ? null : <InputGroup>
                                                    <Input keyboardType='phone-pad' style={{ color: 'gray' }} placeholderTextColor='gray'
                                                        value={this.state.cost}
                                                        onChangeText={this.onChangeText.bind(this)} placeholder={place}
                                                    />
                                                </InputGroup>
                                            }
                                            {
                                                this.props.per != '' && this.props.promotionTypeId == 1 ? <View><Text style={{ paddingLeft: 5, borderBottomColor: '#cdcdcd', paddingVertical: 10, borderBottomWidth: 1, color: 'gray' }}>{i18.t('promotion')}:  {this.state.have}</Text>{
                                                    Platform.OS == 'ios' ? <View style={{ height: 1, width: api.getRealDeviceWidth() * 0.8 - 40, backgroundColor: '#cdcdcd' }}>

                                                    </View> : null
                                                }</View> : null
                                            }
                                            {
                                                this.props.per != '' && this.props.promotionTypeId == 1 ? <View><Text style={{ paddingLeft: 5, borderBottomColor: '#cdcdcd', paddingVertical: 10, borderBottomWidth: 1, color: 'gray' }}>{i18.t('remain')}:  {this.state.remain}</Text>{
                                                    Platform.OS == 'ios' ? <View style={{ height: 1, width: api.getRealDeviceWidth() * 0.8 - 40, backgroundColor: '#cdcdcd' }}>

                                                    </View> : null
                                                }</View> : null

                                            }
                                        </View>
                                        <View style={{ flexDirection: 'row', padding: 10 }}>
                                            <Button onPress={this._close.bind(this)} style={{ flex: 1, backgroundColor: 'gray' }} >{i18.t('close')}</Button>
                                            <Button onPress={this._onDoisoat.bind(this)} style={{ flex: 1, marginLeft: 10, backgroundColor: '#fa6428' }}>{i18.t('vertify')}</Button>
                                        </View>
                                    </View>
                                </View>
                            </View>
                        </ScrollView>
                    </Container>
                </Modal>
            );
        }
        return null

    }
}
const styles = StyleSheet.create({
    button_bottom: {
        borderBottomLeftRadius: 2,
        borderBottomRightRadius: 2
    },
    backdrop: {
        //backgroundColor: 'rgba(0, 0, 0, 0.52);',
        flex: 1
    },
    modal: {
        marginTop: api.getRealDeviceHeight() / 7,
        // marginTop:50,
        maxWidth: 500,
        width: api.getRealDeviceWidth() * 0.8,

        borderRadius: 5, overflow: 'hidden',
        alignSelf: 'center'
    },
    modal_content: {
        backgroundColor: 'white',
        borderRadius: 2,
        flex: 1
    },
    modal_title: {
        borderBottomWidth: 1,
        borderBottomColor: '#ddd',
        padding: 5,
        paddingBottom: 10,
        textAlign: 'center',
        fontSize: 20,
        fontWeight: 'bold'
    },
    modal_text_content: {
        padding: 10,
        flex: 1
    },
    button_container: {
        flex: 1,
        height: 50
    }
});
mapStateToProps = (state) => (state)
mapDispatchToProps = (dispath) => ({
    hideDoisoat: () => dispath(hideDoiSoat()),
})
export default connect(mapStateToProps, mapDispatchToProps)(Doisoat)
