import React, { Component, PropTypes } from 'react';
import {
    ActivityIndicator,
    StyleSheet,
    View,
    Text,
    Modal,
    TouchableHighlight, Platform
} from 'react-native';
import i18 from '../../components/i18'
import {
    Container,
    Header,
    Content,
    Button,
    Footer,
    FooterTab
} from 'native-base'
import { connect } from 'react-redux'
import api from '../../api'
// import sharedStyles from '../style'
class MessageBox extends Component {
    static propTypes = {
        title: PropTypes.string.isRequired,
        content: PropTypes.string.isRequired,
        show: PropTypes.bool.isRequired,
        onClose: PropTypes.func.isRequired
    }
    constructor(props) {
        super(props);
    }

    render() {
        if (this.props.show) {
            return (
                <Modal
                    style={{ zIndex: 10000 }}
                    transparent={true}
                    visible={true}
                    animationType='fade'
                    onRequestClose={() => { }}>
                    <Container
                        style={{
                            flex: 1,
                            backgroundColor: 'rgba(0, 0, 0, 0.52);'
                        }}>
                        <Content keyboardShouldPersistTaps={'handled'}>
                            <View style={styles.backdrop}>
                                <View style={styles.modal}>
                                    <View style={styles.modal_content}>
                                        <Text style={styles.modal_title}>{this.props.title}</Text>
                                        {
                                            Platform.OS == 'ios' ? <View style={{ height: 1, flex: 1, backgroundColor: '#cdcdcd' }}>

                                            </View> : null
                                        }
                                        <View>
                                            <Text style={styles.modal_text_content}>{this.props.content}</Text>
                                        </View>
                                        <Button
                                            style={{ borderRadius: 5, alignSelf: 'center', paddingLeft: api.getRealDeviceWidth() / 9, paddingRight: api.getRealDeviceWidth() / 9, backgroundColor: '#fa6428', marginBottom: 10, marginLeft: 10, marginRight: 10 }}
                                            type='button'
                                            onPress={this.props.onClose}>{this.props.btMsg||i18.t('close')}</Button>
                                    </View>
                                </View>

                            </View>

                        </Content>
                    </Container>
                </Modal>
            );
        }
        return null

    }
}
const styles = StyleSheet.create({
    button_bottom: {
        borderBottomLeftRadius: 2,
        borderBottomRightRadius: 2
    },
    backdrop: {
        flex: 1,
        zIndex: 1000,
    },
    modal: {
        marginTop: api.getRealDeviceHeight() / 3,
        maxWidth: 500,

        borderRadius: 5, overflow: 'hidden',
        minWidth: 300,
        alignSelf: 'center'
    },
    modal_content: {
        backgroundColor: 'white',
        borderRadius: 2,
        marginHorizontal:api.getRealDeviceWidth()*.06
    },
    modal_title: {
        borderBottomWidth: 1,
        borderBottomColor: '#ddd',
        padding: 5,
        textAlign: 'center',
        fontWeight: 'bold'
    },
    modal_text_content: {
        padding: 10,
        flex: 1,
        marginBottom: 20
    },
    button_container: {
        flex: 1,
        height: 50
    }
});
export default MessageBox