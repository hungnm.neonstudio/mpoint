import React, { PureComponent } from 'react';
import { View, ScrollView, Image, StyleSheet, Platform, Text } from 'react-native';
import api from '../api';
import i18 from './i18';
import { connect } from 'react-redux';
// import component
import GiftStick from './tab/giftStick';
import GiftCardStick from './tab/giftCardStick';
import { TabViewAnimated, TabBar, SceneMap } from 'react-native-tab-view';
class MyGift extends PureComponent {
    state = {
        index: 0,
        routes: [
            { key: '1', title: i18.t('giftstick') },
            { key: '2', title: i18.t('stampstick') }
        ],
    };
    _handleChangeTab = index => this.setState({ index });

    _renderHeader = props => <TabBar {...props}
        style={{ backgroundColor: '#fdfdfd' }}
        indicatorStyle={{ borderBottomColor: '#fa6428', borderBottomWidth: 4 }}
        renderLabel={(payload) => { return <Text style={{ color: payload.focused ? '#fa6428' : '#000', fontWeight: '700', fontSize: 15 }}>{payload.route.title}</Text> }}

    />;

    _renderScene = SceneMap({
        '1': () => { return <GiftStick/> },
        '2': () => { return <GiftCardStick  gotoPromotion={this.props.gotoPromotion} gotoGift={this.props.gotoGift}/> },
    });
    _subString(str) {
        if (str.length > 8) {
            return str.substring(0, 5) + '...'
        } else return str
    }
    render() {
        return <Image
            source={require('../img/bg_login.jpg')}
            style={{ width: api.getRealDeviceWidth(), height: api.getRealDeviceHeight() - 115 }}
        ><TabViewAnimated
                style={styles.container}
                navigationState={this.state}
                renderScene={this._renderScene}
                renderHeader={this._renderHeader}
                onRequestChangeTab={this._handleChangeTab}
            /></Image>
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
});
mapStateToProps = (state) => ({ lang: state.langState.lang })
mapDispatchToProps = () => ({})
export default connect(mapStateToProps, mapDispatchToProps)(MyGift)
// import React, {Component} from 'react';
// import {View,ScrollView,Image,Platform,scrollview} from 'react-native';
// import GiftStick from './tab/giftStick';
// import GiftCardStick from './tab/giftCardStick';
// import api from '../api'
// import i18 from './i18'
// import {connect} from 'react-redux'
// import {IndicatorViewPager,PagerTitleIndicator} from 'rn-viewpager'
// class MyGift extends Component {
//      main = <View></View>
//     constructor(props) {
//         super(props);
//         main = <IndicatorViewPager
//                 style={{
//                 width:api.getRealDeviceWidth(),
//                 height:Platform.OS=='ios'? api.getRealDeviceHeight()-151:api.getRealDeviceHeight(),
//                 backgroundColor: 'white',
//                 flexDirection: 'column-reverse'
//             }}
//                 initialPage={this.props.page}
//                 indicator={this._renderTitleIndicator()}>
//                 <View>
//                 <Image
//                         source={ require('../img/bg_login.jpg') }
//                         style= {{ width:api.getRealDeviceWidth(),height:api.getRealDeviceHeight()-155 }}
//                     >

//                     <GiftStick/>
//                     </Image>
//                 </View>
//                 <View>
//                 <Image
//                         source={ require('../img/bg_login.jpg') }
//                         style= {{ width:api.getRealDeviceWidth(),height:api.getRealDeviceHeight()-155 }}
//                     >
//                     <GiftCardStick  gotoPromotion={this.props.gotoPromotion} gotoGift={this.props.gotoGift}/>
//                     </Image>
//                 </View>
//             </IndicatorViewPager>    
//     }
//     _renderTitleIndicator() {
//         return <PagerTitleIndicator titles={[i18.t('giftstick'), i18.t('stampstick')]}
//         itemTextStyle={{ fontWeight:Platform.OS == 'ios' ? '700':'500' }}
//         selectedBorderStyle ={{ borderWidth:2,borderColor:'#fa6428' }}
//         selectedItemTextStyle={{ fontWeight:Platform.OS == 'ios' ? '700':'500' }}
//         />;
//     }
//     render() {
//         if(Platform.OS == 'ios')
//         return (
//         <ScrollView style={{ width:api.getRealDeviceWidth(),height:Platform.OS=='ios'? api.getRealDeviceHeight()-151:api.getRealDeviceHeight() }}>
//             {main}
//         </ScrollView>)
//         else
//         return (
//             main
//         );
//     }
// }
// mapStateToProps=(state)=>({lang:state.langState.lang})
// mapDispatchToProps=()=>({})
// export default connect(mapStateToProps, mapDispatchToProps)(MyGift)