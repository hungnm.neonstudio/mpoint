import React from 'react'
import {
    View,
    Text,
    TouchableOpacity,
    ListView,
    Modal,
    Image
} from 'react-native'
import {connect} from 'react-redux'
import api from '../../api'
import {Button} from 'native-base'
class ItemMymenu extends React.PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            show: false
        };
    }
    componentWillMount() {
        // alert(JSON.stringify((this.props.index)))
    }
    _renderItem = (item) => {
        console.log('====================================');
        console.log(item);
        console.log('====================================');
        if (!this.state.show || this.props.type != 'general') 
            return;
        const ds = new ListView.DataSource({
            rowHasChanged: (r1, r2) => r1 !== r2
        });
        return <Modal
            transparent={true}
            visible={this.state.show}
            animationType='fade'
            onRequestClose={() => {}}>
            <View
                style={{
                height: api.getRealDeviceHeight(),
                width: api.getRealDeviceWidth(),
                backgroundColor: 'rgba(0,0,0,.8)'
            }}>
                <View
                    style={{
                    zIndex: 1,
                    borderRadius: 3,
                    backgroundColor: '#fff',
                    width: api.getRealDeviceWidth() * .9,
                    alignSelf: 'center',
                    flex: 1,
                    marginVertical: api.getRealDeviceHeight() * .09
                }}>
                    <Text
                        style={{
                        paddingVertical: 5,
                        fontSize: 17,
                        fontWeight: '300',
                        alignSelf: 'center'
                    }}>{(item.name || '').toUpperCase()}</Text>
                    <View
                        style={{
                        height: 1,
                        backgroundColor: '#cdcdcd'
                    }}/>
                    <View style={{
                        flex: 1
                    }}>
                        <Image
                            source={{
                            uri: item.img || 'http://g9vietnam.com.vn/wp-content/uploads/2016/12/cac-dich-vu-tu-van-setup-nha-hang-ban-nen-biet1.jpg'
                        }}
                            style={{
                            height: (api.getRealDeviceWidth() - 20) / 1.9113
                        }}/>
                        <View
                            style={{
                            paddingLeft: 10
                        }}>
                            <Text
                                style={{
                                marginTop: 20
                            }}>{i18.t('cost')}: {item.price}
                                {i18.t('moneyUnit')}</Text>
                            <Text
                                style={{
                                marginTop: 5,
                                fontWeight: '400'
                            }}>{i18.t('detail')}
                                :</Text>
                            <Text>{item.decription}</Text>
                        </View>
                    </View>
                    <Button
                        block
                        style={{
                        marginBottom: -0.5,
                        backgroundColor: "#fa6428",
                        borderTopLeftRadius: 0,
                        borderTopRightRadius: 0
                    }}
                        onPress={() => {
                        this.setState({show: false})
                    }}>
                        <Text
                            style={{
                            fontSize: 16,
                            color: '#fff'
                        }}>
                            {i18.t('close')}
                        </Text>
                    </Button>
                </View>
                <TouchableOpacity
                    activeOpacity={1}
                    style={{
                    position: 'absolute',
                    height: api.getRealDeviceHeight(),
                    width: api.getRealDeviceWidth()
                }}
                    onPress={() => {
                    this.setState({show: false})
                }}/>
            </View>
        </Modal>
    }
    _renderCombo(item) {
        if (!this.state.show || this.props.type != 'combo') 
            return;
        const ds = new ListView.DataSource({
            rowHasChanged: (r1, r2) => r1 !== r2
        });
        return <Modal
            transparent={true}
            visible={this.state.expanded}
            animationType='fade'
            onRequestClose={() => {}}>
            <View
                style={{
                height: api.getRealDeviceHeight(),
                width: api.getRealDeviceWidth(),
                backgroundColor: 'rgba(0,0,0,.8)'
            }}>

                <View
                    style={{
                    zIndex: 1,
                    borderRadius: 3,
                    backgroundColor: '#fff',
                    width: api.getRealDeviceWidth() * .9,
                    alignSelf: 'center',
                    flex: 1,
                    marginVertical: api.getRealDeviceHeight() * .09
                }}>
                    <Text
                        style={{
                        paddingVertical: 5,
                        fontSize: 17,
                        fontWeight: '300',
                        color: '#000',
                        alignSelf: 'center'
                    }}>{(item.name || '').toUpperCase()}</Text>
                    <View
                        style={{
                        height: 1,
                        backgroundColor: '#cdcdcd'
                    }}/>
                    <ListView
                        style={{
                        padding: 5
                    }}
                        enableEmptySections={true}
                        dataSource={ds.cloneWithRows(item.itemCombos)}
                        renderRow={(item, sectionID, rowID) => {
                        return (
                            <View
                                style={{
                                marginTop: 5,
                                borderColor: '#cdcdcd',
                                borderWidth: 1,
                                paddingVertical: 5,
                                borderRadius: 3
                            }}>
                                <Text
                                    style={{
                                    marginLeft: 5,
                                    backgroundColor: 'transparent',
                                    fontWeight: '300',
                                    color: '#000'
                                }}>{(item.item.name || '').toUpperCase()}</Text>
                                <Image
                                    style={{
                                    height: (api.getRealDeviceWidth() - 45) / 1.9113,
                                    width: api.getRealDeviceWidth() - 45
                                }}
                                    source={{
                                    uri: item.item.img || 'https://image.freepik.com/free-icon/meat-slice-silhouette_318-42659.jpg',
                                    height: 30,
                                    width: 30
                                }}/>
                                <Text
                                    style={{
                                    marginLeft: 5,
                                    backgroundColor: 'transparent'
                                }}>{i18.t('numitem') + ': ' + item.amount}</Text>
                                <Text
                                    style={{
                                    marginLeft: 5,
                                    backgroundColor: 'transparent'
                                }}>{i18.t('costNoPromotion') + ': ' + item.item.price} {i18.t('moneyUnit')}</Text>
                                <Text
                                    style={{
                                    marginLeft: 5,
                                    backgroundColor: 'transparent'
                                }}>{item.item.decription}</Text>
                            </View>
                        );
                    }}/>
                    <Button
                        block
                        style={{
                        marginBottom: -0.5,
                        backgroundColor: "#fa6428",
                        borderTopLeftRadius: 0,
                        borderTopRightRadius: 0
                    }}
                        onPress={() => {
                        this.setState({show: false})
                    }}>
                        <Text
                            style={{
                            fontSize: 16,
                            color: '#fff'
                        }}>
                            {i18.t('close')}
                        </Text>
                    </Button>
                </View>
                <TouchableOpacity
                    activeOpacity={1}
                    style={{
                    position: 'absolute',
                    height: api.getRealDeviceHeight(),
                    width: api.getRealDeviceWidth()
                }}
                    onPress={() => {
                    this.setState({show: false})
                }}/>
            </View>
        </Modal>

    }
    _changnum(item, plus, type) {
        let arr = [];
        if (type == 'combo') 
            arr = this.props.imenuState.combo;
        else if (type == 'general') 
            arr = this.props.imenuState.item;
        let temp = [];
        arr.map(el => {
            if (item.id != el.id) {
                temp.push(el)
            } else {
                if (el.amount + plus > 0) 
                    temp.push({
                        ...el,
                        amount: el.amount + plus
                    });
                }
            })
        if (type == 'general') 
            api.updateBookItem(temp);
        else if (type == 'combo') 
            api.updateBookCombo(temp);
        }
    render() {
        let item = [];
        let type = this.props.type;
        if (type == 'combo') 
            item = this.props.imenuState.combo[this.props.index];
        if (type == 'general') 
            item = this.props.imenuState.item[this.props.index];
        
        return (
            <TouchableOpacity
                style={{
                flexDirection: 'row',
                backgroundColor: '#fff',
                borderWidth: 1,
                borderColor: '#cdcdcd',
                borderRadius: 5,
                marginTop: 10
            }}
                onPress={() => {
                this.setState({show: true})
            }}>
                {this._renderCombo(item)}
                {this._renderItem(item)}
                <View style={{
                    flex: 5
                }}>
                    <View style={{
                        flex: 1
                    }}/>
                    <Text
                        style={{
                        marginLeft: 5,
                        color: '#000'
                    }}>{(item.name || '').toUpperCase()}</Text>
                    <View style={{
                        flex: 1
                    }}/>
                </View>
                <View style={{
                    flex: 3
                }}>
                    <View style={{
                        flex: 1
                    }}/>
                    <View
                        style={{
                        flexDirection: 'row'
                    }}>
                        <View style={{
                            flex: 1
                        }}>
                            <Text
                                style={{
                                color: '#000'
                            }}>{item.price * item.amount}</Text>
                            {/*<Text style={{ alignSelf:'center',color:'#000' }}>VNĐ</Text>*/}
                        </View>
                        <Text
                            style={{
                            paddingHorizontal: 5,
                            color: '#000'
                        }}>{item.amount}</Text>
                    </View>
                    <View style={{
                        flex: 1
                    }}/>
                </View>
                <View style={{
                    flex: 1
                }}>
                    <TouchableOpacity
                        style={{
                        padding: 5
                    }}
                        onPress={() => {
                        this._changnum(item, 1, type);
                    }}>
                        <Text
                            style={{
                            textAlign: 'center',
                            fontSize: 17,
                            color: '#000'
                        }}>+</Text>
                    </TouchableOpacity>
                    <View
                        style={{
                        height: 1,
                        backgroundColor: '#cdcdcd'
                    }}/>
                    <TouchableOpacity
                        style={{
                        padding: 5
                    }}
                        onPress={() => {
                        this._changnum(item, -1, type);
                    }}>
                        <Text
                            style={{
                            textAlign: 'center',
                            fontSize: 17,
                            color: '#000'
                        }}>-</Text>
                    </TouchableOpacity>
                </View>
            </TouchableOpacity>
        )
    }
}
mapStateToProps = (state) => {
    return {imenuState: state.imenuState}
}
mapDispatchToProps = () => {
    return {}
}
export default connect(mapStateToProps, mapDispatchToProps)(ItemMymenu);