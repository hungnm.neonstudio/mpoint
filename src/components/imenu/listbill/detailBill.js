import React, { Component } from 'react';
import { View, Image, Text, ScrollView, Platform, ActivityIndicator, AsyncStorage, PermissionsAndroid, Modal, ListView, TouchableOpacity } from 'react-native';
import {
    Header,
    Title,
    Container,
    Content,
    Button,
    Icon,
    Input,
    InputGroup
} from 'native-base';
import api from '../../../api';
import dataService from '../../../api/dataService';
import DeviceInfo from 'react-native-device-info'
import { connect } from 'react-redux';
import PopupDialog, { SlideAnimation, DialogTitle } from 'react-native-popup-dialog';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import FontAwesomeIcons from 'react-native-vector-icons/FontAwesome';
import i18 from '../../i18'
class AddCardFromLogin extends Component {

    componentDidMount() {

    }


    render() {
        const ds = new ListView.DataSource({
            rowHasChanged: (r1, r2) => r1 !== r2
        });

        let { createdAt, totalPrice, statusId, VAT } = this.props.data
        // let { name, phone } = this.props.data.endUser
        let { name, address } = this.props.data.shop;
        let status = ''
        switch (statusId) {
            case 1:
                status = i18.t('noprocess')
                break;
            case 2:
                status = i18.t('processing')
                break;
            case 3:
                status = i18.t('processed')
                break;
            case 4:
                status = i18.t('waitPay')
                break;
            case 5:
                status = i18.t('cancelBook')
                break;
            default:
                break;
        }
        return (
            <View style={{ paddingHorizontal: 5 }}>
                <Text>
                    {i18.t('createAt')} : {api.getTime(createdAt, 'DD/MM/YYYY HH:mm')}
                </Text>
                <Text>
                    {i18.t('restaurantName')} : {name}
                </Text>
                <Text>
                    {i18.t('addr')} : {address}
                </Text>
                <Text>
                    {i18.t('status')} : {status}
                </Text>
                <View style={{ marginTop: 10, paddingHorizontal: 10, borderTopColor: '#cdcdcd', borderTopWidth: 1, borderBottomColor: '#cdcdcd', borderBottomWidth: 1 }}>

                    <ListView
                        contentContainerStyle={{ paddingBottom: 10 }}
                        enableEmptySections={true}
                        renderHeader={() => {
                            return <View style={{ flexDirection: 'row' }}>
                                <View style={{ flex: 1 }}>
                                    <Text>Món</Text>
                                </View>
                                <View>
                                    <Text style={{ paddingHorizontal: 3 }}>SL</Text>
                                </View>
                                <View>
                                    <Text style={{ paddingHorizontal: 3 }}>ĐG</Text>
                                </View>
                            </View>
                        }}
                        dataSource={ds.cloneWithRows(this.props.data.book.bookCombos)}
                        renderRow={(item, id) => {
                            return <View style={{ flexDirection: 'row' }}>
                                <View style={{ flex: 1 }}>
                                    <Text>{item.combo.name}</Text>
                                </View>
                                <View>
                                    <Text style={{ paddingHorizontal: 3 }}>{item.amount}</Text>
                                </View>
                                <View>
                                    <Text style={{ paddingHorizontal: 3 }}>{item.price}</Text>
                                </View>
                            </View>
                        }}
                    />
                    <View style={{ justifyContent: 'space-between', flexDirection: 'row' }}>
                        <Text>VAT :</Text>
                        <Text>{VAT}</Text>
                    </View>
                    <View style={{ justifyContent: 'space-between', flexDirection: 'row' }}>
                        <Text>{i18.t('total')} : </Text>
                        <Text>{totalPrice}</Text>
                    </View>
                </View>
            </View>
        );
    }
}
const mapState = (state) => {
    return { navState: state.navState, userState: state.userState }
}
const mapDispatch = (dispatch) => {
    return {
    }
}
export default connect(mapState, mapDispatch)(AddCardFromLogin);