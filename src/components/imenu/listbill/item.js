import React, { Component } from 'react';
import { View, Image, Text, ListView, ScrollView, Platform, AsyncStorage, PermissionsAndroid, Modal, TouchableOpacity } from 'react-native';
import {
    Header,
    Title,
    Container,
    Content,
    Button,
    Icon,
    Input,
    InputGroup, Footer, FooterTab
} from 'native-base';
import api from '../../../api';
import dataService from '../../../api/dataService';
import DeviceInfo from 'react-native-device-info'
import { connect } from 'react-redux';
import FontAwesomeIcons from 'react-native-vector-icons/FontAwesome';
import i18 from '../../i18'
import IconEntypo from 'react-native-vector-icons/Entypo'
import * as Animated from 'react-native-animatable'
import IconsFontAwesome from 'react-native-vector-icons/FontAwesome';
import DetailBill from './detailBill'
class Item extends Component {
    constructor(props) {
        super(props)
        this.state = {
            show: false,
            data: {}
        }
    }
    componentDidMount() {
        dataService.getDetailBill(this.props.data.id).
            then(rs => {
                if (rs.err == 0) {
                    this.setState({
                        data: rs.data
                    })
                }
            })
    }
    _renderPopup = () => {
        const ds = new ListView.DataSource({
            rowHasChanged: (r1, r2) => r1 !== r2
        });
        return <Modal
            transparent={true}
            visible={this.state.show}
            animationType='fade'
            onRequestClose={() => { }}>
            <Modal
                transparent={true}
                visible={this.state.show}
                animationType='slide'
                onRequestClose={() => { }}>
                <View style={{ zIndex: 1, borderRadius: 3, backgroundColor: '#fff', width: api.getRealDeviceWidth() * .9, alignSelf: 'center', flex: 1, marginVertical: api.getRealDeviceHeight() * .09 }}>
                    <Text style={{ paddingVertical: 5, fontSize: 16, fontWeight: '300', alignSelf: 'center' }}>{(i18.t('detailBill')).toUpperCase()}</Text>
                    <View style={{ height: 1, backgroundColor: '#cdcdcd' }} />
                    <View style={{ flex: 1 }}>
                        <DetailBill
                            data={this.state.data}
                        />
                    </View>
                    <Button block style={{ marginBottom: -0.5, backgroundColor: "#fa6428", borderTopLeftRadius: 0, borderTopRightRadius: 0 }}
                        onPress={() => {
                            this.setState({
                                show: false
                            })
                        }}>
                        <Text style={{ fontSize: 16, color: '#fff' }}>
                            {i18.t('close')}
                        </Text>
                    </Button>
                </View>
                <TouchableOpacity
                    activeOpacity={1}
                    style={{ position: 'absolute', height: api.getRealDeviceHeight(), width: api.getRealDeviceWidth() }}
                    onPress={() => {
                        this.setState({
                            show: false
                        })
                    }}
                />
            </Modal>
            <View style={{ height: api.getRealDeviceHeight(), width: api.getRealDeviceWidth(), backgroundColor: 'rgba(0,0,0,.8)' }} />
        </Modal>
    }
    _cancelBill = (id) => {
        
    }
    render() {
        let item = this.props.data
        const ds = new ListView.DataSource({
            rowHasChanged: (r1, r2) => r1 !== r2
        });
        let { statusId } = this.props.data;
        let status = ''
        switch (statusId) {
            case 1:
                status = i18.t('noprocess')
                break;
            case 2:
                status = i18.t('processing')
                break;
            case 3:
                status = i18.t('processed')
                break;
            case 4:
                status = i18.t('waitPay')
                break;
            case 5:
                status = i18.t('cancelBook')
                break;
            default:
                break;
        }
        return (<TouchableOpacity
            onPress={() => {
                this.setState({
                    show: true
                })
            }}
            style={{ backgroundColor: '#fff', borderRadius: 2, borderWidth: 1, borderColor: '#cdcdcd', padding: 5, marginHorizontal: 5, marginTop: 5 }}
        >
            {this._renderPopup()}
            <View style={{ justifyContent: 'space-between', flexDirection: 'row', }}>
                <Text>id bill: {item.id}</Text>
                <Text>{api.getTime(item.createdAt, 'HH:mm DD/MM/YYYY')}</Text>
            </View>
            <View style={{ justifyContent: 'space-between', flexDirection: 'row', }}>
                <Text>VAT{': ' + item.VAT}</Text>
                <Text>{i18.t('status')}: {status}</Text>
            </View>
            <View style={{ justifyContent: 'space-between', flexDirection: 'row', alignItems: 'center', marginTop: 5 }}>
                <Text>{i18.t('total') + ': ' + item.totalPrice}</Text>
                {
                    statusId == 4 ? <TouchableOpacity
                        style={{ padding: 3, paddingHorizontal: 5, borderRadius: 2, backgroundColor: '#fa6428' }}
                    >
                        <Text style={{ color: '#fff' }}>
                            {i18.t('pay')}
                        </Text>
                    </TouchableOpacity> : (statusId == 1 || statusId == 2 || statusId == 3) ? <TouchableOpacity
                        style={{ padding: 3, paddingHorizontal: 5, borderRadius: 2, backgroundColor: '#fa6428' }}
                    >
                        <Text style={{ color: '#fff' }}>
                            {i18.t('cancelBook')}
                        </Text>
                    </TouchableOpacity> : null
                }
            </View>
        </TouchableOpacity>
        );
    }
}
const mapState = (state) => {
    return { navState: state.navState, userState: state.userState, imenuState: state.imenuState }
}
const mapDispatch = (dispatch) => {
    return {
        setUserInfo: (data) => dispatch(setUserInfo(data)),
    }
}
export default connect(mapState, mapDispatch)(Item);