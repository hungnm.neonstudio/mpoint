import React, { Component } from 'react';
import { View, Image, Text, ListView, ScrollView, Platform, ActivityIndicator, AsyncStorage, PermissionsAndroid, Modal, TouchableOpacity } from 'react-native';
import {
    Header,
    Title,
    Container,
    Content,
    Button,
    Icon,
    Input,
    InputGroup, Footer, FooterTab
} from 'native-base';
import api from '../../../api';
import dataService from '../../../api/dataService';
import DeviceInfo from 'react-native-device-info'
import { connect } from 'react-redux';
import PopupDialog, { SlideAnimation, DialogTitle } from 'react-native-popup-dialog';
import { setUserInfo, setUserToken, setUuid } from '../../../actions/userActions'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import i18 from '../../i18'
import IconEntypo from 'react-native-vector-icons/Entypo'
import DatePicker from 'react-native-datepicker'
class Order extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showPopup: false,
            data: null,
            loading: false
        }
    }

    _loadData = () => {
        this.setState({
            loading: true,
            data: null
        });
        dataService.getDetailBill(this.props.imenuState.billId)
            .then(rs => {
                let data = null;
                if (rs.err == 0) data = rs.data;
                this.setState({
                    loading: false,
                    data: data
                })
            })
    }
    viewBill = () => {
        this.setState({ showPopup: true, data: null })
        setTimeout(() => {
            this._loadData();
        }, 30);
    }
    detaillBill = () => {
        if (!this.state.data) return null;
        const ds = new ListView.DataSource({
            rowHasChanged: (r1, r2) => r1 !== r2
        });
        let data = this.state.data;
        return <ScrollView style={{ paddingHorizontal: 5 }}>
            <Text>{api.getTime(data.createdAt, 'HH[h]mm DD/MM/YYYY')}</Text>
            <Text>{data.shop.name}</Text>
            <Text>{data.shop.address}</Text>
            <Text>VAT: {data.VAT}</Text>
            <Text>{i18.t('total')}: {data.totalPrice}</Text>
            <ListView
                style={{ width: api.getRealDeviceWidth(), paddingLeft: 0 }}
                enableEmptySections={true}
                dataSource={ds.cloneWithRows(data.book.bookCombos)}
                renderRow={(item, indexCol, indexRow) => {
                    return <View key={indexRow} style={{ paddingTop: 10 }}>
                        <Text>{item.combo.name}</Text>
                        <Text>{i18.t('numitem')}: {item.amount}</Text>
                        <Text>{i18.t('cost')}: {item.price}</Text>
                        <View style={{ height: 1, backgroundColor: '#cdcdcd' }} />
                    </View>
                }} />
            <ListView
                style={{ width: api.getRealDeviceWidth(), paddingLeft: 0 }}
                enableEmptySections={true}
                dataSource={ds.cloneWithRows(data.book.bookItems)}
                renderRow={(item, indexCol, indexRow) => {
                    return <View key={indexRow} style={{ paddingTop: 10 }}>
                        <Text>{item.item.name}</Text>
                        <Text>{i18.t('numitem')}: {item.amount}</Text>
                        <Text>{i18.t('cost')}: {item.price}</Text>
                        <View style={{ height: 1, backgroundColor: '#cdcdcd' }} />
                    </View>
                }} />
        </ScrollView>;
    }
    pay = () => {
        this.setState({
            showPopup: false
        });
        api.showLoading();
        let isLoading = true;
        dataService.callPay(this.props.imenuState.billId)
            .then(rs => {
                api.hideLoading();
                api.showMessage(rs.msg);
                isLoading: false;
                if (rs.err == 0) {
                    api.resetRoute({ key: 'home' });
                    api.clearBook();
                }
            })
        setTimeout(() => {
            if (isLoading) {
                isLoading = false;
                api.showMessage(i18.t('timeoutTextMsgContentLogin'), i18.t('titleMsgLogin'));
                api.hideLoading();
            }
        }, 30000);
    }
    _popUp = () => {
        return <Modal
            transparent={true}
            visible={this.state.showPopup}
            animationType='fade'
            onRequestClose={() => { }}>
            <View style={{ height: api.getRealDeviceHeight(), width: api.getRealDeviceWidth(), backgroundColor: 'rgba(0,0,0,.8)' }} >            
                <View style={{ width: api.getRealDeviceWidth() * .8, alignSelf: 'center', marginTop: api.getRealDeviceHeight() / 15, marginBottom: api.getRealDeviceHeight() / 15, backgroundColor: '#fff', borderRadius: 5, zIndex: 10 }}>
                    <Text style={{ paddingVertical: 5, fontSize: 17, fontWeight: '300', alignSelf: 'center' }}>{i18.t('detailBill')}</Text>
                    <View style={{ width: api.getRealDeviceWidth() * .8, height: 1, backgroundColor: '#cdcdcd' }} />
                    {this.state.loading ? <ActivityIndicator size='large' style={{ alignSelf: 'center' }} /> : this.detaillBill()}

                    <Button block style={{ borderRadius: 0, borderBottomLeftRadius: 5, borderBottomRightRadius: 5, backgroundColor: '#fa6428' }}
                        onPress={this.pay}
                    >
                        {i18.t('pay')}
                    </Button>
                </View>
                <TouchableOpacity
                    style={{ height: api.getRealDeviceHeight(), width: api.getRealDeviceWidth(), position: 'absolute' }}
                    onPress={() => { this.setState({ showPopup: false }) }}
                />
                </View>
        </Modal>
    }
    render() {
        if (!this.props.imenuState.orderId || !this.props.imenuState.orderId) return null;
        return (
            <TouchableOpacity
                style={{
                    backgroundColor: '#e1b200',
                    borderRadius: 5,
                    marginLeft: 5,
                    paddingVertical: 5,
                    paddingHorizontal: 5
                }}
                onPress={this.viewBill}
            >
                {this._popUp()}
                <Text style={{ color: '#fff' }}>
                    {i18.t('pay')}
                </Text>
            </TouchableOpacity>
        );
    }
}
const mapState = (state) => {
    return { navState: state.navState, userState: state.userState, imenuState: state.imenuState }
}
const mapDispatch = (dispatch) => {
    return {
        setUserInfo: (data) => dispatch(setUserInfo(data)),
    }
}
export default connect(mapState, mapDispatch)(Order);