import React, { PureComponent } from 'react'
import { Image, View, Text, Modal, TouchableOpacity, Platform, ListView, ActivityIndicator } from 'react-native'
import { Button, Container, Title, Header, Content, Icon } from 'native-base'
import api from '../../../api'
import fetch from '../../../api/dataService'
import i18 from '../../i18'
import Item from './item'
import { connect } from 'react-redux'
class ListOrder extends PureComponent {
    numload = 10;
    constructor(props) {
        super(props);
        this.state = {
            order: [],
            canLoadMore: true
        };
    }
    componentWillMount() {
        this._loadMore()
    }
    _loadMore = () => {
        let { shopId } = this.props.imenuState;
        fetch.getListBook(shopId, 2, this.state.order.length, this.numload)
            .then(rs => {
                if (rs.err == 0) {
                    this.setState({
                        order: this.state.order.concat(rs.data),
                        canLoadMore: rs.data.length < this.numload ? false : true
                    })
                } else {
                    this.setState({
                        canLoadMore: false
                    })
                }
            })
    }
    _cancel = () => {
        this.setState({
            canLoadMore: true,
            order: []
        })
        setTimeout(() => {
            this._loadMore()
        }, 100);
    }
    render() {
        const ds = new ListView.DataSource({
            rowHasChanged: (r1, r2) => r1 !== r2
        });
        return <Container>
            <Header
                style={{
                    backgroundColor: '#009688',
                    zIndex: 1000
                }}>
                <Button transparent onPress={() => api.pop()}>
                    <Icon name='ios-arrow-back' style={{ color: Platform.OS == 'ios' ? '#000' : '#fff' }} />
                </Button>
                <Title style={{ alignSelf:'center' } }>
                    {i18.t('myorder')}
                </Title>
            </Header>
            <Image
                source={require('../../../img/bg_login.jpg')}
                style={{ height: api.getRealDeviceHeight() - 58, width: api.getRealDeviceWidth() }}>
                <ListView
                    contentContainerStyle={{ paddingBottom: 10 }}
                    enableEmptySections={true}
                    renderFooter={() => {
                        if (this.state.canLoadMore) return <ActivityIndicator size='large' />
                        if (!this.state.canLoadMore && this.state.order.length == 0) return <Text style={{ alignSelf: 'center',backgroundColor:'transparent' }}>{i18.t('nodata')}</Text>
                        return null;
                    }}
                    onEndReached={this._loadMore.bind(this)}
                    dataSource={ds.cloneWithRows(this.state.order)}
                    renderRow={(item, id) => {
                        return <Item
                            data={item}
                            onCancel={this._cancel.bind(this)}
                        />
                    }}
                />
            </Image>
        </Container>
    }
}
mapStateToProps = (state) => {
    return {
        imenuState: state.imenuState
    }
}
mapDispatchToProps = (dispatch) => {
    return {

    }
}
export default connect(mapStateToProps, mapDispatchToProps)(ListOrder);