import React, { Component } from 'react';
import { View, Image, Text, ListView, ScrollView, Platform, AsyncStorage, PermissionsAndroid, Modal, TouchableOpacity, TextInput } from 'react-native';
import {
    Header,
    Title,
    Container,
    Content,
    Button,
    Icon,
    Input,
    InputGroup, Footer, FooterTab
} from 'native-base';
import api from '../../api';
import dataService from '../../api/dataService';
import DeviceInfo from 'react-native-device-info'
import { connect } from 'react-redux';
import PopupDialog, { SlideAnimation, DialogTitle } from 'react-native-popup-dialog';
import { setUserInfo, setUserToken, setUuid } from '../../actions/userActions'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import i18 from '../i18'
import IconEntypo from 'react-native-vector-icons/Entypo'
import DatePicker from 'react-native-datepicker'
import SelectTable from './selectTable'
class Order extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showPopup: false,
            numberPeople: '1',
            table: this.props.imenuState.tableId[0] || null,
            date: api.getTime((this.props.imenuState.startTime ? this.props.imenuState.startTime : new Date()), 'YYYY/MM/DD'),
            time: api.getTime((this.props.imenuState.startTime ? this.props.imenuState.startTime : new Date()), 'H:mm'),
            status: false
        }
    }

    _changenumberPeople = (flag) => {
        if (this.state.numberPeople == '1' && !flag) return;
        this.setState({ numberPeople: (parseInt(this.state.numberPeople) + (flag ? 1 : -1)) + '' })
    }
    _order = () => {
        if (!this.state.table) return api.showMessage(i18.t('blankTable'));
        let data = this.props.imenuState;

        let listCombo = [];
        data.combo.map(el => {
            listCombo.push({
                id: el.id,
                amount: el.amount,
                price: el.price
            })
        });

        let listItem = [];
        data.item.map(el => {
            listItem.push({
                id: el.id,
                amount: el.amount,
                price: el.price
            })
        });
        let payload = { shopId: data.shopId, listItem: listItem, listCombo: listCombo, people: parseInt(this.state.numberPeople), startTime: this.state.date + ' ' + this.state.time, tableId: [this.state.table] };
        let isloading = true;
        this.setState({
            showPopup: false
        })
        api.showLoading();
        dataService.createBook(payload)
            .then(rs => {
                this.setState({
                    status: true
                })
                if (rs.err == 0) api.updateBook({ orderId: rs.book.id, billId: rs.createBill.id, people: parseInt(this.state.numberPeople), startTime: this.state.date + ' ' + this.state.time, tableId: [this.state.table] })
                api.showMessage(rs.msg)
                isloading = false;
                api.hideLoading();
            })
        setTimeout(function () {
            if (isloading) api.showMessage(i18.t('timeoutTextMsgContentLogin'), i18.t('titleMsgLogin'))
        }, 30000);
    }
    _popUp = () => {
        return <Modal
            transparent={true}
            visible={this.state.showPopup}
            animationType='fade'
            onRequestClose={() => { }}>
            <View style={{ height: api.getRealDeviceHeight(), width: api.getRealDeviceWidth(), backgroundColor: 'rgba(0,0,0,.8)' }} >
                <View style={{ width: 250, alignSelf: 'center', marginTop: api.getRealDeviceHeight() / 5, backgroundColor: '#fff', borderRadius: 5, zIndex: 10 }}>
                    <Text style={{ paddingVertical: 5, fontSize: 17, fontWeight: '300', alignSelf: 'center' }}>{i18.t('fillInfomationRes')}</Text>
                    <View style={{ width: 250, height: 1, backgroundColor: '#cdcdcd' }} />
                    <View style={{ paddingHorizontal: 10, paddingVertical: 10 }}>
                        {/* <View style={{ flexDirection: 'row' }}>
                            <View>
                                <View style={{ flex: 1 }} />
                                <Text>{i18.t('numberPeople')}</Text>
                            </View>
                            <InputGroup style={{ height: 30, width: 60, marginLeft: 10 }}>
                                <Input value={this.state.numberPeople} style={{ textAlign: 'center', fontSize: 15, color: 'gray', margin: 0 }} keyboardType='phone-pad' onChangeText={(t) => { this.setState({ numberPeople: t }) }} />
                            </InputGroup>
                            <IconEntypo name='circle-with-plus' style={{ fontSize: 25, marginTop: 5 }} onPress={() => { this._changenumberPeople(true) }} />
                            <IconEntypo name='circle-with-minus' style={{ fontSize: 25, marginTop: 5 }} onPress={() => { this._changenumberPeople() }} />
                        </View> */}
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                            <View style={{ flexDirection: 'row', borderWidth: 1, borderColor: '#cdcdcd', borderRadius: 5 }}>
                                <Icon name='md-calendar' style={{ padding: 5, backgroundColor: '#cdcdcd', fontSize: 20, borderTopLeftRadius: 3, borderBottomLeftRadius: 3 }} />
                                <DatePicker
                                    style={{ width: 100 }}
                                    date={this.state.date}
                                    mode="date"
                                    placeholder={i18.t('selectTime')}
                                    format="YYYY-MM-DD"
                                    minDate={new Date()}
                                    androidMode='spinner'
                                    maxDate={new Date((new Date()).setDate((new Date().getDate()) + 60))}
                                    confirmBtnText={i18.t('confirm')}
                                    cancelBtnText={i18.t('close')}
                                    showIcon={false}
                                    customStyles={{
                                        dateInput: {
                                            height: 30,
                                            borderWidth: 0,
                                        },
                                        placeholderText: {
                                            color: '#444'
                                        },
                                        dateTouchBody: {
                                            height: 30
                                        }
                                        // ... You can check the source to find the other keys.
                                    }}
                                    onDateChange={(date) => { this.setState({ date: date }) }}
                                />
                            </View>
                            <View style={{ flexDirection: 'row', borderWidth: 1, borderColor: '#cdcdcd', borderRadius: 5 }}>
                                <Icon name='ios-contact' style={{ padding: 5, backgroundColor: '#cdcdcd', fontSize: 20, borderTopLeftRadius: 3, borderBottomLeftRadius: 3 }} />
                                <TextInput value={this.state.numberPeople} style={{ textAlign: 'center', fontSize: 15, color: 'gray', margin: 0, padding: 0, minWidth: 40 }} underlineColorAndroid='transparent' keyboardType='phone-pad' onChangeText={(t) => { this.setState({ numberPeople: t }) }} />
                            </View>
                        </View>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 10 }}>
                            <View style={{ flexDirection: 'row', borderWidth: 1, borderColor: '#cdcdcd', borderRadius: 5 }}>
                                <Icon name='md-time' style={{ padding: 5, backgroundColor: '#cdcdcd', fontSize: 20, borderTopLeftRadius: 3, borderBottomLeftRadius: 3 }} />
                                <DatePicker
                                    style={{ width: 50 }}
                                    date={this.state.time}
                                    mode="time"
                                    placeholder={i18.t('selectTime')}
                                    format="HH:mm"
                                    minDate={new Date()}
                                    androidMode='spinner'
                                    maxDate={new Date((new Date()).setDate((new Date().getDate()) + 60))}
                                    confirmBtnText={i18.t('confirm')}
                                    cancelBtnText={i18.t('close')}
                                    showIcon={false}
                                    customStyles={{
                                        dateInput: {
                                            borderWidth: 0,
                                        },
                                        placeholderText: {
                                            color: '#444'
                                        },
                                        dateTouchBody: {
                                            height: 30
                                        }
                                        // ... You can check the source to find the other keys.
                                    }}
                                    onDateChange={(date) => {
                                        this.setState({ time: date }); console.log('====================================');
                                        console.log(this.state);
                                        console.log('====================================');
                                    }}
                                />
                            </View>
                            <View style={{ flexDirection: 'row', borderWidth: 1, borderColor: '#cdcdcd', borderRadius: 5, alignItems: 'center' }}>
                                <Text style={{ backgroundColor: '#cdcdcd', padding:Platform.OS == 'ios'? 8:6, color: '#000' }}>Số bàn</Text>
                                <SelectTable
                                    idRes={this.state.addID}
                                    time={this.state.date + ' ' + this.state.time}
                                    numberPeople={this.state.numberPeople}
                                    onSelect={(table) => { this.setState({ table: table }) }}
                                />
                            </View>
                        </View>
                        {/* <View style={{ flexDirection: 'row', paddingLeft: 20 }}>
                        <Text style={{ paddingVertical: 10, paddingRight: 10 }}>{i18.t('time')}:</Text>
                        <DatePicker
                            style={{ width: 200, borderWidth: 0 }}
                            date={this.state.date}
                            mode="datetime"
                            placeholder={i18.t('selectTime')}
                            format="YYYY-MM-DD HH:mm"
                            minDate={new Date()}
                            androidMode='spinner'
                            maxDate={new Date((new Date()).setDate((new Date().getDate()) + 60))}
                            confirmBtnText={i18.t('confirm')}
                            cancelBtnText={i18.t('close')}
                            showIcon={false}
                            customStyles={{
                                dateIcon: {
                                    position: 'absolute',
                                    left: 0,
                                    top: 4,
                                    marginLeft: 0
                                },
                                dateInput: {
                                    marginLeft: 0,
                                    borderWidth: 0,
                                    alignSelf: 'auto', alignItems: 'flex-start'
                                },
                                placeholderText: {
                                    color: '#444'
                                }
                                // ... You can check the source to find the other keys.
                            }}
                            onDateChange={(date) => { this.setState({ date: date }) }}
                        />
                    </View> */}
                        {/* <View style={{ flexDirection: 'row', paddingBottom: 10 }}>
                            <Text>
                                {i18.t('tableNum')}:
                        </Text>
                            <Text style={{ paddingHorizontal: 10 }}>
                                {this.state.table || '...'}
                            </Text>
                            <SelectTable
                                idRes={this.state.addID}
                                time={this.state.date}
                                numberPeople={this.state.numberPeople}
                                onSelect={(table) => { this.setState({ table: table }) }}
                            />
                        </View> */}
                    </View>
                    <Button block style={{ borderRadius: 0, borderBottomLeftRadius: 5, borderBottomRightRadius: 5, backgroundColor: '#fa6428' }}
                        onPress={this._order}
                    >
                        {(i18.t('book')).toUpperCase()}
                    </Button>
                </View>
                <TouchableOpacity
                    style={{ height: api.getRealDeviceHeight(), width: api.getRealDeviceWidth(), position: 'absolute' }}
                    onPress={() => { this.setState({ showPopup: false }) }}
                />
                </View>
        </Modal>
    }
    render() {
        if (this.props.imenuState.orderId && this.props.imenuState.billId) return null;
        return (
            <TouchableOpacity
                style={{
                    backgroundColor: '#e1b200',
                    borderRadius: 5,
                    marginLeft: 5,
                    paddingVertical: 5,
                    paddingHorizontal: 5
                }}
                onPress={() => { this.setState({ showPopup: true }) }}
            >
                {this._popUp()}
                <Text style={{ color: '#fff' }}>
                    {i18.t('order')}
                </Text>
            </TouchableOpacity>
        );
    }
}
const mapState = (state) => {
    return { navState: state.navState, userState: state.userState, imenuState: state.imenuState }
}
const mapDispatch = (dispatch) => {
    return {
        setUserInfo: (data) => dispatch(setUserInfo(data)),
    }
}
export default connect(mapState, mapDispatch)(Order);