import React, { Component } from 'react';
import { View, Image, Text, ListView, ScrollView, Platform, AsyncStorage, PermissionsAndroid, Modal, TouchableOpacity } from 'react-native';
import {
    Header,
    Title,
    Container,
    Content,
    Button,
    Icon,
    Input,
    InputGroup, Footer, FooterTab
} from 'native-base';
import api from '../../../api';
import dataService from '../../../api/dataService';
import DeviceInfo from 'react-native-device-info'
import { connect } from 'react-redux';
import FontAwesomeIcons from 'react-native-vector-icons/FontAwesome';
import i18 from '../../i18'
import IconEntypo from 'react-native-vector-icons/Entypo'
import * as Animated from 'react-native-animatable'
import IconsFontAwesome from 'react-native-vector-icons/FontAwesome';
class Item extends Component {
    constructor(props) {
        super(props)
        this.state = {
            show: false,
            choose: false
        }
    }
    componentWillMount() {
        this.props.imenuState.item.map(el => {
            if (el.id == this.props.data.id) {
                return this.setState({
                    choose: true
                });
            }
        })
    }
    _choose() {
        let { id, name, price } = this.props.data;
        let tempArr = this.props.imenuState.item;
        let arr = [];
        if (this.state.choose) {
            tempArr.map(el => {
                if (el.id != id) arr.push(el);
            })

        } else {
            arr = tempArr.concat({
                ...this.props.data,
                amount: 1
            })
        }
        api.updateBookItem(arr)
        this.setState({
            choose: !this.state.choose
        });
    }

    _renderPopup() {
        let { img, decription, name, price } = this.props.data
        const ds = new ListView.DataSource({
            rowHasChanged: (r1, r2) => r1 !== r2
        });
        return <Modal
            transparent={true}
            visible={this.state.show}
            animationType='fade'
            onRequestClose={() => { }}>
            <View style={{ height: api.getRealDeviceHeight(), width: api.getRealDeviceWidth(), backgroundColor: 'rgba(0,0,0,.8)' }} >
                <View style={{ zIndex: 1, borderRadius: 3, backgroundColor: '#fff', width: api.getRealDeviceWidth() * .9, alignSelf: 'center', flex: 1, marginVertical: api.getRealDeviceHeight() * .09 }}>
                    <Text style={{ color: '#000', paddingVertical: 5, fontSize: 17, fontWeight: '300', alignSelf: 'center' }}>{(this.props.data.name || '').toUpperCase()}</Text>
                    <View style={{ height: 1, backgroundColor: '#cdcdcd' }} />
                    <View style={{ flex: 1 }}>
                        <Image
                            source={{ uri: img || 'http://g9vietnam.com.vn/wp-content/uploads/2016/12/cac-dich-vu-tu-van-setup-nha-hang-ban-nen-biet1.jpg' }}
                            style={{ height: ((api.getRealDeviceWidth() * .9 - 10) / 1.911262799), resizeMode: 'stretch' }}
                        />
                        <View style={{ paddingLeft: 10 }}>
                            <Text style={{ marginTop: 20 }}>{i18.t('cost')}: {price} {i18.t('moneyUnit')}</Text>
                            <Text style={{ marginTop: 5, fontWeight: '400' }}>{i18.t('detail')} :</Text>
                            <Text>{decription}</Text>
                        </View>
                    </View>
                    <Button block style={{ marginBottom: -0.5, backgroundColor: "#fa6428", borderTopLeftRadius: 0, borderTopRightRadius: 0 }}
                        onPress={() => {
                            this.setState({
                                show: false
                            })
                        }}>
                        <Text style={{ fontSize: 16, color: '#fff' }}>
                            {i18.t('close')}
                        </Text>
                    </Button>
                </View>
                <TouchableOpacity
                    activeOpacity={1}
                    style={{ position: 'absolute', height: api.getRealDeviceHeight(), width: api.getRealDeviceWidth() }}
                    onPress={() => {
                        this.setState({
                            show: false
                        })
                    }}
                />
            </View>
        </Modal>
    }
    render() {
        let { img, name, price } = this.props.data
        const ds = new ListView.DataSource({
            rowHasChanged: (r1, r2) => r1 !== r2
        });
        return (<TouchableOpacity
            onPress={() => {
                this.setState({ show: !this.state.show })
            }}
            style={{ marginBottom: 10, borderRadius: 3, borderColor: '#cdcdcd', borderWidth: 1 }}
        >
            <Text style={{ color: '#000', paddingLeft: 5, fontWeight: '400', fontSize: 16 }}>{(name || '').toUpperCase()}</Text>
            <Image
                source={{ uri: img }}
                style={{ resizeMode: 'stretch', height: (api.getRealDeviceWidth() - 55) / 1.911262799, borderTopLeftRadius: 3, borderTopRightRadius: 3 }}
            >
                <TouchableOpacity
                    onPress={this._choose.bind(this)}
                    style={{ bottom: 0, right: 0, position: 'absolute', padding: 20 }}
                >
                    {
                        this.state.choose ?
                            <IconsFontAwesome name='check-circle' style={{ backgroundColor: 'transparent', color: '#25c173', fontSize: 28 }} />
                            : <IconEntypo name='circle-with-plus' style={{ backgroundColor: 'transparent', color: '#e1b201', fontSize: 28 }} />
                    }
                </TouchableOpacity>
            </Image>
            <Text style={{ marginLeft: 5 }}>{i18.t('cost')} : {price} {i18.t('moneyUnit')}</Text>
            <Text style={{ marginVertical: 5, marginLeft: 5 }}>{i18.t('showmore')}</Text>
            {this._renderPopup()}
        </TouchableOpacity>
        );
    }
}
const mapState = (state) => {
    return { navState: state.navState, userState: state.userState, imenuState: state.imenuState }
}
const mapDispatch = (dispatch) => {
    return {
        setUserInfo: (data) => dispatch(setUserInfo(data)),
    }
}
export default connect(mapState, mapDispatch)(Item);