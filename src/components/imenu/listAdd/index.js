import React, { Component } from 'react';
import { View, Image, Text, ScrollView, Platform, ActivityIndicator, AsyncStorage, PermissionsAndroid, Modal, ListView, TouchableOpacity } from 'react-native';
import {
    Header,
    Title,
    Container,
    Content,
    Button,
    Icon,
    Input,
    InputGroup
} from 'native-base';
import api from '../../../api';
import cache from '../../../api/cache';
import dataService from '../../../api/dataService';
import DeviceInfo from 'react-native-device-info';
import { connect } from 'react-redux';
import PopupDialog, { SlideAnimation, DialogTitle } from 'react-native-popup-dialog';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import FontAwesomeIcons from 'react-native-vector-icons/FontAwesome';
import i18 from '../../i18';
import IconEntypo from 'react-native-vector-icons/Entypo'
import DatePicker from 'react-native-datepicker'
class AddCardFromLogin extends Component {
    // define const,var global
    numload = 10;
    constructor() {
        super()
        this.state = {
            add: [],
            showPopup: false,
            // numberPeople: '1',
            // table: null,
            img: '',
            resName: '',
            resID: '',
            addID: 0,
            width: api.getRealDeviceWidth() - 10,
            height: (api.getRealDeviceWidth() - 10) / 1.911262799,
            // date: new Date(),
            text: '',
            canLoadMore: true,
            tab: 0,
            intro: ''
        }
    }
    componentWillMount() {
        let { resName, id, img, intro } = this.props.navState.routes[this.props.navState.index];
        this.setState({
            img: img,
            resName: resName,
            resID: id,
            intro: intro
        });
        Image.getSize(img, (width, height) => {
            let ratio = width / height;
            this.setState({
                height: this.state.width / ratio
            });
        })
        setTimeout(() => {
            this._loadMore()
        }, 500)
    }

    _loadMore() {
        if (this.state.canLoadMore)
            dataService.getListAddRes(this.state.add.length, this.numload, this.state.text, this.state.resID)
                .then(rs => {
                    if (rs.err == 0) {
                        this.setState({
                            add: this.state.add.concat(rs.data),
                            canLoadMore: rs.data.length < this.numload ? false : true
                        })
                    } else {
                        this.setState({
                            canLoadMore: false
                        })
                    }
                })
    }
    _renderADD() {
        const ds = new ListView.DataSource({
            rowHasChanged: (r1, r2) => r1 !== r2
        });
        return <ListView
            showsVerticalScrollIndicator={false}
            style={{}}
            enableEmptySections={true}
            dataSource={ds.cloneWithRows(this.state.add)}
            renderFooter={() => {

                if (this.state.canLoadMore) return <ActivityIndicator style={{ alignSelf: 'center', flex: 1 }} size='large' />
                if (!this.state.canLoadMore && this.state.add.length < 1) return <Text style={{ backgroundColor: 'transparent', textAlign: 'center', flex: 1, color: '#fff' }}>{i18.t('nodata')}</Text>
            }}
            renderRow={(item, indexCol, indexRow) => {
                let point = {
                    latitude: item.latitude,
                    longitude: item.longitude
                }

                point = api.getDistance(cache.get('userLocation'), point)
                return <TouchableOpacity key={indexRow} style={{ paddingTop: 10, paddingLeft: 15 }}
                    onPress={() => { this._gotoMenu(item.id) }}
                >
                    {/* <Text style={{ color: '#fff',backgroundColor:'transparent' }}>Cơ sở {parseInt(indexRow) + 1}  {point ? ": " + point + ' Km' : ''}</Text> */}
                    <Text style={{ color: '#fff',backgroundColor:'transparent' }}>{(item.name||'').toUpperCase()}  {point ? ": " + point + ' Km' : ''}</Text>
                    
                    <Text style={{ marginBottom: 10, color: '#fff',backgroundColor:'transparent' }}>{item.address}</Text>
                    <View style={{ height: 1, width: api.getRealDeviceWidth(), backgroundColor: '#cdcdcd' }} />
                </TouchableOpacity>
            }} />
    }

    _gotoMenu = (addID) => {
        // if (!this.state.table) {
        //     api.showMessage(i18.t('blankTable'));
        //     return;
        // }
        let data = {
            shopId: addID,
        }
        api.updateBook(data)
        api.push({ key: 'iMenu', shopID: addID, shopName: this.state.resName });
        // this.setState({ showPopup: false })
    }
    renderIntro = () => {
        return <Text style={{ color: '#fff', paddingHorizontal: 10,backgroundColor:'transparent' }}>
            {this.state.intro}
        </Text>
    }

    renderScreen = () => {
        if (this.state.tab) return this.renderIntro();
        return this._renderADD();
    }
    render() {
        return (
            <View>
                <Header
                    style={{
                        backgroundColor: '#009688'
                    }}>
                    <Button transparent onPress={() => api.pop()}>
                        <Icon name='ios-arrow-back' style={{ color: Platform.OS == 'ios' ? '#000' : '#fff' }} />
                    </Button>
                    <Title style={{ alignSelf:'center' } }>
                        {this.state.resName}
                    </Title>
                    <Button transparent onPress={() => api.resetRoute({ key: 'home' })}>
                        <FontAwesomeIcons name='home' style={{ color: Platform.OS == 'ios' ? '#000' : '#fff', fontSize: 23 }} />
                    </Button>
                </Header>
                <Image
                    style={{ height: api.getRealDeviceHeight() - 58, width: api.getRealDeviceWidth(), paddingHorizontal: 5 }}
                    source={require('../../../img/bg_login.jpg')}
                >
                    <Image source={{ uri: this.state.img }}
                        style={{ marginTop: 5, height: this.state.height, width: this.state.width, resizeMode: 'stretch', borderRadius: 3 }}
                    />
                    <View style={{ flexDirection: 'row', marginTop: 5 ,marginBottom:5}}>
                        <View style={{ flex: 1,  }}>
                        <Text style={{ backgroundColor:'transparent',textAlign: 'center', paddingVertical: 5, color: '#fff', borderBottomColor: "#fa6418" }}
                            onPress={() => {
                                this.setState({
                                    tab: 0
                                })
                            }}
                        >
                            {(i18.t('addr')||'').toUpperCase()}
                            </Text>
                            <View style={{ height:this.state.tab ? 0 : 1,backgroundColor:'#fa6428' }}/>
                        </View>
                        <View style={{ flex: 1,  }}>
                        <Text style={{ backgroundColor:'transparent',flex: 1, textAlign: 'center', paddingVertical: 5, color: '#fff', borderBottomColor: "#fa6418" }}
                            onPress={() => {
                                this.setState({
                                    tab: 1
                                })
                            }}
                        >
                            {(i18.t('intro')||'').toUpperCase()}
                            </Text>
                            <View style={{ height:this.state.tab ? 1 : 0,backgroundColor:'#fa6428' }}/>
                        </View>
                    </View>
                    {this.renderScreen()}
                </Image>
            </View>
        );
    }
}
const mapState = (state) => {
    return { navState: state.navState, userState: state.userState }
}
const mapDispatch = (dispatch) => {
    return {
    }
}
export default connect(mapState, mapDispatch)(AddCardFromLogin);