import { Marker, Callout } from 'react-native-maps'
import React, { PureComponent } from 'react'
import { Image, View, Text, Modal, TouchableOpacity, ListView, ActivityIndicator } from 'react-native'
import { Button } from 'native-base'
import api from '../../api'
import fetch from '../../api/dataService'
import i18 from '../i18'
import {connect} from 'react-redux'
class SelectTable extends PureComponent {
    numload = 10;
    constructor(props) {
        super(props);
        this.state = {
            show: false,
            table: [],
            canLoadMore: true
        };
    }
    loadMore() {
        if (this.state.canLoadMore) fetch.getListTable(this.props.imenuState.shopId,1, 2, this.props.time, null, this.state.table.length, this.numload)
            .then(rs => {
                if (rs.err == 0) this.setState({
                    table: this.state.table.concat(rs.data),
                    canLoadMore: rs.data.length < this.numload ? false : true
                }); else {
                    this.setState({
                        canLoadMore: false
                    })
                }
            })
    }
    init = () => {
        if (this.state.show) return;
        this.setState({
            canLoadMore: true,
            table: []
        })
        setTimeout(() => {
            this.loadMore()
        }, 10);
    }
    renderListTable = (onSelect) => {
        const ds = new ListView.DataSource({
            rowHasChanged: (r1, r2) => r1 !== r2
        });
        return <ListView
            enableEmptySections={true}
            dataSource={ds.cloneWithRows(this.state.table)}
            contentContainerStyle={{
                paddingHorizontal: 5,
                paddingTop: 10,
                flexDirection: 'row',
                flexWrap: 'wrap',
                alignItems: 'stretch'
            }}
            renderFooter={() => {
                if (this.state.canLoadMore) return <View style={{ width: api.getRealDeviceWidth() * .8 }}><ActivityIndicator style={{ alignSelf: 'center' }} size='large' /></View>
                if (!this.state.canLoadMore && this.state.table.length < 1) return <Text style={{ marginBottom: 5, backgroundColor: 'transparent', textAlign: 'center', flex: 1, color: '#888' }}>{i18.t('CantFindTable')}</Text>
            }}
            renderRow={(item, indexCol, indexRow) => {
                
                if (this.props.numberPeople > item.size) return null;
                return <TouchableOpacity key={indexRow} style={{ alignItems: 'center', justifyContent: 'center', marginBottom: 10, borderRadius: 3, marginLeft: 5, width: (api.getRealDeviceWidth() * .8 - 30) / 3, height: (api.getRealDeviceWidth() * .8 - 30) / 3, borderWidth: 1, borderColor: '#cdcdcd' }}
                    onPress={() => { this.setState({ show: false }); onSelect(item.id) }}
                >
                    <Text style={{ fontSize: 17, fontWeight: '200' }}>
                        {item.id}
                    </Text>
                    <Text style={{ position:'absolute',bottom:2,left:2 }}>{i18.t('numberPeople')} :{item.size}</Text>
                </TouchableOpacity>
            }} />
    }
    showPOPUP = () => {
        return <Modal
            transparent={true}
            visible={this.state.show}
            animationType='fade'
            onRequestClose={() => { }}>
            <Modal
                transparent={true}
                visible={this.state.show}
                animationType='slide'
                onRequestClose={() => { }}>
                <View style={{ width: api.getRealDeviceWidth() * .8, alignSelf: 'center', marginTop: api.getRealDeviceHeight() / 5, backgroundColor: '#fff', borderRadius: 5, zIndex: 10 }}>
                    <Text style={{ paddingVertical: 5, fontSize: 17, fontWeight: '300', alignSelf: 'center' }}>{i18.t('selectTable')}</Text>
                    <View style={{ width: api.getRealDeviceWidth() * .8, height: 1, backgroundColor: '#cdcdcd' }} />
                    {this.renderListTable(this.props.onSelect)}
                    <Button block style={{ borderRadius: 0, borderBottomLeftRadius: 5, borderBottomRightRadius: 5, backgroundColor: '#fa6428' }}
                        onPress={() => { this.setState({ show: false }) }}
                    >
                        {i18.t('close')}
                    </Button>
                </View>
                <TouchableOpacity
                    style={{ height: api.getRealDeviceHeight(), width: api.getRealDeviceWidth(), position: 'absolute' }}
                    onPress={() => { this.setState({ show: false }) }}
                />
            </Modal>
            <View
                style={{ height: api.getRealDeviceHeight(), width: api.getRealDeviceWidth(), position: 'absolute', backgroundColor: 'rgba(0,0,0,.5)' }}
            />
        </Modal>
    }
    render() {
        return <View>
            {this.showPOPUP()}
            <Text style={{ textDecorationLine: 'underline' }} onPress={() => {
                this.setState({
                    show: true
                });
                this.init();
            }}>
                {i18.t('selectTable')}
            </Text>
        </View>
    }
}
mapStateToProps=(state)=>{
    return{
        imenuState:state.imenuState
    }
}

export default connect(mapStateToProps, null)(SelectTable)