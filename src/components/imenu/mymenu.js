import React, { Component } from 'react';
import { View, Image, Text, ListView, ScrollView, Platform, AsyncStorage, PermissionsAndroid, Modal, TouchableOpacity } from 'react-native';
import {
    Header,
    Title,
    Container,
    Content,
    Button,
    Icon,
    Input,
    InputGroup, Footer, FooterTab
} from 'native-base';
import api from '../../api';
import dataService from '../../api/dataService';
import DeviceInfo from 'react-native-device-info'
import { connect } from 'react-redux';
import PopupDialog, { SlideAnimation, DialogTitle } from 'react-native-popup-dialog';
import { setUserInfo, setUserToken, setUuid } from '../../actions/userActions'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import i18 from '../i18'
import IconEntypo from 'react-native-vector-icons/Entypo'
import ItemMymenu from './itemmymenu'
import Order from './order'
import Pay from './pay/indexpay'
import AddFood from './addFood/indexAddFood'
class AddCardFromLogin extends Component {
    constructor(props) {
        super(props);
    }

    componentWillMount() {
        // console.log('====================================');
        // console.log(this.props.imenuState);
        // console.log('====================================');
    }
    _renderItem(arr, type) {
        if (!arr) return;
        return arr.map((item, idRow, idCol) => {
            // alert(idRow)
            return (
                <ItemMymenu
                    key={idRow}
                    type={type}
                    //data={item}
                    index={idRow}
                />
            )
        })
    }
    _order = () => {
        let data = this.props.imenuState

        let listCombo = [];
        data.combo.map(el => {
            listCombo.push({
                id: el.id,
                amount: el.amount,
                price: el.price
            })
        });

        let listItem = [];
        data.item.map(el => {
            listItem.push({
                id: el.id,
                amount: el.amount,
                price: el.price
            })
        });
        data = { ...data, listItem: listItem, listCombo: listCombo };
        console.log('====================================');
        console.log(data);
        console.log('====================================');
        let isloading = true;
        api.showLoading();
        dataService.createBook(data)
            .then(rs => {
                // alert(JSON.stringify(rs))
                if (rs.err == 0) api.updateBook({ orderId: rs.book.id })
                api.showMessage(rs.msg)

                console.log('====================================');
                console.log('orderResult', rs);
                console.log('====================================');
                isloading = false;
                api.hideLoading()
            })
        setTimeout(function () {
            if (isloading) api.showMessage(i18.t('timeoutTextMsgContentLogin'), i18.t('titleMsgLogin'))
        }, 30000);
    }

    render() {
        let bill = 0
        this.props.imenuState.item.map(item => {
            bill += item.price * item.amount
        })
        this.props.imenuState.combo.map(item => {
            bill += item.price * item.amount
        })
        return (
            <View style={{ flex: 1 }}>
                <View style={{ flex: 1, backgroundColor: '#171d2b' }}>
                    <ScrollView style={{ paddingHorizontal: 10 }}
                        showsHorizontalScrollIndicator={false}
                    >
                        {
                            this._renderItem(this.props.imenuState.combo, 'combo')
                        }
                        {
                            this._renderItem(this.props.imenuState.item, 'general')
                        }
                    </ScrollView>
                </View>
                <View style={{ flexDirection: 'row', backgroundColor: '#171d2b', width: api.getRealDeviceWidth() }}>
                    <View>
                        <View style={{ flex: 1 }} />
                        <View style={{ flexDirection: 'row' }}>
                            <Order />
                            <Pay />
                            <AddFood/>
                            <TouchableOpacity
                                style={{
                                    backgroundColor: '#e1b200',
                                    borderRadius: 5,
                                    marginLeft: 5,
                                    paddingVertical: 5,
                                    paddingHorizontal: 5
                                }}
                                onPress={() => {
                                    api.push({
                                        key: 'myorder'
                                    })
                                }}
                            >
                                <Text style={{ color: '#fff' }}>
                                    DS order
                                </Text>
                            </TouchableOpacity>
                        </View>
                        <View style={{ flex: 1 }} />
                    </View>
                    <View style={{ flex: 2 }}>
                        <Text style={{ paddingRight: 10, color: '#fff', fontWeight: '400', alignSelf: 'flex-end', paddingVertical: 5 }}>
                            {i18.t('total')}
                        </Text>
                        <View style={{ height: 1, backgroundColor: '#e1b200', alignSelf: 'flex-end', width: 50 }} />
                        <Text style={{ paddingRight: 10, color: '#fff', fontWeight: '400', alignSelf: 'flex-end', paddingVertical: 5 }}>{bill}$</Text>
                    </View>
                </View>
            </View>
        );
    }
}
const mapState = (state) => {
    return { navState: state.navState, userState: state.userState, imenuState: state.imenuState }
}
const mapDispatch = (dispatch) => {
    return {
        setUserInfo: (data) => dispatch(setUserInfo(data)),
    }
}
export default connect(mapState, mapDispatch)(AddCardFromLogin);