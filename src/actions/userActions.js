import {SET_USER_INFO, SET_USER_TOKEN, SET_UUID, SET_LIST_MEMBER} from './actionTypes';

export const setUserToken = (token) => {
    return {type: SET_USER_TOKEN, token}
}

export const setUserInfo = (data) => {
    return {
        type: SET_USER_INFO,
        data
    }
}
export const setUuid = (uuid) => {
    return {
        type: SET_UUID,
        uuid
    }
}
export const setListMember = (listMember) => {
    return {
        type: SET_LIST_MEMBER,
        listMember
    }
}