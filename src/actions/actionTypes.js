export const PUSH_ROUTE = 'PUSH_ROUTE'
export const POP_ROUTE = 'POP_ROUTE'
export const REPLACE_ROUTE = 'REPLACE_ROUTE'
export const RESET_ROUTE = 'RESET_ROUTE'
export const JUMPTO_ROUTE = 'JUMPTO_ROUTE'

export const SHOW_LOADING = 'SHOW_LOADING'
export const HIDE_LOADING = 'HIDE_LOADING'

export const SHOW_MESSAGE_BOX = 'SHOW_MESSAGE_BOX'
export const HIDE_MESSAGE_BOX = 'HIDE_MESSAGE_BOX'

export const SHOW_INPUT_BOX = 'SHOW_INPUT_BOX'
export const HIDE_INPUT_BOX = 'HIDE_INPUT_BOX'

export const SET_USER_INFO = 'SET_USER_INFO'
export const SET_USER_TOKEN = 'SET_USER_TOKEN'
export const SET_UUID = 'SET_UUID'
export const SET_LIST_MEMBER = 'SET_LIST_MEMBER'

export const SHOW_QR_CONFIRM = 'SHOW_QR_CONFIRM'
export const HIDE_QR_CONFIRM = 'HIDE_QR_CONFIRM'

export const SHOW_DOISOAT = 'SHOW_DOISOAT'
export const HIDE_DOISOAT = 'HIDE_DOISOAT'

export const SHOW_RATE = 'SHOW_RATE'
export const HIDE_RATE = 'HIDE_RATE'

export const SET_SHOP_LOCATION = 'SET_SHOP_LOCATION'

export const SET_REG_INFO = 'SET_REG_INFO'

export const SHOW_CONFIRM_BOX = 'SHOW_CONFIRM_BOX'
export const HIDE_CONFIRM_BOX = 'HIDE_CONFIRM_BOX'