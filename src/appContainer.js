import React, { Component } from 'react'
import { NavigationExperimental, Modal } from 'react-native'
import { View, Text } from 'react-native'
import { connect } from 'react-redux'
import { push, pop } from './actions/navActions'
import Login from './components/login'
import Home from './components/home'
import Reg from './components/register'
import RegViettel from './components/regViettel'
import RegUser from './components/regUser'
import RegAddress from './components/regAddress'
import Partner from './components/partner'
import Detail from './components/detail'
import QRscan from './components/QRscan'
import CodeScan from './components/scanCode'
import Comment from './components/comment'
import RegCard from './components/regCard';
import NewRegCard from './components/newRegCard';
import Search from './components/search';
import ShopLocation from './components/shopLocation';
import ListPartner from './components/partners';
import UI from './components/UI'
import AddCard from './components/addCard'
import AddCardFromLogin from './components/addCardFromLogin';
import Viettel from './components/addCardFromLogin.viettel';
import Mpoint from './components/addCardFromLogin.mpoint';
import Listcard from './components/listcard';
import InfoCard from './components/cardInfo';
import BNA from './components/addCardFromLogin.bna';
import History from './components/history'
import Welcome from './components/welcome'
import Intro from './components/intro'
import ListADD from './components/imenu/listAdd'
import ListRestaurant from './components/imenu/listRestaurant'
import ListOrder from './components/imenu/listorder'
import MenuImenu from './components/imenu/menu'
import AddPcode from './components/addPcode'
import Listbill from './components/imenu/listbill';
import SelectCard from './components/selectCard';
import ExchangePoint from './components/exchangePoint';
import AddCardRegiter from './components/addCardRegister';
// import i18n
import i18 from './components/i18'
const { CardStack: NavigationCardStack } = NavigationExperimental
class App extends Component {
    _renderScene = (props) => {
        switch (props.scene.route.key) {
            case 'exchangePoint':
                return <ExchangePoint />
            case 'addCardRegister':
            // case 'login':
            
                return <AddCardRegiter />
            case 'addPcode':
                return <AddPcode />
            case 'listBill':
                // case 'login':
                return <Listbill />
            case 'iMenu':
                // case 'login':
                return <MenuImenu />
            case 'listRestaurant':
                // case 'login':
                return <ListRestaurant />
            case 'myorder':
                // case 'login':
                return <ListOrder />
            // case 'login':
            case 'listAdd':
                return <ListADD />
            case 'welcome':
                return <Welcome />
            // case 'selectCard':
            case 'selectCard':
            
                return <SelectCard />
            case 'login':
                return <Login />
            case 'home':
                return <Home />
            case 'reg':
                return <Reg />
            case 'regVT':
                return <RegViettel />
            case 'regCard':
                return <RegCard />
            case 'newRegCard':
                return <NewRegCard />
            case 'RegUser':
                return <RegUser />
            case 'regAddress':
                return <RegAddress />
            case 'detail':
                return <Detail />
            case 'partner':
                return <Partner />
            case 'QRscan':
                return <QRscan />
            case 'QRscancode':
                return <CodeScan />
            case 'comment':
                return <Comment />
            case 'search':
                return <Search />
            case 'shopLocation':
                return <ShopLocation />
            case 'partners':
                return <ListPartner />
            case 'addCard':
                return <AddCard />
            case 'addCardFromLogin':
                return <AddCardFromLogin />
            case 'viettel':
                return <Viettel />
            case 'mpoint':
                return <Mpoint />
            case 'listcard':
                return <Listcard />
            case 'infoCard':
                return <InfoCard />
            case 'bna':
                return <BNA />
            case 'his':
                return <History />
            case 'intro':
                return <Intro />
        }
    }
    _renderHeader = (props) => {
        return (<UI />)
    }
    render() {
        // i18.locale = this.props.lang
        const { navState } = this.props
        return (<NavigationCardStack
            navigationState={navState}
            renderHeader={this._renderHeader}
            renderScene={this._renderScene} />)
    }
}
const mapState = (state) => {
    return { navState: state.navState, lang: state.langState.lang }
}
const mapDispatch = (dispatch) => {
    return {
        pop: () => {
            dispatch(pop())
        }
    }
}
export default connect(mapState, mapDispatch)(App);