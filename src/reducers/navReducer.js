import {NavigationExperimental,AsyncStorage} from 'react-native'
const {StateUtils: NavigationStateUtils} = NavigationExperimental
import {PUSH_ROUTE, POP_ROUTE, REPLACE_ROUTE, RESET_ROUTE, JUMPTO_ROUTE} from '../actions/actionTypes'
let defaultState = {
    index: 0,
    routes: [
        {
            key: 'login'
        }
    ]
}
const navReducer = (state = defaultState, action) => {
    switch (action.type) {
        case PUSH_ROUTE:
        if(state.routes[state.index].key === (action.route && action.route.key)) return state
            return NavigationStateUtils.push(state, action.route);
        case POP_ROUTE:
        if(state.index===0 || state.routes.length ===1 ) return state
            return NavigationStateUtils.pop(state)
        case REPLACE_ROUTE:
            return NavigationStateUtils.replaceAtIndex(state, action.index,action.route);
        case RESET_ROUTE:
            return NavigationStateUtils.reset(state, [action.route]);
        case JUMPTO_ROUTE:
            return NavigationStateUtils.reset(state, action.key, 0);
        default:
            return state
    }
}

export default navReducer