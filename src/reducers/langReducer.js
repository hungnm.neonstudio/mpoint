initState = {
    lang :'vi'
}

export default (state=initState,action)=>{
    switch (action.type) {
        case 'vi':
            return {
                lang:'vi'
            }
        case 'en':
            return {
                lang:'en'
            }
    
        default:
            break;
    }
    return state
}