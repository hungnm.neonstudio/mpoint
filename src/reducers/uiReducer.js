import {
    SHOW_LOADING,
    HIDE_LOADING,
    SHOW_MESSAGE_BOX,
    HIDE_MESSAGE_BOX,
    SHOW_CONFIRM_BOX,
    HIDE_CONFIRM_BOX,
    SHOW_INPUT_BOX,
    HIDE_INPUT_BOX,
    SHOW_QR_CONFIRM,
    HIDE_QR_CONFIRM,
    SHOW_DOISOAT,
    HIDE_DOISOAT,
    SHOW_RATE,
    HIDE_RATE
} from '../actions/actionTypes'
let inititalState = {
    loading: false,
    showMessageBox: false,
    messageTitle: '',
    messageContent: '',

    // confirm box
    showConfirmBox: false,
    confirmTitle: '',
    confirmContent: '',
    onConfirmOk: () => { },
    onConfirmCancel: () => { },
    confirmCancelText: '',
    confirmOkText: '',
    inputTitle: '',
    inputPlaceholder: '',
    inputSubmit: () => { },
    inputbox: false,
    route: () => { },
    centerButtonContent: '',
    centerButtonFunc: undefined,
    //QR confirm
    QRshow: false,
    QRtitle: '',
    QRcontent: '',
    QRimage: '',
    QRonconfirm: () => { },
    promotionId: '',
    // doi soat
    Doisoatshow: false,
    Doisoattitle: '',
    DoisoatProcess: false,
    ReceiveGift: false,
    promotionTypeId: '',
    doisoatContent: '',
    // binh chon

    Rateshow: false,

    // mGreen
    mShow: false,
    mGreenData: {},
    // storelet
    showStorelet: false,
    contentStorelet: '',
    storeletButtonText: '',
    storeletHandle: () => { },
    stoletPoint: '',
    showStamp: false,

    // login confirm QR
    qr: '',
    showLoginConfirm: false,

    // webview reducer
    showWebview: false,
    webviewData: {},
    btMsg:'',
    funcMsg:()=>{}
}
const uiReducer = (state = inititalState, action) => {
    switch (action.type) {

        case SHOW_LOADING:
            return Object.assign({}, state, { loading: true })
        case HIDE_LOADING:
            return Object.assign({}, state, { loading: false })
        case SHOW_MESSAGE_BOX:
            return Object.assign({}, state, {
                showMessageBox: true,
                messageTitle: action.title,
                messageContent: action.content,
                btMsg:action.btMsg,
                funcMsg:action.funcMsg
            })
        case HIDE_MESSAGE_BOX:
            return Object.assign({}, state, { showMessageBox: false })
        case SHOW_CONFIRM_BOX:
            return Object.assign({}, state, {
                confirmTitle: action.confirmTitle,
                confirmContent: action.confirmContent,
                onConfirmOk: action.onConfirmOk,
                onConfirmCancel: action.onConfirmCancel,
                confirmOkText: action.confirmOkText,
                confirmCancelText: action.confirmCancelText,
                showConfirmBox: true,
                centerButtonContent: action.centerButtonContent,
                centerButtonFunc: action.centerButtonFunc
            })
        case HIDE_CONFIRM_BOX:
            return Object.assign({}, state, {
                onConfirmOk: () => { },
                onConfirmCancel: () => { },
                showConfirmBox: false
            })
        case SHOW_INPUT_BOX:
            if (action.title != undefined) {
                return Object.assign({}, state, {
                    inputbox: true,
                    inputTitle: action.title,
                    inputPlaceholder: action.placeholder,
                    inputSubmit: action.submit,
                    inputContent: action.content
                })
            } else {
                return Object.assign({}, state, {
                    inputbox: true,
                    inputContent: action.content
                })
            }
        case HIDE_INPUT_BOX:
            return Object.assign({}, state, { inputbox: false })
        case SHOW_QR_CONFIRM:
            return Object.assign({}, state, {
                QRshow: true,
                QRtitle: action.title,
                QRcontent: action.content,
                QRimage: action.image,
                QRonconfirm: action.onconfirm,
                promotionId: action.promotionId,
                DoisoatProcess: action.functionRefresh,
                promotionTypeId: action.promotionTypeId,
                ReceiveGift: action.receive,
            })
        case HIDE_QR_CONFIRM:
            return Object.assign({}, state, {
                QRshow: false
            })
        case SHOW_DOISOAT:
            return Object.assign({}, state, {
                Doisoatshow: true,
                Doisoattitle: action.title,
                DoisoatProcess: action.functionRefresh,
                Code: action.code,
                ReceiveGift: action.receive,
                promotionId: action.promotionId,
                doisoatContent: action.content != undefined ? action.content : ''
            })
        case HIDE_DOISOAT:
            return Object.assign({}, state, {
                Doisoatshow: false,
            })
        case SHOW_RATE:
            return Object.assign({}, state, {
                Rateshow: true,
            })
        case HIDE_RATE:
            return Object.assign({}, state, {
                Rateshow: false,
            })
        // mGreen
        case 'show3BT':
            return Object.assign({}, state, {
                mShow: true,
                mGreenData: action.data
            })
        case 'hide3BT':
            return Object.assign({}, state, {
                mShow: false,
            })
        case 'showStorelet':
            return {
                ...state,
                showStorelet: true,
                contentStorelet: action.content,
                storeletButtonText: action.buttonText,
                storeletHandle: action.buttonHandle,
                stoletPoint: action.point,
                showStamp: action.showStamp
            }
        case 'hideStorelet':
            return { ...state, showStorelet: false }
        case 'showLoginConfirm':
            return { ...state, showLoginConfirm: true, qr: action.qr }
        case 'hideLoginConfirm':
            return { ...state, showLoginConfirm: false, qr: '' }
        case 'showWebview':
            return { ...state, showWebview: true, webviewData: action.payload }
        case 'hideWebview':
            return { ...state, showWebview: false }
        default:
            return state
    }
}
export default uiReducer