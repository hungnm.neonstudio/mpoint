import api from './index'
import config from '../config'
const request = {
    get: (url) => {
        //console.log(url)
        return fetch(url)
            .catch(err => { api.showMessage('Vui lòng kiểm tra kết nối internet hoặc khởi động lại ứng dụng') })
            .then((response) => response.json())
    },
    post: (url, data, token) => {
        
        if ((url + '').indexOf('http') == -1) url = config.HOST + (api.getHost() ? ':' + api.getHost() : '') + '/' + url
        
            return new Promise(function (resolve, reject) {
            fetch(url, {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + (token || api.getToken() || "220d5c42-c86e-43fa-85fd-49fc44e7214c"),
                    'Accept-Language': api.getLang()
                },
                body: JSON.stringify(data)
            }).then((response) => {
                
                try {
                    return response.json()
                } catch (err) {
                    reject({ err: 2, msg: 'Phiên làm việc hết hạn' })
                }
            }).then((rs) => {
               if(!rs) {
                   return reject({ err: 2, msg: 'Phiên làm việc hết hạn' })
               }
               console.log(`[POST]${url}`,data,rs);
               resolve(rs);
                // try{
                //     resolve(JSON.parse(data, (key, value) => {
                //         console.log(`[POST]${url}`,value);
                //         return value && value.type === 'Buffer'
                //             ? Buffer.from(value.data)
                //             : value;
                //     }));
                // }catch(err){
                //     console.log('parse json err', err, data);
                // }
                
            }).catch((err) => {
                console.log('err', err,url);
                reject({ err: 1, msg: 'Vui lòng kiểm tra kết nối mạng' });
            })
        });

    }
}

export default request