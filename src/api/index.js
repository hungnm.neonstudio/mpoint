import { Platform, Dimensions, Alert, AsyncStorage } from 'react-native'
import { push, pop, replace, reset } from '../actions/navActions'
import { setgiftcount, setPer } from '../actions/getGiftCountActions';
import { setUserToken, setUserInfo, setListMember } from '../actions/userActions';
import i18 from 'react-native-i18n';
import dataService from './dataService';
import DeviceInfo from 'react-native-device-info';
var moment = require('moment');
import { Analytics, Hits } from 'react-native-google-analytics';

import {
    showLoading,
    hideLoading,
    showMessage,
    showConfirm,
    hideConfirm,
    showQR,
    hideQR,
    showDoisoat,
    hideDoiSoat,
    showInputBox,
    hideInputBox,
    showMgreen,
    hideMgreen
} from '../actions/uiActions'
import { set_reg_info } from '../actions/regAction'
let locationdata = undefined
let per = null
var marginTop,
    deviceWidth,
    deviceHeight;
if (Platform.OS === 'ios') {
    const marginTop = 20;
    var deviceWidth = Dimensions
        .get('window')
        .width
    var deviceHeight = Dimensions
        .get('window')
        .height
} else {
    var deviceWidth = Dimensions
        .get('window')
        .width
    var deviceHeight = Dimensions
        .get('window')
        .height - 20
}
let store = null
// let host = '8888'
let host = '' // product
// let host = '7779' // dev
let api = {
    sendAnalytic: (screen) => {
        let ga = new Analytics('UA-107484845-1', DeviceInfo.getUniqueID(), 1, DeviceInfo.getUserAgent());
        let screenView = new Hits.ScreenView(
            'mPoint',
            screen,
            DeviceInfo.getReadableVersion(),
            DeviceInfo.getBundleId()
        );
        ga.send(screenView);
    },
    getHost() {
        return host
    },
    setHost(_host) {
        host = _host
    },
    setLocationData: (location) => {
        locationdata = location
    },
    getLocationData: () => {
        return locationdata
    },
    setStore: (_store) => {
        store = _store
    },
    getItemStore: async () => {
        setTimeout(function () {
            return 'splash'
        }, 1000);
    },
    // getItemLocal: (key) => {
    //     return localstore.get(key)
    // },
    // setItemLocal: (key, val) => {
    //     return localstore.save(key, val)
    // },

    // clearItemLocal: (key) => {
    //     return localstore.delete(key)
    // },
    requestLogin() {
        if (store.getState().userState.isLogin) return true;
        api.showConfirm(i18.t('needLogin'), i18.t('titleMsgLogin'), i18.t('confirmOKLogin'), i18.t('confirmCancelTextLogin'),
            () => {
                api.push({ key: "login", dontShowConfirm: true });
                api.hideConfirm();
            },
            () => {
                api.hideConfirm();
            }
        )
        return false;
    },
    gotoHome: () => {
        api.hideLoading();
        setTimeout(() => {
            api.showLoading();
        }, 50);
        dataService.getGiftCount()
            .then((rs) => {
                api.setGitfCount(rs.err == 0 ? rs.amount : '0')
            });
        dataService.getListShopGPS()
            .then(gps => {
                api.setLocationData(gps.shops)
            });
        dataService.getUserInfo().then(rs => {
            if (rs.err == 0) {
                api.setUserInfo(rs);
                store.dispatch(push({ key: 'home' }));
            } else {
                api.showMessage(rs.msg);
            }
        });
        dataService.getListMember(undefined, 1, 0).then(rs => {
            if (rs.err == 0) {
                api.setListMember(rs.members);
            }
        })
    },
    getRealDeviceHeight: () => {
        return deviceHeight
    },
    getRealDeviceWidth: () => {
        return deviceWidth
    },
    showLoading: () => {
        store.dispatch(showLoading())
    },
    hideLoading: () => {
        store.dispatch(hideLoading())
    },
    showMessage: (msg, title, btMsg, funcMsg) => {
        if (title == null) {
            title = 'Thông báo'
        }
        store.dispatch(showMessage(msg, title, btMsg, funcMsg))
        //Alert.alert(title, msg)
    },
    showConfirm: (content, title, ok, cancel, onOk, onCancel, centerButtonContent, centerButtonFunc) => {
        store.dispatch(showConfirm(content, title, ok, cancel, onOk, onCancel, centerButtonContent, centerButtonFunc))
    },
    hideConfirm: () => {
        store.dispatch(hideConfirm())
    },
    showQR: (title, content, isGift, promotionId, functionRefresh, promotionTypeId) => store.dispatch(showQR(title, content, isGift, promotionId, functionRefresh, promotionTypeId)),
    hideQR: () => store.dispatch(hideQR()),
    showStorelet: (point, content, buttonText, buttonHandle, showStamp) => store.dispatch({ type: 'showStorelet', point: point, content: content, buttonText: buttonText, buttonHandle: buttonHandle, showStamp: showStamp }),
    hideStorelet: () => store.dispatch({ type: 'hideStorelet' }),
    showDoisoat: (promotionId, title, code, receive, functionRefresh, content) => store.dispatch(showDoisoat(promotionId, title, code, receive, functionRefresh, content)),
    hideDoisoat: () => store.dispatch(hideDoiSoat()),
    showLoginWebConfirm(qr) { store.dispatch({ type: 'showLoginConfirm', qr: qr }) },
    hideLoginWebConfirm() { store.dispatch({ type: 'hideLoginConfirm' }) },
    showWebview: (url, onRedirect) => {
        store.dispatch({ type: 'showWebview', payload: { url: url, onRedirect: onRedirect } })
    },
    hideWebview: () => {
        store.dispatch({ type: 'hideWebview' })
    },
    pop: () => store.dispatch(pop()),
    push: (route) => store.dispatch(push(route)),
    replaceRoute: (route, index) => store.dispatch(replace(route, index)),
    resetRoute: (route) => store.dispatch(reset(route)),
    showInputBox: (placeholder, title, content, onpress) => store.dispatch(showInputBox(placeholder, title, content, onpress)),
    hideInputBox: () => store.dispatch(hideInputBox()),
    setRegInfo: (token, name, address, dob, email, sex, province, district, ward) => store.dispatch(set_reg_info(token, name, address, dob, email, sex, province, district, ward)),
    setUserToken: (token) => {
        store.dispatch(setUserToken(token));
        dataService.postAddDevice(DeviceInfo.getUniqueID(), 'name');
    },
    setUserInfo: (data) => store.dispatch(setUserInfo(data)),
    setListMember: (data) => store.dispatch(setListMember(data)),
    setGitfCount: (count) => store.dispatch(setgiftcount(count)),
    setPer: (per) => store.dispatch(setPer(per)),
    getToken: () => { return (store ? store.getState().userState.token : '') },
    // set Category
    setCategory(arr) {
        store.dispatch({ type: 'setCate', category: arr })
    },
    //set lang
    setLangEn() {
        store.dispatch({ type: 'en' });
        i18.locale = 'en';
        // api.setHost('6666');
        try {
            AsyncStorage.setItem('lang', 'en');
            // alert(dt.refreshToken)
        } catch (error) {
            console.log('Error when saving data', error);
        }
    },
    setLangVi() {
        store.dispatch({ type: 'vi' });
        i18.locale = 'vi';
        // api.setHost('8888');
        try {
            AsyncStorage.setItem('lang', 'vi');
            // alert(dt.refreshToken)
        } catch (error) {
            console.log('Error when saving data', error);
        }
    },
    getLang() {
        if (!store.getState()) return 'en'
        return store.getState().langState.lang
    }
    ,
    // validate input
    validateEmail: (email) => {
        var re = /^([0-9a-zA-Z]([-_\\.]*[0-9a-zA-Z]+)*)@([0-9a-zA-Z]([-_\\.]*[0-9a-zA-Z]+)*)[\\.]([a-zA-Z]{2,9})$/;
        return re.test(email);
    }, validatePhone: (phone) => {
        var re = /^((012|09|016)[0-9]|(08[6-9]|01(88|86|99)))[0-9]{7}$/;
        return re.test(phone);
    },
    validateSpecialCharacter: (text) => {
        return (!/^[a-zA-ZÀÁÂÃÈÉÊẾÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêếìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ\s]+$/.test(text))
    }
    , setShopLocation: (markers) => store.dispatch(getGiftCountActions(markers)),
    getTime(time, format) {
        let zone = Math.abs(new Date().getTimezoneOffset() / 60);
        return moment(time).utcOffset(zone).format(format)
    },
    // mGreen
    showMgreen: (data) => store.dispatch(showMgreen(data)),
    hideMgreen: () => store.dispatch(hideMgreen()),

    // imenu
    updateBook: (payload) => {
        store.dispatch({ type: 'setBook', payload: payload })
    },
    updateBookItem: (payload) => {
        store.dispatch({ type: 'setItem', payload: payload })
    },
    updateBookCombo: (payload) => {
        store.dispatch({ type: 'setCombo', payload: payload })
    },
    clearBook: () => {
        store.dispatch({ type: 'clearImenu' })
    },
    getDistance: (p1, p2) => {
        if (!p1 || !p2 || !p1.latitude || !p1.longitude || !p2.latitude || !p2.longitude) return null;
        let dx = p2.longitude - p1.longitude;
        let dy = p2.latitude - p1.latitude;
        return parseFloat(Math.sqrt(dx * dx + dy * dy) * 160.9).toFixed(1);
    },
    isViettel: (phone) => {
        phone = phone + '';
        let head = phone.substr(1, 2);
        if (head == '86' || head == '96' || head == '97' || head == '98') {
            return true;
        }
        head = phone.substr(1, 3);
        if (head == '162' || head == '163' || head == '164' || head == '165' || head == '166' || head == '167' || head == '168' || head == '169') {
            return true;
        }
        return false;
        var re = /^0(86|(9[6-8])|16[2-9])[0-9]{7}$/;
        return true;
        return re.test(phone);
    }
    ,
    decode: (t, e) => {
        for (var n, o, u = 0, l = 0, r = 0, d = [], h = 0, i = 0, a = null, c = Math.pow(10, e || 5); u < t.length;) {
            a = null,
                h = 0,
                i = 0;
            do
                a = t.charCodeAt(u++) - 63,
                    i |= (31 & a) << h,
                    h += 5;
            while (a >= 32);
            n = 1 & i ? ~(i >> 1) : i >> 1,
                h = i = 0;
            do
                a = t.charCodeAt(u++) - 63,
                    i |= (31 & a) << h,
                    h += 5;
            while (a >= 32);
            o = 1 & i ? ~(i >> 1) : i >> 1,
                l += n,
                r += o,
                d.push([
                    l / c,
                    r / c
                ])
        }
        return d = d.map(function (t) {
            return { latitude: t[0], longitude: t[1] }
        })
    }
}

export default api