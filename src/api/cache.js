
let data = {}
let get = (key) => {
    return data[key]
}
let set = (key, val)=>{
    data[key] = val
}
var cache = {
    get: get,
    set: set
}
export default cache