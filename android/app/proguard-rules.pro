#-dontobfuscate

# React Native

# Keep our interfaces so they can be used by other ProGuard rules.
# See http://sourceforge.net/p/proguard/bugs/466/
-keep,allowobfuscation @interface com.facebook.proguard.annotations.DoNotStrip
-keep,allowobfuscation @interface com.facebook.proguard.annotations.KeepGettersAndSetters
-keep,allowobfuscation @interface com.facebook.common.internal.DoNotStrip

# Do not strip any method/class that is annotated with @DoNotStrip
-keep @com.facebook.proguard.annotations.DoNotStrip class *
-keep @com.facebook.common.internal.DoNotStrip class *
-keepclassmembers class * {
 @com.facebook.proguard.annotations.DoNotStrip *;
 @com.facebook.common.internal.DoNotStrip *;
}

-keepclassmembers @com.facebook.proguard.annotations.KeepGettersAndSetters class * {
 void set*(***);
 *** get*();
}

-keep class * extends com.facebook.react.bridge.JavaScriptModule { *; }
-keep class * extends com.facebook.react.bridge.NativeModule { *; }
-keepclassmembers,includedescriptorclasses class * { native <methods>; }
-keepclassmembers class *  { @com.facebook.react.uimanager.UIProp <fields>; }
-keepclassmembers class *  { @com.facebook.react.uimanager.annotations.ReactProp <methods>; }
-keepclassmembers class *  { @com.facebook.react.uimanager.annotations.ReactPropGroup <methods>; }
-keep class com.dmgapp.rnwebview.** { *; }
-keep class com.reactnative.ivpusic.imagepicker.** { *; }
-keep class com.horcrux.svg.** { *; }
-keep class com.evollu.react.fcm.** { *; }
-keep class com.learnium.RNDeviceInfo.** { *; }
-keep class com.lwansbrough.RCTCamera.** { *; }
-keep class com.cboy.rn.splashscreen.** { *; }
-keep class com.airbnb.android.react.** { *; }
-keep class com.oblador.vectoricons.** { *; }
-keep class com.RNFetchBlob.** { *; }



-keep class com.facebook.** { *; }
-dontwarn com.facebook.react.**

# okhttp

# okio
-keepattributes Signature
-keepattributes Annotation
-keep class okhttp3.** { *; }
-keep interface okhttp3.** { *; }
-dontwarn okhttp3.**
-dontwarn okio.**
-dontwarn sun.misc.Unsafe