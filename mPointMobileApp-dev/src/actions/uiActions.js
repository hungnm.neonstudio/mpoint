import {
    SHOW_LOADING,
    HIDE_LOADING,
    SHOW_MESSAGE_BOX,
    HIDE_MESSAGE_BOX,
    SHOW_INPUT_BOX,
    HIDE_INPUT_BOX,
    SHOW_DOISOAT,
    HIDE_DOISOAT,
    SHOW_QR_CONFIRM,
    HIDE_QR_CONFIRM,
    SHOW_RATE,
    HIDE_RATE,
    SHOW_CONFIRM_BOX,
    HIDE_CONFIRM_BOX
} from './actionTypes'

export const showLoading = () => {
    return {type: SHOW_LOADING}
}
export const hideLoading = () => {
    return {type: HIDE_LOADING}
}
export const showMessage = (content, title,btMsg,funcMsg) => {
    return {type: SHOW_MESSAGE_BOX, content: content, title: title,btMsg,funcMsg}
}
export const hideMessageBox = () => {
    return {type: HIDE_MESSAGE_BOX}
}
export const showInputBox = (placeholder, title,content, onpress) => {
    return {
        type: SHOW_INPUT_BOX,
        title:title,
        placeholder:placeholder,
        submit:onpress,
        content:content
    }
}
export const hideInputBox = () => {
    return {type: HIDE_INPUT_BOX,    
    }
}
export const showDoisoat = (promotionId,title,code,receive,functionRefresh,content) => {
    return {
            type: SHOW_DOISOAT, 
            title:title ,
            code: code,
            receive:receive,
            functionRefresh:functionRefresh,
            promotionId:promotionId,
            content:content
    }
}
export const hideDoiSoat = () => {
    return {type: HIDE_DOISOAT,    
    }
}

export const showQR = (title,content,isGift,promotionId,functionRefresh,typeId) => {
    return {
            type: SHOW_QR_CONFIRM,
            title:title,
            content:content,
            receive:isGift,
            promotionId:promotionId,
            functionRefresh:functionRefresh,
            promotionTypeId:typeId
    }
}
export const hideQR = () => {
    return {type: HIDE_QR_CONFIRM,    
    }
}
export const showRate = () => {
    return {type: SHOW_RATE,    
    }
}
export const hideRate = () => {
    return {type: HIDE_RATE,    
    }
}
export const showConfirm = (content, title, ok, cancel, onOk, onCancel,centerButtonContent,centerButtonFunc) => {
    return {
            type: SHOW_CONFIRM_BOX, 
            showConfirmBox:true,
            confirmTitle:title,
            confirmContent:content,
            onConfirmOk:onOk,
            onConfirmCancel:onCancel,
            confirmCancelText:cancel,
            confirmOkText:ok,
            centerButtonContent:centerButtonContent,
            centerButtonFunc:centerButtonFunc
    }   
}
export const hideConfirm = () => {
    return {
        type: HIDE_CONFIRM_BOX, 

    }
}
// mgreen
export const hideMgreen = () => {
    return {
        type:'hide3BT' , 

    }
}
export const showMgreen = (data) => {
    return {
        type:'show3BT' , 
        data:data
    }
}