import {PUSH_ROUTE, POP_ROUTE, REPLACE_ROUTE, JUMPTO_ROUTE, RESET_ROUTE,CHANGE_RARE_LEVEL} from './actionTypes'

export const push = (route) => {
    return {type: PUSH_ROUTE, route}
}
export const pop = () => {
    return {type: POP_ROUTE}
}
export const replace = (route,index) => {
    return {type: REPLACE_ROUTE,route:route,index:index}
}
export const reset = (route) => {
    return {type: RESET_ROUTE, route: route}
}
export const jumpTo = (key) => {
    return {type: JUMPTO_ROUTE, key: key}
}
export const changRateLevel = (rateLevel) => {
    return {type:CHANGE_RARE_LEVEL,rateLevel:rateLevel }
}