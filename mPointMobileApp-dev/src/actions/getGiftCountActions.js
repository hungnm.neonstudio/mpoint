export const setgiftcount = (count) => {
    return {
        type:'setGiftCount',
        count
    }
}
export const setPer = (per) => {
    return {
        type:'setPer',
        per
    }
}