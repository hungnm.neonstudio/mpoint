import {SET_REG_INFO} from './actionTypes'

export const set_reg_info = (token,name,address,dob,email,sex,province,district,ward)=>{
    return{
        type:SET_REG_INFO,
        token:token,
        name:name,
        address:address,
        dob:dob,
        email:email,
        sex:sex,
        province:province,
        district:district,
        ward:ward
    }
}