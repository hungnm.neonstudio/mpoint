import {createStore, applyMiddleware} from 'redux'
import rootReducer from './reducers/rootReducer'
import thunkMiddleware from 'redux-thunk'
import createLogger from 'redux-logger'
const configureStore = () => {
    const loggerMiddleware = createLogger()
    const store = createStore(rootReducer, applyMiddleware(thunkMiddleware, loggerMiddleware))
    return store
}
export default configureStore
