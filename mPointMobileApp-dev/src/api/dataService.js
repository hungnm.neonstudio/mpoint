import request from './request.js';
import config from '../config';
import cache from './cache'
import location from './locations.json'
import api from '../api'
import DeviceInfo from 'react-native-device-info'
import { Platform, PermissionsAndroid } from 'react-native'
import i18 from '../components/i18';
import RNAccountKit from 'react-native-facebook-account-kit'

// type = 1: promotions; type = 0: gift 1/0/0/15 => type/tab/start/num_of_item
var dataService = {
    listAvaiableMembers: (phone) => {
        var url = 'api/authen/listAvaiableMembers';
        var data = {
            phone: phone
        }
        return request.post(url, data);
    },
    getMainPromotions: (token, type, start, num_of_item) => {
        // var url = "http://mpoint.vn:2540/promotions/getListItems/" + token + "/" +
        // type + "/0/" + start + "/" + num_of_item; console.log("getMainPromotions",
        // url) return request.get(url);
        var url = 'api/listPromotions';
        var data = {
            categoryId: 0,
            type: type,
            start: start,
            number: num_of_item
        }
        return request.post(url, data, token);
    },
    getListPromotion: (token, categoryID, type, start, num_of_item) => {
        // var url = "http://mpoint.vn:2540/promotions/getListItems/" + token + "/" +
        // type + "/1/" + start + "/" + num_of_item; console.log("getFood", url) return
        // request.get(url);
        var url = 'api/listPromotions';
        var data = {
            categoryId: categoryID,
            type: type,
            start: start,
            number: num_of_item
        }
        return request.post(url, data, token);
    },
    getShopping: (token, type, start, num_of_item) => {
        // var url = "http://mpoint.vn:2540/promotions/getListItems/" + token + "/" +
        // type + "/2/" + start + "/" + num_of_item; console.log("getShopping", url)
        // return request.get(url);
        var url = 'api/listPromotions';
        var data = {
            categoryId: 2,
            type: type,
            start: start,
            number: num_of_item
        }
        return request.post(url, data, token);
    },
    getOther: (token, type, start, num_of_item) => {
        // var url = "http://mpoint.vn:2540/promotions/getListItems/" + token + "/" +
        // type + "/3/" + start + "/" + num_of_item; console.log("getOther", url) return
        // request.get(url);
        var url = 'api/listPromotions';
        var data = {
            categoryId: 3,
            type: type,
            start: start,
            number: num_of_item
        }
        return request.post(url, data, token);
    },
    getUserToken: (input, uuid) => {
        var url = 'api/authen/loginNew';
        var data = { input, uuid }
        return request.post(url, data);



        // alert(uuid) var url = "http://mpoint.vn:2540/authen/getCodeByPhone/" + phone
        // + "/" + uuid; console.log("getUserToken", url) return request.get(url);
        var url = 'api/authen/login';
        var data = {
            input: phone,
            uuid: uuid
        }
        return request.post(url, data);
    },
    logOut: () => {
        var url = 'api/logout';
        return request.post(url, {});
    },
    getUserInfo: (token) => {
        // var url = "http://mpoint.vn:2540/authen/getUserInfo/" + token;
        // console.log("getUserInfo", url) return request.get(url);
        var url = 'api/getUserInfo';
        return request.post(url, {}, token);
    },
    getListMember: (token, state, memberTypeId) => {
        var url = 'api/listMembers';
        var data = {
            state: state,
            memberTypeId: memberTypeId
        }
        return request.post(url, data, token);
    },
    getCodeLogin: (phone) => {
        let url = 'http://mpoint.vn:2540/authen/getCodeLogin/' + phone
        return request.get(url)
    },
    getGiftsStick: (token, type, start, number) => {
        // var url = "http://mpoint.vn:2540/promotions/getListGifts/" + token + "/0/0/15";
        // console.log("getGiftsStick", url)
        // return request.get(url);
        var url = 'api/listGifts';
        var data = {
            type: type,
            start: start,
            number: number
        }
        return request.post(url, data, token);
    },
    getGiftCardsStick: (token) => {
        var url = "http://mpoint.vn:2540/promotions/getListGifts/" + token + "/1/0/15";
        console.log("getGiftCardsStick", url)
        return request.get(url);
    },
    getListRegisterMember: (phone) => {
        var url = "http://mpoint.vn:2540/authen/getListMember/0/" + phone;
        console.log("", url)
        return request.get(url);
    },
    changeCard: (token, id) => {
        // var url = "http://mpoint.vn:2540/authen/selectMember_new/" + token + "/" +
        // memberID + "/" + userCode; console.log("getListRegisterMember", url) return
        // request.get(url);
        var url = 'api/changeCard';
        var data = {
            cardId: id
        }
        dataService.getListShopGPS(token).then(gps => {
            api.setLocationData(gps.shops)
        });
        dataService.getGiftCount()
            .then((rs) => {
                api.setGitfCount(rs.err == 0 ? rs.amount : '0')
            });
        return request.post(url, data, token);
    },
    getItemInfo: (token, id) => {
        // let url = 'http://mpoint.vn:2540/promotions/getItemInfo/' + token + '/' + id
        // console.log("getItemInfo", url) return request.get(url)
        var url = 'api/promotionInfo';
        var data = {
            promotionId: id
        }
        return request.post(url, data, token);
    },
    getLike: (token, id) => {
        // let url = 'http://mpoint.vn:2540/promotions/like/' + token + '/' + id
        // console.log("getLike", url) return request.get(url)
        var url = 'api/like';
        var data = {
            promotionId: id
        };
        return request.post(url, data, token);
    },
    getComment: (token, id, start, num_of_item) => {
        // let url = 'http://mpoint.vn:2540/comments/getListComments/' + token + '/' +
        // id + '/' + start + '/' + take console.log(url) return request.get(url)
        var url = 'api/listComments';
        var data = {
            promotionId: id,
            start: start,
            number: num_of_item
        }
        return request.post(url, data, token);
    },
    sendComment: (token, id, comment) => {
        // var url = "http://mpoint.vn:2540/comments/createComment/" + token + "/" + id
        // + "/" + comment console.log("sendComment", url) return request.get(url)
        var url = 'api/comment';
        var data = {
            promotionId: id,
            content: comment
        }
        return request.post(url, data, token);
    },
    getPartnerInfo: (token, id) => {
        // let url = 'http://mpoint.vn:2540/promotions/getPartnerInfo/' + token + '/' + id
        // console.log("getPartnerInfo", url)
        // return request.get(url)
        var url = 'api/partnerInfo';
        var data = {
            id: id
        }
        return request.post(url, data, token);
    },
    getQRcode: (token, id) => {
        // let url = 'http://mpoint.vn:2540/backend/createPromotionCode/' + token + '/'
        // + uuid + '/' + id return request.get(url)
        var url = 'api/getCode';
        var data = {
            promotionId: id
        }
        return request.post(url, data, token);
    },
    getListShopGPS: (token, type) => {
        // var url = 'http://mpoint.vn:2540/promotions/getListShopGPS/undefined/-1';
        // console.log("getListShopGPS", url); return request.get(url);
        d = cache.get('shopLocation')
        if (type != undefined) d == null
        if (d == null) {
            var url = 'api/listShops';
            var data = {

            }
            d = request.post(url, data, token);
            cache.set('shopLocation', d)
            // alert('KO')
        }
        return d
    },
    getShopMarkerInfo: (id) => {
        var url = "http://mpoint.vn:2540/promotions/getShopMarkerInfo/" + id;
        console.log("getShopMarkerInfo", url);
        return request.get(url);
    },
    getRoutesDirection: (origin, destination, mode) => {
        var url = 'https://maps.googleapis.com/maps/api/directions/json?origin=' + origin + '&destination=' + destination + '&key=AIzaSyDEpCvp3ix8WcxqcUFB_Vg4kn19HW-9IEg&mode=' + mode;
        console.log("getRoutesDirection", url);
        return request.get(url);
    },
    sendRate: (token, id, rateLevel) => {
        // let url = 'http://mpoint.vn:2540/promotions/rate/' + token + '/' + id + '/' +
        // rateLevel // alert(url) return request.get(url)
        var url = 'api/rate';
        var data = {
            promotionId: id,
            rate: rateLevel
        }
        return request.post(url, data, token);
    },
    confirmQR: (promotionId, username, pass, code, cost, token) => {
        // let url = 'http://mpoint.vn:2540/backend/checkCodeByShop/' + username + '/' + pass + '/' + code + '/' + cost
        // return request.get(url)
        let url = 'api/checkCodeByPhone'
        let data = {
            promotionId: promotionId,
            code: code,
            username: username,
            password: pass,
            cost: cost
        }
        return request.post(url, data, token)
    },
    suggestText: (token, keyWord) => {
        var url = 'api/suggestText';
        var data = {
            text: keyWord,
        }
        return request.post(url, data, token);
    },
    search: (token, keyWord, start, num_of_item) => {
        // var url = "http://mpoint.vn:2540/promotions/search/" + token + "/" + type + "/" + keyWord + "/" + start + "/" + num_of_item;
        // console.log('search', url);
        // return request.get(url);
        var url = 'api/search';
        var data = {
            text: keyWord,
            start: start,
            number: num_of_item
        }
        return request.post(url, data, token);
    },
    // for register
    getUserCode: () => {
        let url = 'http://mpoint.vn:2540/authen/generateOpenMemberCode/100'
        return request.get(url)
    },
    getMemberInfo: (userCode) => {
        let url = 'http://localhost:8080/#/register/' + userCode + '/123'
        return request.get(url)
    },
    setMemberEnable: (token, user_code, member_id, enable) => {
        var url = 'http://mpoint.vn:2540/authen/setMemberEnable/' + token + '/' + user_code + '/' + member_id + '/' + enable;
        console.log('setMemberEnable', url);
        return request.get(url);

    },
    toggleCard: (token, id) => {
        var url = 'api/toggleCard';
        var data = {
            cardId: id
        }
        return request.post(url, data, token);
    },
    regGetOTP: (phone, code) => {
        let url = 'http://mpoint.vn:2540/authen/confirmPhone/' + phone + '/' + code + '/100'
        return request.get(url)
    },

    getListProvinces: () => {
        // var url = "http://mpoint.vn:2540/meta/getListProvinces";
        // return request.get(url);
        return location.provinces
    },
    getListDistricts: (idProvinces) => {

        // let url = 'http://mpoint.vn:2540/meta/getListDistricts/' + idProvinces
        // return request.get(url)
        district = []
        location.districts.map(item => {
            if (parseInt(item.parent) == idProvinces)
                district.push(item)
        })
        return district
    },
    getListWards: (idDistricts) => {
        // let url = 'http://mpoint.vn:2540/meta/getListWards/' + idDistricts
        // return request.get(url)
        ward = []
        location.wards.map(item => {
            if (parseInt(item.parent) == idDistricts)
                ward.push(item)
        })
        return ward
    },
    updateUserInfo: (token, name, address, dob, email, sex, province, district, ward) => {
        let url = 'http://mpoint.vn:2540/authen/updateUserInfo/' + token + '/' + name + '/' + address + '/' + dob + '/' + email + '/' + sex + '/' + province + '/' + district + '/' + ward
        return request.get(url)
    },
    verifyCode: (phone, code, uuid) => {
        let url = 'http://mpoint.vn:2540/authen/verifyCode/' + phone + '/' + code + '/' + uuid
        return request.get(url)
    },
    getListPartners: (token, start, num_of_item) => {
        // var url = 'http://mpoint.vn:2540/promotions/getListPartners/' + token + '/' + start + '/' + num_of_item;
        // return request.get(url);
        var url = 'api/listPartners';
        var data = {
            start: start,
            number: num_of_item
        }
        return request.post(url, data, token);
    },
    // for register
    postVerifyOtpLogin: (otpId, otp, uuid,merchantKey,accountKitCode) => {
        let url = 'api/authen/verifyOtpLogin'
        let data = {
            id: otpId,
            otp: otp,
            uuid: uuid,
            merchantKey,accountKitCode
        }
        return request.post(url, data, 'token')
    },
    postVerifyOtpRegister: (otp, otpID, accountKitCode) => {
        let url = 'api/authen/verifyOtpRegister/'
        let data = {
            id: otpID,
            otp: otp,
            accountKitCode
        }
        return request.post(url, data, '123')
    },
    postReg: (phone) => {
        let url = '/api/authen/registerNew'
        let data = {
            phone: phone
        }
        return request.post(url, data)
    },
    postListMember: (memberTypeId, state, token) => {
        let url = 'api/listMembers'
        let data = {
            memberTypeId: memberTypeId,
            state: state,
        }
        return request.post(url, data, token)
        //1b48d321-21c2-4fad-ba86-300ae40fd04c
    },
    postRegisterFreeCard: (memberId, token) => {
        let url = 'api/registerFreeCard'
        let data = {
            memberId: memberId
        }
        return request.post(url, data, token)
    },
    postAddCard: (code, token) => {
        let url = 'api/addCard'
        let data = {
            code: code
        }
        return request.post(url, data, token)
    },
    postUpdateUserInfo: (name, address, gender, birthday, email, districtId, provinceId, wardId, Avatar, token) => {
        let url = 'api/updateUserInfo'
        let data = {
            name: name,
            address: address,
            gender: gender,
            birthday: birthday,
            email: email,
            districtId: districtId,
            provinceId: provinceId,
            wardId: wardId,
            avatar: Avatar
        }
        return request.post(url, data, token)
    },
    postAddDevice: (uuid, name, token) => {
        let url = 'api/addDevice'
        let data = {
            uuid: uuid,
            name: name
        }
        return request.post(url, data, token)
    },
    // for login
    postRefreshToken: (refreshToken) => {
        let url = 'api/authen/refreshToken'
        let data = {
            refreshToken: refreshToken
        }
        return request.post(url, data, 'token')
    },
    postScanQR: (qrcode, token) => {
        let url = 'api/scanQR'
        let data = {
            qrcode: qrcode
        }
        return request.post(url, data, token)
    }
    // notification
    , regNotification(platform, uuid, registerId, token, phone) {
        let url = 'api/registerId'
        let data = {
            platform: platform,
            uuid: uuid,
            registerId: registerId,
            phone: phone
        }
        return request.post(url, data, token)
    },
    //get AD man hinh ngoai screen = 1 , man hinh  trong screen = 2
    getAd(token, screen) {
        let url = 'api/getAdvertisement'
        let data = {
            screen: screen
        }
        return request.post(url, data, token)
    },
    // lay so qua
    getGiftCount(token) {
        let url = 'api/amountGift'
        let data = {
        }
        return request.post(url, data, token)
    },
    // shareFacebook
    shareFacebook(token, promotionId) {
        let url = 'api/getFacebook'
        let data = {
            promotionId: promotionId
        }
        return request.post(url, data, token)
    },

    // mgreen
    getMgreen(token, promotionId) {
        let url = 'api/qrMgreen'
        let data = {
            promotionId: promotionId
        }
        return request.post(url, data, token)
    },
    mGreenSubmit(token, promotionId, type, shopId, partnerId, tasktime, bookedtime) {
        let url = 'api/submitTask'
        let data = {
            promotionId: promotionId,
            type: type,
            shopId: shopId,
            partnerId: partnerId,
            tasktime: tasktime,
            bookedtime: bookedtime
        }
        return request.post(url, data, token)
    },
    // **history** \\ 
    // get list recieved promotion
    getPromotionRecieve(token, start, numLoad, search) {
        let url = 'api/listSentPromotion'
        let data = {
            start: start,
            number: numLoad,
            search: search || ''
        }
        return request.post(url, data, token)
    },
    // get list used promotion
    getPromotionUsed(token, start, numLoad, search) {
        let url = 'api/listGivenPromotion'
        let data = {
            start: start,
            number: numLoad,
            search: search || ''
        }
        return request.post(url, data, token)
    },
    // get history Point
    getExchangePointHistory(token, start, numLoad, search) {
        let url = 'api/exchangePointHistory'
        let data = {
            start: start,
            number: numLoad,
            search: search || ''
        }
        return request.post(url, data, token)
    },
    // get his gift non used
    getHisGift(token, start, numLoad, search) {
        let url = 'api/listSentGift'
        let data = {
            start: start,
            number: numLoad,
            search: search || ''
        }
        return request.post(url, data, token)
    },
    // get his gift used
    getHisGiftUsed(token, start, numLoad, search) {
        let url = 'api/listGivenGift'
        let data = {
            start: start,
            number: numLoad,
            search: search || ''
        }
        return request.post(url, data, token)
    },

    // get list category
    getListCategory() {
        let url = 'api/authen/listCategory'
        return request.post(url, {}, '123')
    },

    // iMenu
    getListRestaurant(start, number, search) {
        let url = 'api/listPartnerRes'
        let data = {
            start: start + '',
            number: number + '',
            search: (search || '')
        }
        return request.post(url, data)
    },
    getListAddRes(start, number, search, partnerId) {
        let url = 'api/listShopRes'
        let data = {
            start: start + '',
            number: number + '',
            search: (search || ''),
            partnerId: partnerId
        }
        return request.post(url, data)
    },
    getListTable(shopId, status, type, searchTime, date, start, number) {
        let url = 'api/listTable'
        let data = {
            start: start,
            number: number,
            shopId: shopId,
            status: status,
            type: type,
            searchTime: searchTime,
            date: date
        }
        console.log('====================================');
        console.log(data);
        console.log('====================================');
        return request.post(url, data)
    },
    getListInfoCombo() {
        let url = 'api/listInfoCombo'
        return request.post(url, {}, store)
    },
    createBook(data) {
        let url = 'api/createBook'
        console.log('====================================');
        console.log(JSON.stringify(data));
        console.log('====================================');
        return request.post(url, data)
    },
    getListItemImenu(categoryId, searchString) {
        let url = 'api/listInfoCombo'
        let data = {
            categoryId: start,
            searchString: numLoad
        }
        return request.post(url, data)
    },
    getListCombo(menuReId, searchString, start, number) {
        let url = 'api/listCombo'
        let data = {
            menuReId: menuReId,
            searchString: searchString,
            start: start,
            number: number
        }
        return request.post(url, data)
    },
    listCategoryRes(shopId, searchString, start, number) {
        let url = 'api/listCategoryRes'
        let data = {
            shopId: shopId,
            searchString: searchString,
            start: start,
            number: number
        }
        return request.post(url, data, api.getToken())
    },
    bookTable(tableId, startTime, shopId, item, combo) {
        let url = 'api/createBook'
        let data = {
            tableId: tableId,
            startTime: startTime,
            shopId: shopId,
            item: item,
            combo: combo
        }
        return request.post(url, data)
    },
    getItemCategory(categoryId, searchString, start, number) {
        let url = 'api/listItem'
        let data = {
            categoryId: categoryId,
            searchString: searchString,
            start: start,
            number: number
        }
        return request.post(url, data)
    },
    getListBill: (shopId, type, start, number) => {
        let url = 'api/listBill'
        let data = {
            shopId: shopId,
            type: type,
            start: start,
            number: number
        }
        return request.post(url, data)
    },
    getDetailBook: (bookId) => {
        let url = 'api/getDetailBook'
        let data = {
            bookId: bookId
        }
        return request.post(url, data);
    },
    getDetailBill: (billId) => {
        let url = 'api/detailBill'
        let data = {
            billId: billId
        }
        return request.post(url, data)
    },
    getListBook: (shopId, type, start, number) => {
        let url = 'api/getlistBook'
        let data = {
            shopId: shopId,
            type: type,
            start: start,
            number: number
        }
        return request.post(url, data)
    },
    cancelBook: (bookId) => {
        let url = 'api/cancelOrder'
        let data = {
            bookId: bookId
        }
        return request.post(url, data)
    },
    getStampInfo: (promotionId) => {
        let url = 'http://mpoint.vn:3000/stampInfo'
        let data = {
            promotionId: promotionId
        }
        return request.post(url, data);
    },
    verifyStampToken: (promotionId, token, code, cost) => {
        let url = 'http://mpoint.vn:3000/verifyTokenStamp';
        let data = {
            promotionId: promotionId,
            token: token,
            code: code,
            cost: cost
        }
        return request.post(url, data);
    },
    addFreeCard: (token) => {
        let url = 'api/addCardFree';
        return request.post(url, {}, token).then(rs => {
            if (rs.err == 0) {
                dataService.getListMember(undefined, 1, 0)
                    .then(kq => {
                        if (kq.err == 0) {
                            api.setListMember(kq.members)
                        }
                    })
            }
        });
    },
    exchangePoint: (sourceMember, destinationMember, sourcePoint) => {
        let url = 'api/changePointCard';
        let data = { sourceMember, destinationMember, sourcePoint }
        return request.post(url, data);
    },
    getPoint: (cardID) => {
        let url = 'api/getPointCard';
        let data = { partnerId: cardID }
        return request.post(url, data);
    },
    // send gps user
    insertGPS: () => {
        _checkPer().then(rs => {
            if (rs) {
                navigator.geolocation.getCurrentPosition(position => {
                    cache.set('userLocation', position.coords);
                    let url = 'api/gps'
                    let data = {
                        latitude: position.coords.latitude + '',
                        longitude: position.coords.longitude + '',
                        uuid: DeviceInfo.getUniqueID()
                    }
                    request.post(url, data).then(rs => {
                        // alert(JSON.stringify(rs))
                    })
                }, (error) => { }, {
                        enableHighAccuracy: false,
                        timeout: 15000,
                        maximumAge: 1000
                    })
            }
        })
    },
    callPay: (billId) => {
        let url = 'api/callPay'
        let data = {
            billId: billId
        }
        return request.post(url, data)
    },
    addFood: (data) => {
        let url = 'api/addMenu'
        return request.post(url, data)
    },
    getAccountKitToken: (getToken, phone) => {
        if (!phone) phone = '';
        RNAccountKit.configure({
            responseType: 'code',
            titleType: 'login',
            initialAuthState: '',
            initialEmail: 'some.initial@email.com',
            initialPhoneCountryPrefix: '+84',
            initialPhoneNumber: phone,
            facebookNotificationsEnabled: true || false, // true by default
            readPhoneStateEnabled: true || false, // true by default,
            receiveSMS: true || false, // true by default,
            countryWhitelist: ['VN'], // [] by default
            countryBlacklist: ['US'], // [] by default
            defaultCountry: 'VN'
        });
        RNAccountKit.loginWithPhone()
            .then((token) => {
                console.log('====================================');
                console.log(token);
                console.log('====================================');
                if (!token) {
                    getToken();
                } else {
                    getToken(token);
                }
            }, cancel => {
                getToken();
            })
    }
}
_checkPer = () => {
    return new Promise((done) => {
        if (Platform.OS === 'android' && Platform.Version >= 23) {
            PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION).then((result) => {
                if (result) {
                    console.log("Permission is OK");
                    return done(true)
                } else {
                    PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION).then((result) => {
                        if (result) {
                            console.log("User accept");
                            return done(true)
                        } else {
                            return done()
                            console.log("User refuse");
                            api.showMessage(i18.t('errLocationPer'), i18.t('titleMsgLogin'))
                        }
                    });
                }
            });
        } else {
            done(true)
        }
    })

}
export default dataService;