import {SET_REG_INFO} from '../actions/actionTypes'
let initState ={
        token:'',
        name:'',
        address:'',
        dob:'',
        email:'',
        sex:1,
        province:'',
        district:'',
        ward:''
}

const regReducer = (state = initState,action)=>{
    if(action.type === SET_REG_INFO){
        return Object.assign({},state,{
            token:action.token===''?state.token:action.token,
            name:action.name===''?state.name:action.name,
            address:action.address===''?state.address:action.address,
            dob:action.dob===''?state.dob:action.dob,
            email:action.email===''?state.email:action.email,
            sex:action.sex===''?state.sex:action.sex,
            province:action.province===''?state.province:action.province,
            district:action.district===''?state.district:action.district,
            ward:action.ward===''?state.ward:action.ward
        })
    }
    else return state
}

export default regReducer