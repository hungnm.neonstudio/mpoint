import { SET_USER_INFO, SET_USER_TOKEN, SET_UUID, SET_LIST_MEMBER } from '../actions/actionTypes';

var initialState = {
    isLogin: false,
    token: 'd9d4685b-8546-4224-87ba-610c0623e729',
    uuid: '',
    data: {
        "endUser": {
            "id": 29075,
            "phone": "",
            "name": null,
            "address": null,
            "gender": null,
            "birthday": null,
            "email": null,
            "activated": true,
            "avatar": "http://static.mpoint.vn/Uploads/avatar_default.png",
            "createdAt": "2017-11-08T03:18:22.000Z",
            "updatedAt": "2017-11-08T04:15:34.000Z",
            "districtId": null,
            "provinceId": null,
            "wardId": null,
            "currentMemberId": 354,
            "denyOtpTime": "2017-11-08T04:15:34.000Z",
            "tokens": [
                {
                    "id": 43612,
                    "token": "e2805ead-b34d-41d4-8d5d-fdc84a42878e",
                    "scope": "",
                    "expiredAt": "2017-11-08T21:00:11.000Z",
                    "createdAt": "2017-11-08T04:20:11.000Z",
                    "updatedAt": "2017-11-08T04:20:11.000Z",
                    "endUserId": 29075
                }
            ],
            "district": null,
            "province": null,
            "ward": null
        },
        "active": {
            "id": 58271,
            "code": "mPoint",
            "addPoint": 0,
            "subPoint": 0,
            "memberId": 354,
            "cardGroupId": 66,
            "partner": {
                "id": 354,
                "name": "mPoint",
                "shortName": "mPoint",
                "slogan": "Ví thẻ ưu đãi và tích điểm trên Mobile",
                "groupPrefix": "",
                "pointPrefix": "",
                "logo": "http://mpoint.vn:8888/static/Uploads/1490241747763Logo mpoint 300x300px.jpg",
                "card": "http://mpoint.vn:8888/static/Uploads/1489551676986the-mpoint.png",
                "background": ""
            },
            "cardGroup": {
                "id": 66,
                "name": "Bạc",
                "groupId": "b"
            }
        },
        "mloyalty": {
            "id": 58270,
            "code": "FPIP90KCM",
            "addPoint": 0,
            "subPoint": 0,
            "memberId": 171,
            "cardGroupId": 1,
            "partner": {
                "id": 171,
                "name": "Mediaone",
                "shortName": "Mediaone",
                "slogan": "Ví thẻ ưu đãi và tích điểm trên Mobile",
                "groupPrefix": "",
                "pointPrefix": "",
                "logo": "http://mpoint.vn:8888/static/Uploads/cb364ab611e8e5433f906cf872f2351c20160809074312.jpg",
                "card": "http://mpoint.vn:8888/static/Uploads/57ad496a8b5835401e353d6504131d4bdf4193e810b2720160812035834.png",
                "background": ""
            },
            "cardGroup": {
                "id": 1,
                "name": "Bạc",
                "groupId": "b"
            }
        },
    },
    listMember: [
        {
            "id": 354,
            "name": "mPoint",
            "shortName": "mPoint",
            "slogan": "Ví thẻ ưu đãi và tích điểm trên Mobile",
            "detail": "Thẻ ưu đãi và tích điểm mPoint dành cho tất cả các khách hàng sử dụng Ví thẻ điện tử trên Mobile mPoint. \r\n\r\nSau khi tải Ví thẻ mPoint, chỉ với vài thao tác đơn giản, đăng ký thẻ bằng số số điện thoại của mình là Quý khách có 1 thẻ ưu đãi mPoint với hàng ngàn ưu đãi trong điện thoại di động của mình. \r\nHướng dẫn sử dụng:\r\nNhấn vào biểu tượng Mpoint trên điện thoại  để mở ví thẻ => Nhập số điện thoại để đăng nhập => Chọn ưu đãi và nhấn \"Nhận ưu đãi\", quý khách sẽ nhận được mã ưu đãi sau đó đưa mã ưu đãi cho nhân viên cửa hàng.\r\n\r\nĐối tượng:Tất cả các khách hàng sử dụng Ví thẻ điện tử trên Mobile mPoint. Đăng ký thẻ bằng số điện thoại.\r\n\r\nHotline hỗ trợ đăng ký thẻ: 04 39393966 (từ 9:00-18:00, thứ Hai đến thứ Sáu)",
            "website": "http://mpoint.vn",
            "phone": "0439393559",
            "introduce": "-Thẻ ưu đãi và tích điểm mPoint dành cho tất cả khách hàng sử dụng ví điện tử trên  Mobile Mpoint\r\n-Sau khi tải Ví thẻ mPoint, chỉ với vài thao tác đơn giản đăng ký thẻ bằng số điện thoại của mình là Qúy Khách có 1 thẻ ưu đãi mPoint với hàng ngàn ưu đãi trong điện thoại của mình\r\n",
            "howuse": "-Nhấp vào biểu tượng mPoint trên điện thoại để mở ví\r\n-Nhập số điện thoại để đăng nhập\r\n-Chọn ưu đãi và nhấn \"Nhận ưu đãi\", Qúy Khách sẽ nhận được mã ưu đãi sau đó đưa mã ưu đãi cho nhân viên cửa hàng",
            "object": "-Dành cho mọi đối tượng",
            "memberTypeId": 2,
            "pointPrefix": "",
            "groupPrefix": "",
            "card": "http://mpoint.vn:8888/static/Uploads/1489551676986the-mpoint.png",
            "logo": "http://mpoint.vn:8888/static/Uploads/1490241747763Logo mpoint 300x300px.jpg",
            "email": "mediaone@gmail.com",
            "exchangeRates": [
                {
                    "id": 2,
                    "point": 1,
                    "money": 1,
                    "currency": "VND",
                    "createdAt": null,
                    "updatedAt": null,
                    "partnerId": 354
                }
            ],
            "memberCards": [
                {
                    "id": 58271,
                    "code": "X6CHY88YQ",
                    "status": true,
                    "addPoint": 0,
                    "subPoint": 0,
                    "createdAt": "2017-11-08T03:18:27.000Z",
                    "updatedAt": "2017-11-08T03:18:27.000Z",
                    "endUserId": 29075,
                    "cardGroupId": 66,
                    "memberId": 354,
                    "cardGroup": {
                        "name": "Bạc"
                    }
                }
            ]
        },
    ]
}
const userReducer = (state = initialState, action) => {
    switch (action.type) {
        case SET_USER_TOKEN:
            return Object.assign({}, state, { token: action.token, isLogin: true })
        case SET_USER_INFO:
            return Object.assign({}, state, { data: action.data })
        case SET_UUID:
            return Object.assign({}, state, { uuid: action.uuid })
        case SET_LIST_MEMBER:
            return Object.assign({}, state, { listMember: action.listMember })
        default:
            return state;
    }
}
export default userReducer;