var initState = {
    GiftCount:'',
    Percent:'',
    Category:[]
}

const locationReducer = (state = initState, action) => {
    switch (action.type) {
        case 'setGiftCount':
            return {
                ...state,
                GiftCount:action.count
            }
        case 'setPer':
            return {
                ...state,
                Percent:action.per
            }
        case 'setCate': 
            return {
                ...state,
                Category:action.category
            }
        default:
            return state;
    }
}

export default locationReducer;