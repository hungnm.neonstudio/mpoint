import {combineReducers} from 'redux';
import navState from './navReducer';
import uiState from './uiReducer';
import userState from './userReducer';
import giftCountState from './giftCountReducer';
import regState from './regReducer';
import langState from './langReducer';
import imenuState from './imenuReducer'
const rootReducer = combineReducers({navState, imenuState,uiState, userState,regState,giftCountState,langState});
export default rootReducer;