var initState = {
    orderId: null,
    billId: null,
    tableId: [],
    people: 1,
    startTime: null,
    shopId: 0,//null,
    item: [],
    combo: []
}

const imenuReducer = (state = initState, action) => {
    switch (action.type) {
        case 'setBook':
            return Object.assign({}, state, action.payload)
        case "setItem":
            return {
                ...state, item: action.payload,
            }
        case "setCombo":
            return {
                ...state, combo: action.payload,
            }
        case "clearImenu":
            return Object.assign({}, state, initState)
        default:
            return state;
    }
}

export default imenuReducer;