import React,{Component} from 'react'
import {
  AppRegistry,View,Text
} from 'react-native';
import {Button} from 'native-base'
import {connect} from 'react-redux'
import {push, pop} from '../actions/navActions'
import api from '../api'
import dataService from '../api/dataService'
import {
  Analytics,Hits
} from 'react-native-google-analytics';
import Camera from 'react-native-camera';
import i18 from './i18'
let flag = true
let code = ''
class BarcodeScannerApp extends Component {
  constructor(props) {
    super(props);

    this.state = {
      torchMode: 'off',
      cameraType: 'back',
      code:'',
      type:''
    };
  }

  barcodeReceived(e) {
    if(this.props.navState.routes[this.props.navState.index].key != 'QRscancode')return 
    if(code==e.data) return
    code = e.data
    if(code =='') return
    if(flag){
      flag=false
      let route = this.props.navState.routes[this.props.navState.index-1]
      route = Object.assign({},route,{
        code:code
      })
        api.pop()
        api.pop()
        api.push(route)
        setTimeout(()=>{
          code = ''
          flag = true
        },1000)
    }
  }

  render() {
    let detectSize = api.getRealDeviceWidth()*0.9
      if(this.props.userState.uuid!== undefined){
            ga = new Analytics('UA-90479808-1',this.props.userState.uuid, 1, 'userAgent');
            var screenView = new Hits.ScreenView('Mpoint','QRscan')
            ga.send(screenView);
        }
    return (
        <Camera style={{ flex:1 }} 
          onBarCodeRead={this.barcodeReceived.bind(this)}
          defaultOnFocusComponent={true}
         >
          <View style={{ backgroundColor:'#009688',paddingBottom:10,paddingTop:10}}>
            <Text style={{ color:'#fff',textAlign:'center' }}>{i18.t('scanQrTitle')}</Text>
          </View>
          <View style={{ height:detectSize,width:detectSize,borderColor:'#fff',borderWidth:1,alignSelf:'center',marginTop:((api.getRealDeviceHeight()-56)-detectSize)/2 }}>

          </View>
          <View style={{ padding:10 ,bottom:1,position:'absolute',width:api.getRealDeviceWidth()}}>
            <Button style={{ borderRadius:20,paddingRight:30,paddingLeft:30,backgroundColor:'#fa6428',alignSelf:'center' }} onPress={()=>this.props.pop()}>{i18.t('close')}</Button>
        </View>
        </Camera>
        
    );
  }
}
mapStateToProps = (state) => ({navState: state.navState, userState: state.userState})

mapDispatchToProps = (dispatch) => ({
    pop: () => dispatch(pop()),
    push: (route) => dispatch(push(route)),
    
})
export default connect(mapStateToProps, mapDispatchToProps)(BarcodeScannerApp)