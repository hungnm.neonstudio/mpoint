import React, { Component } from 'react'
import {
    Header,
    Container,
    Content,
    Button,
    Title,
    Icon,
    List,
    Card,
    Col,
    Row,
    Input,
    InputGroup
} from 'native-base'
import i18 from '../i18'

import dataService from '../../api/dataService'
import { View, Text, ActivityIndicator, Image, ListView, Platform } from 'react-native'
import { connect } from 'react-redux'
import api from '../../api'
import Reply from './replies'
let num_load = 10
let userInfo = {}
class Comment extends Component {
    constructor(props) {
        super(props)
        this.state = ({
            data: [],
            isLoading: true,
            canLoadMore: true,
            start: 0,
            currentComment: '',
            isSendding: false
        })
        setTimeout(() => {
            this._loadMore()
        }, 1000)

        // get userInfo
        let user = this.props.userState
        this.userInfo = {
            user_name: user.data.endUser.name,
            avatar: user.data.endUser.avatar
        }
    }
    componentDidMount() {
        setTimeout(() => {
            if (this.state.data.length < 1) {
                api.showMessage(i18.t('timeoutTextMsgContentLogin'), i18.t('titleMsgLogin'))
            }
        }, 10000)
    }
    _comment = () => {
        let com = {
            // user_name: this.userInfo.user_name, content: this.state.currentComment,
            // avatar: this.userInfo.avatar, create_time: new Date()
            content: this.state.currentComment,
            createdAt: new Date(),
            endUser: {
                avatar: this.userInfo.avatar,
                name: this.userInfo.user_name
            }
        }

        if (this.state.currentComment !== '' && !this.state.isSendding) {
            this.setState({ isSendding: true })
            return (dataService.sendComment(this.props.userState.token, this.props.navState.routes[this.props.navState.index].id, this.state.currentComment).then((data) => {
                api.showMessage(data.msg, i18.t('titleMsgLogin'))
                // alert(data.msg)
                this.setState({
                    data: [
                        com, ...this.state.data
                    ],
                    currentComment: '',
                    isSendding: false
                })
            }))
        }
    }
    _loadMore() {
        if (this.state.canLoadMore)
            dataService.getComment(this.props.userState.token, this.props.navState.routes[this.props.navState.index].id, this.state.start, num_load).then((data) => {
                
                if (data.comments.length < num_load)
                    this.setState({ canLoadMore: false })
                this.setState({
                    data: this.state.data.concat(data.comments),
                    isLoading: false,
                    start: this.state.start + data.comments.length
                })
            })
    }
    _lessThanTen(t) {
        return t < 10
            ? '0' + t
            : t
    }
    _DateTime(t) {
        let d = new Date(t)
        return (
            <Text style={{
                fontSize: 12
            }}>{api.getTime(t, 'HH:mm DD/MM/YYYY')}</Text>
        )
    }
    _renderListComment() {
        console.log('====================================');
        console.log('comment Data',this.state.data);
        console.log('====================================');
        // console.log('data at _renderListComment', this.state.data);
        // console.error('err', this.state.data);
        const ds = new ListView.DataSource({
            rowHasChanged: (r1, r2) => r1 !== r2
        });
        if (this.state.isLoading) {
            return (<ActivityIndicator size='large' />);
        } else if (this.state.data.length > 0) {
            console.log('data at comment', this.state.data);
            return (
                <Card
                        style={{ marginBottom:50 }}>
                    <ListView
                        dataSource={ds.cloneWithRows(this.state.data)}
                        onEndReached
                        ={() => this._loadMore()}
                        renderRow={(item, i) => {
                            return (
                                <View style={{ padding:5,paddingBottom:0 }}>
                                    <View>
                                        <View style={{ flexDirection:'row' }}>
                                            <Image
                                                source={{
                                                    uri: item.endUser.avatar
                                                }}
                                                style={{
                                                    width: api.getRealDeviceWidth() / 12,
                                                    height: api.getRealDeviceWidth() / 12,
                                                    borderRadius: api.getRealDeviceWidth() / 24
                                                }} />
                                                <View style={{ marginLeft:10,flex:1 }}>
                                                    <View style={{ flexDirection:'row' }}>
                                                            <Text
                                                                style={{
                                                                    fontWeight: 'bold',
                                                                    fontSize: 13,
                                                                    flex:1
                                                                }}>{item.endUser.name}
                                                            </Text>
                                                        {this._DateTime(item.createdAt)}
                                                    </View>
                                                    <Text
                                                        style={{
                                                            fontSize: 13
                                                        }}>{item.content}
                                                    </Text>
                                                </View>
                                            </View>
                                            <View style={{ marginTop:5,flex:1,height:1,backgroundColor:'#cdcdcd' }}/>
                                    </View>
                                    <Reply data={item.replies}/>
                                </View>
                            );
                        }}
                        renderFooter={() => {
                            if (this.state.canLoadMore) {
                                return (<ActivityIndicator size='large' />);
                            }
                        }} />
                </Card>
            )
        } else
            return (
                <Text>{i18.t('noneData')}</Text>
            )
    }
    render() {
        if (this.state !== null) {
            console.log(this.state)
            return (
                <Container>
                    <Header
                        style={{
                            backgroundColor: '#009688'
                        }}>
                        <Button transparent onPress={() => api.pop()}>
                            <Icon name='ios-arrow-back' style={{ color: Platform.OS == 'ios' ? '#000' : '#fff' }} />
                        </Button>
                        <Title>{i18.t('comment')}</Title>
                    </Header>

                    <Image
                        style={{ height: api.getRealDeviceHeight() - 56, width: api.getRealDeviceWidth() }}
                        source={require('../../img/bg_login.jpg')}
                    >
                        <Content style={{
                            padding: 10
                        }}>

                            <Card
                                style={{
                                    flexDirection: 'row',
                                    padding: 10
                                }}>
                                <Image
                                    source={{
                                        uri: this.userInfo.avatar
                                    }}
                                    style={{
                                        height: api.getRealDeviceWidth() / 8,
                                        width: api.getRealDeviceWidth() / 8,
                                        borderRadius: api.getRealDeviceWidth() / 16,
                                        maxHeight: 70,
                                        maxWidth: 70
                                    }} />
                                <Input
                                    placeholderTextColor='gray'
                                    placeholder={i18.t('addComment')}
                                    value={this.state.currentComment}
                                    onChangeText={(t) => this.setState({ currentComment: t })}
                                    style={{
                                        borderWidth: 1,
                                        borderColor: '#cdcdcd',
                                        marginLeft: 10,
                                        borderRadius: 2
                                    }} />
                                <Button transparent onPress={() => this._comment()}>
                                    <Icon name="ios-paper-plane-outline" />
                                </Button>
                            </Card>
                            {this._renderListComment()}
                        </Content>
                    </Image>
                </Container>
            )
        } else
            return (
                <Container>
                    <Header
                        style={{
                            backgroundColor: '#009688'
                        }}>
                        <Button transparent onPress={this.props.pop}>
                            <Icon name='ios-arrow-back' style={{ color: Platform.OS == 'ios' ? '#000' : '#fff' }} />
                        </Button>
                        <Title>{i18.t('comment')}</Title>
                    </Header>

                    <Content>
                        <ActivityIndicator size='large' />
                    </Content>
                </Container>
            )
    }
}

mapStateToProps = (state) => ({ navState: state.navState, userState: state.userState })
mapDispatchToProps = (dispatch) => ({})

export default connect(mapStateToProps, mapDispatchToProps)(Comment)