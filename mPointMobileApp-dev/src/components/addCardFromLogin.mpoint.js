import React, {Component} from 'react';
import IconQR from 'react-native-vector-icons/FontAwesome';
import {View, Image, Text, ScrollView,Platform,Keyboard,AsyncStorage,PermissionsAndroid} from 'react-native';
import {
    Header,
    Title,
    Container,
    Content,
    Button,
    Icon,
    Input,
    InputGroup
} from 'native-base';
import api from '../api';
import dataService from '../api/dataService';
import DeviceInfo  from 'react-native-device-info'
import {connect} from 'react-redux';
import PopupDialog,{ SlideAnimation,DialogTitle } from 'react-native-popup-dialog';
import {setUserInfo, setUserToken, setUuid} from '../actions/userActions'
import i18 from './i18'
let phone = ''
let otpID = ''
let OTP  = ''
let cardID = ''
let check =''
let _showConfirm=(msg)=>{
    api.hideLoading()
    api.showConfirm(msg!=undefined && msg!=''?msg:i18.t('confirmTextLogin'),i18.t('confirmTitleLogin'),i18.t('confirmOKLogin'),i18.t('confirmCancelTextLogin'),
        function onok(){
            api.push({key:'RegUser'})
            api.hideConfirm()
        },
        function oncancel(){
            api.showLoading()
            api.resetRoute({key:'home'})
            api.hideConfirm()
        }
    )
}
let setGiftCount = (token)=>{
    dataService.getGiftCount(token)
    .then((rs)=>{
            api.setGitfCount(rs.err==0?rs.amount:'0')
    })
}
class AddCardFromLogin extends Component {
    constructor(){
        super()
        this.state={
            showinput:true,
            isloading:false,
            card:'',
            data:{
                introduce:'',
                howuse:'',
                object:''
            }
        }
    }
    componentWillUnmount(){
        phone = ''
        otpID = ''
        OTP  = ''
        cardID = ''
        check = ''
    }
    componentWillMount(){
        
        if(this.props.navState.routes[this.props.navState.index].phone!=undefined)
        {
            phone = this.props.navState.routes[this.props.navState.index].phone
            this.setState({phone:phone})
        }
    }
    componentDidMount(){
        if(this.props.navState.routes[this.props.navState.index].item!=undefined)
        this.setState({data:this.props.navState.routes[this.props.navState.index].item})
        console.log(this.state.data)
    }

    _checkPhoneViettel(){
        let startNumber = phone.substring(0,4)
        // alert(startNumber)
        if( startNumber == '0163' || startNumber == '0164' || startNumber == '0165' || startNumber == '0166' || startNumber == '0167' ||startNumber == '0168' ||startNumber == '0169') return true
        startNumber = phone.substring(0,3)
        //  alert(startNumber)
         if( startNumber == '097' || startNumber == '096' || startNumber == '098' || startNumber == '095') return true
         return false
    }
    _onpress(){
        Keyboard.dismiss()
        if(phone == '' ||  !api.validatePhone(phone)  || !this._checkPhoneViettel() )
        {
            api.showMessage(i18.t('blankPhone'),i18.t('titleMsgLogin'))
            return
        }
        this.setState({isloading:true,phone:phone})
        api.showLoading()
        dataService.getUserToken(cardID,DeviceInfo.getUniqueID())
        .then(rs=>{
            this.setState({isloading:false})
            if(rs.err == 4){
                otpID = rs.otpId
                //dang nhap thanh cong => otp=>token=>postRegisterFreeCard=>success=>getuserInfo=>promotion
                this.DialogOTP.openDialog();
            }
            else if(rs.err == 0){
                //dang nhap thanh cong => otp=>token=>postRegisterFreeCard=>success=>getuserInfo=>promotion
                // dataService.postRegisterFreeCard(123,rs.token)
                // .then(a=>{})
                api.setUserToken(rs.token)
                dataService.getListShopGPS(rs.token)
                                                .then(gps=>{
                                                    api.setLocationData(gps.shops)
                                                })
                setGiftCount(rs.token)
                    dataService
                        .postAddDevice(DeviceInfo.getUniqueID(), DeviceInfo.getUniqueID(), rs.token)
                        .then(rs => {})
                                            dataService.postAddCard(cardID,rs.token)
                        .then(data=>{
                            if(data.err == 0 ){
                                try {
                            AsyncStorage.setItem('@RFTK:key', rs.refreshToken);
                            // alert(dt.refreshToken)
                        } catch (error) {
                            console.log('Error when saving data', error);
                        }
                                dataService
                            .getUserInfo(rs.token)
                            .then((datas) => {
                                console.log('datas', datas);
                                if (datas.err == 0) {

                                    this
                                        .props
                                        .setUserInfo(datas)
                                    api.resetRoute({key: 'home'})
                                }
                                // api.hideLoading()
                            })
                        }
                        else{
                            api.hideLoading()
                        }
                        api.showMessage(data.msg,i18.t('titleMsgLogin'))
                 })
            }
             else if(rs.err !== 2){
                //dang nhap thanh cong => otp=>token=>postRegisterFreeCard=>success=>getuserInfo=>promotion
                this._reg()
            }
            else if(rs.err !== 4){
                //dang nhap thanh cong => otp=>token=>postRegisterFreeCard=>success=>getuserInfo=>promotion
                otpID = rs.otpId
            }
            else {
                //loi => register => otp => token=>postRegisterFreeCard
                api.showMessage(i18.t('invalidPhone'),i18.t('titleMsgLogin'))
            }
            api.hideLoading()
        })
        setTimeout(()=>{
            if(this.state.isloading)
           { api.hideLoading()
            api.showMessage(i18.t('timeoutTextMsgContentLogin'),i18.t('titleMsgLogin'))}
        },15000)
        // this.popupDialog.openDialog();
    }
    _onInputPhoneOK = () =>{
        this.setState({phone:phone})
        this.popupDialog.closeDialog();
        this.DialogOTP.openDialog();
    }
     _onconfirmOTP(){
        if(OTP != '')
        {
         this.regDialogOTP.closeDialog()
        api.showLoading()
        dataService.postVerifyOtpLogin(otpID,OTP,DeviceInfo.getUniqueID())
        .then(rs=>{
                if (rs.err == 0) {
                    api.setUserToken(rs.token)
                    dataService.getListShopGPS(rs.token)
                                                .then(gps=>{
                                                    api.setLocationData(gps.shops)
                                                })
                    setGiftCount(rs.token)
                    dataService
                        .postAddDevice(DeviceInfo.getUniqueID(), DeviceInfo.getUniqueID(), rs.token)
                        .then(rs => {})
                        dataService.postRegisterFreeCard(123,rs.token)
                        .then(data=>{
                            if(data.err == 0 || data.err == 2){
                                try {
                            AsyncStorage.setItem('@RFTK:key', rs.refreshToken);
                            // alert(dt.refreshToken)
                        } catch (error) {
                            console.log('Error when saving data', error);
                        }
                                dataService
                            .getUserInfo(rs.token)
                            .then((datas) => {
                                console.log('datas', datas);
                                if (datas.err == 0) {

                                    this
                                        .props
                                        .setUserInfo(datas)
                                    api.resetRoute({key: 'home'})
                                }
                                // api.hideLoading()
                            })
                        }
                        else{
                            api.hideLoading()
                        }
                        // api.showMessage(data.msg,i18.t('titleMsgLogin'))
                        })
                    //setTimeout(this._gotoPromotion(), 1000);

                } 
               else{
                   this.regDialogOTP.openDialog()
                  api.hideLoading()
                  api.showMessage(rs.msg,i18.t('titleMsgLogin'))  
               }
        })
    }
        else{
            api.showMessage(i18.t('errOtpMsgLogin'),i18.t('titleMsgLogin'))
        }
    }

_onconfirmRegOTP(){
        if(OTP != '')
        {
        api.showLoading()
        dataService.postVerifyOtpRegister(phone,OTP,otpID)
        .then((rs)=>{
            // api.hideInputBox()
                if(rs.err == 0)
                {
                    // api.hideInputBox()
                    api.setUserToken(rs.token)
                    dataService.getListShopGPS(rs.token)
                                                .then(gps=>{
                                                    api.setLocationData(gps.shops)
                                                })
                    setGiftCount(rs.token)
                    dataService.postRegisterFreeCard(354,rs.token)
                .then(kq=>{
                   
                    if(kq.err == 0){
                        dataService
                            .getUserInfo(rs.token)
                            .then((datas) => {
                                console.log('datas', datas);
                                if (datas.err == 0) {
                                    dataService.postAddDevice(DeviceInfo.getUniqueID(),DeviceInfo.getUniqueID(),rs.token)
                                    .then(rs1=>{

                                    })
                                    api.setUserInfo(datas)
                                     _showConfirm(kq.msg.replace('bạn có thể mở thẻ để sử dụng hoặc cập nhật lại thông tin của bạn','quý khách vui lòng nhập chính xác thông tin liên hệ để được chăm sóc và nhận quà tặng'))
                                }
                            })
                    }else{
                        // api.showMessage(kq.msg,i18.t('titleMsgLogin'))
                        api.hideLoading()
                    }
                })
                    // dataService.changeCard(rs.token,526)
                    // .then(data=>{
                        
                    // })
                    try {
                                     AsyncStorage.setItem('@RFTK:key', rs.refreshToken);
                                       // alert(dt.refreshToken)
                                         } catch (error) {
                                      console.log('Error when saving data', error);
                                      }
                    // api.setUserToken('125064b0-12bc-4a38-a03f-717156e4b6e4')
                    
                    // dataService.postRegisterFreeCard(123,rs.token)
                    // // dataService.postRegisterFreeCard(350,'44046d79-a01c-46aa-95e9-4632cd65ac74')
                    // .then(kq=>{
                        
                        // if(kq.err == 0){
                            // alert('123')
                            // api.resetRoute({key:'login'})
                            // api.push({key:'home'})

        //  dataService.postRegisterFreeCard(123,rs.token)
        // .then(rsx=>{
        //     api.hideLoading()
            // api.showMessage(rsx.msg,i18.t('titleMsgLogin'))
           
        // })
                            
                        // }
                    //     api.showMessage(kq.msg,i18.t('titleMsgLogin'))
                    //     api.hideLoading()
                    // })
                }
                else {
                    api.showMessage(rs.msg,i18.t('titleMsgLogin'))
                    api.hideLoading()
                }   
                api.hideLoading()
        })
    }
        else{
            api.showMessage(i18.t('errOtpMsgLogin'),i18.t('titleMsgLogin'))
        }
    }
    _reg(){
       
         api.showLoading()
            dataService.postReg(phone)
            .then(rs=>{
                if(rs.err ==0 ){
                    otpID = rs.otpId
                    //alert(rs.otpId)
                    // api.showInputBox('Nhập OTP','Nhập OTP','',this._onConfirmOTP.bind(this))
                    // api.setRegInfo('781160c2-f543-4ec8-8f66-b425a38af40c')
                    this.popupDialog.closeDialog();
                    this.regDialogOTP.openDialog();
                    }
                    else{
                        api.showMessage(rs.msg,i18.t('titleMsgLogin'))
                    }
                    setTimeout(()=>{
                        api.hideLoading()
                    },1)
                })
    }
      _checkCamPer(){
                if (Platform.OS === 'android' && Platform.Version >= 23)
        PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.CAMERA).then((result) => {
                if (result) {
                  console.log("Permission is OK");
                  camPer = true
                  api.push({key:'QRscancode'})
                } else {
                  PermissionsAndroid.requestPermission(PermissionsAndroid.PERMISSIONS.CAMERA).then((result) => {
                    if (result) {
                      console.log("User accept");
                       api.push({key:'QRscancode'})
                    } else {
                      console.log("User refuse");
                      api.showMessage(i18.t('errCamPer'),i18.t('titleMsgLogin'))
                    }
                  });
                }
          });
          else  api.push({key:'QRscancode'})
        }
    _checkLogin = async() => {
        this.setState({phone:phone})
        if(phone != '' && api.validatePhone(phone))
        {
            this.setState({isloading:true})
            api.showLoading()
        dataService.getUserToken(phone,DeviceInfo.getUniqueID())
        .then(rs=>{
            this.setState({isloading:false})
              if (rs.err == 0) {
                        
                        api.setUserToken(rs.token)
                        dataService.getListShopGPS(rs.token)
                                                .then(gps=>{
                                                    api.setLocationData(gps.shops)
                                                })
                        setGiftCount(rs.token)
                        dataService.postRegisterFreeCard(354,rs.token)
                        .then(data=>{
                            if(data.err == 0 || data.err == 2){
                                try {
                            AsyncStorage.setItem('@RFTK:key', rs.refreshToken);
                            // alert(dt.refreshToken)
                        } catch (error) {
                            console.log('Error when saving data', error);
                        }
                                dataService
                            .getUserInfo(rs.token)
                            .then((datas) => {
                                console.log('datas', datas);
                                if (datas.err == 0) {

                                    this
                                        .props
                                        .setUserInfo(datas)
                                    api.resetRoute({key: 'home'})
                                }
                                // api.hideLoading()
                            })
                        }
                        else{
                            api.hideLoading()
                        }
                        // api.showMessage(data.msg,i18.t('titleMsgLogin'))
                        })
                        
                        // setTimeout(this._gotoPromotion(), 1000);

                    }
            else if (rs.err == 4) {
                        otpID = rs.otpId
                        api.hideLoading()
                        // alert(otpID)
                        this._onInputPhoneOK()
                    } 
            else if(rs.err == 2){
               this._reg()
            }
            else{
                api.hideLoading()
                api.showMessage(rs.msg,i18.t('titleMsgLogin'))
            }
        }
        
        )
        setTimeout(()=>{
           if(this.state.isloading){
                api.hideLoading()
            api.showMessage(i18.t('timeoutTextMsgContentLogin'),i18.t('titleMsgLogin'))
           }
        },15000)
}
        else{
            api.showMessage(i18.t('blankPhone'),i18.t('titleMsgLogin'))
        }
    }

    _renderIntroduce(arr){
        
        return arr.length ==1 && arr[0]==''? <Text  style={{ color:'#fff' }}>
                 <Text style={{ color:'#2FCC77' }}>•</Text> {i18.t('updating')}.
            </Text>: arr.map((item,id)=>(
            item==''?null:<Text key={id}  style={{ color:'#fff' }}>
                 <Text style={{ color:'#fff' }}>•</Text> {item.replace('-','')}.
            </Text>
        ))
    }
    _renderHowUse(arr){
        return (
            <View
                style={{ padding:10,backgroundColor:'rgba(20,20,20,.5)',borderBottomRightRadius:5,borderTopRightRadius:5,borderBottomLeftRadius:5 }}
            >
                {arr.length == 1 && arr[0]==''? <Text  style={{ color:'#fff' }}>
                 <Text style={{ color:'#2FCC77' }}>•</Text> {i18.t('updating')}.
            </Text>:arr.map((item,id)=>(
        item==''?null:<Text  style={{ color:'#fff' }}>
             <Text key={id} style={{ color:'#fff' }}>»</Text> {item.replace('-','')}
        </Text>))}
            </View>
        )
    }
    _renderObject(arr){
        return (
            <View
                style={{ padding:10,backgroundColor:'rgba(20,20,20,.5)',borderBottomRightRadius:5,borderTopRightRadius:5,borderBottomLeftRadius:5 }}
            >
                {
                    arr.length == 1  && arr[0]==''? <Text  style={{ color:'#fff' }}>
                 <Text style={{ color:'#2FCC77' }}>•</Text> {i18.t('updating')}.
            </Text>:arr.map((item,id)=>(
            item==''?null:<Text  style={{ color:'#fff' }}>
             <Text key={id} style={{ color:'#fff' }}>»</Text> {item.replace('-','')}
        </Text>
        ))
                }
            </View>
        )
    }
    componentDidUpdate(){
        if(this.props.navState.routes[this.props.navState.index].code!=undefined && check!=this.props.navState.routes[this.props.navState.index].code && phone !=this.props.navState.routes[this.props.navState.index].code)
        {this.setState({phone:this.props.navState.routes[this.props.navState.index].code})
        phone = this.props.navState.routes[this.props.navState.index].code
        check = this.props.navState.routes[this.props.navState.index].code
    }
    }
    render() {
        return (
            <Container>
                
                <Header
                    style={{
                    backgroundColor: '#009688',
                    zIndex:1000
                }}>
                    <Title style={{ alignSelf:'center' } }>
                        {i18.t('titleAddCardPage')}
                    </Title>
                    <Button transparent onPress={() => api.pop()}>
                        <Icon name='ios-arrow-back' style={{ color:Platform.OS == 'ios'?'#000':'#fff' }}/>
                    </Button>
                </Header>
                    <Image
                        style={{ marginTop:-56,height:api.getRealDeviceHeight(),width:api.getRealDeviceWidth() }}
                        source={require('../img/bg_login.jpg')}
                    >
                    <ScrollView
                        keyboardShouldPersistTaps={'handled'}
                    >
                        <View
                        style={{
                            marginTop:66,
                            backgroundColor: 'white',
                            borderRadius: 10,
                            width:api.getRealDeviceWidth()-20,
                            height:(api.getRealDeviceWidth-20)/1.57,
                            //resizeMode: 'stretch',
                            margin: 10
                    }}>
                    <View style={{
                            marginTop: 10,
                            borderColor:'#cdcdcd',
                            borderBottomWidth:1,
                        }}>
                        <View style={{ flexDirection:'row-reverse',paddingHorizontal: 10, }}>
                            
                            <Image style={{ width:api.getRealDeviceWidth()/4,height:api.getRealDeviceWidth()/(4*1.57),marginLeft:10,marginBottom:10,right:0,borderRadius:2 }} source ={{ uri:this.state.data.card }} />
                            <Text style={{textAlign: 'center', fontSize: Platform.OS == 'ios'?11:api.getRealDeviceWidth()>320?13:12,fontWeight:'400',textAlignVertical:'center',color:'#000',marginTop:Platform.OS == 'ios'?10:0}}>{i18.t('leftHeadAddCardPage')} {'\n'}{i18.t('rightHeadAddCardPage')}</Text>
                        </View>
                    </View>
                    <View
                            style={{ margin:5,backgroundColor:'rgba(255, 255, 255, 1)',
                            width:api.getRealDeviceWidth()-40,
                            alignSelf:'center',
                            borderRadius:5,
                            paddingVertical:5
                             }}
                        >
                            <View style={{ flexDirection:'row' }}> 
                                <View style={{ flex:7 }}>
                                   
                            <Input
                                returnKeyLabel='TIẾP TỤC'
                                onSubmitEditing={()=>{ this._checkLogin() }}
                             keyboardType='phone-pad'
                             keyboardType='phone-pad'
                             placeholder={i18.t('inputPlaceholderSDT')} onChangeText={(t)=>{phone = t;}}  defaultValue = {phone}
                             style={{
                                backgroundColor: '#fff',
                                borderColor:'#cdcdcd',
                                borderWidth:1,
                            }}
                            />
                        <Button block onPress={this._checkLogin.bind(this)}
                            style={{ backgroundColor:'#fa6428',marginTop:5 }}
                        >
                                {(i18.t('continue')).toUpperCase()}
                            </Button>
                                </View> 
                                <View style={{ flex:3,marginTop:15}} onTouchEndCapture={()=>{
                                    this._checkCamPer()
                                    }}>
                                   <View style={{ alignSelf:'center',borderColor:'#000',borderWidth:1,borderRadius:21,paddingHorizontal:Platform.OS == 'ios'?8 :6,paddingVertical:Platform.OS =='ios'?5 :1 }}>
                                        <IconQR name="qrcode" size={Platform.OS =='ios'?30: 40} color="#000"/>
                                   </View>
                                    <Text  style={{ color:'#000',textAlign:'center' }}>{i18.t('scantitle')}</Text>
                                </View>
                            </View>
                        </View>
                    </View>
                    
                    {/*<ScrollView
                        style={{
                        height: 1,
                        backgroundColor: 'gray',
                        marginRight: 10,
                        marginLeft: 10,minHeight:api.getRealDeviceHeight()
                    }}></ScrollView>*/}
                    <View style={{
                        flex: 1,
                        minHeight:api.getRealDeviceHeight()/2,
                        padding:10,
                        paddingBottom:70
                    }}>
                        {/*<Text
                            style={{
                            margin: 10
                        }}>
                            {detail}
                        </Text>*/}
                        <View
                style={{ padding:5,backgroundColor:'rgba(20,20,20,.5)',borderBottomRightRadius:5,borderTopRightRadius:5,borderBottomLeftRadius:5 }}
            >
                        <Text style={{ fontSize:Platform.OS=='ios'?15:20,fontWeight:'400',color:'#fff',textAlign:'center'}}>
                            {i18.t('bodyAddcardPage')} {(this.state.data.name!=undefined?this.state.data.name:'').toUpperCase()}
                        </Text>
                        </View>
                        <View style={{ flexDirection:'row',marginTop:10 }}>
                                <Image source={ require('../img/icon/lienhe.png') } style={{ width:20*4.47,height:20,resizeMode:'stretch' }}
                                >
                                <Text style={{ color:'#fff',fontSize:13,marginLeft:5,textAlignVertical:'center',backgroundColor:'transparent' }}>{i18.t('intro')}</Text>
                                </Image>
                        </View>
                        <View
                style={{ padding:10,backgroundColor:'rgba(20,20,20,.5)',borderBottomRightRadius:5,borderTopRightRadius:5,borderBottomLeftRadius:5 }}
            >
                        <Text   style={{ color:'#fff' }}>{this.state.data.introduce!=''?this.state.data.introduce:'- '+i18.t('updating')}</Text>
                        </View>
                        {/*{this._renderIntroduce(this.state.data.introduce.split('\n'))}*/}
                        {/*<Text style={{ fontSize:18,color:'#2FCC77',fontWeight:'300' }}>
                            Hướng dẫn sử dụng
                        </Text>*/}
                        <View style={{ flexDirection:'row',marginTop:10 }}>
                                <Image source={ require('../img/icon/nen.png') } style={{ width:20*7,height:20,resizeMode:'stretch' }}
                                >
                                <Text style={{ color:'#fff',fontSize:13,marginLeft:5,textAlignVertical:'center',backgroundColor:'transparent'  }}>{i18.t('instruction')}</Text>
                                </Image>
                        </View>
                        <View
                style={{ padding:10,backgroundColor:'rgba(20,20,20,.5)',borderBottomRightRadius:5,borderTopRightRadius:5,borderBottomLeftRadius:5 }}
            >
                        <Text style={{ color:'#fff' }}>{this.state.data.howuse!=''?this.state.data.howuse:'- '+i18.t('updating')}</Text>
                        </View>
                        <Image source={ require('../img/icon/lienhe.png') } style={{  width:20*4.47,height:20,resizeMode:'stretch',marginTop:10 }}
                                >
                        <Text style={{ marginTop:Platform.OS == 'ios'?3:0 ,marginLeft:5,color:'#fff', fontSize: 13,backgroundColor:'transparent'}}>
                            {i18.t('object')}
                        </Text>
                        </Image>
                        <View
                style={{ padding:10,backgroundColor:'rgba(20,20,20,.5)',borderBottomRightRadius:5,borderTopRightRadius:5,borderBottomLeftRadius:5 }}
            >
                        <Text style={{ color:'#fff' }}>{this.state.data.object!=''?this.state.data.object:'- '+i18.t('updating')}</Text>
                    </View>
                    </View>
                    </ScrollView>
                     {/*// otp reg POP up */}
                                         <PopupDialog
                                         width={api.getRealDeviceWidth()*0.9}
                                         height={230}
                                         dismissOnTouchOutside={false}
                                         closeOnHardwareBackPress={false}
        dialogTitle={<DialogTitle title={i18.t('otpTitle')} titleStyle={{ padding:3 }} titleTextStyle={{ margin:0 }} />}
    ref={(regDialogOTP) => { this.regDialogOTP = regDialogOTP; }}
    dialogAnimation = { new SlideAnimation({ slideFrom: 'bottom' }) }
  >
        <View>
        <Text style={{ textAlign:'center',fontSize:14 }}>{i18.t('contentLeftInputLogin')}   {this.state.phone}, {i18.t('contentRightInputLogin')} </Text>
      <InputGroup style={{ borderColor:'#cdcdcd',borderWidth:1,marginHorizontal:5 }}><Input placeholder='Mã OTP'  keyboardType='phone-pad'
       onChangeText={t=>OTP = t}/></InputGroup>
    <View style={{ flexDirection:'row',marginTop:10,padding:10 }}>
        <Button block style={{ flex:1,backgroundColor:'#cdcdcd' }} onPress={()=>this.regDialogOTP.closeDialog()}> {i18.t('Cancel')}</Button>
        <Button block style={{ flex:1,marginLeft:10,backgroundColor:'#fa6428' }} onPress={this._onconfirmRegOTP.bind(this)}>{i18.t('continue')}</Button>
    </View>
    </View>
  </PopupDialog>
                     <PopupDialog
                     width={api.getRealDeviceWidth()*0.9}
                     height={230}
                     dismissOnTouchOutside={false}
                     closeOnHardwareBackPress={false}
        dialogTitle={<DialogTitle title={i18.t('addPhone')} titleStyle={{ padding:3 }} titleTextStyle={{ margin:0 }}/>}
    ref={(popupDialog) => { this.popupDialog = popupDialog; }}
    dialogAnimation = { new SlideAnimation({ slideFrom: 'bottom' }) }
    style={{ backgroundColor:'red' }}
  >
      <InputGroup><Input placeholder={i18.t('inputPlaceholderSDT')}   keyboardType='phone-pad' 
      onChangeText={t=>{phone = t;this.setState({phone:t})}}
      defaultValue={phone}
      /></InputGroup>
    <View style={{ flexDirection:'row',marginTop:10,padding:10 }}>
        <Button block style={{ flex:1,backgroundColor:'#cdcdcd' }}  onPress={()=>this.popupDialog.closeDialog()}> {i18.t('Cancel')}</Button>
        <Button block style={{ flex:1,marginLeft:10,backgroundColor:'#fa6428' }} onPress={this._checkLogin}>{i18.t('continue')}</Button>
    </View>
  </PopupDialog>
                    {/*// otp POP up */}
                                         <PopupDialog
                                         width={api.getRealDeviceWidth()*0.9}
                                         height={230}
                                         dismissOnTouchOutside={false}
                                         closeOnHardwareBackPress={false}
        dialogTitle={<DialogTitle title={i18.t('otpTitle')} titleStyle={{ padding:3 }} titleTextStyle={{ margin:0 }}/>}
    ref={(DialogOTP) => { this.DialogOTP = DialogOTP; }}
    dialogAnimation = { new SlideAnimation({ slideFrom: 'bottom' }) }
  >
       <View style={{ paddingBottom:20 }}>
            <Text style={{ textAlign:'center' }}>{i18.t('contentLeftInputLogin')}    {this.state.phone}, {i18.t('contentRightInputLogin')} </Text>
      <InputGroup><Input placeholder={i18.t('inputPlaceholderLogin_checkLogin')}  keyboardType='phone-pad'
      
       onChangeText={t=>OTP = t}/></InputGroup>
    <View style={{ flexDirection:'row',marginTop:10,padding:10 }}>
        <Button block style={{ flex:1,backgroundColor:'#cdcdcd' }} onPress={()=>this.DialogOTP.closeDialog()}> {i18.t('Cancel')}</Button>
        <Button block style={{ flex:1,marginLeft:10,backgroundColor:'#fa6428' }} onPress={this._onconfirmOTP.bind(this)}>{i18.t('continue')}</Button>
    </View>
       </View>
  </PopupDialog>
                </Image>
            </Container>
        );
    }
}
const mapState = (state) => {
    return {navState: state.navState,userState: state.userState}
}
const mapDispatch = (dispatch) => {
    return {
        setUserInfo: (data) => dispatch(setUserInfo(data)),
    }
}
export default connect(mapState, mapDispatch)(AddCardFromLogin);