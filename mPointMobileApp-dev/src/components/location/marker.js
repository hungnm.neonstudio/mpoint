import { Marker, Callout } from 'react-native-maps'
import React, { PureComponent } from 'react'
import { Image, View, Text } from 'react-native'
import api from '../../api'
export default class MK extends PureComponent {
    getDate(datetime) {
        return api.getTime(new Date(datetime), 'HH[h]mm DD-MM-YYYY')
    }
    render() {
        let { marker, selectLocation } = this.props
        return <Marker
            coordinate={{
                latitude: marker.latitude,
                longitude: marker.longitude
            }}
            pinColor='red'
            onCalloutPress={() => {
                //alert('Select shop' + marker.partner.id);
                api.push({ key: 'detail', id: marker.partner.Promotions[0].id })
            }}
            onPress={() => {
                selectLocation(marker.latitude + ',' + marker.longitude);
            }}>
            <View>
                <Image
                    style={{ height: 48, width: 30, paddingTop: 3, alignItems: 'center' }}
                    source={require('../../img/marker.png')}
                >
                    <Image
                        style={{ height: 24, width: 24, zIndex: 1, borderRadius: 12 }}
                        source={{ uri: marker.partner ? marker.partner.logo ? (marker.partner.logo).replace('Uploads', 'Thumbnails') : 'i' : 'i' }}
                    />
                </Image>
            </View>

            <Callout
                style={{
                    width: api.getRealDeviceWidth() / 1.5, zIndex: 1000
                }}
                onPress={() => {
                    api.push({ key: 'detail', id: marker.partner.Promotions[0].id })
                }}
            >
                <View
                    onTouchEndCapture={() => {
                        api.push({ key: 'detail', id: marker.partner.Promotions[0].id })
                    }
                    }>

                    <Text
                        style={{
                            padding: 2,
                            color: '#387ef5'
                        }}
                        onPress={() => {
                            api.push({ key: 'partner', partnerId: marker.partner.Promotions.id })
                        }}
                    >{(marker.partner != undefined || marker.partner.name != undefined ? marker.partner.name : '').toUpperCase()}</Text>
                    <Text
                        style={{
                            //fontSize: 15,
                            //fontWeight: 'bold',
                            padding: 2
                        }}>{marker.name != undefined ? marker.partner.Promotions[0].name : '...'}</Text>

                    <Text
                        style={{
                            padding: 2
                        }}>
                        {i18.t('here')} {marker.address != undefined ? marker.address : '...'}
                    </Text>
                    <Text style={{ padding: 2 }}>{i18.t('applyTo')}: {marker.partner != undefined && marker.partner.Promotions != undefined && marker.partner.Promotions[0].applyTo != undefined ? this.getDate(marker.partner.Promotions[0].applyTo) : i18.t('updating')}</Text>
                    <Text style={{ padding: 2, textDecorationLine: 'underline' }}>{i18.t('linkCallout')}</Text>
                </View>
            </Callout>
        </Marker>
    }
}