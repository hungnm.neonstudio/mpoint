import React, {Component} from 'react';
import {View, Image, Text, ScrollView,Platform,AsyncStorage,Keyboard,PermissionsAndroid} from 'react-native';
import {
    Header,
    Title,
    Container,
    Content,
    Button,
    Icon,
    Input,
    InputGroup
} from 'native-base';
import i18 from './i18'
import IconQR from 'react-native-vector-icons/FontAwesome';
import api from '../api';
import dataService from '../api/dataService';
import DeviceInfo  from 'react-native-device-info'
import {connect} from 'react-redux';
import PopupDialog,{ SlideAnimation,DialogTitle } from 'react-native-popup-dialog';
import {setUserInfo, setUserToken, setUuid} from '../actions/userActions'
let cardID = ''
let check = ''
class AddCardFromLogin extends Component {

    constructor(){
        super()
        this.state={
            showinput:true,
            phone:'',
            isloading:false,
            card:'',
            data:{
                introduce:'',
                howuse:'',
                object:''
            }
        }
    }
    componentDidMount(){
        if(this.props.navState.routes[this.props.navState.index].item!=undefined)
        this.setState({data:this.props.navState.routes[this.props.navState.index].item})
        console.log(this.state.data)
    }
     componentWillUnnount(){
       cardID = ''
       check = ''
    }
    componentDidUpdate(){
        if(this.props.navState.routes[this.props.navState.index].code!=undefined && check!=this.props.navState.routes[this.props.navState.index].code && this.state.card !=this.props.navState.routes[this.props.navState.index].code )
        {this.setState({card:this.props.navState.routes[this.props.navState.index].code})
        cardID = this.props.navState.routes[this.props.navState.index].code
        check = this.props.navState.routes[this.props.navState.index].code
     }
    }
    _addCard(){
        Keyboard.dismiss()
        if(this.state.card != ''){
            token = this.props.userState.token
            let isloading = true
            api.showLoading()
            dataService.postAddCard(this.state.card,token)
            .then(rs=>{
                if(rs.err == 0){
                    dataService.getUserInfo(token)
                    .then((kq)=>{
                        if(kq.err==0){
                            api.setUserInfo(kq)
                        }
                    })
                }
                api.hideLoading()
                api.showMessage(rs.msg.replace(', bạn có thể mở thẻ để sử dụng hoặc cập nhật lại thông tin của bạn',''),i18.t('titleMsgLogin'))
                isloading =  false
            })
            setTimeout(()=>{
            if(isloading)
           { api.hideLoading()
            api.showMessage(i18.t('timeoutTextMsgContentLogin'),i18.t('titleMsgLogin'))}
        },15000)
        }
        else{
            api.showMessage(i18.t('blankCard'),i18.t('titleMsgLogin'))
        }

    }
 _checkCamPer(){
                if (Platform.OS === 'android' && Platform.Version >= 23)
        PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.CAMERA).then((result) => {
                if (result) {
                  console.log("Permission is OK");
                  camPer = true
                  api.push({key:'QRscancode'})
                } else {
                  PermissionsAndroid.requestPermission(PermissionsAndroid.PERMISSIONS.CAMERA).then((result) => {
                    if (result) {
                      console.log("User accept");
                       api.push({key:'QRscancode'})
                    } else {
                      console.log("User refuse");
                      api.showMessage(i18.t('errCamPer'),i18.t('titleMsgLogin'))
                    }
                  });
                }
          });
          else  api.push({key:'QRscancode'})
        }
    _renderIntroduce(arr){
        
        return arr.length ==1 && arr[0]==''? <Text>
                 <Text style={{ color:'#2FCC77' }}>•</Text> {i18.t('updating')}.
            </Text>: arr.map((item,id)=>(
            item==''?null:<Text key={id}>
                 <Text style={{ color:'#2FCC77' }}>•</Text> {item.replace('-','')}.
            </Text>
        ))
    }
    _renderHowUse(arr){
        return(
            <View
                style={{ padding:10,backgroundColor:'rgba(20,20,20,.5)',borderBottomRightRadius:5,borderTopRightRadius:5,borderBottomLeftRadius:5 }}
            >
                {arr.length == 1 && arr[0]==''? <Text style={{ color:'#fff' }}>
                 <Text style={{ color:'#fff' }}>•</Text> {i18.t('updating')}.
            </Text>:arr.map((item,id)=>(
        item==''?null:<Text style={{ color:'#fff' }}>
             <Text key={id} style={{ color:'#fff' }}>»</Text> {item.replace('-','')}
        </Text>))}
            </View>
        )
    }
    _renderObject(arr){
        return (
            <View
                style={{ padding:10,backgroundColor:'rgba(20,20,20,.5)',borderBottomRightRadius:5,borderTopRightRadius:5,borderBottomLeftRadius:5 }}
            >
                {
                    arr.length == 1  && arr[0]==''? <Text style={{ color:'#fff' }}>
                 <Text style={{ color:'#fff' }}>•</Text> {i18.t('updating')}.
            </Text>:arr.map((item,id)=>(
            item==''?null:<Text style={{ color:'#fff' }}>
             <Text key={id} style={{ color:'#fff' }}>»</Text> {item.replace('-','')}
        </Text>
        ))
                }
            </View>
        )
    }
    render() {
        return (
            <Container>
                <Header
                    style={{
                    backgroundColor: '#009688'
                }}>
                    <Title style={{ alignSelf:'center' } }>
                        {i18.t('titleAddCardPage')}
                    </Title>
                    <Button transparent onPress={() => api.pop()}>
                        <Icon name='ios-arrow-back' style={{ color:Platform.OS == 'ios'?'#000':'#fff' }}/>
                    </Button>
                </Header>
                <Image
                        style={{ height:api.getRealDeviceHeight()-56,width:api.getRealDeviceWidth() }}
                        source={require('../img/bg_login.jpg')}
                    >
                <ScrollView
                    keyboardShouldPersistTaps={'handled'}
                >
                    <View
                        style={{
                            backgroundColor: 'white',
                            borderRadius: 10,
                            width:api.getRealDeviceWidth()-20,
                            height:(api.getRealDeviceWidth-20)/1.57,
                            //resizeMode: 'stretch',
                            margin: 10
                    }}>
                    <View style={{
                            marginTop: 10,
                            borderColor:'#cdcdcd',
                            borderBottomWidth:1,
                        }}>
                        <View style={{ flexDirection:'row-reverse',paddingHorizontal: 10, }}>
                            <Image style={{ width:api.getRealDeviceWidth()/4,height:api.getRealDeviceWidth()/(4*1.57),marginLeft:10,marginBottom:10,right:0,borderRadius:2 }} source ={{ uri:this.state.data.card }} />
                            <Text style={{textAlign: 'center', fontSize: Platform.OS == 'ios'?11:api.getRealDeviceWidth()>320?13:12,fontWeight:'400',textAlignVertical:'center',color:'#000',marginTop:Platform.OS == 'ios'?10:0}}>{i18.t('leftHeadAddCardPage')} {'\n'}{i18.t('rightHeadAddCardPage')}</Text>
                        </View>
                    </View>
                    <View
                            style={{ margin:5,backgroundColor:'rgba(255, 255, 255, 1)',
                            width:api.getRealDeviceWidth()-40,
                            alignSelf:'center',
                            borderRadius:5,
                            paddingVertical:5
                             }}
                        >
                            <View style={{ flexDirection:'row' }}> 
                                <View style={{ flex:7 }}>

                            <Input 
                            returnKeyLabel={i18.t('continue')}
                            onSubmitEditing={()=>{ this._addCard() }}
                            placeholder={i18.t('placeHolderInputCard')} onChangeText={(t)=>{this.setState({card:t})}}  defaultValue={this.state.card}
                              style={{
                                backgroundColor: '#fff',
                                borderColor:'#cdcdcd',
                                borderWidth:1,
                            }}
                            /> 
                        <Button block onPress={this._addCard.bind(this)}
                            style={{ backgroundColor:'#fa6428',marginTop:5 }}
                        >
                                {(i18.t('continue')).toUpperCase()}
                            </Button>
                                </View> 
                                <View style={{ flex:3,marginTop:15}} onTouchEndCapture={()=>{
                                    this._checkCamPer()
                                    }}>
                                   <View style={{ alignSelf:'center',borderColor:'#000',borderWidth:1,borderRadius:21,paddingHorizontal:Platform.OS == 'ios'?8 :6,paddingVertical:Platform.OS =='ios'?5 :1 }}>
                                        <IconQR name="qrcode" size={Platform.OS =='ios'?30: 40} color="#000"/>
                                   </View>
                                    <Text  style={{ color:'#000',textAlign:'center' }}>{i18.t('scantitle')}</Text>
                                </View>
                            </View>
                        </View>
                    </View>
                    <View style={{
                        flex: 1,
                        minHeight:api.getRealDeviceHeight()/2,
                        padding:10,
                        paddingBottom:70
                    }}>
                    <View
                style={{ padding:5,backgroundColor:'rgba(20,20,20,.5)',borderBottomRightRadius:5,borderTopRightRadius:5,borderBottomLeftRadius:5 }}
            >
                        <Text style={{ fontSize:Platform.OS=='ios'?18:20,fontWeight:'400',color:'#fff',textAlign:'center'}}>
                            {i18.t('bodyAddcardPage')} {(this.state.data.name!=undefined?this.state.data.name:'').toUpperCase()}
                        </Text>
                        </View>
                        {/*<Text style={{ fontSize:18,color:'#2FCC77',fontWeight:'300' }}>
                            Giới thiệu
                        </Text>*/}
                        <View style={{ flexDirection:'row',marginTop:10 }}>
                                <Image source={ require('../img/icon/lienhe.png') } style={{ width:20*4.47,height:20,resizeMode:'stretch' }}
                                >
                                <Text style={{ color:'#fff',fontSize:13,marginLeft:5,textAlignVertical:'center',backgroundColor:'transparent' }}>{i18.t('intro')}</Text>
                                </Image>
                        </View>
                        <View
                style={{ padding:10,backgroundColor:'rgba(20,20,20,.5)',borderBottomRightRadius:5,borderTopRightRadius:5,borderBottomLeftRadius:5 }}
            >
                        <Text style={{ color:'#fff' }}>{this.state.data.introduce!=''?this.state.data.introduce:'- '+i18.t('updating')}</Text>
                        </View>
                        {/*<Text style={{ fontSize:18,color:'#2FCC77',fontWeight:'300' }}>
                            Hướng dẫn sử dụng
                        </Text>*/}
                        <View style={{ flexDirection:'row',marginTop:10 }}>
                                <Image source={ require('../img/icon/nen.png') } style={{ width:20*7,height:20,resizeMode:'stretch' }}>
                                <Text style={{ color:'#fff',fontSize:13,marginLeft:5,textAlignVertical:'center',backgroundColor:'transparent'  }}>{i18.t('instruction')}</Text>
                                </Image></View>
                        <View
                style={{ padding:10,backgroundColor:'rgba(20,20,20,.5)',borderBottomRightRadius:5,borderTopRightRadius:5,borderBottomLeftRadius:5 }}
            >
                        <Text style={{ color:'#fff' }}>{this.state.data.howuse!=''?this.state.data.howuse:'- '+i18.t('updating')}</Text>
                        </View>
                        <Image source={ require('../img/icon/lienhe.png') } style={{  width:20*4.47,height:20,resizeMode:'stretch',marginTop:10 }}
                                >
                        <Text style={{ marginTop:Platform.OS == 'ios'?3:0 ,marginLeft:5,color:'#fff', fontSize: 13,backgroundColor:'transparent'}}>
                            {i18.t('object')}
                        </Text>
                        </Image>
                        <View
                style={{ padding:10,backgroundColor:'rgba(20,20,20,.5)',borderBottomRightRadius:5,borderTopRightRadius:5,borderBottomLeftRadius:5 }}
            >
                        <Text style={{ color:'#fff' }}>{this.state.data.object!=''?this.state.data.object:'- '+i18.t('updating')}</Text>
                    </View>
                    </View>
                    </ScrollView>
                    </Image>
                {/*<View style={{ bottom:10,position:'absolute',width:api.getRealDeviceWidth() }}>
                    <Button
                        style={{ alignSelf:'center',borderRadius:20,paddingHorizontal:20,paddingVertical:5,backgroundColor:'#fa6428' }}
                        onPress={this._checkCamPer}
                    >
                        <Text style={{ textAlignVertical:'center',textAlign:'center',fontSize:18,color:'#fff',fontWeight:'300' }}>Quét mã thẻ</Text>
                    </Button>
                </View>*/}
            </Container>
        );
    }
}
const mapState = (state) => {
    return {navState: state.navState,userState: state.userState}
}
const mapDispatch = (dispatch) => {
    return {
        setUserInfo: (data) => dispatch(setUserInfo(data)),
    }
}
export default connect(mapState, mapDispatch)(AddCardFromLogin);