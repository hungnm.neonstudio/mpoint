import React, { PureComponent } from 'react';
import { View, ScrollView, Image, StyleSheet, Platform, Text } from 'react-native';
import api from '../api';
import i18 from './i18';
import { connect } from 'react-redux';
// import component
import AnotherCard from './tab/chooseAnotherCard';
import CardInfo from './tab/cardInfo'
import { TabViewAnimated, TabBar, SceneMap } from 'react-native-tab-view';
class MyGift extends PureComponent {
    state = {
        index: this.props.page ? this.props.page : 0,
        routes: [
            { key: '1', title: i18.t('chooseOtherCard') },
            { key: '2', title: i18.t('cardInfo') }
        ],
    };
    _handleChangeTab = index => this.setState({ index });

    _renderHeader = props => <TabBar {...props}
        style={{ backgroundColor: '#fdfdfd' }}
        indicatorStyle={{ borderBottomColor: '#fa6428', borderBottomWidth: 4 }}
        renderLabel={(payload) => { return <Text style={{ color: payload.focused ? '#fa6428' : '#000', fontWeight: '700', fontSize: 15 }}>{payload.route.title}</Text> }}
    />;

    _renderScene = SceneMap({
        '1': () => { return <AnotherCard gotoPromotion={this.props.gotoPromotion} gotoUpdate={this.props.gotoUpdate} /> },
        '2': () => { return <CardInfo gotoPromotion={this.props.gotoPromotion} /> },
    });
    _subString(str) {
        if (str.length > 8) {
            return str.substring(0, 5) + '...'
        } else return str
    }
    render() {
        return <Image
            source={require('../img/bg_login.jpg')}
            style={{ width: api.getRealDeviceWidth(), height: api.getRealDeviceHeight() - 115 }}
        ><TabViewAnimated
                style={styles.container}
                navigationState={this.state}
                renderScene={this._renderScene}
                renderHeader={this._renderHeader}
                onRequestChangeTab={this._handleChangeTab}
            /></Image>
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
});
mapStateToProps = (state) => ({ lang: state.langState.lang })
mapDispatchToProps = () => ({})
export default connect(mapStateToProps, mapDispatchToProps)(MyGift)
// import React, {Component} from 'react';
// import {View,ScrollView,Image,Platform} from 'react-native';
// import AnotherCard from './tab/chooseAnotherCard';
// import CardInfo from './tab/cardInfo'
// import api from '../api'
// import i18 from './i18'
// import {connect} from 'react-redux'
// import {IndicatorViewPager,PagerTitleIndicator} from 'rn-viewpager'
// class Card extends Component {
//     main = <View></View>
//     constructor(props) {
//         super(props);
//         main =<IndicatorViewPager
//                 initialPage={this.props.page}
//                 style={{
//                 width:api.getRealDeviceWidth(),
//                 height:Platform.OS=='ios'? api.getRealDeviceHeight()-151:api.getRealDeviceHeight(),
//                 backgroundColor: 'white',
//                 flexDirection: 'column-reverse'
//             }}
//                 indicator={this._renderTitleIndicator()}>
//                 <View
//                     style={{
//                     backgroundColor: '#dce0e3'
//                 }}>
//                     <Image
//                         source={ require('../img/bg_login.jpg') }
//                         style= {{ width:api.getRealDeviceWidth(),height:api.getRealDeviceHeight()-151 }}
//                     >
//                     <AnotherCard gotoPromotion={this.props.gotoPromotion} gotoUpdate = {this.props.gotoUpdate}/>
//                     </Image>
//                 </View>
//                 <View
//                     style={{
//                     backgroundColor: '#dce0e3'
//                 }}>
//                 <Image
//                         source={ require('../img/bg_login.jpg') }
//                         style= {{ width:api.getRealDeviceWidth(),height:api.getRealDeviceHeight()-151 }}
//                     >
//                     <CardInfo gotoPromotion={this.props.gotoPromotion}/>
//                 </Image>
//                 </View>
//             </IndicatorViewPager>
//     }
//     _renderTitleIndicator() {
//         return <PagerTitleIndicator titles={[i18.t('chooseOtherCard'), i18.t('cardInfo')]}
//         itemTextStyle={{ fontWeight:Platform.OS == 'ios' ? '700':'500' }}
//         selectedBorderStyle ={{ borderWidth:2,borderColor:'#fa6428' }}
//         selectedItemTextStyle={{ fontWeight:Platform.OS == 'ios' ? '700':'500' }}
//         />;
//     }

//     render() {
//         if(Platform.OS == 'ios')
//         return (
//         <ScrollView keyboardShouldPersistTaps={'handled'} style={{ width:api.getRealDeviceWidth(),height:Platform.OS=='ios'? api.getRealDeviceHeight()-151:api.getRealDeviceHeight() }}>
//             {main}
//         </ScrollView>)
//         else
//         return (
//             main
//         );
//     }
// }
// mapStateToProps=(state)=>({lang:state.langState.lang})
// mapDispatchToProps=()=>({})
// export default connect(mapStateToProps, mapDispatchToProps)(Card)