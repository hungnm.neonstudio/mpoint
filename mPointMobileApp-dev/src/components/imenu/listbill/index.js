import React, { Component } from 'react';
import { View, Image, Text, ScrollView, Platform, ActivityIndicator, AsyncStorage, PermissionsAndroid, Modal, ListView, TouchableOpacity } from 'react-native';
import {
    Header,
    Title,
    Container,
    Content,
    Button,
    Icon,
    Input,
    InputGroup
} from 'native-base';
import api from '../../../api';
import dataService from '../../../api/dataService';
import DeviceInfo from 'react-native-device-info'
import { connect } from 'react-redux';
import PopupDialog, { SlideAnimation, DialogTitle } from 'react-native-popup-dialog';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import FontAwesomeIcons from 'react-native-vector-icons/FontAwesome';
import i18 from '../../i18'
import IconEntypo from 'react-native-vector-icons/Entypo'
import DatePicker from 'react-native-datepicker'
import Item from './item'
class AddCardFromLogin extends Component {
    // define const,var global
    numload = 10;
    constructor() {
        super()
        this.state = {
            canLoadMore: true,
            bill: []
        }
    }
    componentWillMount() {
        this._loadMore()
    }

    _loadMore() {
        let { shopId } = this.props.imenuState
        if (this.state.canLoadMore)
            dataService.getListBill(shopId, 1, this.state.bill.length, this.numload)
                .then(rs => {
                    if (rs.err == 0) {
                        this.setState({
                            bill: this.state.bill.concat(rs.data),
                            canLoadMore: rs.data.length < this.numload ? false : true
                        })
                    } else {
                        this.setState({
                            canLoadMore: false
                        })
                    }
                })
    }

    render() {
        const ds = new ListView.DataSource({
            rowHasChanged: (r1, r2) => r1 !== r2
        });
        return (
            <Container>
                <Header
                    style={{
                        backgroundColor: '#009688',
                        zIndex: 1000
                    }}>
                    <Button transparent onPress={() => api.pop()}>
                        <Icon name='ios-arrow-back' style={{ color: Platform.OS == 'ios' ? '#000' : '#fff' }} />
                    </Button>
                    <Title style={{ alignSelf:'center' } }>
                        {i18.t('listBill')}
                    </Title>
                    <Button transparent onPress={() => api.resetRoute({ key: 'home' })}>
                        <FontAwesomeIcons name='home' style={{ color: Platform.OS == 'ios' ? '#000' : '#fff', fontSize: 23 }} />
                    </Button>
                </Header>
                <Image
                    source={require('../../../img/bg_login.jpg')}
                    style={{ height: api.getRealDeviceHeight() - 58, width: api.getRealDeviceWidth() }}>
                    <ListView
                        contentContainerStyle={{ paddingBottom: 10 }}
                        enableEmptySections={true}
                        renderFooter={() => {
                            if (this.state.canLoadMore) return <ActivityIndicator size='large' />
                            if (!this.state.canLoadMore && this.state.bill.length == 0) return <Text style={{ alignSelf: 'center' }}>{i18.t('nodata')}</Text>
                            return null;
                        }}
                        onEndReached={this._loadMore.bind(this)}
                        dataSource={ds.cloneWithRows(this.state.bill)}
                        renderRow={(item, id) => {
                            return <Item
                                data={item}
                            />
                        }}
                    />
                </Image>
            </Container>
        );
    }
}
const mapState = (state) => {
    return { navState: state.navState, userState: state.userState, imenuState: state.imenuState }
}
const mapDispatch = (dispatch) => {
    return {
    }
}
export default connect(mapState, mapDispatch)(AddCardFromLogin);