import React, { Component } from 'react';
import { View, Image, Text, ListView, ScrollView, Platform, AsyncStorage, PermissionsAndroid, Modal, TouchableOpacity } from 'react-native';
import {
    Header,
    Title,
    Container,
    Content,
    Button,
    Icon,
    Input,
    InputGroup, Footer, FooterTab
} from 'native-base';
import api from '../../../api';
import dataService from '../../../api/dataService';
import DeviceInfo from 'react-native-device-info'
import { connect } from 'react-redux';
import PopupDialog, { SlideAnimation, DialogTitle } from 'react-native-popup-dialog';
import { setUserInfo, setUserToken, setUuid } from '../../../actions/userActions'
import IconsFontAwesome from 'react-native-vector-icons/FontAwesome';
import i18 from '../../i18'
import Mymenu from '../mymenu'
import IconEntypo from 'react-native-vector-icons/Entypo'
import * as Animated from 'react-native-animatable'
class ItemCombo extends Component {
    constructor(props) {
        super(props)
        this.state = {
            expanded: false,
            choose: false
        }
    }
    componentDidMount() {
        this.props.imenuState.combo.map(el => {
            if (el.id == this.props.data.id) {
                return this.setState({
                    choose: true
                })
            }
        })
    }
    _renderItem() {
        const ds = new ListView.DataSource({
            rowHasChanged: (r1, r2) => r1 !== r2
        });
        return <Modal
            transparent={true}
            visible={this.state.expanded}
            animationType='fade'
            onRequestClose={() => { }}>
            <View style={{ height: api.getRealDeviceHeight(), width: api.getRealDeviceWidth(), backgroundColor: 'rgba(0,0,0,.8)' }} >
                <View style={{ zIndex: 1, borderRadius: 3, backgroundColor: '#fff', width: api.getRealDeviceWidth() * .9, alignSelf: 'center', flex: 1, marginVertical: api.getRealDeviceHeight() * .09 }}>
                    <Text style={{ paddingVertical: 5, fontSize: 17, fontWeight: '300', alignSelf: 'center', color: '#000' }}>{(this.props.data.name || '').toUpperCase()}</Text>
                    <View style={{ height: 1, backgroundColor: '#cdcdcd' }} />
                    <ListView
                        style={{ padding: 5 }}
                        enableEmptySections={true}
                        dataSource={ds.cloneWithRows(this.props.data.itemCombos)}
                        renderRow={(item, sectionID, rowID) => {
                            return (
                                <View style={{ marginBottom: 10, borderColor: '#cdcdcd', borderWidth: 1, borderRadius: 3 }}>
                                    <Text style={{ fontSize: 16, marginLeft: 5, marginVertical: 5, color: '#000' }}>{(item.item.name || '').toUpperCase()}</Text>
                                    <Image
                                        source={{ uri: item.item.img }}
                                        style={{ resizeMode: 'stretch', height: ((api.getRealDeviceWidth() * .9) / 1.911262799) }}
                                    />
                                    <Text style={{ marginTop: 5, marginHorizontal: 5 }}>{item.item.decription}</Text>
                                    <View style={{ paddingHorizontal: 5, paddingBottom: 5 }}>
                                        <Text>{i18.t('numitem') + ': ' + item.amount}</Text>
                                        <Text>{i18.t('costNoPromotion') + ': ' + item.item.price} {i18.t('moneyUnit')}</Text>
                                    </View>
                                </View>
                            );
                        }} />
                    <Button block style={{ marginBottom: -0.5, backgroundColor: "#fa6428", borderTopLeftRadius: 0, borderTopRightRadius: 0 }}
                        onPress={() => {
                            this.setState({
                                expanded: !this.state.expanded
                            })
                        }}>
                        <Text style={{ fontSize: 16, color: '#fff' }}>
                            {i18.t('close')}
                        </Text>
                    </Button>
                </View>
                <TouchableOpacity
                    activeOpacity={1}
                    style={{ position: 'absolute', height: api.getRealDeviceHeight(), width: api.getRealDeviceWidth() }}
                    onPress={() => {
                        this.setState({
                            expanded: !this.state.expanded
                        })
                    }}
                />
            </View>
        </Modal>
    }
    _choose = () => {
        let { id, name, price } = this.props.data;
        let tempArr = this.props.imenuState.combo;
        let arr = [];
        if (this.state.choose) {
            tempArr.map(el => {
                if (el.id != id) arr.push(el);
            })

        } else {
            arr = tempArr.concat({
                ...this.props.data,
                amount: 1
            })
        }
        api.updateBookCombo(arr)
        this.setState({
            choose: !this.state.choose
        });
    }
    render() {
        let { img, name, id, price } = this.props.data;
        return (
            <TouchableOpacity
                style={{ marginBottom: 10, borderRadius: 3, borderColor: '#cdcdcd', borderWidth: 1 }}
                onPress={() => { this.setState({ expanded: true }) }}
            >
                {this._renderItem()}
                <Text style={{ color: '#000', paddingLeft: 5, fontWeight: '400', fontSize: 16, marginVertical: 3 }}>{(name || '').toUpperCase()}</Text>
                <Image
                    source={{ uri: img ? img : 'https://image.freepik.com/free-icon/meat-slice-silhouette_318-42659.jpg' }}
                    style={{ resizeMode: 'stretch', height: (api.getRealDeviceWidth() - 55) / 1.911262799, borderTopLeftRadius: 3, borderTopRightRadius: 3 }}
                >
                    <TouchableOpacity style={{ bottom: 0, right: 0, position: 'absolute', padding: 20 }}
                        onPress={this._choose.bind(this)}
                    >
                        {this.state.choose ? <IconsFontAwesome name='check-circle' style={{ color: '#25c173', fontSize: 28, backgroundColor: 'transparent' }} />
                            : <IconEntypo name='circle-with-plus' style={{ backgroundColor: 'transparent', color: '#e1b201', fontSize: 28 }} />

                        }
                    </TouchableOpacity>
                </Image>
                <Text style={{ color: '#000', paddingLeft: 5, fontWeight: '400' }}>{i18.t('cost')}: {price}  {i18.t('moneyUnit')}</Text>
                <Text style={{ paddingLeft: 5, paddingBottom: 5 }}>{i18.t('showmore')}</Text>
            </TouchableOpacity>
        )
    }
}
const mapState = (state) => {
    return { navState: state.navState, userState: state.userState, imenuState: state.imenuState }
}
const mapDispatch = (dispatch) => {
    return {
        setUserInfo: (data) => dispatch(setUserInfo(data)),
    }
}
export default connect(mapState, mapDispatch)(ItemCombo);