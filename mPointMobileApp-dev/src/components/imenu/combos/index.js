import React, { Component } from 'react';
import { View, Image, Text, ListView, ScrollView, Platform, ActivityIndicator, AsyncStorage, PermissionsAndroid, Modal, TouchableOpacity } from 'react-native';
import {
    Header,
    Title,
    Container,
    Content,
    Button,
    Icon,
    Input,
    InputGroup, Footer, FooterTab
} from 'native-base';
import api from '../../../api';
import dataService from '../../../api/dataService';
import DeviceInfo from 'react-native-device-info'
import { connect } from 'react-redux';
import PopupDialog, { SlideAnimation, DialogTitle } from 'react-native-popup-dialog';
import { setUserInfo, setUserToken, setUuid } from '../../../actions/userActions'
import IconsFontAwesome from 'react-native-vector-icons/FontAwesome';
import i18 from '../../i18'
import Mymenu from '../mymenu'
import ItemCombo from './itemCombo'
import Combos from '../combos'
import IconEntypo from 'react-native-vector-icons/Entypo'
class AddCardFromLogin extends Component {
    constructor(props) {
        super(props)
        this.state = {
            expanded: false,
            item: [],
            canloadMore: true
        }
    }
    numload = 10
    componentWillReceiveProps(nextProps) {
        this._loadMore();
    }
    componentWillMount() {
        this._loadMore()
    }
    _loadMore() {
        if (this.state.canloadMore)
            // dataService.getListCombo(this.props.id, '', this.state.item.length, this.numload)
            dataService.getListCombo(1, '', this.state.item.length, this.numload)
                .then(rs => {
                    console.log('====================================');
                    console.log(rs);
                    console.log('====================================');
                    if (rs.err == 0) {
                        let data = this.props.imenuState.combo;
                        let temp = [];
                        rs.data.map(el => {
                            let have = false;
                            data.map(dt => {
                                if (el.id == dt.id) {
                                    have = true; return;
                                }
                            });
                            if (have) {
                                temp.push({ ...el, selected: true })
                            } else {
                                temp.push(el)
                            }
                        })
                        this.setState({
                            item: this.state.item.concat(temp),
                            canloadMore: rs.data.length < this.numload ? false : true
                        })
                    } else {
                        this.setState({
                            canloadMore: false
                        })
                    }
                })
    }

    render() {
        const ds = new ListView.DataSource({
            rowHasChanged: (r1, r2) => r1 !== r2
        });
        return (
            <View>
                <Text style={{ fontFamily: 'Times New Roman', color: '#000', textAlign: 'center', fontSize: 20, marginVertical: 5 }}>{(i18.t('listCombo')).toUpperCase()}</Text>
                <ListView
                    showsVerticalScrollIndicator={false}
                    style={{ padding: 5 }}
                    enableEmptySections={true}
                    dataSource={ds.cloneWithRows(this.state.item)}
                    renderFooter={() => {
                        if (this.state.canloadMore) {
                            return <ActivityIndicator size='large' />
                        }
                        if (!this.state.canloadMore && this.state.item.length == 0) {
                            return <Text>{i18.t('nodata')}</Text>
                        }
                    }}
                    renderRow={(item, sectionID, rowID) => {
                        return (
                            <ItemCombo data={item} />
                        );
                    }} />
            </View>
        );
    }
}
const mapState = (state) => {
    return { navState: state.navState, userState: state.userState, imenuState: state.imenuState }
}
const mapDispatch = (dispatch) => {
    return {
        setUserInfo: (data) => dispatch(setUserInfo(data)),
    }
}
export default connect(mapState, mapDispatch)(AddCardFromLogin);