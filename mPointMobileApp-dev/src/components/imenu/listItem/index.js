import React, { Component } from 'react';
import { View, ActivityIndicator, Image, Text, ListView, ScrollView, Platform, AsyncStorage, PermissionsAndroid, Modal, TouchableOpacity } from 'react-native';
import {
    Header,
    Title,
    Container,
    Content,
    Button,
    Icon,
    Input,
    InputGroup, Footer, FooterTab
} from 'native-base';
import api from '../../../api';
import dataService from '../../../api/dataService';
import DeviceInfo from 'react-native-device-info'
import { connect } from 'react-redux';
import FontAwesomeIcons from 'react-native-vector-icons/FontAwesome';
import i18 from '../../i18'
import Item from './item'
import IconEntypo from 'react-native-vector-icons/Entypo'
class ListItem extends Component {
    numload = 10;
    constructor(props) {
        super(props)
        this.state = {
            item: [],
            canLoadMore: true,
        }
    }
    componentWillReceiveProps(nextProps) {
        this.setState({
            item: [],
            canLoadMore: true
        });
        setTimeout(() => {
            this._loadMore()
        }, 50);
    }
    componentWillMount = () => {
        // alert(this.props.categoryID)
        this._loadMore()
    }
    _loadMore = () => {
        if (this.state.canLoadMore)
            dataService.getItemCategory(this.props.categoryID, '', this.state.item.length, this.numload)
                .then(rs => {
                    console.log('====================================');
                    console.log(rs);
                    console.log('====================================');
                    if (rs.err == 0) {
                        this.setState({
                            item: this.state.item.concat(rs.data),
                            canLoadMore: rs.data.length < this.numload ? false : true
                        })
                    }
                    else {
                        this.setState({
                            canLoadMore: false
                        })
                    }
                })
    }
    render() {
        const ds = new ListView.DataSource({
            rowHasChanged: (r1, r2) => r1 !== r2
        });
        return (
            <View>
                <Text style={{ fontFamily: 'Times New Roman', color: '#000', textAlign: 'center', fontSize: 20, marginVertical: 5 }}>{(this.props.categoryName || '').toUpperCase()}</Text>
                <ListView
                    showsVerticalScrollIndicator={false}
                    renderFooter={() => {
                        if (this.state.canLoadMore) {
                            return <ActivityIndicator size='large' />
                        }
                        if (!this.state.canLoadMore && this.state.item.length == 0) {
                            return <Text>{i18.t('nodata')}</Text>
                        }
                    }}
                    style={{ padding: 5 }}
                    enableEmptySections={true}
                    dataSource={ds.cloneWithRows(this.state.item)}
                    onEndReached={this._loadMore.bind(this)}
                    renderRow={(item, itemID, id) => {
                        return (
                            <Item data={item} />
                        );
                    }} />
            </View>
        );
    }
}
const mapState = (state) => {
    return { navState: state.navState, userState: state.userState }
}
const mapDispatch = (dispatch) => {
    return {
        setUserInfo: (data) => dispatch(setUserInfo(data)),
    }
}
export default connect(mapState, mapDispatch)(ListItem);