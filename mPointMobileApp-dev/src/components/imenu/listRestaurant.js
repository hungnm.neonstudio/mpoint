import React, { Component } from 'react';
import { View, Image, Text, ScrollView, Platform, ActivityIndicator, ListView, AsyncStorage, PermissionsAndroid, Modal, TouchableOpacity } from 'react-native';
import {
    Header,
    Title,
    Container,
    Content,
    Button,
    Icon,
    Input,
    InputGroup
} from 'native-base';
import api from '../../api';
import dataService from '../../api/dataService';

import cache from '../../api/cache';
import DeviceInfo from 'react-native-device-info'
import { connect } from 'react-redux';
import PopupDialog, { SlideAnimation, DialogTitle } from 'react-native-popup-dialog';
import { setUserInfo, setUserToken, setUuid } from '../../actions/userActions'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import i18 from '../i18';
import IconEntypo from 'react-native-vector-icons/Entypo';
import FontAwesomeIcons from 'react-native-vector-icons/FontAwesome';
class AddCardFromLogin extends Component {
    numload = 10;
    constructor() {
        super()
        this.state = {
            restaurant: [],
            showPopup: false,
            numberPeople: '1',
            canLoadMore: true,
            text: ''
        }
    }
    componentWillMount() {
        this._loadMore();
    }
    _loadMore() {
        if (this.state.canLoadMore)
            dataService.getListRestaurant(this.state.restaurant.length, this.numload, this.state.text)
                .then(rs => {
                    if (rs.err == 0) {
                        this.setState({
                            restaurant: this.state.restaurant.concat(rs.data),
                            canLoadMore: rs.data.length < this.numload ? false : true
                        })
                    } else {
                        this.setState({
                            canLoadMore: false
                        })
                    }
                }
                )
    }
    _renderRestaurant() {
        const ds = new ListView.DataSource({
            rowHasChanged: (r1, r2) => r1 !== r2
        });
        return <ListView
            style={{ width: api.getRealDeviceWidth(), padding: 5 }}
            enableEmptySections={true}
            dataSource={ds.cloneWithRows(this.state.restaurant)}
            renderFooter={() => {
                if (this.state.canLoadMore) return <ActivityIndicator style={{ alignSelf: 'center', flex: 1 }} size='large' />
                if (!this.state.canLoadMore && this.state.restaurant.length < 1) return <Text style={{ backgroundColor: 'transparent', textAlign: 'center', flex: 1, color: '#fff' }}>{i18.t('nodata')}</Text>
            }}
            renderRow={(item, id) => {
                return <Restaurant
                    data={item}
                    onPress={() => { api.push({ key: 'listAdd', img: item.card, resName: item.name, id: item.id, intro: item.introduce }) }}
                />
            }} />
    }
    to = null
    _onSearch = (text) => {
        this.setState({
            canLoadMore: true,
            restaurant: [],
            text: text
        })
        clearTimeout(this.to);
        this.to = setTimeout(() => {
            this._loadMore()
        }, 500)
    }
    render() {
        return (
            <Container>
                <Header
                    style={{
                        backgroundColor: '#009688',
                        zIndex: 1000
                    }}>
                    <Button transparent onPress={() => api.pop()}>
                        <Icon name='ios-arrow-back' style={{ color: Platform.OS == 'ios' ? '#000' : '#fff' }} />
                    </Button>
                    <Title style={{ alignSelf:'center' } }>
                        {i18.t('titleRestaurant')}
                    </Title>
                    {/*<Button transparent
                        onPress={() => { api.push({ key: 'myorder' }) }}
                    >
                        <Text style={{ fontSize: 17, color: '#fff' }}>
                            {i18.t('myorder')}
                        </Text>
                    </Button>*/}
                    <Button transparent onPress={() => api.resetRoute({ key: 'home' })}>
                        <FontAwesomeIcons name='home' style={{ color: Platform.OS == 'ios' ? '#000' : '#fff', fontSize: 23 }} />
                    </Button>

                </Header>
                <Image style={{ height: api.getRealDeviceHeight() - 56, width: api.getRealDeviceWidth() }}
                    source={require('../../img/bg_login.jpg')}
                >
                    <InputGroup
                        iconRight
                        style={{ backgroundColor: '#fff', marginHorizontal: 5, borderRadius: 3, marginTop: 10 }}
                    >
                        <Input
                            placeholder={i18.t('searchRes')}
                            onChangeText={this._onSearch}
                            ref='search'
                        />
                        {
                            this.state.text ? <Icon name='ios-close' style={{ paddingRight: 10 }} onPress={() => {
                                this.refs.search._textInput.clear();
                                this._onSearch('');
                            }} /> : <Icon name='ios-search' onPress={() => {
                                this.refs.search._textInput.focus();
                            }} />
                        }
                    </InputGroup>
                    {this._renderRestaurant()}
                </Image>
            </Container>
        );
    }
}
const mapState = (state) => {
    return { navState: state.navState, userState: state.userState }
}
const mapDispatch = (dispatch) => {
    return {
        setUserInfo: (data) => dispatch(setUserInfo(data)),
    }
}
export default connect(mapState, mapDispatch)(AddCardFromLogin);

class Restaurant extends Component {
    constructor(props) {
        super(props)
    }
    render() {
        let { slogan, img, logo, name, open, close } = this.props.data;
        let opening = '';
        let now = (new Date()).getHours();
        if (now >= parseInt(open.substr(0, 2)) && now <= parseInt(close.substr(0, 2))) {
            opening = i18.t('opening');
        } else {
            opening = i18.t('closed')
        }
        return (
            <TouchableOpacity
                activeOpacity={1}
                onPress={this.props.onPress}
                style={{ backgroundColor: '#fff', marginTop: 5, padding: 5, flexDirection: 'row' }}
            >
                <Image style={{ width: api.getRealDeviceWidth() / 5, height: api.getRealDeviceWidth() / 5, resizeMode: 'stretch' }} source={{ uri: logo }} />
                <View style={{ paddingLeft: 5, flex: 1 }}>
                    <Text style={{ backgroundColor: '#fff', marginLeft: 2, fontSize: 16, fontWeight: '400' }}>{name}</Text>
                    <Text style={{ backgroundColor: '#fff', paddingVertical: 2 }}>{slogan}</Text>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <Icon name='md-time' style={{ color: '#cdcdcd', fontSize: 13 }} />
                        <Text style={{ backgroundColor: '#fff', paddingVertical: 2, fontSize: 14, marginLeft: 5 }}>{opening} {open.substr(0, 5) + '-' + close.substr(0, 5)}</Text>
                    </View>
                </View>
            </TouchableOpacity>
        )
    }
}