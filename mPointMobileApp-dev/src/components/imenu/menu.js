import React, { Component } from 'react';
import { View, Image, Text, ListView, ScrollView, Platform, AsyncStorage, PermissionsAndroid, Modal, TouchableOpacity } from 'react-native';
import {
    Header,
    Title,
    Container,
    Content,
    Button,
    Icon,
    Input,
    InputGroup, Footer, FooterTab
} from 'native-base';
import api from '../../api';
import dataService from '../../api/dataService';
import DeviceInfo from 'react-native-device-info'
import { connect } from 'react-redux';
import PopupDialog, { SlideAnimation, DialogTitle } from 'react-native-popup-dialog';
import { setUserInfo, setUserToken, setUuid } from '../../actions/userActions'
import FontAwesomeIcons from 'react-native-vector-icons/FontAwesome';
import i18 from '../i18'
import Mymenu from './mymenu'
import Combos from './combos'
import ListItem from './listItem'
import IconEntypo from 'react-native-vector-icons/Entypo'
class AddCardFromLogin extends Component {
    numload = 10;
    constructor() {
        super()
        this.state = {
            category: [
                {
                    id: -1,
                    icon: 'https://s-media-cache-ak0.pinimg.com/originals/ac/94/d6/ac94d621f22b5ee7c14ae3d2e9e04015.png'
                }
            ],
            canloadmore: true,
            shopID: 159,
            showPopup: false,
            numberPeople: '1',
            tab: 1,
            type: 'combo',
            categoryID: 0,
            categoryName: ''
            //  type:'general'

        }
    }
    componentWillMount() {
        let { shopID } = this.props.navState.routes[this.props.navState.index];
        if (!shopID) shopID = this.props.imenuState.shopId;
        this.setState({
            shopID: shopID
        })
        setTimeout(() => {
            this._loadMore();
        }, 30)
    }
    isLoading = false;
    _loadMore() {
        if (!this.state.canloadmore || this.isLoading) return;
        this.isLoading = true;
        dataService.listCategoryRes(this.state.shopID, '', this.state.category.length - 1, this.numload)
            .then(rs => {
                console.log('====================================');
                console.log(rs);
                console.log('====================================');
                if (rs.err == 0 && rs.data) {
                    this.setState({
                        category: this.state.category.concat(rs.data.categoryRes),
                        canloadmore: rs.data.categoryRes.length < this.numload ? false : true
                    })
                }
                else {
                    this.setState({ canloadmore: false });
                }
                isLoading = false;
            })
    }
    _renderCategory() {
        if (!this.state.tab) return null
        const ds = new ListView.DataSource({
            rowHasChanged: (r1, r2) => r1 !== r2
        });
        return <View>
            <ScrollView style={{ height: api.getRealDeviceHeight() - 112, width: 45 }}>
                <ListView
                    style={{ padding: 5 }}
                    enableEmptySections={true}
                    dataSource={ds.cloneWithRows(this.state.category)}
                    onEndReached={this._loadMore.bind(this)}
                    renderRow={(item, itemID, id) => {
                        let self = this;
                        return (
                            <TouchableOpacity
                                onPress={() => {
                                    if (id > 0) {
                                        this.setState({ type: '' + id, categoryID: item.id, categoryName: item.name });
                                    } else {
                                        this.setState({ type: 'combo' });
                                    }

                                }}
                            >
                                <Image
                                    source={{ uri: item.icon ? item.icon : 'https://image.freepik.com/free-icon/meat-slice-silhouette_318-42659.jpg' }}
                                    style={{ height: 30, width: 30, borderRadius: 15, alignSelf: 'center', marginTop: 10 }}
                                />
                            </TouchableOpacity>
                        );
                    }} />
            </ScrollView>
        </View>
    }
    _renderContent() {
        if (!this.state.tab) {
            return <Mymenu />
        } else {
            if (this.state.type == 'combo') return <Combos id={this.state.shopID} />
            else return <ListItem categoryID={this.state.categoryID} categoryName={this.state.categoryName} />
        }
    }
    render() {
        return (
            <Container>
                <Header
                    style={{
                        backgroundColor: '#009688',
                        zIndex: 1000
                    }}>
                    <Button transparent onPress={() => { api.pop(); api.clearBook() }}>
                        <Icon name='ios-arrow-back' style={{ color: Platform.OS == 'ios' ? '#000' : '#fff' }} />
                    </Button>
                    <Title  style={{ alignSelf:'center' } }>
                        {this.props.navState.routes[this.props.navState.index].shopName}
                    </Title>
                    <Button transparent onPress={() => { api.clearBook(); api.resetRoute({ key: 'home' }) }}>
                        <FontAwesomeIcons name='home' style={{ color: Platform.OS == 'ios' ? '#000' : '#fff', fontSize: 23 }} />
                    </Button>
                </Header>
                <View style={{ flexDirection: 'row', height: api.getRealDeviceHeight() - 112 }}>
                    <View style={{ backgroundColor: '#171d2b' }}>
                        {this._renderCategory()}
                    </View>
                    <View style={{ backgroundColor: '#fff', flex: 1, height: api.getRealDeviceHeight() - 112 }} >
                        {this._renderContent()}
                    </View>
                </View>
                <Footer style={{ backgroundColor: '#009688' }}>
                    <View style={{ flexDirection: 'row' }}>
                        <TouchableOpacity style={{ justifyContent:'center',flex: 1, height: 56, backgroundColor: this.state.tab ? '#fa6428' : '#009688',paddingBottom:Platform.OS == 'ios'?6:0 }}
                            onPress={() => { this.setState({ tab: 1 }) }}
                        >
                            <Text style={{ textAlign: 'center', color: '#fff' }}>{i18.t('resMenu')}</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={{  justifyContent:'center',flex: 1, height: 56, backgroundColor: !this.state.tab ? '#fa6428' : '#009688',paddingBottom:Platform.OS == 'ios'?6:0 }}
                            onPress={() => { this.setState({ tab: 0 }) }}
                        >
                            <Text style={{ textAlign: 'center', color: '#fff' }}>{i18.t('myMenu')}</Text>
                        </TouchableOpacity>
                    </View>
                </Footer>
            </Container>
        );
    }
}
const mapState = (state) => {
    return { navState: state.navState, userState: state.userState, imenuState: state.imenuState }
}
const mapDispatch = (dispatch) => {
    return {
        setUserInfo: (data) => dispatch(setUserInfo(data)),
    }
}
export default connect(mapState, mapDispatch)(AddCardFromLogin);