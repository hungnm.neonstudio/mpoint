import React, { PureComponent } from 'react'
import { Image, View, Text, Modal, TouchableOpacity, Platform, ListView, ActivityIndicator, ScrollView } from 'react-native'
import { Button, Container, Title, Header, Content, Icon } from 'native-base'
import api from '../../../api'
import fetch from '../../../api/dataService'
import i18 from '../../i18'
import Item from './item'
import { connect } from 'react-redux';
import dataService from '../../../api/dataService';
class ListOrder extends PureComponent {
    state = {
        data: null,
        loading: true
    }
    componentWillMount() {
        dataService.getDetailBook(this.props.id)
            .then(rs => {
                this.setState({
                    loading: false,
                    data: rs.data
                })
            })
    }
    renderItem = (arr, type) => {
        return arr.map((el, index) => {
            return <View key={index} style={{ marginBottom: 10, borderColor: '#cdcdcd', borderWidth: 1, borderRadius: 3 }}>
                <Text style={{ fontSize: 16, marginLeft: 5, marginVertical: 2 }}>{type ? el.combo.name : el.item.name}</Text>
                <Text style={{ marginHorizontal: 5 }}>{i18.t('numitem')}: {el.amount}</Text>
                <Text style={{ marginHorizontal: 5 }}>{i18.t('cost')}: {el.price} vnđ</Text>
                {/* <View style={{ flexDirection: 'row', justifyContent: 'space-between', paddingHorizontal: 5, paddingBottom: 5 }}>
                    <Text>{i18.t('numitem') + ': ' + item.amount}</Text>
                    <Text>{i18.t('costNoPromotion') + ': ' + item.item.price}</Text>
                </View> */}
            </View>
        });
    }
    render() {
        let data = this.state.data;
        if (this.state.loading) return <ActivityIndicator />;
        return <ScrollView
            style={{ paddingHorizontal: 5 }}
        >
            <Text>ID order : {data.id}</Text>
            <Text>{i18.t('numberPeople')} : {data.people}</Text>
            <Text>{i18.t('time')} : {api.getTime(data.startTime, 'YYYY-MM-DD HH:mm')}</Text>
            <Text style={{ marginHorizontal: 5 }}>{i18.t('tableNum')}: {this.props.table}</Text>
            {this.renderItem(data.bookCombos, 1)}
            {this.renderItem(data.bookItems)}
        </ScrollView>
    }
}
mapStateToProps = (state) => {
    return {
        imenuState: state.imenuState
    }
}
mapDispatchToProps = (dispatch) => {
    return {

    }
}
export default connect(mapStateToProps, mapDispatchToProps)(ListOrder);