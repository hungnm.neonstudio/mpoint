import React, { Component } from 'react';
import { View, Image, Text, ListView, ScrollView, Platform, AsyncStorage, PermissionsAndroid, Modal, TouchableOpacity, TouchableHighlight } from 'react-native';
import {
    Header,
    Title,
    Container,
    Content,
    Button,
    Icon,
    Input,
    InputGroup, Footer, FooterTab
} from 'native-base';
import api from '../../../api';
import fetch from '../../../api/dataService';
import dataService from '../../../api/dataService';
import DeviceInfo from 'react-native-device-info'
import { connect } from 'react-redux';
import FontAwesomeIcons from 'react-native-vector-icons/FontAwesome';
import i18 from '../../i18';
import DetaillBook from './detailOrder'
import IconEntypo from 'react-native-vector-icons/Entypo';
import * as Animated from 'react-native-animatable';
import IconsFontAwesome from 'react-native-vector-icons/FontAwesome';
class Item extends Component {
    constructor(props) {
        super(props)
        this.state = {
            show: false,
            data: {}
        }
    }
    componentWillMount() {
    }
    loadData = () => {
        dataService.getDetailBook(this.props.data.id)
            .then(rs => {
                this.setState({
                    data: rs.data
                })
            })
    }
    _renderPopup = () => {
        if (!this.state.show) return;
        const ds = new ListView.DataSource({
            rowHasChanged: (r1, r2) => r1 !== r2
        });
        return <Modal
            transparent={true}
            visible={this.state.show}
            animationType='fade'
            onRequestClose={() => { }}>
            <Modal
                transparent={true}
                visible={this.state.show}
                animationType='slide'
                onRequestClose={() => { }}>
                <View style={{ zIndex: 1, borderRadius: 3, backgroundColor: '#fff', width: api.getRealDeviceWidth() * .9, alignSelf: 'center', flex: 1, marginVertical: api.getRealDeviceHeight() * .09 }}>
                    <Text style={{ paddingVertical: 5, fontSize: 16, fontWeight: '300', alignSelf: 'center' }}>{(i18.t('orderDetail')).toUpperCase()}</Text>
                    <View style={{ height: 1, backgroundColor: '#cdcdcd' }} />
                    <View style={{ flex: 1 }}>
                        <DetaillBook id={this.props.data.id} table={this.props.data.tablebooks[0].table.name} />
                    </View>
                    <Button block style={{ marginBottom: -0.5, backgroundColor: "#fa6428", borderTopLeftRadius: 0, borderTopRightRadius: 0 }}
                        onPress={() => {
                            this.setState({
                                show: false
                            })
                        }}>
                        <Text style={{ fontSize: 16, color: '#fff' }}>
                            {i18.t('close')}
                        </Text>
                    </Button>
                </View>
                <TouchableOpacity
                    activeOpacity={1}
                    style={{ position: 'absolute', height: api.getRealDeviceHeight(), width: api.getRealDeviceWidth() }}
                    onPress={() => {
                        this.setState({
                            show: false
                        })
                    }}
                />
            </Modal>
            <View style={{ height: api.getRealDeviceHeight(), width: api.getRealDeviceWidth(), backgroundColor: 'rgba(0,0,0,.8)' }} />
        </Modal>
    }
    _cancelBook = () => {
        let isLoading = true;
        let refresh = this.props.onCancel;
        api.showLoading();
        fetch.cancelBook(this.props.data.id)
            .then(rs => {
                api.hideLoading();
                api.showMessage(rs.msg);
                if (refresh) refresh();
            })
        setTimeout(function () {
            if (isLoading) {
                isLoading = false;
                api.hideLoading();
            }
        }, 30000);
    }
    render() {
        let { startTime, people, id, statusId } = this.props.data;
        let status = '';
        switch (statusId) {
            case 1:
                status = i18.t('noprocess')
                break;
            case 2:
                status = i18.t('processing')
                break;
            case 3:
                status = i18.t('processed')
                break;
            case 4:
                status = i18.t('waitPay')
                break;
            case 5:
                status = i18.t('cancelBook')
                break;
            default:
                break;
        }
        return (<TouchableOpacity
            onPress={() => {
                this.setState({
                    show: true
                })
            }}
            style={{ backgroundColor: '#fff', borderRadius: 2, borderWidth: 1, borderColor: '#cdcdcd', padding: 5, marginHorizontal: 5, marginTop: 5 }}
        >
            {this._renderPopup()}
            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                <Text>
                    {i18.t('numberPeople')} : {people}
                </Text>
                <Text>
                    {api.getTime(startTime, 'HH:mm DD/MM/YYYY')}
                </Text>
            </View>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                <Text>
                    {i18.t('status')} : {status}
                </Text>
                {
                    statusId < 3 ? <TouchableHighlight underlayColor='red' style={{ paddingHorizontal: 3, paddingVertical: 2, backgroundColor: '#fa6428', borderRadius: 2 }}
                        onPress={this._cancelBook}
                    >
                        <Text style={{ color: 'white', fontSize: 14 }}>
                            {i18.t('cancelBook')}
                        </Text>
                    </TouchableHighlight> : null
                }
            </View>
        </TouchableOpacity>
        );
    }
}
const mapState = (state) => {
    return { navState: state.navState, userState: state.userState, imenuState: state.imenuState }
}
const mapDispatch = (dispatch) => {
    return {
        setUserInfo: (data) => dispatch(setUserInfo(data)),
    }
}
export default connect(mapState, mapDispatch)(Item);