import React, { Component } from 'react';
import { View, Image, Text, ListView, ScrollView, Platform, AsyncStorage, PermissionsAndroid, Modal, TouchableOpacity } from 'react-native';
import {
    Header,
    Title,
    Container,
    Content,
    Button,
    Icon,
    Input,
    InputGroup, Footer, FooterTab
} from 'native-base';
import api from '../../../api';
import dataService from '../../../api/dataService';
import DeviceInfo from 'react-native-device-info'
import { connect } from 'react-redux';
import PopupDialog, { SlideAnimation, DialogTitle } from 'react-native-popup-dialog';
import { setUserInfo, setUserToken, setUuid } from '../../../actions/userActions'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import i18 from '../../i18'
import IconEntypo from 'react-native-vector-icons/Entypo'
import DatePicker from 'react-native-datepicker'
class Order extends Component {
    constructor(props) {
        super(props);
        this.state = {

        }
    }


    _addFood = () => {
        let data = this.props.imenuState

        let listCombo = [];
        data.combo.map(el => {
            listCombo.push({
                id: el.id,
                amount: el.amount,
                price: el.price
            })
        });

        let listItem = [];
        data.item.map(el => {
            listItem.push({
                id: el.id,
                amount: el.amount,
                price: el.price
            })
        });
        let payload = { bookId: data.orderId, listItem: listItem, listCombo: listCombo };
        let isloading = true;
        api.showLoading();
        dataService.addFood(payload)
            .then(rs => {
                if (rs.err == 0) { api.updateBookCombo([]); api.updateBookItem([]) }
                api.showMessage(rs.msg);
                isloading = false;
                api.hideLoading();
            });
        setTimeout(function () {
            if (isloading) api.showMessage(i18.t('timeoutTextMsgContentLogin'), i18.t('titleMsgLogin'))
        }, 30000);
    }
    render() {
        if (!this.props.imenuState.orderId || (this.props.imenuState.combo.length < 1 && this.props.imenuState.item.length < 1)) return null;
        return (
            <TouchableOpacity
                style={{
                    backgroundColor: '#e1b200',
                    borderRadius: 5,
                    marginLeft: 5,
                    paddingVertical: 5,
                    paddingHorizontal: 5
                }}
                onPress={this._addFood}
            >
                {/* {this._popUp()} */}
                <Text style={{ color: '#fff' }}>
                    {i18.t('addFood')}
                </Text>
            </TouchableOpacity>
        );
    }
}
const mapState = (state) => {
    return { navState: state.navState, userState: state.userState, imenuState: state.imenuState }
}
const mapDispatch = (dispatch) => {
    return {
        setUserInfo: (data) => dispatch(setUserInfo(data)),
    }
}
export default connect(mapState, mapDispatch)(Order);