import React, {Component, PropTypes} from 'react';
import {
    StyleSheet,
    View,
    Text,
    Modal,
    Slider,
    Alert,
    Platform
} from 'react-native';
import {Container, Content, Button,Input,Row,Icon} from 'native-base'
import {connect} from 'react-redux'
import {hideRate} from '../../actions/uiActions'
import api from '../../api'
import dataService from '../../api/dataService'
import i18 from '../../components/i18'
// import sharedStyles from '../styles'
class Slide extends Component {
    /**
   * Optional Flowtype state and timer types
   */
    static propTypes = {
        show:PropTypes.bool.isRequired,
        
    }
    constructor(props) {
        super(props);
        this.state={
            val:5 ,
            data:[i18.t('1star'),i18.t('2star'),i18.t('3star'),i18.t('4star'),i18.t('5star')],
        }
        // console.log(props.rou)
    }
    _onRate(){
        let token = this.props.userState.token
        let id = this.props.navState.routes[this.props.navState.index].id
        let rateLevel = this.state.val
        let rateAvg = 10
        dataService.sendRate(token,id,rateLevel)
        .then((d)=>{
            
            if(d.err ==0 ){
                rateAvg = d.rate
                api.pop()
                api.push({key:'detail',id:id,rateLevel:rateAvg})
                //Alert.alert("Thông báo",d.msg)
                
            }
                api.showMessage(d.msg, i18.t('titleMsgLogin'));
            
            this.props.hideRate()
        })
        
            
      
        
    }
    render() {
        if (this.props.show) {
            let rs = ''
            return (
                <Modal
                    transparent={true}
                    visible={true}
                    animationType='slide'
                    onRequestClose={() => {}}>
                    <Container
                        style={{
                        flex: 1,
                        backgroundColor: 'rgba(0, 0, 0, 0.52);'
                    }}>
                        <Content>
                            <View style={styles.backdrop}>
                                <View style={styles.modal}>
                                    <View style={styles.modal_content}>
                                        <Text style={ styles.modal_title }>{(i18.t('rateTitle')).toUpperCase()}</Text>
                                       <View style={{ height:1,flex:1,backgroundColor:'#cdcdcd' }}>
                                    </View>
                                        <Text style={ styles.modal_text_content }>{this.state.data[this.state.val-1]}</Text>
                                        <Row>
                                            <Icon name='ios-star-outline' style={{ flex:1 }}/>
                                            <Slider maximumValue={5} minimumValue={1} style={{ flex:6 }} onValueChange={(val)=>this.setState({val:val})}  step={1} value = {this.state.val}/>
                                            <Icon name='ios-star'  style={{ flex:1,color:'red' }}/>
                                        </Row>
                                        <Row style={{ marginTop:20 }}>
                                            <Button onPress={()=>this.props.hideRate()} style={{ flex:1,backgroundColor:'gray' }}>{i18.t('close')}</Button>
                                             <Button onPress={this._onRate.bind(this)} style={{ flex:1,marginLeft:10,backgroundColor:'#fa6428' }}>{i18.t('rateTitle')}</Button>  
                                        </Row>
                                    </View>
                                </View>

                            </View>

                        </Content>
                    </Container>
                </Modal>
            );
        }
        return null

    }
}
const styles = StyleSheet.create({
    button_bottom: {
        borderBottomLeftRadius: 2,
        borderBottomRightRadius: 2
    },
    backdrop: {
        //backgroundColor: 'rgba(0, 0, 0, 0.52);',
        zIndex: 1000,
        flex: 1
    },
    modal: {
        // marginTop: api.getRealDeviceHeight() / 3,
        marginTop:50,
        maxWidth: 500,
        width:api.getRealDeviceWidth()*0.9,

        borderRadius:5,        overflow: 'hidden',
        alignSelf:'center'
    },
    modal_content: {
        backgroundColor: 'white',
        borderRadius: 2,
        flex: 1,
        padding:10
    },
    modal_title: {
        padding: 5,
        paddingBottom: 10,
        textAlign: 'center',
        fontWeight:'bold'
    },
    modal_text_content: {
        padding: 10,
        flex: 1,
        textAlign:'center',
        marginTop:20
    },
    button_container: {
        flex: 1,
        height: 50
    }
});
mapStateToProps = (state) => ({userState:state.userState,navState:state.navState})
mapDispatchToProps = (dispath) => ({
    hideRate:()=>dispath(hideRate()),
})
export default connect(mapStateToProps, mapDispatchToProps)(Slide)