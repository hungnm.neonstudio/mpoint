import React, {Component, PropTypes} from 'react';
import {
    ActivityIndicator,
    StyleSheet,
    View,
    Text,
    Modal,Platform,
    TouchableOpacity,Image
} from 'react-native';
import {Container, Content, Button, Footer, FooterTab} from 'native-base'
import {connect} from 'react-redux'
import api from '../../api'
import * as Animatable from 'react-native-animatable';
import i18 from '../../components/i18'


// import sharedStyles from '../style'
class ConfirmBox extends Component {
    constructor(props) {
        super(props);
    }
    img={
        gift:require('../../img/center.png'),
        stamp:require('../../img/icon/bought.png')
    }
    render() {
        let {stoletPoint,showStorelet,contentStorelet,storeletButtonText,storeletHandle,showStamp} = this.props.uiState
        
        if (showStorelet) {
            return (
                <Modal
                    transparent={true}
                    visible={true}
                    animationType='slide'
                    onRequestClose={() => {}}>
                    <Container
                        style={{
                        flex: 1,
                        //backgroundColor: 'rgba(0, 0, 0, 0.52);'
                    }}>
                    
                       <View style ={{ height:api.getRealDeviceHeight(),width:api.getRealDeviceWidth(),position:'absolute',backgroundColor: 'rgba(0, 0, 0, 0.7);' }}>                  
                    <View style={{ position:'absolute',height:api.getRealDeviceHeight() }}>
                        <View style={{ flex:3 }}/>
                        <Animatable.Image animation = 'rotate' easing='linear' direction='normal' duration={10000} iterationCount={110500} source ={require('../../img/quay.png')} style = {{height:api.getRealDeviceWidth()*.8,width:api.getRealDeviceWidth()}}/>
                        <View style={{ flex:8 }}/>
                    </View>
                    <View style={{ position:'absolute',height:api.getRealDeviceHeight()  }}>
                        <View style={{ flex:3 }}/>
                        <Animatable.Image animation = 'rotate' easing='linear' direction='reverse' duration={10000} iterationCount={501105000} source ={require('../../img/quay.png')} style = {{ height:api.getRealDeviceWidth()*.8,width:api.getRealDeviceWidth() }}/>
                        <View style={{ flex:8 }}/>
                    </View>
                    <View style={{ position:'absolute',height:api.getRealDeviceHeight()  }}>
                        <View style={{ flex:3 }}/>
                        <Animatable.Image animation = 'zoomIn' easing='linear' direction='reverse' duration={10000} iterationCount={501105000} source ={require('../../img/quay.png')} style = {{ height:api.getRealDeviceWidth()*.8,width:api.getRealDeviceWidth() }}/>
                        <View style={{ flex:8 }}/>
                    </View>
                    <View style={{ position:'absolute',height:api.getRealDeviceHeight()  }}>
                        <View style={{ flex:3 }}/>
                        <Animatable.Image animation = 'zoomOut' easing='linear' direction='reverse' duration={10000} iterationCount={501105000} source ={require('../../img/quay.png')} style = {{ height:api.getRealDeviceWidth()*.8,width:api.getRealDeviceWidth() }}/>
                        <View style={{ flex:8 }}/>
                    </View>
                    <View style={{ flex:15 }}/>
                    {stoletPoint?<View style={{ height:api.getRealDeviceWidth()*.3,width:api.getRealDeviceWidth()*.3,borderRadius:api.getRealDeviceWidth()*.15,backgroundColor:'rgba(255,0,0,.8)',alignSelf:'center' }}>
                        <View style={{ flex:1 }}/>
                            <Text style={{ marginTop:-3,fontFamily:'Times New Roman',paddingHorizontal:2,textAlign:'center',color:'#fff',fontSize:stoletPoint.length>10?13:stoletPoint.length>6?22:27,fontWeight:'400' }}>{stoletPoint}</Text>
                        <View style={{ flex:1 }}/>
                        
                    </View>:<Image source ={showStamp?this.img.stamp:this.img.gift} style = {{ height:api.getRealDeviceWidth()/3.8,width:api.getRealDeviceWidth()/3.8,alignSelf:'center' }}/>}
                    <View style={{ flex:27 }}/>
               </View>
                    <View style ={{ height:api.getRealDeviceHeight(),width:api.getRealDeviceWidth(),position:'absolute',zIndex:100000,paddingTop:api.getRealDeviceHeight()*.6 }}>
                        <Text style={{ color:'#fff',textAlign:'center',marginHorizontal:api.getRealDeviceWidth()*.1,fontSize:16 }}>{contentStorelet}</Text>
                    </View>
                    </Container>
                    <View style={{ width:api.getRealDeviceWidth(),zIndex:9000000,bottom:10,position:'absolute', }}>
                        <View style={{ flexDirection:'row',alignSelf:'center' }}>
                            <Button style={{ borderRadius:5,backgroundColor:'#fa6428' }} onPress={api.hideStorelet}>
                            <Text style={{ paddingVertical:5,paddingHorizontal:30,color:'#fff' }}>{i18.t('close')}</Text>
                        </Button>
                        {
                            storeletButtonText?<Button style={{ marginLeft:api.getRealDeviceWidth()*.2,borderRadius:5,backgroundColor:'#fa6428' }} onPress={storeletHandle}>
                                                    <Text style={{ paddingVertical:5,paddingHorizontal:30,color:'#fff' }}>{storeletButtonText}</Text>
                                                </Button>:null
                        }
                        </View>
                    </View>
                </Modal>
            );
        }
        return null

    }
}
const styles = StyleSheet.create({
    button_bottom: {
        borderBottomLeftRadius: 2,
        borderBottomRightRadius: 2
    },
    backdrop: {
        zIndex: 1000,
        flex: 1
    },
    modal: {
        marginTop: api.getRealDeviceHeight() / 3,
        maxWidth: 500,
        padding: 10,
        overflow: 'hidden',
        alignSelf:'center'
    },
    modal_content: {
        backgroundColor: 'white',
        borderRadius: 2,
        flex: 1,
        alignSelf:'center'
    },
    modal_title: {
        borderBottomWidth: 1,
        borderBottomColor: '#ddd',
        padding: 5,
        paddingBottom: 10,
        textAlign: 'center',
        fontWeight:'bold'
    },
    modal_text_content: {
        padding: 10,
        flex: 1
    },
    button_container: {
        flex: 1,
        height: 50
    },
    buttonText:{
        fontSize:Platform.OS == 'ios'?13:15,
        color:'#fff'
    } 
});
mapStateToProps=(state)=>({uiState:state.uiState,per:state.giftCountState.Percent})
mapDispatchToProps=(dispatch)=>({})
export default connect(mapStateToProps, mapDispatchToProps)(ConfirmBox)