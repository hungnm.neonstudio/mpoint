import React, { Component, PropTypes } from 'react';
import {
    ActivityIndicator,
    StyleSheet,
    View,
    Text,
    Modal, Platform,
    TouchableOpacity, Image,WebView
} from 'react-native';
import { Container, Content, Button, Footer, FooterTab } from 'native-base'
import { connect } from 'react-redux'
import api from '../../api'
import * as Animatable from 'react-native-animatable';
import i18 from '../../components/i18'
import WebViewAndroid from 'react-native-dmgapp-webview-android'

// import sharedStyles from '../style'
class ConfirmBox extends Component {
    constructor(props) {
        super(props);
    }
    // onNavigationStateChange = ({ url }) => {
    //     // console.log(url)
    //     if(url.indexOf('http')==-1) {
    //         // this.refs.web.stopLoading();
    //         // this.refs.web.goBack();
    //         // api.hideWebview();
    //         return false;
    //     }
    //     if (url.indexOf('close') != -1) return api.hideWebview();
    //     // return this.props.uiState.webviewData.onRedirect('http://homi.cn/success?token=123');
    //     return this.props.uiState.webviewData.onRedirect(url);
    // }
    onShouldStartLoadWithRequest=({url})=>{
        url = decodeURIComponent(url);
        console.log(url)
        if (url.indexOf('close') != -1) return api.hideWebview();
        if(url.indexOf('http')==-1){
            this.props.uiState.webviewData.onRedirect(url);
            return false;
        }
        return true;
    }
    render() {
        let { showWebview, webviewData } = this.props.uiState;
        if (showWebview) {
            return (
                <Modal
                    transparent={true}
                    visible={true}
                    animationType='slide'
                    onRequestClose={() => { }}>
                    <WebView
                        ref='web'
                        source={{uri: webviewData.url}}
                        //onNavigationStateChange={this.onNavigationStateChange.bind(this)}
                        style={{ backgroundColor:'rgba(0,0,0,.5)',height:api.getRealDeviceHeight(),width:api.getRealDeviceWidth() }}
                        onShouldStartLoadWithRequest={this.onShouldStartLoadWithRequest}
                        //style={{marginTop: 20}}
                    />
                    {/* <WebViewAndroid
                        ref="WebViewAndroidDmgApp"
                        style={{ backgroundColor: 'transparent', height: api.getRealDeviceHeight(), width: api.getRealDeviceWidth() }}
                        javaScriptEnabled={true}
                        geolocationEnabled={false}
                        builtInZoomControls={true}
                        supportZoom={true}
                        loadWithOverviewMode={true}
                        useWideViewPort={true}
                        onNavigationStateChange={this.onNavigationStateChange.bind(this)}
                        url={webviewData.url} // or use the source(object) attribute... 
                    /> */}
                    {/* <Text style={{ fontWeight:'300',textAlign:'center',marginTop:(api.getRealDeviceHeight()/1.7) ,color:'#000',position:'absolute',zIndex:1000 }}>
                        Vui lòng nhấn tem vào màn hình để xác thực
                    </Text> */}
                </Modal>
            );
        }
        return null

    }
}
const styles = StyleSheet.create({
    button_bottom: {
        borderBottomLeftRadius: 2,
        borderBottomRightRadius: 2
    },
    backdrop: {
        zIndex: 1000,
        flex: 1
    },
    modal: {
        marginTop: api.getRealDeviceHeight() / 3,
        maxWidth: 500,
        padding: 10,
        overflow: 'hidden',
        alignSelf: 'center'
    },
    modal_content: {
        backgroundColor: 'white',
        borderRadius: 2,
        flex: 1,
        alignSelf: 'center'
    },
    modal_title: {
        borderBottomWidth: 1,
        borderBottomColor: '#ddd',
        padding: 5,
        paddingBottom: 10,
        textAlign: 'center',
        fontWeight: 'bold'
    },
    modal_text_content: {
        padding: 10,
        flex: 1
    },
    button_container: {
        flex: 1,
        height: 50
    },
    buttonText: {
        fontSize: Platform.OS == 'ios' ? 13 : 15,
        color: '#fff'
    }
});
mapStateToProps = (state) => ({ uiState: state.uiState })
mapDispatchToProps = (dispatch) => ({})
export default connect(mapStateToProps, mapDispatchToProps)(ConfirmBox)