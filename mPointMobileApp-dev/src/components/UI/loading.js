import React, {Component, PropTypes} from 'react';
import {ActivityIndicator, StyleSheet, View} from 'react-native';
import {connect} from 'react-redux'
import api from '../../api'

class Loading extends Component {
    static propTypes = {
        show: PropTypes.bool.isRequired
    }
    constructor(props) {
        super(props);
    }

    render() {
        if (this.props.show) {
            return (<ActivityIndicator animating={true} style={[styles.centering]} size="large"/>);
        }
        return null
    }
}
const styles = StyleSheet.create({
    centering: {
        alignItems: 'center',
        justifyContent: 'center',
        padding: 8,
        backgroundColor: 'rgba(0,0,0,0.5)',
        position: 'absolute',
        top: 0,
        zIndex: 10000,
        flex: 1,
        width: api.getRealDeviceWidth(),
        height: api.getRealDeviceHeight() + 50
    },
    gray: {
        backgroundColor: 'red'
    },
    horizontal: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        padding: 8
    }
});
export default Loading