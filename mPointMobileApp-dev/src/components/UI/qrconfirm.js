import React, { Component, PropTypes } from 'react';
import {
    ActivityIndicator,
    StyleSheet,
    View,
    Text,
    Modal,
    TouchableHighlight,
    Image, Platform
} from 'react-native';
import { Container, Content, Button, Footer, FooterTab } from 'native-base'
import { connect } from 'react-redux'
import api from '../../api'
import { showDoisoat, hideDoiSoat } from '../../actions/uiActions'
import dataService from '../../api/dataService'
// import sharedStyles from '../style'
import QRCode from 'react-native-qrcode';
import i18 from '../../components/i18';
import Barcode from 'react-native-barcode-builder';
import { stamp } from '../nativeModule';

class Qrconfirm extends Component {
    state = {
        cost: 0
    }
    static propTypes = {
        title: PropTypes.string.isRequired,
        content: PropTypes.string.isRequired,
        show: PropTypes.bool.isRequired,
        //onConfirm: PropTypes.func.isRequired
    }
    constructor(props) {
        super(props);
    }
    verifytoken = (token) => {
        console.log('====================================')
        console.log(token, token)
        console.log('====================================')
        if (token) {
            api.hideWebview();
            setTimeout(() => {
                api.showLoading();
            }, 10);
            dataService.verifyStampToken(this.props.promotionId, token, this.props.content, this.state.cost)
                .then(data => {
                    api.hideLoading();
                    if (data.err == 0) {
                        api.showStorelet(data.point ? data.point + '\n' + i18.t('point') : this.props.per ? this.props.per + '%' : '', data.msg)
                        dataService.getGiftCount()
                            .then((rs) => {
                                api.setGitfCount(rs.err == 0 ? rs.amount : '0')
                            })
                        dataService.getUserInfo()
                            .then((o => {
                                if (o.err == 0)
                                    api.setUserInfo(o)
                            }))
                    } else if (data.err == 11) {
                        api.showStorelet(null, data.msg, null, null, true)
                    }
                    else {
                        api.showMessage(data.msg, i18.t('titleMsgLogin'))
                    }
                    if (this.props.functionRefresh != undefined && (data.err == 0 || data.err == 11 || data.err == 4)) this.props.functionRefresh()

                })
        }
    }
    getStampInfo = () => {
        setTimeout(() => {
            api.showLoading();
            // stamp.setDescription('Vui lòng nhấn tem vào màn hình để xác thực');
            dataService.getStampInfo(this.props.content)
                .then(rs => {
                    if (rs.err == 0) {
                        stamp.getEchosserviceUrl(rs.regionCode, rs.merchantId, rs.id, rs.auk, rs.merchantId, url => {
                            api.hideLoading();
                            api.showWebview(url, (url) => {
                                console.log('====================================')
                                console.log(url)
                                console.log('====================================')
                                let i = url.indexOf('onstamp');
                                if (i > 0) {
                                    url = url.slice(url.indexOf('?') + 1, url.length);
                                    let payload = JSON.parse(url);
                                    console.log('====================================')
                                    console.log(payload)
                                    console.log('====================================')
                                    this.verifytoken(payload.token)
                                }
                            })
                        }, err => {
                            alert(err)
                        })
                    }
                })
        }, 10);
    }
    onPress = (msg) => {
        console.log(msg)
        if (!msg) api.hideQR();
        setTimeout(() => {
            if (this.props.promotionTypeId == 7) {
                let c = 0;
                api.showInputBox('', i18.t('fillInfo'), msg || i18.t('fillTotalBill'), (cost) => {
                    try {
                        c = parseInt(cost);
                        this.setState({
                            cost: c
                        });
                        this.getStampInfo();
                    } catch (error) {
                        this.onPress(i18.t('invalidBill'));
                    }
                })
            }
            else {
                this.getStampInfo();
            }
        }, 50)
    }
    renderCode = ()=>{
        if(!this.props.content) return null;
        let code = [];
        code = this.props.content.split('');
        return (
            <View style={{ alignSelf:'center',flexDirection:'row',marginTop:5 }}>
                {
                    code.map((el,index)=>{
                        return <Text key={index} style={{ color: '#000',paddingHorizontal:2, fontSize: 15, fontWeight: '500' }}>{el}</Text>
                    })
                }
            </View>
        )
    }
    render() {
        if (this.props.show) {
            return (
                <Modal
                    transparent={true}
                    visible={true}
                    animationType='fade'
                    onRequestClose={() => { }}>
                    <Container
                        style={{
                            flex: 1,
                            backgroundColor: 'rgba(0, 0, 0, 0.52);'
                        }}>
                        <Content>
                            <View style={styles.backdrop}>
                                <View style={styles.modal}>
                                    <View style={styles.modal_content}>
                                        <Text style={styles.modal_title}>{this.props.title}</Text>
                                        {
                                            Platform.OS == 'ios' ? <View style={{ height: 1, flex:1, backgroundColor: '#cdcdcd' }}>

                                            </View> : null
                                        }
                                        <View
                                            style={{
                                                marginTop: 0,
                                                alignItems: 'center'
                                            }}>
                                            <View style={{ padding: 5, paddingBottom: 0 }}>
                                                <Text style={{ textAlign: 'center', padding: 5, fontSize: 16 }}> {i18.t('code')} Barcode</Text>
                                                <Barcode value={this.props.content} format="CODE128" height={50} />
                                            </View>
                                            <Text style={{ fontSize: 16, paddingVertical: 5 }}>{i18.t('code')} QR code</Text>
                                            <QRCode size={api.getRealDeviceWidth() / 2}
                                                value={this.props.content}
                                                bgColor='#000'
                                                fgColor='#fff' />
                                            {this.renderCode()}
                                        </View>
                                        <Text style={{ marginHorizontal: 10 }}>{i18.t('selectVerifyMethod')}:</Text>
                                        <View style={{ flexDirection: 'row', marginHorizontal: 10 }}>
                                            <Button
                                                onPress={() => this.onPress('')}
                                                style={{ height:30,flex: 1, backgroundColor: '#fa6428', marginBottom: 3, padding: 0 }}
                                            >{i18.t('viaStamp')}
                                            </Button>
                                            <Button
                                                onPress={() => { api.showDoisoat(this.props.promotionId, i18.t('checkcodeTitle'), this.props.content, this.props.receive, this.props.functionRefresh); api.hideQR() }}
                                                style={{
                                                    height:30,
                                                    flex: 1,
                                                    marginLeft: 10,
                                                    backgroundColor: '#fa6428',
                                                    padding: 0
                                                }}>{i18.t('viaAccount')}
                                            </Button>
                                        </View>

                                        <Button block
                                            onPress={() => api.hideQR()}
                                            style={{
                                                backgroundColor: '#cacaca',
                                                marginHorizontal: 10,
                                                marginBottom: 5
                                            }}>{i18.t('close')}
                                        </Button>
                                    </View>
                                </View>

                            </View>

                        </Content>
                    </Container>
                </Modal>
            );
        }
        return null

    }
}
const styles = StyleSheet.create({
    button_bottom: {
        borderBottomLeftRadius: 2,
        borderBottomRightRadius: 2
    },
    backdrop: {
        flex: 1
    },
    modal: {
        marginTop: 20,
        maxWidth: 500,

        borderRadius: 5, width: api.getRealDeviceWidth() * 0.8,
        overflow: 'hidden',
        alignSelf: 'center'
    },
    modal_content: {
        backgroundColor: 'white',
        borderRadius: 2,
        flex: 1
    },
    modal_title: {
        borderBottomWidth: 1,
        borderBottomColor: '#ddd',
        padding: 5,
        paddingVertical: 10,
        textAlign: 'center'
    },
    modal_text_content: {
        marginTop: 10,
        marginBottom: 0,
        padding: 10,
        fontSize: 16,
        flex: 1
    },
    button_container: {
        flex: 1,
        height: 50
    }
});
mapStateToProps = (state) => (state)
mapDispatchToProps = (dispath) => ({
    hideDoisoat: () => dispath(hideDoiSoat()),
})
export default connect(mapStateToProps, mapDispatchToProps)(Qrconfirm)