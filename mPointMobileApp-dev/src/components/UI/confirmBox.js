import React, { Component, PropTypes } from 'react';
import {
    ActivityIndicator,
    StyleSheet,
    View,
    Text,
    Modal, Platform,
    TouchableHighlight, Image
} from 'react-native';
import { Container, Content, Button, Footer, FooterTab } from 'native-base'
import { connect } from 'react-redux'
import api from '../../api'
import * as Animatable from 'react-native-animatable';

// import sharedStyles from '../style'
class ConfirmBox extends Component {
    static propTypes = {
        title: PropTypes.string.isRequired,
        content: PropTypes.string.isRequired,
        show: PropTypes.bool.isRequired,
        onOk: PropTypes.func.isRequired,
        onCancel: PropTypes.func.isRequired,
        confirmOkText: PropTypes.string.isRequired,
        confirmCancelText: PropTypes.string.isRequired
    }
    constructor(props) {
        super(props);
    }

    render() {
        if (this.props.show) {
            return (
                <Modal
                    transparent={true}
                    visible={true}
                    animationType='fade'
                    onRequestClose={() => { }}>
                    <Container
                        style={{
                            flex: 1,
                            backgroundColor: 'rgba(0, 0, 0, 0.52);'
                        }}>
                        <Content
                            keyboardShouldPersistTaps={'handled'}
                        >
                            <View style={styles.backdrop}>
                                <View style={styles.modal}>
                                    <View style={styles.modal_content}>
                                        <Text style={styles.modal_title}>{this.props.title}</Text>
                                        {
                                            Platform.OS == 'ios' ? <View style={{ height: 1, flex: 1, backgroundColor: '#cdcdcd' }}>

                                            </View> : null
                                        }
                                        <Text style={styles.modal_text_content}>{this.props.content}</Text>
                                        <View
                                            style={{
                                                flexDirection: 'row'
                                            }}>
                                            <Button
                                                block
                                                style={{
                                                    flex: 1,
                                                    backgroundColor: '#fa6428',
                                                    marginBottom: 20,
                                                    marginLeft: 10,

                                                }}
                                                type='button'
                                                onPress={this.props.onOk}><Text style={styles.buttonText}>{this.props.confirmOkText}</Text></Button>

                                            {
                                                this.props.centerButtonContent != undefined && this.props.centerButtonContent != null && this.props.centerButtonContent != '' ?
                                                    <Button block
                                                        style={{
                                                            flex: 1,
                                                            marginLeft: 5,
                                                            marginBottom: 20,
                                                            backgroundColor: '#888',
                                                        }}
                                                        onPress={this.props.centerButtonFunc}
                                                    ><Text style={styles.buttonText}>{this.props.centerButtonContent}</Text></Button> : null
                                            }
                                            <Button
                                                block
                                                style={{

                                                    marginRight: 10,
                                                    marginLeft: 5,
                                                    flex: 1,
                                                    backgroundColor: '#cacaca',
                                                    marginBottom: 20
                                                }}
                                                type='button'
                                                onPress={this.props.onCancel}><Text style={styles.buttonText}>{this.props.confirmCancelText}</Text></Button>
                                        </View>
                                    </View>
                                </View>

                            </View>

                        </Content>
                    </Container>
                    {/*<View style ={{ zIndex:-1,height:api.getRealDeviceHeight(),width:api.getRealDeviceWidth(),position:'absolute',backgroundColor: 'rgba(0, 0, 0, 0.52);' }}>
                    <View style={{ position:'absolute',height:api.getRealDeviceHeight() }}>
                        <View style={{ flex:1 }}/>
                        <Animatable.Image animation = 'rotate' easing='linear' direction='normal' duration={3000} iterationCount={110500} source ={require('../../img/quay.png')} style = {{  height:api.getRealDeviceWidth(),width:api.getRealDeviceWidth()}}/>
                        <View style={{ flex:1 }}/>
                    </View>
                    <View style={{ position:'absolute',height:api.getRealDeviceHeight() }}>
                        <View style={{ flex:1 }}/>
                        <Animatable.Image animation = 'rotate' easing='linear' direction='reverse' duration={3000} iterationCount={501105000} source ={require('../../img/quay.png')} style = {{ position: 'absolute', height:api.getRealDeviceWidth(),width:api.getRealDeviceWidth(),marginTop:api.getRealDeviceHeight()*0.2 }}/>
                        <View style={{ flex:1 }}/>
                    </View>
                    <View style={{ flex:1 }}/>
                    <Image source ={require('../../img/center.png')} style = {{ height:api.getRealDeviceWidth()/3.5,width:api.getRealDeviceWidth()/3.5,alignSelf:'center' }}/>
                    <View style={{ flex:1 }}/>
               </View>*/}
                </Modal>
            );
        }
        return null

    }
}
const styles = StyleSheet.create({
    button_bottom: {
        borderBottomLeftRadius: 2,
        borderBottomRightRadius: 2
    },
    backdrop: {
        zIndex: 1000,
        flex: 1,
    },
    modal: {
        marginTop: api.getRealDeviceHeight() / 3,
        maxWidth: 700,
        overflow: 'hidden',
        alignSelf: 'center',
        width: api.getRealDeviceWidth() * .9,
        borderRadius: 5
    },
    modal_content: {
        backgroundColor: 'white',
        borderRadius: 2,
        flex: 1,
    },
    modal_title: {
        borderBottomWidth: 1,
        borderBottomColor: '#ddd',
        padding: 5,
        paddingBottom: 10,
        textAlign: 'center',
        fontWeight: 'bold'
    },
    modal_text_content: {
        padding: 10,
        flex: 1,
        textAlign: 'center',
        fontSize: 14
    },
    button_container: {
        flex: 1,
        height: 50
    },
    buttonText: {
        fontSize: 13,
        color: '#fff'
    }
});
export default ConfirmBox