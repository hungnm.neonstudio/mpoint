import {
    Container,
    Header,
    Content,
    Button,
    Icon,
    Title,
    Card,
    Text,
    Input,
    InputGroup,
    CardItem,
    ListItem
} from 'native-base'
import { setUserInfo, setListMember } from '../actions/userActions'
import React, { Component } from 'react'
import { Image, ListView, View, TouchableOpacity, ScrollView, ActivityIndicator, Platform, TextInput } from 'react-native'
import { connect } from 'react-redux';
import { pop } from '../actions/navActions';
import dataService from '../api/dataService';
import api from '../api';
import MyCardItem from '../components/tab/cardItem';
import i18 from '../components/i18';
let CardID = ''
let uuid = ''
let phone = ''
let otpID = ''
let token = ''
let type = '';
class RegCard extends Component {
    constructor(props) {
        super(props);
        phone = this.props.navState.routes[this.props.navState.index].phone
        uuid = this.props.userState.uuid
        this.state = {
            members: [],
            memberClose: [],
            memberGroup: [],
            isLoading: true,
            card: this.props.navState.routes[this.props.navState.index].card || ''
        }
    }
    ds = new ListView.DataSource({
        rowHasChanged: (r1, r2) => r1 !== r2
    });
    componentWillMount() {
        dataService.listAvaiableMembers(phone || 0)
            .then(rs => {
                if (rs.err == 0) {
                    this.setState({ members: rs.members, isloadding: false })
                }
                else {
                    api.showMessage(rs.msg)
                }
            })
        // if (phone == '' || phone == undefined) 
        dataService.listAvaiableMembers('0')
            .then(rs => {
                if (rs.err == 0) {
                    this.setState({ memberClose: rs.memberClose, memberGroup: rs.memberGroup, isloadding: false })
                }
                else {
                    api.showMessage(rs.msg)
                }
            })
    }
    selectCard = ({ cardId }) => {
        // alert(cardId)
        api.showLoading();
        dataService.changeCard(undefined, cardId).then(rs => {
            api.hideLoading();
            if (rs.err == 0) {
                this.gotoUpdateUserInfo();
            } else {
                api.showMessage(rs.msg);
            } 
        })
    }
    gotoUpdateUserInfo = () => {
        api.showConfirm(i18.t('contentConfirmBNA'), i18.t('titleMsgLogin'), i18.t('confirmOKLogin'), i18.t('confirmCancelTextLogin'),
            function onok() {
                api.push({ key: 'RegUser' });
                api.hideConfirm()
            },
            function oncancel() {
                api.gotoHome();
                api.hideConfirm();
            }
        )
    }
    renderInputCode = () => {
        if (!this.props.navState.routes[this.props.navState.index].phone) return null;
        return (
            <View style={{ paddingHorizontal: 5 }}>
                <Text style={{ color: 'white' }}>
                    {i18.t('input_card_title')}
                </Text>
                <View style={{ marginTop: 10, flexDirection: 'row', backgroundColor: 'white', borderRadius: 3, justifyContent: 'center' }}>
                    <TextInput
                        underlineColorAndroid='transparent'
                        style={{ padding: 0, margin: 0, flex: 1, paddingHorizontal: 10 }}
                        onChangeText={(t) => { CardID = t }}
                        onSubmitEditing={() => {
                            type = 'code';
                            this._reg();
                        }}
                    />
                    <TouchableOpacity
                        onPress={() => {
                            type = 'code';
                            this._reg();
                        }}
                        activeOpacity={0.8}
                        style={{ alignItems: 'center', paddingHorizontal: 5, backgroundColor: '#fa6428', flexDirection: 'row', borderTopRightRadius: 3, borderBottomRightRadius: 3 }}
                    >
                        <Text style={{ color: '#fff', paddingBottom: 2 }}>
                            Tiếp tục
                        </Text>
                        <Icon name='md-arrow-forward' style={{ marginLeft: 10, color: 'white' }} />
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
    _renderOpen(arr, type) {
        if (type == 2) {
            return (
                <View
                    style={{ paddingBottom: 10 }}
                >
                    <ListView
                        enableEmptySections={true}
                        dataSource={this.ds.cloneWithRows(arr)}
                        renderRow={(item, sectionID, id) => {
                            console.log(item.banner)
                            return (
                                <TouchableOpacity
                                    key={id}
                                    style={{
                                        alignSelf: 'center',
                                        borderRadius: 10
                                    }}
                                    onPress={() => {
                                        api.push({ key: 'bna', item: item, partners: item.partners, phone: phone })
                                    }}><Image
                                        source={{
                                            uri: item.banner
                                        }}
                                        style={{
                                            borderRadius: 5,
                                            width: api.getRealDeviceWidth() - 10,
                                            height: (api.getRealDeviceWidth() - 10) / 5.7,
                                            resizeMode: 'stretch',
                                            marginVertical: 5
                                        }} />
                                </TouchableOpacity>
                            );
                        }}
                    />
                </View>
            );
        }
        return (
            <View
                style={{ paddingBottom: 10, width: api.getRealDeviceWidth() }}
            >
                <ListView
                    enableEmptySections={true}
                    dataSource={this.ds.cloneWithRows(arr)}
                    contentContainerStyle={{
                        flexDirection: 'row',
                        flexWrap: 'wrap',
                        alignItems: 'stretch'
                    }}
                    renderRow={(item, sectionID, id) => (
                        <TouchableOpacity
                            key={id}
                            style={{
                                alignSelf: 'center'
                            }}
                            onPress={() => {
                                {/*if(item.id == 123){
                                 api
                                .push({key: 'viettel', id: item.id, card: item.card, detail: item.detail, name: item.name})
                                }else
                                if( item.id == 354){
                                    api
                                .push({key: 'mpoint', id: item.id, card: item.card, detail: item.detail, name: item.name})
                                }*/}
                                if (type) {
                                    this.selectCard({ cardId: item.memberCards[0].id })
                                    {/* if (item.id == 123) api.push({ key: 'viettel', item: item, phone: this.props.navState.routes[this.props.navState.index].phone })
                                    if (item.id == 354) api.push({ key: 'mpoint', item: item, phone: this.props.navState.routes[this.props.navState.index].phone }) */}
                                }
                                else
                                    api.push({ key: 'addCardFromLogin', item: item, phone: this.props.navState.routes[this.props.navState.index].phone })
                                console.log(item)
                            }}><Image
                                source={{
                                    uri: item.card
                                }}
                                style={{
                                    width: (api.getRealDeviceWidth() - 25) / 2,
                                    height: (api.getRealDeviceWidth() - 25) / 3,
                                    resizeMode: 'stretch',
                                    margin: 5,
                                    borderRadius: 10
                                }}
                            /></TouchableOpacity>
                    )}
                />
            </View>
        )
    }
    _onRegConfirmOTP(otp) {

        if (otp == undefined || otp == '') {
            api.showMessage(i18.t('errOtpMsgLogin'))
            return
        }
        api.hideInputBox()
        setTimeout(() => api.showLoading(), 1)
        dataService.postVerifyOtpRegister(phone, otp, otpID)
            .then((rs) => {
                if (rs.err == 0) {
                    token = rs.token
                    api.setUserToken(rs.token)
                    dataService.getListShopGPS(rs.token)
                        .then(gps => {
                            api.setLocationData(gps.shops)
                        })
                    dataService
                        .postAddDevice(uuid, uuid, rs.token)
                        .then(rs => { })
                    try {
                        AsyncStorage.setItem('@RFTK:key', rs.refreshToken);
                        // alert(dt.refreshToken)
                    } catch (error) {
                        console.log('Error when saving data', error);
                    }
                    if (type == 'code') {
                        dataService
                            .postAddCard(CardID, token)
                            .then(rs => {
                                // api.hideLoading()
                                // api.showMessage(rs.msg, 'Thông báo')
                                if (rs.err == 0) {
                                    dataService.getGiftCount(token)
                                        .then((rs) => {
                                            api.setGitfCount(rs.err == 0 ? rs.amount : '0')
                                        })
                                    dataService
                                        .getUserInfo(token)
                                        .then((datas) => {
                                            console.log('datas', datas);
                                            if (datas.err == 0) {
                                                dataService
                                                    .postAddDevice(uuid, uuid, token)
                                                    .then(rs => { })
                                                dataService
                                                    .getListMember(token, 1, 0)
                                                    .then((data) => {

                                                        api.setUserInfo(datas)
                                                        console.log('data after toggle', data);
                                                        api.setListMember(data.members);
                                                        api.hideLoading();
                                                        api.showConfirm(i18.t('contentConfirmBNA'), i18.t('titleConfirmBNA'), i18.t('confirmOKLogin'), i18.t('confirmCancelTextLogin'),
                                                            function onok() {
                                                                api.push({ key: 'RegUser' })
                                                                api.hideConfirm()
                                                            },
                                                            function oncancel() {
                                                                api.showLoading()
                                                                api.resetRoute({ key: 'home' })
                                                                api.hideConfirm()
                                                            }
                                                        )
                                                        api.hideLoading()

                                                    })

                                                // api.push({key:'home'})
                                            }
                                        })

                                }
                            })
                    } else {
                        dataService
                            .postRegisterFreeCard(CardID, token)
                            .then((rs) => {
                                api.showMessage(rs.msg)
                                if (rs.err == 0) {
                                    dataService
                                        .getUserInfo(token)
                                        .then((datas) => {
                                            console.log('datas', datas);
                                            if (datas.err === 0) {
                                                dataService
                                                    .getListMember(token, 1, 0)
                                                    .then((data) => {
                                                        api.setUserInfo(datas)

                                                        console.log('data after toggle', data);
                                                        api.setListMember(data.members);
                                                        api.resetRoute({ key: 'home' })

                                                    })
                                                dataService
                                                    .postAddDevice(uuid, uuid, token)
                                                    .then(rs => { })
                                            }
                                        })
                                }
                            })
                    }

                    try {
                        AsyncStorage.setItem('@RFTK:key', rs.refreshToken);
                        // alert(dt.refreshToken)
                    } catch (error) {
                        console.log('Error when saving data', error);
                    }

                } else if (rs.err == 2) {
                    // api.showMessage(rs.msg,'Thông báo')
                    api.showInputBox(undefined, undefined, rs.msg, this._onConfirmOTP)
                    api.hideLoading()
                }
            })
    }
    _reg() {
        if (type == 'code') {
            if (!CardID) return api.showMessage(i18.t('blankCodeAddPcodepage'))
            api.showLoading();
            dataService.postAddCard(CardID)
                .then(rs => {
                    api.hideLoading();
                    if (rs.err == 0) {
                        this.gotoUpdateUserInfo();
                        CardID = '';
                    } else {
                        api.showMessage(rs.msg);
                    }
                })
        }
        else {
            api.showLoading();
            dataService.postReg(phone)
                .then(rs => {
                    api.hideLoading()
                    if (rs.err == 0) {
                        otpID = rs.otpId
                        api.showInputBox(i18.t('otpTitle'), i18.t('otpTitle'), i18.t('contentLeftInputLogin') + phone + i18.t('contentRightInputLogin'), this._onRegConfirmOTP)
                    }
                    else {
                        api.showMessage(rs.msg)
                    }
                })
        }

    }


    _renderListMembers() {
        // alert(this.state.listMembers.length)
        if (this.state.listMembers.length > 0) {
            return (
                this.state.listMembers.map((item, id) =>
                    <TouchableOpacity
                        key={id}
                        style={{ alignSelf: 'center' }}
                        onPress={() => {
                            CardID = item.id
                            type = ''
                            this._reg()
                        }}><Image
                            source={{
                                uri: item.card
                            }}
                            style={{
                                width: api.getRealDeviceWidth() / 2.3,
                                height: api.getRealDeviceWidth() / 3.3,
                                resizeMode: 'stretch',
                                margin: 5,
                            }} /></TouchableOpacity>
                )
            )
        }
        else {
            return null
        }
    }
    componentDidMount() {
        api.hideLoading()
    }
    _onSelect(idCard) {
        api.showLoading();
        dataService.postAddCard(cardID).then(rs => {
            api.hideLoading();
            if (rs.err == 0) {
                this.gotoUpdateUserInfo();
            } else {
                api.showMessage(rs.msg);
            }

        });
    }
    renderListCard() {
        let { phone } = this.props.navState.routes[this.props.navState.index];
        if (phone) return null;
        let minus = Platform.OS == 'ios' ? 0 : 4
        let fontSize = api.getRealDeviceWidth() > 320 ? 13 : 12;
        let marginTop = api.getRealDeviceWidth() < 320 ? -8 - minus : 0 - minus;
        let imgHeight = api.getRealDeviceWidth() > 320 ? 25 : 23;
        return <View>
            {this.renderInputCode()}
            {(this.state.memberClose.length < 1 && !this.state.isLoading) ? null : <Image
                source={require('../img/bg.png')}
                style={{
                    marginTop: 30,
                    width: imgHeight * 13.7, alignSelf: 'center',
                    resizeMode: 'stretch',
                    marginBottom: 5,
                    height: imgHeight
                }}
            >
                <Text style={{ marginTop: marginTop, color: '#fff', alignSelf: 'center', fontWeight: '400', fontSize: fontSize }}>{i18.t('cardOfBusiness')}</Text>
            </Image>}
            {this.state.memberGroup.length < 1 && this.state.isLoading ? <ActivityIndicator size='large' style={{ alignSelf: 'center' }} /> : null}
            {this._renderOpen(this.state.memberGroup, 2)}
            {/*<Text style={{ textAlign:'center',color:'gray',paddingTop:10,borderTopColor:'#cdcdcd',borderTopWidth:1,paddingVertical:10 }}>Thẻ ưu đãi đăng ký bằng mã thẻ</Text>*/}
            {this.state.memberGroup.length < 1 && !this.state.isLoading ? null : <Image
                source={require('../img/bg.png')}
                style={{
                    width: imgHeight * 13.95, alignSelf: 'center',
                    resizeMode: 'stretch',
                    marginBottom: 5,
                    height: imgHeight
                }}
            >
                <Text style={{ marginTop: marginTop, color: '#fff', alignSelf: 'center', fontWeight: '400', fontSize: fontSize }}>{i18.t('regByCard')}</Text>
            </Image>
            }
            {this.state.memberClose.length < 1 && this.state.isLoading && this.props.navState.routes[this.props.navState.index].phone == undefined ? <ActivityIndicator size='large' style={{ alignSelf: 'center' }} /> : null}
            {this._renderOpen(this.state.memberClose, false)}
        </View>
    }
    render() {

        let { phone } = this.props.navState.routes[this.props.navState.index];
        let listCardName = `${i18.t('card')} mPoint`;
        if (api.isViettel(phone)) {
            listCardName = `${i18.t('card')} mPoint và thẻ Viettel`;
        };
        if (this.state.card) listCardName = `${i18.t('card')} ${this.state.card}, ${listCardName}`
        // listCardName = 'ví thẻ ưu đãi mPoint'
        return (
            <Container>
                <Header
                    style={{
                        backgroundColor: '#009688'
                    }}>
                    <Button
                        transparent
                        onPress={() => {
                            api.pop()
                        }}>
                        <Icon name='ios-arrow-back' style={{ color: Platform.OS == 'ios' ? '#000' : '#fff' }} />
                    </Button>
                    <Title style={{ alignSelf: 'center' }}>{i18.t('selectCardToUse')}</Title>
                </Header>

                <Image
                    source={require('../img/bg_login.jpg')}
                    style={{ width: api.getRealDeviceWidth(), height: api.getRealDeviceHeight() - 60 }}
                ><Content
                    keyboardShouldPersistTaps={'handled'}
                    style={{
                        width: api.getRealDeviceWidth()
                    }}>
                        {/* 
                        <View>
                            <Image
                                style={{
                                    height: api.getRealDeviceWidth() / 5,
                                    width: api.getRealDeviceWidth() / 5 * 2.13,
                                    alignSelf: 'center',
                                    marginTop: 10
                                }}
                                source={require('../img/logo_deperecate.png')} />
                            <Text
                                style={{
                                    color: '#fff',
                                    marginTop: 5,
                                    alignSelf: 'center',
                                    fontWeight: '500',
                                    marginBottom: 10,
                                }}>{i18.t('headLogin')}</Text>
                        </View> */}
                        {/* <Text style={{ textAlign:'center',paddingHorizontal:10,color:"#fff",backgroundColor:'transparent',marginBottom:10 }}>Chúc mừng bạn đăng ký thành công {listCardName} với số điện thoại {phone} mời bạn chọn thẻ để sử dụng</Text> */}
                        {/* <Image
                            source={require('../img/bg.png')}
                            style={{
                                width: imgHeight * 13.95,
                                alignSelf: 'center',
                                resizeMode: 'stretch',
                                height: imgHeight,
                                marginVertical: 5,
                                flexDirection: 'column'
                            }}
                        >
                            <Text style={{ marginTop: marginTop, color: '#fff', alignSelf: 'center', fontWeight: '400', fontSize: fontSize }}>{i18.t('regByphone')}</Text>
                        </Image> */}
                        <Text style={{ textAlign: 'center', color: '#fff',fontSize:17 }}>Vui lòng chọn các thẻ miễn phí sau để sử dụng</Text>
                        {this.state.members.length < 1 && this.state.isLoading ? <ActivityIndicator size='large' style={{ alignSelf: 'center' }} /> : null}
                        {this._renderOpen(this.props.userState.listMember, true)}

                        {/*<Text style={{ textAlign:'center',color:'black',paddingTop:10,borderTopColor:'#cdcdcd',borderTopWidth:1,paddingVertical:10,fontWeight:'200' }}>
                        CỘNG ĐỒNG DOANH NGHIỆP
                    </Text>*/}



                    </Content>
                </Image>
            </Container>
        )
    }
}
mapState = (state) => {
    return { userState: state.userState, navState: state.navState, lang: state.langState.lang }
}
mapDispatch = (dispatch) => {
    return {
        pop: () => dispatch(pop()),
        setUserInfo: (data) => dispatch(setUserInfo(data)),
        setListMember: (listMember) => dispatch(setListMember(listMember))
    }
}
export default connect(mapState, mapDispatch)(RegCard);