import React, { Component } from 'react';
import { ScrollView, Text, ListView, Platform, AsyncStorage, ActivityIndicator, View, Image, TouchableOpacity, StyleSheet } from 'react-native';
import { connect } from 'react-redux';
import Swiper from 'react-native-swiper'
import api from '../api'
import i18 from './i18'
class Intro extends Component {
    constructor(props) {
        super(props);
        this.state = {
            index: 0,
            step: false
        }
    }

    continued() {
        if (this.state.index < 2) {
            this.setState({
                index: this.state.index + 1
            });
        }
        else {
            api.pop()
        }
    }
    prevIndex = null
    curIndex = 0
    scroll(e, state, context) {

        this.prevIndex = this.curIndex
        this.curIndex = state.index
        if (this.curIndex == 0 && this.prevIndex == 2) {
            api.pop()
        }
        this.setState({
            index: state.index
        })
    }

    gotoLogin() {
        try {
            AsyncStorage.setItem('first', 'first');
            api.resetRoute({ key: 'login' })
        } catch (error) {
            console.log('Error when saving data', error);
        }
    }
    // continued(){
    //     if(this.state.index<2){
    //         this.setState({
    //            index: this.state.index+1
    //         });
    //     }
    //     else{
    //         api.pop()
    //     }
    // }
    // prevIndex=null
    // curIndex = 0
    // scroll(e, state, context){

    //     this.prevIndex = this.curIndex
    //     this.curIndex = state.index
    //     if(this.curIndex == 0 && this.prevIndex == 2 ){
    //         api.pop()
    //     }
    //     this.setState({
    //         index:state.index
    //     })
    // }
    render() {
        let show = <View></View>
        show = <View style={{ height: api.getRealDeviceHeight(), width: api.getRealDeviceWidth() }}>
            <TouchableOpacity
                onPress={() => {
                    api.pop()
                }}
                style={{ paddingVertical: 5, paddingHorizontal: 20, borderRadius: 5, backgroundColor: '#e87515', zIndex: 1000, position: 'absolute', bottom: 10, left: 20 }}
            >
                <Text style={{ color: '#fff' }}>
                    {i18.t('close')}
                </Text>
            </TouchableOpacity>
            <TouchableOpacity
                onPress={api.pop}
                style={{ paddingVertical: 5, paddingHorizontal: 20, borderRadius: 5, backgroundColor: '#e87515', zIndex: 1000, position: 'absolute', bottom: 10, right: 20 }}
            >
                <Text style={{ color: '#fff' }}>
                    {i18.t('buttonTextLogin')}
                </Text>
            </TouchableOpacity>
            <Swiper
                height={api.getRealDeviceHeight()}
                width={api.getRealDeviceWidth()}
                //autoplay
                //autoplayTimeout={5}
                buttonWrapperStyle={{ backgroundColor: '#fff' }}
                showsButtons={true}
                //onMomentumScrollEnd={this.scroll.bind(this)}
                prevButton={<Text></Text>}
                // nextButton={<Text style={{ paddingVertical:5,paddingHorizontal:20,borderRadius:5,backgroundColor:'#e87515',color:'#fff'}}>Tiếp tục</Text>}
                dot={<View style={{ backgroundColor: 'rgba(50,50,50,.8)', width: 5, height: 5, borderRadius: 4, marginLeft: 3, marginRight: 3, marginTop: 3, marginBottom: 3 }} />}
                activeDot={<View style={{ backgroundColor: '#333', width: 8, height: 8, borderRadius: 4, marginLeft: 3, marginRight: 3, marginTop: 3, marginBottom: 3 }} />}
                paginationStyle={{
                    bottom: 5, alignSelf: 'center'
                }}
                buttonWrapperStyle={{ backgroundColor: 'transparent', flexDirection: 'row', position: 'absolute', top: api.getRealDeviceHeight() - 50, left: 0, paddingRight: 20, height: 50 }}
            >
                <Image source={require('../img/splash/1.jpg')}
                    style={{ height: api.getRealDeviceHeight(), width: api.getRealDeviceWidth(), resizeMode: 'stretch' }}
                />
                <Image source={require('../img/splash/2.jpg')}
                    style={{ height: api.getRealDeviceHeight(), width: api.getRealDeviceWidth(), resizeMode: 'stretch' }}
                />

                <Image source={require('../img/splash/3.jpg')}
                    style={{ height: api.getRealDeviceHeight(), width: api.getRealDeviceWidth(), resizeMode: 'stretch' }}
                />
            </Swiper>
        </View>
        if (this.state.step) {
            return show;
        }
        return (
            <ScrollView style={{ backgroundColor: '#fff', flex: 1, paddingTop: Platform.OS == 'ios' ? 10 : 0 }}>
                <View>
                    <Text style={{
                        color: 'rgb(0,150,136)', marginLeft: 0,
                        marginRight: 0, fontSize: 20, padding: 20, paddingBottom: 0, fontWeight: 'bold'
                    }}>
                        {i18.t('titleWel')}</Text>
                </View>
                <View style={{ justifyContent: 'center', paddingTop: api.getRealDeviceHeight() > 480 && Platform.OS != 'ios' ? 30 : 10 }}>
                    <View>
                        <Text style={styles.textV}>
                            {i18.t('titleCard')}
                        </Text>
                        <View style={{ flexDirection: 'row', marginLeft: 30, marginRight: 30, }}>
                            <Image style={styles.image} source={require('../img/vi_intro.png')} />
                            <Text style={styles.text}>{i18.t('titleIncle')}</Text>
                        </View>
                    </View>
                    <View style={{ justifyContent: 'center', paddingTop: api.getRealDeviceHeight() > 480 && Platform.OS != 'ios' ? 30 : 10 }}>

                        <Text style={styles.textV}>
                            {i18.t('titleMpoint')}
                        </Text>
                        <View style={{ flexDirection: 'row', marginLeft: 30, marginRight: 30, }}>
                            <Image style={styles.image} source={require('../img/the_intro.png')} />
                            <Text style={styles.text}>
                                {i18.t('titleGive')}
                            </Text>
                        </View>
                    </View>
                    <View style={{ justifyContent: 'center', paddingVertical: api.getRealDeviceHeight() > 480 && Platform.OS != 'ios' ? 30 : 10 }}>
                        <Text style={styles.textV}>
                            {i18.t('titleDiscount')}
                        </Text>
                        <View style={{ flexDirection: 'row', marginLeft: 30, marginRight: 30, }}>
                            <Image style={styles.image} source={require('../img/sale_intro.png')} />
                            <Text style={styles.text}>
                                {i18.t('titleOpen')}
                            </Text>
                        </View>
                    </View>
                </View>
                <View>
                    <Text style={{ color: 'rgb(54,54,54)', fontSize: 14, alignSelf: 'center', textAlign: 'center', marginLeft: 16, marginRight: 15, fontWeight: 'bold', fontStyle: 'italic', marginTop: 10 }}>
                        {i18.t('titleCap')}
                    </Text>
                    <Text style={{ color: 'rgb(54,54,54)', fontSize: 14, alignSelf: 'center', marginLeft: 20, marginRight: 20, fontWeight: 'bold', fontStyle: 'italic', marginBottom: 10 }}>
                        {i18.t('titleCap1')}
                    </Text>
                    <TouchableOpacity style={styles.btnT} onPress={() => { this.setState({ step: true }) }}>
                        <Text style={{ fontSize: 18, color: '#fff', justifyContent: 'center', alignSelf: 'center' }}>
                            {i18.t('buttonBegin')}
                        </Text>
                    </TouchableOpacity>
                </View>
            </ScrollView>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
    text: {
        color: 'rgb(12,12,12)',
        fontSize: 14,
        marginLeft: 30,
        marginRight: 30,
        lineHeight: Platform.OS == 'ios' ? 18 : 22
    },
    image: {
        width: 45, height: 48, resizeMode: 'contain'
    },
    btnT: {
        backgroundColor: 'rgb(255,87,34)',
        width: 273, height: 50, justifyContent: 'center',
        alignSelf: 'center', borderRadius: 25,
        marginVertical: Platform.OS == 'ios' ? 5 : 15
    },
    textV: {
        color: 'rgb(0,150,136)', fontWeight: 'bold', marginLeft: 105,
    }


});
const mapStateToProps = (state) => {
    return { navState: state.navState, userState: state.userState }
}
const mapDispatchToProp = (dispatch) => {
    return {

    }
}
export default connect(mapStateToProps, mapDispatchToProp)(Intro)