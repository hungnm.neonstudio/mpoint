import React, { Component } from 'react'
import {
  AppRegistry, View, Text, Platform
} from 'react-native';
import { Button } from 'native-base';
import { connect } from 'react-redux';
import { push, pop } from '../actions/navActions'
import api from '../api'
import i18 from '../components/i18'
import dataService from '../api/dataService';
import Camera from 'react-native-camera';
let flag = true
let token = ''
let functionrefresh = () => { }
let gotoUpdate = () => { }
class BarcodeScannerApp extends Component {
  constructor(props) {
    super(props);
    token = this.props.userState.token
    this.state = {
      torchMode: 'off',
      cameraType: 'back',
      code: '',
      type: ''
    };
  }
  componentDidMount() {
    api.sendAnalytic('scanCode');
    functionrefresh = functionrefresh = this.props.navState.routes[this.props.navState.index].refreshPromotion
    gotoUpdate = this.props.navState.routes[this.props.navState.index].gotoUpdateUserInfo
  }
  _showConfirm(rs) {
    co = false
    api.hideLoading()
    api.showConfirm(
      rs.mgreen.popup,
      i18.t('titleMsgLogin'),
      i18.t('buttonTextLogin'),
      i18.t('close'),
      function conti() {
        api.hideConfirm()
        api.showMgreen(rs)
      },
      function close() {
        api.hideConfirm()
      },
      rs.mgreen.booked != 1 ? undefined : i18.t('cancelOrder'),
      rs.mgreen.booked != 1 ? undefined : function cancelOrder() {
        let co = true
        dataService.mGreenSubmit(
          token,
          rs.promotion,
          2,
          rs.shopId,
          rs.partnerId,
          '',
          'this.state.bookedtime'
        ).then((rs) => {
          api.showMessage(rs.msg, i18.t('titleMsgLogin'))
          if (rs.err == 0) {
            api.hideConfirm()
          }
          co = false
        })
      }
    )
    dataService.getUserInfo(this.props.userState.token)
      .then(kq => {
        if (kq.err == 0)
          api.setUserInfo(kq)
      })
    functionrefresh()
    setTimeout(() => {
      if (co) {
        api.hideConfirm()
        api.showMessage(i18.t('timeoutTextMsgContentLogin'), i18.t('titleMsgLogin'))
      }
    }, 15000)
  }
  barcodeReceived(e) {
    if (this.props.navState.routes[this.props.navState.index].key != 'QRscan') return
    api.setPer('')
    if (flag) {
      flag = false
      api.pop()
      if ((e.data + '').substr(0, 4) == 'WEB_') return setTimeout(() => { flag = true, api.showLoginWebConfirm(e.data + '') }, 50)
      setTimeout(() => {
        api.showLoading()
      }, 1)
      setTimeout(() => {
        dataService.postScanQR(e.data, this.props.userState.token)
          .then(rs => {
            dataService.getUserInfo(token)
              .then(res => {
                if (res.err == 0) api.setUserInfo(res)
              })
            dataService.getGiftCount(token)
              .then((res) => {
                api.setGitfCount(res.err == 0 ? res.amount : '0')
              })
            flag = true;
            api.hideLoading();
            if (rs.err == 100) {
              if (rs.mgreen.err == 10) {
                api.hideLoading()
                return api.showConfirm(rs.mgreen.msg, i18.t('updateUserInfo'), i18.t('update'), i18.t('close'),
                  function Update() {
                    api.hideConfirm();
                    gotoUpdate();
                  },
                  function Close() {
                    api.hideConfirm();
                  })
              }
              if (rs.mgreen.err != 0) {
                flag = true;
                api.hideLoading()
                return api.showMessage(rs.mgreen.msg, i18.t('titleMsgLogin'))
              }
              if (rs.mgreen.popup != undefined) this._showConfirm(rs)
              else api.showMgreen(rs)
            } else if (rs.err == 103) {
              let dt = rs.res.data || ''
              if (!dt) return;
              let data = {
                orderId: rs.res.book ? rs.res.book.id : null,
                billId: rs.res.bill ? rs.res.bill.id : null,
                tableId: [dt.id],
                startTime: rs.res.book ? api.getTime(rs.res.book.startTime, 'YYYY-MM-DD hh:mm:ss') : api.getTime(new Date(), 'YYYY-MM-DD hh:mm:ss'),
                shopId: dt.shopId,
              }
              api.updateBook(data)
              api.push({ key: 'iMenu', shopID: dt.shopId, shopName: dt.shopName });
            }
            else if (rs.err == 0) {
              api.push({ key: 'detail', id: rs.promotionId })
              api.showMessage(rs.msg, i18.t('titleMsgLogin'))
            }
            else if (rs.err == 99) {
              api.push({ key: 'detail', id: rs.promotionId })
            }
            else if (rs.err == 101) if (rs.pos.err == 0) {
              switch (rs.pos.type) {
                case 1, 2:
                  let point = rs.pos.point + '\n' + i18.t('point');
                  return api.showStorelet(point, rs.pos.msg);
                case 3:
                  // return api.showStorelet(undefined, rs.pos.msg,null,null,true);
                  return api.showStorelet(undefined, rs.pos.msg);
                case 4:
                  return api.showStorelet(undefined, rs.pos.msg, null, null, true);
                default:
                  break;
              }
              // api.showStorelet(rs.pos.point+'\n'+i18.t('point'),rs.msg)
            }
            else api.showMessage(rs.msg, i18.t('titleMsgLogin')); else api.showMessage(rs.msg, i18.t('titleMsgLogin'));
            api.hideLoading()
            flag = true
          })
      }, 1000)
    }
    setTimeout(() => {
      if (!flag) {
        api.hideLoading()
        flag = true
        api.showMessage(i18.t('timeoutTextMsgContentLogin'), i18.t('titleMsgLogin'))
      }
    }, 15000)
  }

  render() {
    let detectSize = api.getRealDeviceWidth() * 0.9
    return (
      <Camera style={{ flex: 1 }}
        onBarCodeRead={this.barcodeReceived.bind(this)}
        defaultOnFocusComponent={true}
      >
        <View style={{ backgroundColor: '#009688', paddingBottom: 10, paddingTop: Platform.OS == 'ios' ? 20 : 10 }}>
          <Text style={{ color: '#fff', textAlign: 'center' }}>{i18.t('titleQrScan')}</Text>
        </View>
        <View style={{ height: detectSize, width: detectSize, borderColor: '#fff', borderWidth: 1, alignSelf: 'center', marginTop: ((api.getRealDeviceHeight() - 56) - detectSize) / 2 }}>

        </View>
        <View style={{ padding: 10, bottom: 1, position: 'absolute', width: api.getRealDeviceWidth() }}>
          <Button style={{ borderRadius: 20, paddingRight: 30, paddingLeft: 30, backgroundColor: '#fa6428', alignSelf: 'center' }} onPress={() => this.props.pop()}>{i18.t('close')}</Button>
        </View>
      </Camera>

    );
  }
}
mapStateToProps = (state) => ({ navState: state.navState, userState: state.userState })

mapDispatchToProps = (dispatch) => ({
  pop: () => dispatch(pop()),
  push: (route) => dispatch(push(route)),

})
export default connect(mapStateToProps, mapDispatchToProps)(BarcodeScannerApp)