import React, {Component} from 'react';
import {View,ScrollView,Image,Platform,Text,Picker,ListView,ActivityIndicator} from 'react-native';
import {IndicatorViewPager, PagerTitleIndicator} from 'rn-viewpager';
import {Container,Header,Icon,Button} from 'native-base'
import api from '../../api';
import dataService from '../../api/dataService';
import { connect } from 'react-redux';
import Item from './item'
import i18 from '../../components/i18'

class History extends Component {
    ds = new ListView.DataSource({
            rowHasChanged: (r1, r2) => r1 !== r2
        });
    numload = 10
     main = <View></View>
    constructor(props) {
        super(props);
        this.state={
            itemReceived:[],
            itemUsed:[],
            canLoadMoreRecieve:true,
            canLoadMoreUsed:true,
            loadingRecieve:true,
            loadingUsed:true
        }
       
    }
    // function refresh
    refresh(){
        this.setState({
            itemReceived:[],
            itemUsed:[],
            canLoadMoreRecieve:true,
            canLoadMoreUsed:true,
            loadingRecieve:true,
            loadingUsed:true
        });
        this.loadMoreRecieve()
        this.loadMoreUsed()
    }
    componentDidMount(){
        this.loadMoreRecieve();
        this.loadMoreUsed();
    }
    // function to load more history received 
    loadMoreRecieve(){
       if(this.state.canLoadMoreRecieve) dataService.getPromotionRecieve(this.props.token,this.state.itemReceived.length,this.numload)
        .then(rs=>{
            if(rs.err == 0) this.setState({
                itemReceived:this.state.itemReceived.concat (rs.listSentPromotion),
                canLoadMoreRecieve:rs.listSentPromotion.length<this.numload?false:true,
                loadingRecieve:false
            });
        })
    }
    // function to load more history used 
    loadMoreUsed(){
        if(this.state.canLoadMoreUsed) dataService.getPromotionUsed(this.props.token,this.state.itemUsed.length,this.numload)
        .then(rs=>{
            if(rs.err == 0) this.setState({
                itemUsed:this.state.itemUsed.concat( rs.listGivenPromotion),
                canLoadMoreUsed:rs.listGivenPromotion.length<this.numload?false:true,
                loadingUsed:false                
            });
        })
    }
    // this function to return title
     _renderTitleIndicator() {
        return <PagerTitleIndicator titles={[i18.t('recievedCode'), i18.t('usedCode')]}
            //style
        itemTextStyle={{ fontWeight:Platform.OS == 'ios' ? '700':'500' }}
        selectedBorderStyle ={{ borderWidth:2,borderColor:'#fa6428' }}
        selectedItemTextStyle={{ fontWeight:Platform.OS == 'ios' ? '700':'500' }}
        />;
    }
    // render list recieve promotion

    renderRecieve(){
        var id = 1
        return (
            <ListView
                enableEmptySections={true}
                onEndReached={()=>{ if(this.state.itemReceived.length>0) this.loadMoreRecieve(); }}
                renderHeader={()=>{
                    if(this.state.itemReceived.length==0 && !this.state.loadingRecieve)
                        return <View style={{ width:api.getRealDeviceWidth() }}><Text style={{ textAlign:'center',color:'#fff',backgroundColor:'transparent' }}>{i18.t('noneData')}</Text></View>
                }}
                dataSource = {this.ds.cloneWithRows(this.state.itemReceived)}
                renderRow={(item)=>(
                    <Item
                        id = {id++}
                        shop={item.partner?item.partner.name:''}
                        des = {item.promotion?item.promotion.name:''}
                        code={item.code}
                        type='recieve'
                        day={item.createdAt}
                        promotionId={item.promotion?item.promotion.promotionId:''}
                        img ={item.partner?item.partner.logo:''}
                        functionRefresh={this.refresh.bind(this)}
                        gift={false}
                        promotionTypeId={item.promotion?item.promotion.promotionTypeId:''}
                    />
                )}
                renderFooter={()=>{
                    if(this.state.loadingRecieve) return <ActivityIndicator size='large'/>
                }}
            />
        )
    }
    // render list used promotion

    renderUsed(){
        var id = 1
        return (
            <ListView
                enableEmptySections={true}
                dataSource = {this.ds.cloneWithRows(this.state.itemUsed)}
                onEndReached={()=>{ if(this.state.itemUsed.length>0) this.loadMoreRecieve(); }}
                renderHeader={()=>{
                    if(this.state.itemUsed.length==0 && !this.state.loadingUsed)
                        return <View style={{ width:api.getRealDeviceWidth() }}><Text  style={{ textAlign:'center',color:'#fff',backgroundColor:'transparent' }}>{i18.t('noneData')}</Text></View>
                }}
                renderRow={(item)=>(
                    <Item
                        id = {id++}
                        shop={item.partner!=null && item.partner!=undefined?item.partner.name:''}
                        des = {item.promotion!=null && item.promotion!=undefined?item.promotion.name:''}
                        code={item.code}
                        type='used'
                        day={item.createdAt}
                        promotionId={item.promotionId}
                        manager={item.manager!=null && item.manager!= undefined?item.manager.name:''}
                        img ={item.partner!=null && item.partner!=undefined?item.partner.logo:''}
                        functionRefresh={this.refresh.bind(this)}
                    />
                )}
                renderFooter={()=>{
                    if(this.state.loadingUsed) return <ActivityIndicator size='large'/>
                }}
            />
        )
    }
    render() {
         main = <IndicatorViewPager
                style={{
                width:api.getRealDeviceWidth(),
                height:Platform.OS=='ios'? api.getRealDeviceHeight()-155:api.getRealDeviceHeight()-56,
                backgroundColor: 'white',
                flexDirection: 'column-reverse'
            }}
                indicator={this._renderTitleIndicator()}>
                    <View>
                        <Image
                        source={ require('../../img/bg_login.jpg') }
                        style= {{ width:api.getRealDeviceWidth(),height:api.getRealDeviceHeight()-95 }}
                        >
                        {this.renderRecieve()}
                        </Image>
                    </View>
                    <View>
                        <Image
                        source={ require('../../img/bg_login.jpg') }
                        style= {{ width:api.getRealDeviceWidth(),height:api.getRealDeviceHeight()-95 }}
                         >
                        {this.renderUsed()}
                        </Image>
                    </View>
                </IndicatorViewPager>
        if(Platform.OS == 'ios')
        return (
        <ScrollView keyboardShouldPersistTaps={'handled'} style={{ width:api.getRealDeviceWidth(),height:api.getRealDeviceHeight()-56 }}>
            {main}
        </ScrollView>)
        else
        return (
            main
        );
    }
}
function mapStateToProp(state) {
    return { token: state.userState.token ,navState:state.navState,giftCount:state.giftCountState.GiftCount};
}
function mapDispatchToProp(dispatch) {
    return{};
}
export default connect(mapStateToProp, mapDispatchToProp)(History);