import React, {Component} from 'react';
import {View, Text} from 'react-native';
import {CardItem, Thumbnail} from 'native-base';
export default class Item extends Component {
    render() {
        return (
            <View style={{
                flexDirection: 'row',
                backgroundColor: '#fff',
                marginTop: 5,
                paddingVertical: 10,
                paddingHorizontal: 5,borderTopLeftRadius:2,borderTopRightRadius:2
            }}>

                <Thumbnail
                    square
                    source={this.props.logo}
                    size={50}
                    style={{
                    marginRight: 5,
                    resizeMode: 'stretch'
                }}/>
                <View
                    style={{
                    flexDirection: 'column',
                    flex: 1
                }}>
                    <Text
                        style={{
                        fontSize: 15,
                        fontWeight: 'bold',
                        flexWrap: 'wrap',
                        color: '#ff5a3b'
                    }}>{this.props.name.toUpperCase()}</Text>
                    <Text style={{fontWeight: 'bold'}}>{this.props.title.toUpperCase()}</Text>
                </View>
            </View>
        );
    }
}