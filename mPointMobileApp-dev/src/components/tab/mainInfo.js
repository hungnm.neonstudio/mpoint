import React, {Component} from 'react';
import {
    Image,
    View,
    Text,
    TouchableOpacity,
    ListView,
    Modal,
    ScrollView,
    ActivityIndicator,Platform,PermissionsAndroid
} from 'react-native';
import {List, ListItem, Card, CardItem, Icon} from 'native-base';
import {connect} from 'react-redux';
import api from '../../api';
import ImagePicker from 'react-native-image-picker';
import RNFetchBlob from 'react-native-fetch-blob'
import config from '../../config';
var bg_panel = require('../../img/user-panel.png');
var options = {
    title: 'Chọn ảnh',
    cancelButtonTitle: 'Hủy',
    takePhotoButtonTitle: 'Chụp ảnh…',
    quality: 0.2,
    chooseFromLibraryButtonTitle: 'Chọn từ thư viện…',
    noData:true,
    storageOptions: {
        skipBackup: true,
        path: 'images',
        waitUntilSaved:true
    }
};

class CardInfo extends Component {
    _checkStoragePer(){
        PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE).then((result) => {
                if (result) {
                  console.log("Permission is OK");
                  this._uploadAvatar()
                } else {
                  PermissionsAndroid.requestPermission(PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE).then((result) => {
                    if (result) {
                      console.log("User accept");this._uploadAvatar()
                    } else {
                      console.log("User refuse");
                      api.showMessage('Không thể truy bộ nhớ do không được cấp quyền','Thông báo')
                    }
                  });
                }
          });
    }
    _checkCamPer(){
                if (Platform.OS === 'android' && Platform.Version >= 23)
        PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.CAMERA).then((result) => {
                if (result) {
                  console.log("Permission is OK");
                  camPer = true
                  this._checkStoragePer()
                } else {
                  PermissionsAndroid.requestPermission(PermissionsAndroid.PERMISSIONS.CAMERA).then((result) => {
                    if (result) {
                      console.log("User accept");
                      this._checkStoragePer()
                    } else {
                      console.log("User refuse");
                      api.showMessage('Không thể truy cập máy ảnh do không được cấp quyền','Thông báo')
                    }
                  });
                }
          });
          else this._uploadAvatar()

        // if(!PermissionsAndroid.checkPermission(PermissionsAndroid.PERMISSIONS.CAMERA))
//         async function requestCameraPermission() {
//   try {
//     const granted = await PermissionsAndroid.requestPermission(
//       PermissionsAndroid.PERMISSIONS.CAMERA,
//       {
//         'title': 'Thông báo',
//         'message': 'Hãy cho phần mềm sử dụng chức năng chụp ảnh của thiết bị!'
//       }
//     )
//     if (granted == PermissionsAndroid.RESULTS.GRANTED) {
//       console.log("You can use the camera")
//       rs = true
//     } else {
//       console.log("Camera permission denied")
//        rs = false
//        alert('Vao day')
//     }
//   } catch (err) {
//       alert(err)
//       console.warn(err)
//       rs = false
//   }
  
// }
// return rs
    }
    _uploadAvatar() {
    api.showLoading();
        ImagePicker.showImagePicker(options, (response) => {
            console.log(RNFetchBlob.wrap(response.path))
            // alert(response.fileSize)
            console.log('Response = ', response);
            if (response.didCancel) {
                console.log('User cancelled image picker');
                api.hideLoading()
                return
            } else if (response.error) {
                api.showMessage('Có lỗi xảy ra','Thông báo')
                console.log('ImagePicker Error: ', response.error);
                return
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            } else {
                // let source;

                // // You can display the image using either data...
                // source = {
                //     uri: 'data:image/jpeg;base64,' + response.data
                // };

                // // Or a reference to the platform specific asset location
                // if (Platform.OS === 'android') {
                //     source = {
                //         uri: response.uri
                //     };
                // } else {
                //     source = {
                //         uri: response
                //             .uri
                //             .replace('file://', '')
                //     };
                // }
                RNFetchBlob.fetch('POST', config.HOST+'api/uploadAvatar', {
                    Authorization: 'Bearer ' + this.props.token
                }, [
                    {
                        name: 'file',
                        filename: response.fileName,
                        type: response.type,
                        data: 'RNFetchBlob-file://'+response.uri.replace('file://','')//RNFetchBlob.wrap(response.path)
                    }
                ]).then((res) => {
                    console.log(res)
                    res = JSON.parse(res.data)
                    this.setState({avt:response.uri})
                    // dataService
                    //     .getUserInfo(this.props.token)
                    //     .then((datas) => {
                    //         if (datas.err === 0) {
                    //             api.setUserInfo(datas);
                    api.showMessage(res.msg,'Thông báo')
                    api.hideLoading();
                    api.setUserInfo(Object.assign({}, userInfo.data,
                            {endUser:{
                                ...userInfo.data.endUser,
                                avatar:response.uri
                            }
                            }
                        ))
                   
                            // }
                        // })
                }).catch((err) => {
                    // alert(err.text())
                    //console.log(err.text());
                    api.hideLoading();
                    api.showMessage("Có lỗi xảy ra !!! Nguyên nhân có thể do thiết bị hết bộ nhớ. Hãy thử khởi động lại ứng dụng", 'Thông báo');
                })
            }
        });
    }
    render() {
        var data = this.props.data;
        var havePromotion = this.props.havePromotion
        var pro = null;
        if (!havePromotion) {
            pro = <Text style={{
                textAlign: 'center'
            }}>Chưa có ưu đãi/quà tặng trong mục này</Text>;
        }
        
        var mLevel = data.mloyalty.cardGroup != undefined
            ? '  '+data.mloyalty.cardGroup.name
            : 'Chưa có hạng';
        var mPoint = '  '+(data.mloyalty.addPoint - data.mloyalty.subPoint);
        var user_name = data.endUser.name != null
            ? data.endUser.name
            : 'QUÝ KHÁCH';
        var member_name = this.props.userState.data.active.partner.name
            //: '...';
        let groupPrefix = data.active.partner.groupPrefix != ''
            ? data.active.partner.groupPrefix+':'
            : '';
        let pointPrefix = data.active.partner.pointPrefix != ''
            ? data.active.partner.pointPrefix+':'
            : '';
        var logo = data.active.partner != undefined
            ? data.active.partner.logo
            : '...';
        var avatar = data.endUser.avatar;
        var point = '  '+(data.active.addPoint - data.active.subPoint);
        var group_name = data.active.cardGroup != undefined
            ? '  '+data.active.cardGroup.name
            : '...';
        return (
            <ScrollView>
                <View
                    style={{
                    margin: 5,
                    borderRadius: 5,
                    flex: 1
                }}>
                    <CardItem>
                        <Image
                            source={bg_panel}
                            style={{
                            width: api.getRealDeviceWidth()-10,
                            height:(api.getRealDeviceWidth()-10)/1.9,
                            resizeMode: 'stretch'
                        }}>
                            <Image
                                source={{
                                uri: logo
                            }}
                                style={{
                                width: 50,
                                height: 50,
                                resizeMode: 'stretch',
                                margin: 5,
                                top:0,
                                left:0,position:'absolute'
                            }}/>
                            <TouchableOpacity
                                onPress={() => {
                                this._checkCamPer();
                            }}>
                                <View
                                    style={{
                                    alignSelf: 'center',height: 150
                                }}>
                                    {/*<Icon
                                        name='ios-camera-outline'
                                        style={{
                                        fontSize: 40,
                                        color: '#000',
                                        zIndex: 1000,
                                        marginLeft: 40,
                                        marginTop: 100
                                    }}/>*/}
                                </View>
                                <Image
                                    source={{
                                    uri: this.state  == undefined || this.state.avt==undefined?avatar:this.state.avt
                                }}
                                    style={{
                                    width: api.getRealDeviceWidth()/5,
                                    height: api.getRealDeviceWidth()/5,
                                    alignSelf: 'center',
                                    borderRadius: api.getRealDeviceWidth()/10,
                                    marginTop: -120
                                }}></Image>
                            </TouchableOpacity>
                            <Text
                                style={{
                                alignSelf: 'center',
                                fontSize: 15,
                                fontWeight: 'bold',
                                paddingTop: 20,
                                backgroundColor:'transparent'
                            }}
                            onPress={()=>gotoinfo()}
                            >XIN CHÀO {user_name.toUpperCase()}</Text>
                            <Text
                                style={{
                                alignSelf: 'center',
                                textAlign: 'center',
                                backgroundColor:'transparent'
                            }}>Bạn đang sử dụng thẻ ưu đãi của {member_name}</Text>
                        </Image>
                    </CardItem>
                    <View
                        style={{
                        flexDirection: 'row',
                        backgroundColor: '#fff',
                        margin: 5,
                        padding: 5,
                        borderRadius: 5,
                        //height: 50
                    }}>

                        <View
                            style={{
                            flexDirection: 'row',
                            flex: 1,
                        }}>
                        <View>
                            <Text
                                style={{
                                flex: 1
                            }}>{pointPrefix}</Text>
                            <Text
                                style={{
                                flex: 1,
                            }}>Điểm:</Text>
                        </View>
                        <View>
                            <Text
                                style={{
                                flex: 1
                            }}>{pointPrefix!=''?point:''}</Text>
                            <Text
                                style={{
                                flex: 1
                            }}>{mPoint}</Text>
                        </View>
                            {/*<Text
                                style={{
                                flex: 1
                            }}>Điểm: {point}</Text>
                            <Text
                                style={{
                                flex: 1
                            }}>Hạng: {group_name}</Text>*/}
                        </View>
                        <View
                            style={{
                            flexDirection: 'row',
                            flex: 1
                        }}>
                        <View>
                           <Text
                                style={{
                                flex: 1
                            }}>{groupPrefix}</Text>
                            <Text
                                style={{
                                flex: 1
                            }}>Hạng:</Text>
                        </View>
                        <View>
                            <Text
                                style={{
                                flex: 1
                            }}>{groupPrefix!=''?group_name:''}</Text>
                            <Text
                                style={{
                                flex: 1
                            }}>{mLevel}</Text>
                        </View>
                            {/*<Text
                                style={{
                                flex: 1
                            }}>Điểm: {mPoint}</Text>
                            <Text
                                style={{
                                flex: 1
                            }}>Hạng: {mLevel}</Text>*/}
                        </View>
                    </View>
                    {pro}
                </View>
                {/*{this.state?<View><Text style={{ textAlign:'center' }}>Chưa có ưu đãi trong mục này</Text></View>:null}*/}
            </ScrollView>
        );
    }
}
const mapState = (state) => {
    return {userState: state.userState,navState:state.navState}
}
const mapDispatch = (dispatch) => {
    return {
        push: (route) => dispatch(push(route)),
        setUserInfo: (data) => dispatch(setUserInfo(data)),
        setListMember: (listMember) => dispatch(setListMember(listMember))
    }
}
export default connect(mapState, mapDispatch)(CardInfo)