import React, {Component} from 'react';
import {View, TouchableOpacity, Image,Text} from 'react-native';
import api from '../../api';
import {Badge} from 'native-base'
export default class MyCardItem extends Component {
    constructor(props) {
        super(props);
    }
    _renderCount(count){
        if(count>-1)
        return (<Badge style={{ right:3,position:'absolute',marginTop:3 }}>{count}</Badge>)
    }
    render() {
        var opacity = this.props.opacity;
        return (
            <View
                style={this.props.style}>
                <TouchableOpacity onPress={this.props.onCLick}
                    style={{alignSelf:'center'}}
                >
                    <Image
                        source={this.props.img}
                        style={{
                        width: (api.getRealDeviceWidth() - 20) / 2,
                        height: ((api.getRealDeviceWidth() - 20) / 2)/1.57,
                        resizeMode: 'stretch',
                        margin: 5,
                        opacity: opacity,
                        borderRadius:10
                    }}>
                        {this.props.text?<Text style={{ backgroundColor:'transparent',color:'#a6a6a6',fontWeight:'400',fontSize:15,alignSelf:'center',marginTop:(api.getRealDeviceWidth() - 20)/5 }}>{this.props.text}</Text>:null}
                        {this._renderCount(this.props.count)}
                    </Image>
                </TouchableOpacity>
            </View>
        );
    }
}