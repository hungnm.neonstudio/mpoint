import React, { Component } from 'react';
import { Image, View, Text, ScrollView, TextInput, Modal, Platform, KeyboardAvoidingView, DeviceEventEmitter, Keyboard } from 'react-native';
import { connect } from 'react-redux';
import { Card, CardItem, Icon, InputGroup, Input, Radio, Button, Container, Content, Fab } from 'native-base';
import api from '../../api';
var moment = require('moment');
import DatePicker from 'react-native-datepicker';
import dataService from '../../api/dataService';
import ModalDropdown from 'react-native-modal-dropdown';
import QRCode from 'react-native-qrcode';
import Barcode from 'react-native-barcode-builder';
import i18 from '../i18';
let Province = ''
let District = ''
let Ward = ''
let Address = ''
let now = new Date();
let point = 0
let oldPoint = 0
let gotoPromotion = () => { }
class CardInfo extends Component {
    constructor(props) {
        super(props);
        var data = this.props.userState.data;
        var user_name,
            member_name,
            card,
            avatar,
            point,
            group_name,
            phone,
            user_email,
            address,
            enduser_birthday;
        user_name = data.endUser.name;
        member_name = data.active.partner != undefined ? data.active.partner.name : null;
        card = data.active.partner != undefined ? data.active.partner.card : null;
        avatar = data.endUser.avatar;
        point = data.active.addPoint - data.active.subPoint;
        group_name = data.active.cardGroup != undefined ? data.active.cardGroup.name : null;
        phone = data.endUser.phone;
        user_email = data.endUser.email;
        address = data.endUser.address;
        enduser_birthday = data.endUser.birthday;
        this.state = {
            marginBottom: 50,
            gender: this.props.userState.data.endUser.gender == 0 ? false : true,
            user_email: user_email,
            address: address,
            enduser_birthday: enduser_birthday ? (new Date(enduser_birthday).getDate() + '-' + (new Date(enduser_birthday).getMonth() + 1) + '-' + new Date(enduser_birthday).getFullYear()) :( moment().subtract(20, 'years')).format('DD-MM-YYYY'),
            user_name: user_name != undefined ? user_name : '',
            member_name: member_name,
            card: card,
            avatar: avatar,
            point: point,
            group_name: group_name,
            phone: phone,
            districtId: this.props.userState.data.endUser.districtId,
            provinceId: this.props.userState.data.endUser.provinceId,
            wardId: this.props.userState.data.endUser.wardId,
            province: '',
            district: '',
            ward: '',
            qr: false,
            listprovince: dataService.getListProvinces(),
            listdistrict: dataService.getListDistricts(this.props.userState.data.endUser.provinceId),
            listward: dataService.getListWards(this.props.userState.data.endUser.districtId)

        }
    }
    keyboardDidShow(e) {
        oldPoint = point
        this.refs.scr.scrollTo({ y: point + e.endCoordinates.height - 40, x: 0, })
        // let newSize = api.getRealDeviceHeight() - e.endCoordinates.height-105
        // this.setState({
        //   visibleHeight: newSize
        // })
    }

    keyboardDidHide(e) {
        this.refs.scr.scrollTo({ y: oldPoint, x: 0, })
        // this.setState({
        //   visibleHeight: api.getRealDeviceHeight()-155
        // })
    }
    componentWillUnmount() {
        // this.keyboardDidShowListener.remove()
        // this.keyboardDidHideListener.remove()
    }
    componentDidMount() {
        gotoPromotion = this.props.gotoPromotion
        // this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this.keyboardDidShow.bind(this))
        // this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this.keyboardDidHide.bind(this))
        this.state.listprovince.map(item => {
            if (parseInt(item.id) == this.props.userState.data.endUser.provinceId) {
                this.setState({ province: item.name })
            }
        })
        this.state.listdistrict.map(item => {
            if (parseInt(item.id) == this.props.userState.data.endUser.districtId) {
                this.setState({ district: item.name })
            }
        })
        this.state.listward.map(item => {
            if (parseInt(item.id) == this.props.userState.data.endUser.wardId) {
                this.setState({ ward: item.name })
            }
        })
    }
    // Province
    _getProvinceList() {
        return this.refs['PROVINCELIST'];
    }

    _selectProvice(i) {
        Province = this.state.listprovince[i].id
        this.setState({ listdistrict: dataService.getListDistricts(Province), provinceId: Province })
        setTimeout(() => {
            this._selectDistrict(0)
        }, 500)
        // dataService.getListDistricts(this.state.listprovince[i].id)
        // .then((data)=>{
        //     if(data!==undefined && data!==null )
        //     this.setState({
        //         listdistrict:data,
        //         provinceId:this.state.listprovince[i].id,
        //     })
        //     this._selectDistrict(0)
        // })
    }

    // district
    _selectDistrict(i) {
        District = this.state.listdistrict[i].id
        this.setState({
            district: this.state.listdistrict[i].name,
            districtId: this.state.listdistrict[i].id,
            listward: dataService.getListWards(District),
        })
        setTimeout(() => {
            this._selectWard(0)
        }, 500)
        // dataService.getListWards(this.state.listdistrict[i].id)
        // .then((data)=>{
        //     if(data!==undefined && data!==null )
        //     this.setState({
        //         listward:data,
        //         districtId:this.state.listdistrict[i].id,
        //         district:this.state.listdistrict[i].name,
        //         ward:data[0].name,
        //         wardId:data[0].id
        //     })
        // })
    }
    // Ward
    _selectWard(i) {
        Ward = this.state.listward[i].id
        this.setState({
            wardId: this.state.listward[i].id,
            ward: this.state.listward[i].name
        })
    }
    // data.mloyalty.cardGroup.name
    _edit() {
        if (!api.requestLogin()) { return; }
        name = this._rePlaceDoubleSpace(this.state.user_name)
        if (name == '') {
            api.showMessage(i18.t('blankName'), i18.t('titleMsgLogin'))
            return
        }
        if (api.validateSpecialCharacter(name)) {
            api.showMessage(i18.t('invalidSpecialCharacter'), i18.t('titleMsgLogin'))
            return
        }
        if (name.length > 99) { api.showMessage(i18.t('longName'), i18.t('titleMsgLogin')); return }
        if (this.state.user_name.length > 99) {
            api.showMessage(i18.t('longName'), i18.t('titleMsgLogin'))
            return
        }
        if (this.state.enduser_birthday == null || this.state.enduser_birthday == undefined || this.state.enduser_birthday == '') {
            api.showMessage(i18.t('blankDOB'), i18.t('titleMsgLogin'))
            return
        }
        // if(this.state.user_name.length<5){
        //     api.showMessage('Họ tên của quý khách',i18.t('titleMsgLogin'))
        //     return
        // }
        if (this.state.user_email == '') {
            api.showMessage(i18.t('blankEmail'), i18.t('titleMsgLogin'))
            return
        }
        if (!api.validateEmail(this.state.user_email)) {
            api.showMessage(i18.t('invalidEmail'), i18.t('titleMsgLogin'))
            return
        }
        addr = this._rePlaceDoubleSpace(this.state.address)
        if (addr == '') {
            api.showMessage(i18.t('blankADD'), i18.t('titleMsgLogin'))
            return
        }
        if (this.state.provinceId == '' || this.state.provinceId == undefined) {
            api.showMessage(i18.t('blankProvince'), i18.t('titleMsgLogin'))
            return
        }
        if (this.state.districtId == '' || this.state.districtId == undefined) {
            api.showMessage(i18.t('blankDistrict'), i18.t('titleMsgLogin'))
            return
        }
        if (this.state.wardId == '' || this.state.wardId == undefined) {
            api.showMessage(i18.t('blankWard'), i18.t('titleMsgLogin'))
            return
        }
        //     if(this.state.user_email == '' || this.state.address == '' || this.state.user_name == '' ||this.state.enduser_birthday == '' || this.state.enduser_birthday == undefined || this.state.enduser_birthday == null)
        //     {
        //         api.showMessage('Hãy điền đầy đủ các thông tin', i18.t('titleMsgLogin'))
        //         return
        //     }
        //     if(this.state.wardId == '' || this.state.districtId =='' || this.state.provinceId ==''){
        //         api.showMessage('Vui lòng chọn đầy đủ địa chỉ: Tỉnh thành phố, Quận huyện, Xã phường thị trấn')
        //         return
        //     }
        dataService.postUpdateUserInfo(name, addr, this.state.gender ? 1 : 0, this.state.enduser_birthday, this.state.user_email,
            this.state.districtId,
            this.state.provinceId,
            this.state.wardId, '', this.props.userState.token
        ).then(rs => {
            //api.showMessage(rs.msg,i18.t('titleMsgLogin'))
            dataService.getUserInfo(this.props.userState.token)
                .then(kq => {
                    if (kq.err == 0) {
                        api.setUserInfo(kq)
                    }
                })
            if (rs.err == 0) {
                api.showConfirm(rs.msg, i18.t('titleMsgLogin'),
                    i18.t('continue'),
                    i18.t('close'), function OK() {
                        api.hideConfirm()
                        gotoPromotion()
                    },
                    function Cancel() {
                        api.hideConfirm()
                    })
            } else {
                api.showMessage(rs.msg, i18.t('titleMsgLogin'))
            }
        })
    }
    _renderList = (arr) => {
        if (arr !== undefined && arr.length > 0) {
            return arr.map((item) => (
                item.name
            ))
        }
    }
    _rePlaceDoubleSpace(str) {
        let s = str
        while (s.search('  ') != -1) {
            s = s.replace('  ', ' ')
        }
        return s.trim();
    }
    keyUp(y) {
        this.refs.scr.scrollTo({ y: api.getRealDeviceWidth() / 1.9 + y })
    }
    renderDot(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
    }
    handleScroll(event) {
        point = event.nativeEvent.contentOffset.y
    }
    render() {
        var data = this.props.userState.data
        var user_code = this.props.userState.data.endUser.name;
        var mLevel = data.mloyalty.cardGroup != undefined
            ? '  ' + data.mloyalty.cardGroup.name
            : i18.t('noneRank');
        var mPoint = this.renderDot('  ' + (data.active.addPoint - data.active.subPoint));
        var user_name = data.endUser.name != null
            ? data.endUser.name
            : i18.t('customer');
        var member_name = data.active.partner != undefined
            ? data.active.partner.name
            : '...';
        let groupPrefix = data.active.partner.groupPrefix != ''
            ? data.active.partner.groupPrefix + ':'
            : '';
        let pointPrefix = data.active.partner.pointPrefix != ''
            ? data.active.partner.pointPrefix + ':'
            : '';
        var logo = data.active.partner != undefined
            ? data.active.partner.logo
            : '...';
        var avatar = data.endUser.avatar;
        var point = this.renderDot('  ' + (data.mloyalty.addPoint - data.mloyalty.subPoint));
        var group_name = data.active.cardGroup != undefined
            ? '  ' + data.active.cardGroup.name
            : '...';
        var active_code = this.props.userState.data.active.code;
        var giftCount = 0;
        return (
            <View style={{ height: this.state.visibleHeight }}>
                <Modal
                    transparent={true}
                    visible={this.state.qr}
                    animationType='slide'
                    onRequestClose={() => { }}>
                    <Container>
                        <Content
                            style={{
                                flex: 1,
                                backgroundColor: 'rgba(0,0,1, 0.5);'
                            }}>
                            <View style={{ borderTopLeftRadius: 5, borderTopRightRadius: 5, backgroundColor: '#fff', marginHorizontal: 15, alignItems: 'center', paddingBottom: api.getRealDeviceHeight() / 10, marginTop: api.getRealDeviceHeight() * 0.1 }}>
                                <Text style={{ fontSize: Platform.OS == 'ios' ? 13 : 14, paddingVertical: 10, paddingHorizontal: 5, fontWeight: 'bold' }}>{i18.t('qrTitle')}</Text>
                                <View style={{ height: 1, width: api.getRealDeviceWidth() - 30, backgroundColor: '#cdcdcd' }} />
                                <View style={{ padding: 5 }}>
                                    <Text style={{ textAlign: 'center', padding: 5 }}>Barcode</Text>
                                    <Barcode value={active_code} format="CODE128" height={api.getRealDeviceWidth() / 6} />
                                </View>
                                <View style={{ padding: 5, marginTop: 15 }}>
                                    <Text style={{ textAlign: 'center', padding: 5 }}>QR code</Text>
                                    <QRCode size={api.getRealDeviceWidth() / 2}
                                        value={active_code}
                                        bgColor='#000'
                                        fgColor='#fff'
                                    />
                                </View>

                            </View>
                            <Button
                                style={{
                                    marginLeft: 15,
                                    marginRight: 15,
                                    backgroundColor: '#cacaca',
                                    borderRadius: 0,
                                    borderBottomLeftRadius: 5,
                                    borderBottomRightRadius: 5
                                }}
                                block
                                onPress={() => {
                                    this.setState({ qr: false })
                                }}>
                                <Text
                                    style={{
                                        color: '#444444'
                                    }}>{i18.t('close')}</Text>
                            </Button>
                        </Content>
                    </Container>
                </Modal>
                <View style={{ minHeight: api.getRealDeviceHeight(), width: api.getRealDeviceWidth() }}>
                    <ScrollView
                        onScroll={this.handleScroll}
                        keyboardShouldPersistTaps={'handled'}
                        ref='scr'
                        style={{
                            marginHorizontal: 5,
                            borderRadius: 5,
                            flex: 1
                        }}>
                        <Image
                            source={{
                                uri: this.state.card
                            }}
                            style={{
                                marginTop: 5,
                                height: (api.getRealDeviceWidth() - 10) / 1.57,
                                resizeMode: 'stretch'
                            }}>
                            <Text
                                style={{
                                    right: 10,
                                    bottom: 25,
                                    zIndex: 10,
                                    position: 'absolute',
                                    fontWeight: 'bold',
                                    fontSize: 15,
                                    backgroundColor: 'transparent'
                                }}>{(active_code != undefined && active_code != null ? active_code : '').toUpperCase()}</Text>
                            <Text
                                style={{
                                    right: 10,
                                    bottom: 10,
                                    zIndex: 10,
                                    position: 'absolute',
                                    fontWeight: 'bold',
                                    fontSize: 15,
                                    backgroundColor: 'transparent'
                                }}>{(user_code != undefined && user_code != null ? user_code : '').toUpperCase()}</Text>
                        </Image>
                        <View
                            style={{

                                backgroundColor: '#fff',
                                marginVertical: 5,
                                marginTop: 10,
                                padding: 5,
                                borderRadius: 5,
                                width: api.getRealDeviceWidth() - 10
                                //height: 50
                            }}>
                            <View
                                style={{ flexDirection: 'row', alignSelf: 'center' }}
                            >
                                <View
                                    style={{
                                        flexDirection: 'row',
                                        alignSelf: 'center'
                                        //flex: 5
                                    }}>
                                    <View>
                                        {pointPrefix == '' ? null : <Text
                                            style={{
                                                flex: 1
                                            }}>{pointPrefix}</Text>}
                                        <Text
                                            style={{
                                                flex: 1,
                                            }}>mPoint:</Text>
                                    </View>
                                    <View>
                                        {pointPrefix == '' ? null : <Text
                                            style={{
                                                flex: 1
                                            }}>{mPoint}</Text>}
                                        <Text
                                            style={{
                                                flex: 1
                                            }}>{point}</Text>
                                    </View>
                                    {/*<Text
                                style={{
                                flex: 1
                            }}>Điểm: {point}</Text>
                            <Text
                                style={{
                                flex: 1
                            }}>Hạng: {group_name}</Text>*/}
                                </View>
                                <View
                                    style={{
                                        flexDirection: 'row',
                                        alignSelf: 'center',
                                        marginLeft: api.getRealDeviceWidth() > 320 ? api.getRealDeviceWidth() / 5 : (point.length >= 7 || mPoint.length >= 7) ? 10 : api.getRealDeviceWidth() / 5
                                        //flex: 3
                                    }}>
                                    <View>
                                        {groupPrefix == '' ? null : <Text
                                            style={{
                                                flex: 1
                                            }}>{groupPrefix}</Text>}
                                        <Text
                                            style={{
                                                flex: 1
                                            }}>{i18.t('rank')}:</Text>
                                    </View>
                                    <View>
                                        {groupPrefix == '' ? null : <Text
                                            style={{
                                                flex: 1
                                            }}>{group_name}</Text>}
                                        <Text
                                            style={{
                                                flex: 1
                                            }}>{mLevel}</Text>
                                    </View>
                                </View>
                            </View>
                        </View>
                        <Button
                            onPress={() => { this.setState({ qr: true }) }}
                            iconRight
                            style={{
                                width: api.getRealDeviceWidth() - 10,
                                borderRadius: 5,
                                alignSelf: 'center',
                                paddingHorizontal: 30,
                                paddingVertical: 5,
                                marginVertical: 10,
                                backgroundColor: '#fa6428'
                            }}
                        >
                            {i18.t('btQRtext')}
                            <Icon name='md-barcode' />
                        </Button>
                        <View
                            style={{
                                marginVertical: 5,
                                borderRadius: 5,
                                backgroundColor: '#cdcdcd'
                            }}>
                            <InputGroup style={{ borderTopLeftRadius: 5, borderTopRightRadius: 5, paddingTop: 5, paddingBottom: 5, paddingLeft: 18, backgroundColor: '#fff' }}>
                                <Icon
                                    name='ios-person'
                                    style={{
                                        color: '#444',
                                        marginRight: 10, marginTop: 5
                                    }} />

                                <Input
                                    onFocus={() => { this.keyUp(50) }}
                                    autoCapitalize='characters'
                                    onChangeText={t => { this.setState({ user_name: t }) }}
                                    placeholder={i18.t('placeHolderInputName')}
                                    placeholderTextColor='#444'
                                    style={{
                                        color: '#444',
                                        //fontSize: 20,
                                    }}
                                    defaultValue={this.state.user_name != '' ? this.state.user_name : null}
                                />
                            </InputGroup>
                            <View
                                style={{
                                    flexDirection: 'row',
                                    backgroundColor: '#fff',
                                    margin: 1,
                                    padding: 10
                                }}>
                                <Icon
                                    name='ios-calendar'
                                    style={{
                                        color: '#444',
                                        marginRight: 10,
                                        marginTop: 5, textAlign: 'center'
                                    }} />
                                <DatePicker
                                    style={{ width: 200, borderWidth: 0 }}
                                    date={this.state.enduser_birthday}
                                    mode="date"
                                    placeholder={i18.t('placeholderChooseDOB')}
                                    format="DD-MM-YYYY"
                                    minDate="01-01-1900"
                                    androidMode='spinner'
                                    maxDate={now}
                                    confirmBtnText={i18.t('confirm')}
                                    cancelBtnText={i18.t('close')}
                                    showIcon={false}
                                    customStyles={{
                                        dateIcon: {
                                            position: 'absolute',
                                            left: 0,
                                            top: 4,
                                            marginLeft: 0
                                        },
                                        dateInput: {
                                            marginLeft: 0,
                                            borderWidth: 0,
                                            alignSelf: 'auto', alignItems: 'flex-start'
                                        },
                                        placeholderText: {
                                            color: '#444'
                                        }
                                        // ... You can check the source to find the other keys.
                                    }}
                                    onDateChange={(date) => { this.setState({ enduser_birthday: date.substr(0, 10) }) }}
                                // onDateChange={(date) => {n=date}}
                                />
                            </View>
                            <View
                                style={{
                                    flexDirection: 'row',
                                    backgroundColor: '#fff',
                                    margin: 1,
                                    padding: 10
                                }}>
                                <Icon
                                    name='ios-people'
                                    style={{
                                        color: '#444',
                                        marginRight: 10, textAlign: 'center'
                                    }} />
                                <Radio style={{ marginLeft: 10 }} onPress={() => { this.setState({ gender: false }) }} selected={!this.state.gender} /><Text style={{ marginLeft: 5, marginTop: 5 }}>{i18.t('men')}</Text>
                                <Radio style={{ marginLeft: 10 }} onPress={() => { this.setState({ gender: true }) }} selected={this.state.gender} /><Text style={{ marginLeft: 5, marginTop: 5 }}>{i18.t('women')}</Text>
                            </View>
                            <View
                                style={{
                                    flexDirection: 'row',
                                    backgroundColor: '#fff',
                                    margin: 1,
                                    padding: 10
                                }}>
                                <Icon
                                    name='ios-call'
                                    style={{
                                        color: '#444',
                                        marginRight: 10, textAlign: 'center'
                                    }} />
                                <Input
                                    style={{ color: '#444', minHeight: 30 }}
                                    editable={false}
                                    defaultValue={this.state.phone}
                                    onChangeText={p => { this.setState({ phone: p }) }}
                                    placeholder={i18.t('inputPlaceholderSDT')}
                                    placeholderTextColor='#444'
                                    keyboardType='phone-pad'
                                />
                            </View>
                            <InputGroup style={{ paddingTop: 5, paddingBottom: 5, paddingLeft: 18, backgroundColor: '#fff' }}>
                                <Icon
                                    name='ios-mail'
                                    style={{
                                        color: '#444',
                                        marginRight: 10,
                                        paddingTop: 8, textAlign: 'center'
                                    }} />
                                <Input
                                    onFocus={() => { this.keyUp(280) }}
                                    defaultValue={this.state.user_email != '' ? this.state.user_email : null} style={{ color: '#444', minHeight: 30 }}
                                    onChangeText={(text) => { this.setState({ user_email: text }) }}
                                    placeholder='Email'
                                    placeholderTextColor='#444'

                                />
                            </InputGroup>
                            <InputGroup style={{ paddingTop: 5, paddingBottom: 5, paddingLeft: 18, backgroundColor: '#fff' }}>
                                <Icon
                                    name='ios-home'
                                    style={{
                                        color: '#444',
                                        marginRight: 10,
                                        paddingTop: 8
                                    }} />
                                <Input
                                    onFocus={() => { this.keyUp(370) }}
                                    defaultValue={this.state.address != '' ? this.state.address : null}
                                    style={{ color: '#444' }}
                                    onChangeText={(text) => { this.setState({ address: text }) }}
                                    placeholder={i18.t('addr')}
                                    placeholderTextColor='#444'
                                />
                            </InputGroup>
                            <View
                                style={{
                                    flexDirection: 'row',
                                    backgroundColor: '#fff',
                                    margin: 1,
                                    padding: 10,
                                    paddingLeft: 0
                                }}>
                                {/*<Icon
                            name='ios-home'
                            style={{
                            color: '#444',
                            marginRight: 10,
                            marginTop: 8
                        }}/>*/}
                                <Text style={{ color: '#444', textAlignVertical: 'center', marginLeft: 10, marginTop: Platform.OS == 'ios' ? 10 : 0 }}>{i18.t('province')}: </Text>
                                <ModalDropdown
                                    dropdownTextStyle={{ fontSize: 13 }}
                                    options={this._renderList(this.state.listprovince)}
                                    defaultValue={(this.state.province != '' ? this.state.province : i18.t('chooseProvince')) + ' ▼'}
                                    onSelect={(id) => this._selectProvice(id)}
                                    textStyle={{ fontSize: 15, color: '#444', }}
                                    style={{ padding: 10 }}
                                    dropdownStyle={{ width: api.getRealDeviceWidth() * 0.9, height: api.getRealDeviceHeight() * 0.5 }}
                                />
                            </View>
                            <View
                                style={{
                                    flexDirection: 'row',
                                    backgroundColor: '#fff',
                                    margin: 1,
                                    padding: 10,
                                    paddingLeft: 0
                                }}>
                                {/*<Icon
                            name='ios-home'
                            style={{
                            color: '#444',
                            marginRight: 10,
                            marginTop: 8
                        }}/>*/}
                                <Text style={{ color: '#444', textAlignVertical: 'center', marginLeft: 10, marginTop: Platform.OS == 'ios' ? 10 : 0 }}>{i18.t('district')}: </Text>
                                <ModalDropdown
                                    dropdownTextStyle={{ fontSize: 13 }}
                                    options={this._renderList(this.state.listdistrict)}
                                    defaultValue={(this.state.district != '' ? this.state.district : i18.t('chooseDistrict')) + ' ▼'} //'Chọn quận huyện ▼'
                                    onSelect={(id) => this._selectDistrict(id)}
                                    textStyle={{ fontSize: 15, color: '#444', }}
                                    style={{ padding: 10 }}
                                    dropdownStyle={{ width: api.getRealDeviceWidth() * 0.9, height: api.getRealDeviceHeight() * 0.5 }}
                                />
                            </View>
                            <View
                                style={{
                                    flexDirection: 'row',
                                    backgroundColor: '#fff',
                                    margin: 1,
                                    padding: 10,
                                    paddingLeft: 0,
                                    borderBottomLeftRadius: 5,
                                    borderBottomRightRadius: 5
                                }}>
                                {/*<Icon
                            name='ios-home'
                            style={{
                            color: '#444',
                            marginRight: 10,
                            marginTop: 8
                        }}/>*/}
                                <Text style={{ color: '#444', textAlignVertical: 'center', marginLeft: 10, marginTop: Platform.OS == 'ios' ? 10 : 0 }}>{i18.t('ward')} : </Text>
                                <ModalDropdown
                                    dropdownTextStyle={{ fontSize: 13 }}
                                    options={this._renderList(this.state.listward)}
                                    defaultValue={(this.state.ward != '' ? this.state.ward : i18.t('chooseWard')) + ' ▼'}
                                    onSelect={(id) => this._selectWard(id)}
                                    textStyle={{ fontSize: 15, color: '#444', }}
                                    style={{ padding: 10 }}
                                    dropdownStyle={{ width: api.getRealDeviceWidth() * 0.9, height: api.getRealDeviceHeight() * 0.5 }}
                                />
                            </View>
                        </View>
                        <Button style={{ paddingHorizontal: 30, alignSelf: 'center', borderRadius: 10, marginTop: 10, backgroundColor: '#fa6428', marginBottom: this.state.marginBottom }}
                            onPress={this._edit.bind(this)}
                        >
                            <Text style={{ color: '#fff' }}>
                                {i18.t('update')}
                            </Text>
                        </Button>
                    </ScrollView>
                </View>
                {/*<Fab
             containerStyle={{bottom: 15,right:15}}
                //style={{ height:46,width:46,borderRadius:23,bottom:15,right:15,position:'absolute',backgroundColor:'#5067FF' }}
                onPress={this._edit.bind(this)}
                >
                <Icon name='md-checkmark' style={{ color:"#fff",textAlign:'center',textAlignVertical:'center',height:46  }}/>
            </Fab>*/}
            </View>
        );
    }
}
const mapState = (state) => {
    return { userState: state.userState }
}
const mapDispatch = (dispatch) => {
    return {}
}
export default connect(mapState, mapDispatch)(CardInfo);