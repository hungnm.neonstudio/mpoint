import React, {Component} from 'react';
import {View, Image, Text, ScrollView,Platform,AsyncStorage,PermissionsAndroid} from 'react-native';
import {
    Header,
    Title,
    Container,
    Content,
    Button,
    Icon,
    Input,
    InputGroup
} from 'native-base';
import api from '../api';
import dataService from '../api/dataService';
import DeviceInfo  from 'react-native-device-info'
import {connect} from 'react-redux';
import PopupDialog,{ SlideAnimation,DialogTitle } from 'react-native-popup-dialog';
import {setUserInfo, setUserToken, setUuid} from '../actions/userActions';
import i18 from './i18';
let phone = ''
let otpID = ''
let OTP  = ''
let cardID = ''
class AddCardFromLogin extends Component {
    constructor(){
        super()
        this.state={
            showinput:true,
            phone:phone!=''?phone:'',
            isloading:false,
            card:'',
            data:{
                introduce:'',
                howuse:'',
                object:''
            }
        }
    }
    componentDidMount(){
        if(this.props.navState.routes[this.props.navState.index].item!=undefined)
        this.setState({data:this.props.navState.routes[this.props.navState.index].item})
        console.log(this.state.data)
    }
    _renderIntroduce(arr){
        
        return arr.length ==1 && arr[0]==''? <Text>
                 <Text style={{ color:'#2FCC77' }}>•</Text> {i18.t('updating')}.
            </Text>: arr.map((item,id)=>(
            item==''?null:<Text key={id}>
                 <Text style={{ color:'#2FCC77' }}>•</Text> {item.replace('-','')}.
            </Text>
        ))
    }
   _renderHowUse(arr){
        return (
            <View
                style={{ padding:10,backgroundColor:'rgba(20,20,20,.5)',borderBottomRightRadius:5,borderTopRightRadius:5,borderBottomLeftRadius:5 }}
            >
                {arr.length == 1 && arr[0]==''? <Text style={{ color:'#fff' }}>
                 <Text style={{ color:'#fff' }}>•</Text> {i18.t('updating')}.
            </Text>:arr.map((item,id)=>(
        item==''?null:<Text style={{ color:'#fff' }}>
             <Text key={id} style={{ color:'#fff' }}>»</Text> {item.replace('-','')}
        </Text>))}
            </View>
        )
    }
    _renderObject(arr){
        return (
            <View
                style={{ padding:10,backgroundColor:'rgba(20,20,20,.5)',borderBottomRightRadius:5,borderTopRightRadius:5,borderBottomLeftRadius:5 }}
            >
                {
                    arr.length == 1  && arr[0]==''? <Text style={{ color:'#fff' }}>
                 <Text style={{ color:'#fff' }}>•</Text> {i18.t('updating')}.
            </Text>:arr.map((item,id)=>(
            item==''?null:<Text style={{ color:'#fff' }}>
             <Text key={id} style={{ color:'#fff' }}>»</Text> {item.replace('-','')}
        </Text>
        ))
                }
            </View>
        )
    }
    render() {
        return (
            <Container>
                <Header
                    style={{
                    backgroundColor: '#009688'
                }}>
                    <Title style={{ alignSelf:'center' } }>
                        {i18.t('titleNewRegCard')}
                    </Title>
                    <Button transparent onPress={() => api.pop()}>
                        <Icon name='ios-arrow-back'  style={{ color:Platform.OS == 'ios'?'#000':'#fff' }}/>
                    </Button>
                </Header>
                <Image
                        style={{ height:api.getRealDeviceHeight()-56,width:api.getRealDeviceWidth() }}
                        source={require('../img/bg_login.jpg')}
                    >
                    <ScrollView>
                    <Image
                        source={{
                        uri: this.state.data.card
                    }}
                        style={{
                        height: (api.getRealDeviceWidth()-10)/1.57,
                        resizeMode: 'stretch',
                        margin: 10
                    }}>
                    </Image>

                    <View style={{
                        flex: 1,
                        minHeight:api.getRealDeviceHeight()/2,
                        padding:10,
                        paddingBottom:70
                    }}>
                        <Text style={{ fontSize:20,fontWeight:'400',color:'#fff',textAlign:'center',marginBottom:10 }}>
                            {i18.t('bodyAddcardPage')} {(this.state.data.name!=undefined?this.state.data.name:'').toUpperCase()}
                        </Text>
                        {/*<Text style={{ fontSize:18,color:'#2FCC77',fontWeight:'300' }}>
                            Giới thiệu
                        </Text>*/}
                        <View style={{ flexDirection:'row',marginTop:10 }}>
                                <Image source={ require('../img/icon/Gioithieu.png') } style={{ width:20*4.47,height:20,resizeMode:'stretch' }}/>
                        </View>
                        <View
                style={{ padding:10,backgroundColor:'rgba(20,20,20,.5)',borderBottomRightRadius:5,borderTopRightRadius:5,borderBottomLeftRadius:5 }}
            >
                        <Text style={{ color:'#fff' }}>{this.state.data.introduce!=''?this.state.data.introduce:'- '+ i18.t('updating')}</Text>
                        </View>
                        {/*<Text style={{ fontSize:18,color:'#2FCC77',fontWeight:'300' }}>
                            Hướng dẫn sử dụng
                        </Text>*/}
                        <View style={{ flexDirection:'row',marginTop:10 }}>
                                <Image source={ require('../img/icon/huongdan.png') } style={{ width:20*6.96,height:20,resizeMode:'stretch' }}/>
                        </View>
                        {this._renderHowUse(this.state.data.howuse.split('\n'))}
                        <View style={{ flexDirection:'row',marginTop:10 }}>
                                <Image source={ require('../img/icon/doituong.png') } style={{ width:20*4.42,height:20,resizeMode:'stretch' }}/>
                        </View>
                        {this._renderObject(this.state.data.object.split('\n'))}
                    </View>
                    </ScrollView>
                </Image>
            </Container>
        );
    }
}
const mapState = (state) => {
    return {navState: state.navState,userState: state.userState}
}
const mapDispatch = (dispatch) => {
    return {
        setUserInfo: (data) => dispatch(setUserInfo(data)),
    }
}
export default connect(mapState, mapDispatch)(AddCardFromLogin);