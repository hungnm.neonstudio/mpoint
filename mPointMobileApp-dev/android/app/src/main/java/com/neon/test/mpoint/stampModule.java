package com.neon.test.mpoint;

import android.util.Log;
import android.widget.Toast;

import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.onetwocm.echossserviceprovider.EchossServiceProvider;

/**
 * Created by Nguyen Manh Hung on 28-Jul-17.
 */

public class stampModule extends ReactContextBaseJavaModule {
    EchossServiceProvider esp = new EchossServiceProvider();

    public stampModule(ReactApplicationContext reactContext) {
        super(reactContext);
    }

    @Override
    public String getName() {
        return "stamp";
    }
    @ReactMethod
    public void showMessage(String a){
        Toast.makeText(this.getReactApplicationContext(),a,Toast.LENGTH_SHORT).show();
    }
    @ReactMethod
    public void getEchosserviceUrl(String regionCode , String scheme, String merchantCode, String userCode , String licenseId , String authKey, final Callback done, final Callback err){
        Log.d("regionCode", regionCode);
        Log.d("scheme", scheme);
        Log.d("merchantCode", merchantCode);
        Log.d("userCode", userCode);
        Log.d("licenseId", licenseId);
        Log.d("authKey", authKey);
        esp.getEchossServiceURL(getReactApplicationContext(), regionCode, scheme, merchantCode, userCode, licenseId, authKey, new EchossServiceProvider.OnDataListener() {
                    @Override
                    public void onSuccess(String s) throws Exception {
                        done.invoke(s);
                    }

                    @Override
                    public void onError(String s, String s1) {
                        err.invoke(s,s1);
                    }
                });
    }

    @ReactMethod
    public void setBackgroundColor(String bgColor){
        esp.setBackgroundColor(bgColor);
    }

    @ReactMethod
    public void setBackgroundOpacity(String opacity){
        esp.setBackgroundOpacity(opacity);
    }

    @ReactMethod
    public void setDescription(String description){
        esp.setDescription(description);
    }

    @ReactMethod
    public void setLoadingYn(String bool){
        esp.setLoadingYn(bool);
    }
}
