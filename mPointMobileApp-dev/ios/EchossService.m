//
//  EchossService.m
//  mpoint
//
//  Created by Tien Phan on 8/22/17.
//  Copyright © 2017 Facebook. All rights reserved.
//

#import "EchossService.h"
#import "echossServiceProvider.h"
@implementation EchossService
EchossServiceProvider *esp;

- (id) init {
  self = [super init];
  if (self != nil) {
    // initializations go here.
    esp = [[EchossServiceProvider alloc] init];
  }
  return self;
}
RCT_EXPORT_MODULE(stamp);


// get echoss service url
RCT_EXPORT_METHOD(getEchosserviceUrl:(NSString *)regionCode
                  scheme:(NSString *)scheme
                  merchantCode:(NSString *)merchantCode
                  userCode:(NSString *)userCode
                  licenseId:(NSString *)licenseId
                  authKey:(NSString *)authKey
                  Done:(RCTResponseSenderBlock)Done
                  Err:(RCTResponseSenderBlock)Err
){
//  [esp setDescription:@"Vui lòng nhấn tem vào màn hình để xác thực"];
//  [esp setLoadingYn:@"Y"];
//  [esp setBackgroundOpacity:@"0.3"];

  NSString *pageURL = [esp getEchossServiceURLwithRegion: regionCode
                                                  scheme: scheme
                                            merchantCode: merchantCode
                                                userCode: userCode
                                               licenseId: licenseId
                                                 authKey: authKey];
  Done(@[pageURL]);
  pageURL = nil;
}

// set BG color
RCT_EXPORT_METHOD(setBackgroundColor:(NSString *)color){
  
  [esp setBackgroundColor: color];
}

// set opacity
RCT_EXPORT_METHOD(setBackgroundOpacity:(NSString *)opacity){
  
  [esp setBackgroundColor: opacity];
}
// set loading
RCT_EXPORT_METHOD(setLoadingYn:(NSString *)loading){
  
  [esp setLoadingYn: loading];
}

// set description
RCT_EXPORT_METHOD(setDescription:(NSString *)des){
  
  [esp setDescription: des];
}
@end
