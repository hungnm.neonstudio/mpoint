//
//  echossServiceProvider.m
//  echossServiceProvider
//
//  Created by Seung Nam on 2017. 06. 05..
//  Copyright © 2017년 12cm. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EchossServiceProvider : NSObject

    - (NSString *) getEchossServiceURLwithRegion: (NSString *) regionCd
                                          scheme: (NSString *) scheme
                                    merchantCode: (NSString *) merchantCode
                                        userCode: (NSString *) userCode
                                       licenseId: (NSString *) licenseId
                                         authKey: (NSString *) authKey;

    - (void) setBackgroundColor: (NSString *) color;
    
    - (void) setBackgroundOpacity: (NSString *) opacity;
    
    - (void) setDescription: (NSString *) desc;
    
    - (void) setLoadingYn: (NSString *) loadingYn;
    
@end
